<?php
/**
 * Settings.class.php
 * 
 * Implementatrion of Settings API for SkyLine
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.03
 * 
 * 01/06/2012  1.00    Andrew J. Williams    Initial Version
 * 23/08/2012  1.01    Andrew J. Williams    TrackerBase VMS Log 29 - Alterations to Skyline API to work with ServiceBase
 * 17/10/2012  1.02    Andrew J. Williams    GetSettings should search for client number on NetworkServiceProvider and default client account numbers
 * 25/10/2012  1.03    Andrew J. Williams    Issue 108 - GetSetting Client Serach should be the same as PutSettings
 ******************************************************************************/

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');

class SettingsAPI extends SkylineAPI {
    
    
    /**
     * Description
     * 
     * Deal with get  
     * 
     * @param array $args   Associative array of arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    
    public function get( $args ) {
        
        $api_settings_model = $this->loadModel('APISettings');
               
        /* Check if ClientAccountNo set */
        if (isset($args['ClientAccountNo'])) {
            /* Yes - send the response back which relates to the Client with the
             *  same ClientAccountNo. (also checking that set for the Newtork 
             * Service provider table and default client */ 
            $where = " AND (
                                (
                                    nsp.`AccountNo` = '{$args['ClientAccountNo']}'
                                    AND clt.`ClientID` = dc.`ClientID`
                                )
                            OR clt.`AccountNumber` = '{$args['ClientAccountNo']}'
                        )
                     ";            
        } else {
            /* No - , send back the details for all Clients which are linked to 
             * Networks that the Service Centre is linked to (where service 
             * centre is linked to the user logging in), this list should 
             * exclude clients which have a service centre ID directly linked to 
             * them; and then add to this list the clients details which have 
             * the service centre ID on them which is linked to the user*/
            $where = "";
        }
               
        $rec_clients = $api_settings_model->getSettingsClientAccountNo($this->user->UserID, $where);
                
        $response = array(
                         'Settings' =>  array(
                                             'Username' => $this->user->Username,
                                             'Accounts' => $this->recordSetSerialise($rec_clients, 'Account')
                                            )
                         );
        
        for ($n=0; $n<count($rec_clients); $n++) {
            if ( (! is_null($rec_clients[$n]['ClientAccountNo'])) AND ($rec_clients[$n]['ClientAccountNo'] != '') ) {               
                $rec_unit_type = $api_settings_model->getSettingsClientUnitTypes($rec_clients[$n]['ClientAccountNo'],'FieldCallRate', 'field call');   
                $response['Settings']['Accounts'][$n]['Account']['UnitTypes'] = array( $this->recordSetSerialise($rec_unit_type, 'UnitType') );
            
                $rec_unit_type_workshop = $api_settings_model->getSettingsClientUnitTypes($rec_clients[$n]['ClientAccountNo'],'WorkshopRate', 'workshop');
                
                for ($w = 0; $w < count($rec_unit_type_workshop); $w++) {
                    
                    if (is_array($response['Settings']['Accounts'][$n]['Account']['UnitTypes'])) {
                                                
                        for ($f = 0; $f < count($response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0]); $f++) {                            
                            $match = 0;
                            if (
                                ($response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f]['UnitType']['UnitTypeName'] == $rec_unit_type_workshop[$w]['UnitTypeName']) 
                                && ($response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f]['UnitType']['RepairSkill'] == $rec_unit_type_workshop[$w]['RepairSkill'])
                                && ($response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f]['UnitType']['CompletionStatus'] == $rec_unit_type_workshop[$w]['CompletionStatus'])
                            ) {
                                $response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f]['UnitType']['WorkshopRate'] = $rec_unit_type_workshop[$w]['WorkshopRate'];
                                $match = 1;
                                break;
                            } 
                        }
                        if ($match == 0) { 
                            $response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f+1]['UnitType']['UnitTypeName'] = $rec_unit_type_workshop[$w]['UnitTypeName'];
                            $response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f+1]['UnitType']['RepairSkill'] = $rec_unit_type_workshop[$w]['RepairSkill'];
                            $response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f+1]['UnitType']['CompletionStatus'] = $rec_unit_type_workshop[$w]['CompletionStatus'];
                            $response['Settings']['Accounts'][$n]['Account']['UnitTypes'][0][$f+1]['UnitType']['WorkshopRate'] = $rec_unit_type_workshop[$w]['WorkshopRate'];
                        }
                    }
                }
                
                $rec_manufacturers = $api_settings_model->getSettingsClientManufacturers($rec_clients[$n]['ClientAccountNo']);
                $response['Settings']['Accounts'][$n]['Account']['Manufacturers'] = $rec_manufacturers;
                
                $rec_service_type = $api_settings_model->getSettingsServiceTypes($rec_clients[$n]['ClientAccountNo']);   
                $response['Settings']['Accounts'][$n]['Account']['ServiceTypes'] = array( $this->recordSetSerialise($rec_service_type, 'ServiceType') );
            }
        } 
                       
        $this->sendResponse(200, $response);
    }
    
    public function put( $args ) {
        
        $this->sendResponse(501);
                
    }
    
    public function post( $args ) {
        
        $this->sendResponse(501);
                
    }
    
    public function delete( $args ) {
        
        $this->sendResponse(501);
        
        
    }
}
 
?>