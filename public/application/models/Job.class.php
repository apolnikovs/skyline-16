<?php
/**
 * This is the Job Model which handles all actions related to
 * booking a Job within skyline
 * 
 * @package skyline
 * @subpackage CustomModel
 * @author Simon Tsang <s.tsang@pccsuk.com>
 * @version 1.74
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/02/2012  1.00    Simon Tsang           Initial Version
 * 07/03/2012  1.01    Nageswara Rao Kanteti Integrated Datatable
 * 07/03/2012  1.03    Simon Tsang           Job advanced search
 * 13/03/2012  1.04    Simon Tsang           Job turnaround timeline
 * 15/03/2012  1.05    Brian Etherington     Changed Job Search ID text to SL no
 * 15/03/2012  1.06    Simon Tsang           Inline editing
 * 13/03/2012  1.07    Nageswara Rao Kanteti Open and Overdue Jobs
 * 19/03/2012  1.08    Simon Tsang           Correct URL to data controller
 * 19/03/2012  1.09    Nageswara Rao Kanteti Open and Overdue Jobs amendments
 * 19/03/2012  1.10    Nageswara Rao Kanteti Added audit trail actions
 * 29/03/2012  1.11    Nageswara Rao Kanteti Amendments to Contact History Actions, Open and Overdue job pages
 * 03/04/2012  1.12    Simon Tsang           Corrected input fields and error message
 * 26/04/2012  1.13    Simon Tsang           Various updates
 * 02/05/2012  1.14    Simon Tsang           Various updates and Table Factory
 * 14/06/2012  1.15    Simon Tsang           Rollback to timeline model
 * 29/06/2012  1.16    Brian Etherington     Update to Model Table
 * 29/06/2012  1.17    Brian Etherington     Implemented Job Booking
 * 11/07/2012  1.18    Andrew J. Williams    Skyline API changes
 * 18/07/2012  1.19    Nageswara Rao Kanteti Bug Fixing
 * 19/07/2012  1.20    Andrew J. Williams    Fix issues #17 and #35
 * 19/07/2012  1.21    Nageswara Rao Kanteti Bug Fixing part 2
 * 25/07/2012  1.22    Andrew J. Williams    Fix issues #38
 * 06/08/2012  1.23    Nageswara Rao Kanteti Joe's Test report 3 has been done
 * 13/08/2012  1.24    Nageswara Rao Kanteti Servive provider delction done on job booking process
 * 16/08/2012  1.25    Andrew J. Williams    Samsung API changes
 * 28/08/2012  1.25A   Andrew J. Williams    TrackerBase VMS Log 29 - Alterations to Skyline API to work with ServiceBase
 * 30/08/2012  1.26    Andrew J. Williams    Test Report 103 
 * 31/08/2012  1.27    Brian Etherington     Added ModelNumber to fetch result
 * 17/09/2012  1.28    Brian Etherington     Fix Issue #71
 *                                           Update fetchAllOpen and fetchAllOpenStats to exclude cancelled jobs
 *                                           Added ActiveJobFilter and CancelledJobFilter
 * 20/09/2012  1.29    Brian etherington     Update fetchAllOverdue and fetchAllOverdueStats to exclude cancelled jobs
 * 23/09/2012  1.30    Nageswara Rao Kanteti added some fields to select query in fetch method.
 * 26/09/2012  1.30A   Andrew J. Williams    Graphical Analysis Queries - getPercentageJobsOverDays
 * 28/09/2012  1.31    Nageswara Rao Kanteti added method fetchUnallocatedJobs. 
 * 16/10/2012  1.32    brian Etherington     added SeviceTypeCode to fetch method returned data.
 * 09/11/2012  1.33    Andrew J. Williams    Added fetchJobStatusPeriod (for Status Change Report)
 * 13/11/2012  1.34    Andrew J. Williams    Added getIdFromNetworkRef and getLastStatus
 * 20/11/2012  1.35    Brian Etherington     Fix typo in branchId (should be BranchID) in getJobsByStatusNumber
 * 20/11/2012  1.36    Brian Etherington     Temporary(read permanent) fix to eliminate duplicate rows in numContactHistory caused by error in API spec 
 *                                           For some reason this is impossible to fix in either the RMATRacjer API and Service Centre API
 *                                           Note: This ONLY resolves the display of duplicate records.  The error in the RMA and SB API specs
 *                                                 means that duplicate rows in the contact history table will continue to accumulate with each
 *                                                 and every API job update call.
 * 23/11/2012  1.37    Andrew J. Williams    Graphical Analysis - added getJobsAwaitingParts, getJobsAwaitingAppointment and getAverageDaysFromBookingDate
 * 28/11/2012  1.38    Vykintas Rutkunas     Homepage Open/Overdue Jobs facility - added getJobsByStatusNumber, getOverdueJobsNumber
 *                                           getOverdueJobs, getJobsByStatus, updateJobStatus
 * 26/11/2012  1.38A   Vykintas Rutkunas     Overdue Jobs
 * 27/11/2012  1.39    Andrew J. Williams    Graphical Analysis - added getJobsAverageEstimateOutsanding
 * 28/11/2012  1.39    Vykintas Rutkunas     Table state saving/loading to/from the database - saveTableState, loadTableState
 * 28/11/2012  1.40    Andrew J. Williams    Graphical Analysis - added getJobsByAppointmentEngineer, getJobsByStatusCount, getJobsByCountyCount, getJobsCountryCount
 * 29/11/2012  1.41    Andrew J. Williams    Graphical Analysis - added getJobsBookedInWeekEnding
 * 01/12/2012  1.42    Vykintas Rutkunas     Added setAuthNo
 * 03/12/2012  1.43    Andrew J. Williams    Graphical Analysis - added getPercentageJobsBetweenDays
 * 04/12/2012  1.44    Andrew J. Williams    Aded getJobsByDatesByServiceCentreCount
 * 05/12/2012  1.45    Andrew J. Williams    Fixed issue with Get Jobs By Status 
 * 05/12/2012  1.46    Andrew J. Williams    Filter by user type / is now working for Perfromance Graphs
 * 07/12/2012  1.47    Andrew J. Williams    Added extra fields to query in fetchJobStatusPeriod
 * 06/12/2012  1.48    Vykintas Rutkunas     Added checkRARequired, setRARequired
 * 10/12/2012  1.49    Andrew J. Williams    Issue 152 - Remove duplicate lines from status change export
 * 11/12/2012  1.50    Andrew J. Williams    Extra fields for Status Change Report
 * 14/12/2012  1.51    Brian etherington     Change getDaysFromBooking to use ClosedDate instead of RepairCompleteDate
 * 03/01/2013  1.52    Brian Etherington     Job update screen issues
 * 06/01/2013  1.53    Nageswara Rao Kanteti Diary Wallboard
 * 11/01/2013  1.54    Andrew J. Williams    Issue 177 - Skyline export changes
 * 30/01/2013  1.55    Brian Etherington     TrackerBase #156 Add UserServiceProviderID to fetch($JobId) array.
 * 04/02/2013  1.56    Brian Etherington     Updated all Job table updates to include ModifiedUserID & ModifiedDate
 * 05/02/2013  1.57    Vykintas Rutkunas     Job update parts table
 * 07/02/2013  1.58    Andrew J. Williams    Changes to Open TAT screens for Joe - countJobsGraphicalAnalysisFilter and exportJobsGraphicalAnalysis
 * 12/02/2013  1.59    Vykintas Rutkunas     Service Appraisal Prep for Demo
 * 15/02/2013  1.60    Andrew J. Williams    Added code to return appropriate Model & UnitType based on ServiceBaseXxxxx fields ot ID
 * 18/02/2013  1.61    Andrew J. Williams    Job class data now also contans Booked By Contact Name (required for customer recipy / job card)
 * 01/03/2013  1.62    Andrew J. Williams    Issue 242 - Do not return 'ADJUSTMENT' Parts in Status Report
 * 11/03/2013  1.63    Brian Etherington     Moved Job Filters to helpers/Functons class
 * 22/03/2013  1.64    Andrew J. Williams    Extra fields for Status Change Report
 * 25/03/2013  1.65    Andrew J. Williams    Changes to fetchOpenTAT for definition of OpenJob
 * 05/04/2013  1.66    Andrew J. Williams    Added getJobsByCompletionCount for Closed TAT Graphical Analysis
 * 05/04/2013  1.67    Vykintas Rutkunas     Hopmepage open jobs table cahange 
 * 08/04/2013  1.68    Andrew J. Williams    Dials addes to Closed Jobs Screen
 * 09/04/2013  1.69    Nageswara Rao Kanteti Status TAT done but needs to be tested fully
 * 10/04/2013  1.70    Andrew J. Williams    Fixed query errors in getClosedJobs
 * 11/04/2013  1.71    Nageswara Rao Kanteti Chart issue is fixed
 * 11/04/1013  1.72    Vykintas Rutkunas     Job Class Fix
 * 07/05/2013  1.73    Brian Etherington     sort Job Fetch select to order by Shipping ID and limit result to the most recent row
 *                                           (because shipping is a child table causing the job select return multiple joined results!!)
 * 08/05/2013  1.74    Andrew J. Williams    Added getWhereNotDowloadedToSc (Primarily for UTL API)
 * 08/05/2013  1.75    Brian Etherington     move Job Fetch limit clause to after the user job filter
 * 17/05/2013  1.76    Andrew J. Williams    Added GetServiceCentreName
 * 07/06/2013  1.77    Andris Polnikovs      Changed Overdue and Closed Jobs to use Display Preferences in datatables
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');
require_once('Functions.class.php');

class Job extends CustomModel {

    private $conn;
    private $mysqli;
    public $current_record = null;
    public $status_history = null;
    public $parts_used = null;
    public $contact_history = null;
    public $status_changes = null;
    public $appointments = null;
    public $product_details = null;
    public $service_centre_contacts = null;
    public $config;
    
    private $dbColumns = array( 't1.JobID', 't1.ServiceCentreJobNo', 't2.ContactLastName', 't2.PostalCode', 't1.RepairDescription', 't4.StatusName' );
    private $matches = null;    
    private $permissions = 'R';
    private $tables = 'job AS t1 LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
                    LEFT JOIN status_history        AS t3 ON t1.JobID = t3.JobID
                    LEFT JOIN status                AS t4 ON t3.StatusID = t4.StatusID
                    LEFT JOIN county                AS t5 ON t2.CountyID = t5.CountyID
                    LEFT JOIN country               AS t6 ON t2.CountryID = t6.CountryID
                    LEFT JOIN customer_title        AS t7 ON t2.CustomerTitleID = t7.CustomerTitleID
                    LEFT JOIN product               AS t8 ON t1.ProductID = t8.ProductID
                    LEFT JOIN model                 AS t9 ON t1.ModelID = t9.ModelID
                    LEFT JOIN unit_type             AS t10 ON t9.UnitTypeID = t10.UnitTypeID
                    LEFT JOIN manufacturer          AS t11 ON t9.ManufacturerID = t11.ManufacturerID
                    ';

    # db name and display names
    public $display_columns = array(
        'DateBooked' => 'Date',
        'Days' => 'Days',
        'JobID' => 'Skyline No.',
        'ServiceCentreJobNo' => 'Job No.',
        'ContactLastName' => 'Consumer',
        'PostalCode' => 'Post Code',
	'ServiceBaseUnitType' => "Product Type",
	'CompanyName' => "Service Provider",
       //'RepairDescription' => 'Description',
        'StatusName' => 'Status',
        'BrandMaxTurnaroundTime' => 'd1',
        'SkylineMaxTurnaroundTime' => 'd2'
    );
    
    public $job_type_group = array(
        'all' => 'All', 
        'instore' => 'In Store', 
        'withsupplier' => 'With Supplier',
        'collection' => 'Awaiting Collection'
    );

    # standard search signle field search
    public $search_fields = array(
        'JobID' => 'SL No.',
        'ServiceCentreJobNo' => 'Job No.',
        'PostalCode' => 'Post Code',
        'ContactLastName' => 'Surname'
    );
    
    /* 
     * @var string $super_filter 
     */
    public $super_filter = '';

    /* 
     * @var string $search_value 
     */    
    public $search_value = '';    
    public $search_group = '';
    public $search_result = '';
    
    public $selected_job_group = 'JobID';
    public $selected_job_type_group = 'all';
    
    public $staging = 'draft';

    # advanced search combination fields
    public $advanced_search_fields = array(
        'ContactLastName'      => array('label'=>'Customer Surname',            'value' => '', 'list'=>false),
        'HouseNoBuildingName'  => array('label'=>'House No or Building Name',   'value' => '', 'list'=>false),
        'Street'               => array('label'=>'Street',                      'value' => '', 'list'=>false),
        'Town'                 => array('label'=>'Town',                        'value' => '', 'list'=>false),
        'PostalCode'           => array('label'=>'Post Code',                   'value' => '', 'list'=>false),
        'MinDays'              => array('label'=>'Min days since booking',      'value' => '', 'list'=>false),
        'MaxDays'              => array('label'=>'Max Days since booking',      'value' => '', 'list'=>false),
        'SystemStatus'         => array('label'=>'System Status',               'value' => '', 'list'=>true),
        'JobStatus'            => array('label'=>'Job Status',                  'value' => '', 'list'=>true),   
        'BrandStatus'          => array('label'=>'Brand Status',                'value' => '', 'list'=>true),
        'Brand'                => array('label'=>'Brand',                       'value' => '', 'list'=>true),
        'Branch'               => array('label'=>'Branch',                      'value' => '', 'list'=>true),
        'BookingPerson'        => array('label'=>'Booking Person',              'value' => '', 'list'=>true),
        'StockCode'            => array('label'=>'Stock Code',                  'value' => '', 'list'=>true),
        'Manufacturer'         => array('label'=>'Manufacturer',                'value' => '', 'list'=>true),
        'ProductType'          => array('label'=>'Product Type',                'value' => '', 'list'=>true),
        'ModelNo'              => array('label'=>'Model No',                    'value' => '', 'list'=>true),
        'SerialNo'             => array('label'=>'Serial No',                   'value' => '', 'list'=>true),
        'ServiceCentre'        => array('label'=>'Service Centre',              'value' => '', 'list'=>true),
        'UnitLocation'         => array('label'=>'UnitLocation',                'value' => '', 'list'=>true)           
    );      

    
    
    /**
     * 
     * @param CustomController $controller
     *
     * @author Simon Tsang s.tsang@pccsuk.com	
     * 
     * @example from CustomController $this->loadModel('Job'); 
     ********************************************/
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );  
        
        //Initialize MySQLi connection
        $dbArr = explode("=",$this->controller->config['DataBase']['Conn']);
        $dbName = $dbArr[2];
        $dbHostArr = explode(";",$dbArr[1]);
        $dbHost = $dbHostArr[0];
        $this->mysqli = new mysqli( $dbHost,
                                    $this->controller->config['DataBase']['Username'],
                                    $this->controller->config['DataBase']['Password'],
                                    $dbName
                                );
	
	$this->config = $this->controller->readConfig('application.ini');
  
    }

    
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */
    public function create($args) {
        $result = null;
        $job_table = TableFactory::Job();
        $cmd = $job_table->insertCommand($args);
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'SUCCESS',
                             'jobId' => $this->conn->lastInsertId());
        } else {
            $result = array('status' => 'FAIL',
                            'jobId' => 0,
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
    
    
    
    /*
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */  
    
    public function update($args) {
        $result = null;
        $job_table = TableFactory::Job();
        $args['ModifiedUserID'] = $this->controller->user->UserID;
        $args['ModifiedDate'] = date('Y-m-d H:i:s');
        $cmd = $job_table->updateCommand($args);
        if($this->Execute($this->conn, $cmd, $args)) {
            $result = ['status' => 'SUCCESS', 'message' => ''];
        } else {
            $result = ['status' => 'FAIL', 'message' => $this->lastPDOError()];          
        }
        return $result;
    }
    
    
    
    /**
     * assignServiceCentre
     * 
     * Assigning service centre to Job.
     * 
     * @param array $args
     * @return array $result
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     */   
    public function assignServiceCentre($args) {
        
        $dataArray = array();
        
        $dataArray['ServiceProviderID'] = isset($args['ServiceProviderID'])?$args['ServiceProviderID']:false;
        $dataArray['JobID']             = isset($args['JobID'])?$args['JobID']:false;
        
        $skyline                        = $this->controller->loadModel('Skyline');
        $dataArray['StatusID']          = $skyline->getStatusID('02 ALLOCATED TO AGENT');
        
        
        if($dataArray['ServiceProviderID'] && $dataArray['JobID'])
        {
            $result = $this->update($dataArray);
            
            //calling api....
            if($result['status']=="SUCCESS")
            {    
                
                  
                  
                   $this->fetch($dataArray['JobID']);

		   $cur = $this->current_record; 
                   
                   $emailModel = $this->controller->loadModel('Email'); 
                
                   //Sending job booking email - starts here..
                   if($cur['AutoSendEmails'] == "1")
                   {
                            
                             
                             if($cur['ContactEmail']) {
                                 
                                 
                                if($cur['EmailType']=='CRM')
                                {   
                                    $emailsDetails = $emailModel->fetchRow(['EmailCode' => "job_booking_crm"]); 
                                    
                                }
                                else if($cur['EmailType']=='Tracker')
                                {   
                                    $emailsDetails = $emailModel->fetchRow(['EmailCode' => "job_booking_smart_service_tracker"]); 
                                    
                                }
                                else
                                {
                                    $emailsDetails = $emailModel->fetchRow(['EmailCode' => "job_booking"]);
                                }  
                               
                                 
                                 
                                 $emailsDetails["toEmail"] = $cur["ContactEmail"];
                                 $emailsDetails["fromName"] = "Skyline";
                                 $emailsDetails["fromEmail"] = "no-reply@skylinecms.co.uk";
                                 $emailsDetails["replyEmail"] = ($cur['ServiceReplyEmail']) ? $cur['ServiceReplyEmail'] : false;
                                 $emailsDetails["replyName"] = ($cur['ServiceCentreName']) ? $cur['ServiceCentreName'] : false;
                                 $emailsDetails["ccEmails"] = false;
                                 
                                 
                                 $emailModel->processEmail($emailsDetails, $cur);
                             }
                         
                   }     
                   //Sending job booking email - ends here..
                
                
                  
                    $ServiceProvidersModel   = $this->controller->loadModel('ServiceProviders');
                    $ServiceProvidersDetails = $ServiceProvidersModel->fetchRow(array("ServiceProviderID"=>$_POST['ServiceProviderID']));
                    
                    
                    
                    
                    
                    if(is_array($ServiceProvidersDetails))
                    {    
                        
                        //Sending Service Instruction email to service provider's admin supervisor - starts here..
                        if($cur['SPEmailServiceInstruction']=='Active' && ($cur['SPAdminSupervisorEmail'] || $cur['SPServiceManagerEmail']) && $_POST['ServiceProviderID'])
                        {
                            
                                $email_service_instruction  =  $emailModel->fetchRow(['EmailCode' => "email_service_instruction"]);
                                
                                //$this->log($email_service_instruction);
                                
                                if(is_array($email_service_instruction) && count($email_service_instruction)>0)
                                {    
                                    $sp_send_email1 = ($cur['SPAdminSupervisorEmail'])?$cur['SPAdminSupervisorEmail']:false;
                                    $sp_send_email2 = false;
                                    if(!$sp_send_email1)
                                    {
                                      $sp_send_email1 =  $cur['SPServiceManagerEmail']; 
                                    }
                                    else if($cur['SPServiceManagerEmail'])
                                    {
                                        $sp_send_email2 = $cur['SPServiceManagerEmail'];
                                    }  
                                    
                                    
                                    $email_service_instruction["toEmail"] = $sp_send_email1;
                                    $email_service_instruction["fromName"] = "Skyline";
                                    $email_service_instruction["fromEmail"] = "no-reply@skylinecms.co.uk";
                                    $email_service_instruction["replyEmail"] = false;
                                    $email_service_instruction["replyName"] = false;
                                    $email_service_instruction["ccEmails"] = $sp_send_email2;
                                    
                                    $emailModel->processEmail($email_service_instruction, $cur);
                                    
                               }
                            
                        }
                        //Sending Service Instruction email to service provider's admin supervisor - ends here..
                        
                        
                        $api = $this->controller->loadModel('APIJobs');

                        $rmaResponse = $api->rmaPutNewJob($dataArray['JobID']); 
                        
                        
                        if($rmaResponse && $rmaResponse=="SC0001")//If Rma traker call responds with success.
                        {    
                            if($ServiceProvidersDetails['Platform']=="ServiceBase")
                            {
                                $api->putNewJob($dataArray['JobID']); 
                            }
                            else if($ServiceProvidersDetails['Platform']=="API") {
                                
                                $this->update(array("DownloadedToSC"=>0, "JobID"=>$dataArray['JobID']));
                                
                            }
                            else if($ServiceProvidersDetails['Platform']=="Skyline" || $ServiceProvidersDetails['Platform']=="RMA Tracker") {
                                
                                $this->update(array("DownloadedToSC"=>1, "JobID"=>$dataArray['JobID']));
                            }
                        }
                        
                        
                        
                    }
                
                
            }
        }
        else
        {
            $result = array('status' => 'FAIL',
                            'message' => '');      
        }
        
        return $result;
    }
    
    
    
    /**
     * cancelJob
     * 
     * This method cancel job.
     * 
     * @param array $args
     * @return array $result
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>	 
     */   
    public function cancelJob($args) {
        
        $dataArray = array();
       
        $dataArray['JobID'] = isset($args['JobID'])?$args['JobID']:false;
        
        if($dataArray['JobID'])
        {
            $skyline  = $this->controller->loadModel('Skyline');
            $statusID = $skyline->getStatusID('29 JOB CANCELLED');
            if($statusID)
            {
                $dataArray['StatusID'] = $statusID;
                
                $result = $this->update($dataArray);
                
                if($result['status']=="SUCCESS")
                {
                    $result['status']="OK";
                    
                    $result['message'] = "The job has been cancelled successfully.";
                }
                else
                {
                    $result['status']="ERROR";
                }
                
            }
            else
            {
                $result = array('status' => 'ERROR',
                            'message' => '');  
                
            }    
            
        }
        else
        {
            $result = array('status' => 'ERROR',
                            'message' => '');      
        }
        
        
       // $this->controller->log(var_export($result, true));
        
        return $result;
    }
    
    
    
     /**
     * Short Description.
     * 
     * Description.
     * 
     * @param array $args
     * @return array $result
     */   
    public function delete($args) {
        $result = null;
        $job_table = TableFactory::Job();
        $cmd = $job_table->deleteCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'OK',
                             'message' => '');
        } else {
            $result = array('status' => 'ERROR',
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }   
    
    
    
    /**
    * Short Description.
    * 
    * Description.
    * 
    * @param int $JobID
    * @return array $result
    * 
    */ 
    
    public function fetch($JobID) {
        
        if($this->current_record == null || $this->current_record['JobID'] != $JobID) {
        
                $sql = "SELECT	    t1.*, 
				    cu.*,
				    t7.Title AS ContactTitle, 
				    t8.ProductNo, 
				    
				    CASE
					WHEN t9.ModelNumber IS NOT NULL
					THEN t9.ModelNumber
					ELSE t1.ServiceBaseModel
				    END AS ModelNumber,

				    t9.ModelDescription, 
				    
				    CASE
					WHEN t9.UnitTypeID IS NOT NULL THEN @unitTypeID := t9.UnitTypeID
					WHEN t8.UnitTypeID IS NOT NULL THEN @unitTypeID := t8.UnitTypeID
					ELSE	CASE	
						    WHEN t1.ServiceBaseUnitType IS NOT NULL
						    THEN @unitTypeID := (SELECT ut.UnitTypeID FROM unit_type AS ut WHERE ut.UnitTypeName = t1.ServiceBaseUnitType LIMIT 1)
						    ELSE @unitTypeID := NULL
						END
				    END AS UnitTypeID,

				    CASE
					WHEN @unitTypeID IS NOT NULL THEN t10.UnitTypeName
					ELSE t1.ServiceBaseUnitType
				    END AS UnitTypeName,

				    CASE
					WHEN mft.ManufacturerName IS NOT NULL
					THEN mft.ManufacturerName
					ELSE t1.ServiceBaseManufacturer
				    END AS ManufacturerName,
                                    mft.ManufacturerLogo,

				    t1.ProductLocation AS PhysicalLocation, 
				    t5.Name AS CountyName, 
				    t6.Name AS CountryName,
				    CONCAT_WS(', ', t13.CompanyName,t13.BuildingNameNumber,t13.Street,t13.LocalArea,t13.TownCity,t13.PostalCode) AS ServiceCentreAddress,
				    t13.`CompanyName` AS `ServiceCentreName`,
				    t13.ContactPhone AS ServiceCentreTelephone,
				    t13.ContactEmail AS ServiceCentreEmail,
				    t13.ReplyEmail AS ServiceReplyEmail,
                                    t13.AdminSupervisorEmail AS SPAdminSupervisorEmail,
                                    t13.AdminSupervisorForename AS SPAdminSupervisorForename,
                                    t13.AdminSupervisorSurname AS SPAdminSupervisorSurname,
                                    t13.ServiceManagerEmail AS SPServiceManagerEmail,
                                    t13.EmailServiceInstruction AS SPEmailServiceInstruction,
				    IF (b.`UseAddressProgram` = 'Yes', 1, 0) AS `PostcodeLookup`,
				    t1.ServiceBaseUnitType,
				    DATE_FORMAT(t1.DateBooked,'%d%m/%Y'),
				    t14.CompanyName AS Network,
                                    t14.ContactEmail AS NetworkContactEmail,
				    t15.ClientName AS Client,
				    t15.EnableProductDatabase,
				    u.Username AS User,
				    CONCAT(u.ContactFirstName, ' ', u.ContactLastName) AS UserFullName,
                                    CONCAT(u.`ContactFirstName`,' ',u.`ContactLastName`) AS `BookedByName`,
                                    u.ServiceProviderID as UserServiceProviderID,
				    t18.BrandName AS Brand,
				    t18.BrandLogo,
                                    t18.BrandID AS JobBrandID,
				    t18.AutoSendEmails,
                                    t18.EmailType,
                                    t18.TrackerURL,
				    b.BranchName AS Branch,
				    b.BuildingNameNumber AS BranchBuildingNameNumber,
				    b.Street AS BranchStreet,
				    b.LocalArea AS BranchLocalArea,
				    b.TownCity AS BranchTownCity,
				    b.PostalCode AS BranchPostalCode,
				    b.ContactEmail AS BranchContactEmail,
				    b.ContactPhone AS BranchContactPhone,
				    b.ContactPhoneExt AS BranchContactPhoneExt,                        
				    IF (ISNULL(stm.`ServiceTypeName`), t19.ServiceTypeName, stm.`ServiceTypeName`) AS ServiceTypeName,
				    t19.ServiceTypeCode,
				    jt.Type AS JobTypeName,
				    t4.StatusName,
				    ra.RAStatusName,
                                    t18.`CustomerTermsConditions`,
				    t1.Accessories,
				    t1.UnitCondition,
				    shipping.CourierID,
				    courier.CourierName,
				    shipping.ConsignmentNo,
				    DATE_FORMAT(shipping.ConsignmentDate, '%d/%m/%Y') AS ConsignmentDate,
				    t10.RepairSkillID,
				    mft.ManufacturerID
                                    
                    FROM	    job AS t1 
		    
		    LEFT JOIN	    branch AS b			ON t1.BranchID = b.BranchID
		    LEFT JOIN	    customer AS cu		ON t1.CustomerID = cu.CustomerID
		    LEFT JOIN	    status AS t4		ON t1.StatusID = t4.StatusID
		    LEFT JOIN	    county AS t5		ON cu.CountyID = t5.CountyID
		    LEFT JOIN	    country AS t6		ON cu.CountryID = t6.CountryID
		    LEFT JOIN	    customer_title AS t7	ON cu.CustomerTitleID = t7.CustomerTitleID
		    LEFT JOIN	    product AS t8		ON t1.ProductID = t8.ProductID
		    LEFT JOIN	    model AS t9			ON t1.ModelID = t9.ModelID";
//		    LEFT JOIN	    unit_type AS t10		ON @unitTypeID = t10.UnitTypeID
            $sql .= " LEFT JOIN	    unit_type AS t10		ON t9.unitTypeID = t10.UnitTypeID
		    LEFT JOIN	    manufacturer AS mft		ON t1.ManufacturerID = mft.ManufacturerID
		    LEFT JOIN	    service_provider AS t13	ON t1.ServiceProviderID = t13.ServiceProviderID
		    LEFT JOIN	    network AS t14		ON t1.Networkid = t14.NetworkID
		    LEFT JOIN	    client AS t15		ON t1.ClientID = t15.ClientID
		    LEFT JOIN	    user AS u			On t1.BookedBy = u.UserID
		    LEFT JOIN	    brand_branch AS t17		ON t1.BranchID = t17.BranchID
		    LEFT JOIN	    brand AS t18		ON t17.BrandID = t18.BrandID
		    LEFT JOIN	    service_type AS t19		ON t1.ServiceTypeID = t19.ServiceTypeID
                    LEFT JOIN	    service_type_alias AS sta	ON t19.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                    LEFT JOIN	    service_type AS stm		ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status='Active'
		    LEFT JOIN	    job_type AS jt		ON t1.JobTypeID = jt.JobTypeID
		    LEFT JOIN	    ra_status	AS ra		ON t1.RAStatusID = ra.RAStatusID
		    LEFT JOIN	    shipping			ON shipping.JobID = t1.JobID
		    LEFT JOIN	    courier			ON shipping.CourierID = courier.CourierID

                    WHERE	    t1.JobID = :JobID 
            ";
        
//           echo $sql."<BR>"; 
            
            if(isset($this->controller->user)) {    
                $sqlFilter = $this->UserJobFilter("t1");

                if($sqlFilter !== "") {
                    $sql .= $sqlFilter;
                }
            }
            
            $sql .= " order by shipping.ShippingID desc limit 1";
            
            $args = ["JobID" => $JobID];
        
            $recordset = $this->Query($this->conn, $sql, $args); 
            
            $this->current_record = isset($recordset[0]) ? $recordset[0] : [];
            $this->status_history = null;
            $this->parts_used = null;
            $this->contact_history = null;
            $this->status_changes = null;
            $this->appointments = null;
            $this->product_details = null;
            $this->service_centre_contacts = null;
                        
            //$this->current_record['DaysFromBooking']   = $this->getDaysFromBooking( $JobID , $this->current_record['RepairCompleteDate']);
            $this->current_record['DaysFromBooking'] = $this->getDaysFromBooking($JobID, isset($this->current_record['ClosedDate']) ? $this->current_record['ClosedDate'] : null);
            $this->current_record['JobTurnaroundTime'] = 0;
            $this->current_record['DaysRemaining'] = 0;            
            
            $this->current_record['TurnaroundTimeType'] = '';
            $this->current_record['TurnaroundTimeValue'] = 0;           
        
        }
        
//        echo '<pre>';
//        print_r($this->current_record);
//        echo '</pre>';
        
        return $this->current_record;
	
    }
    
    
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     * @param int $JobID
     * @return array $result
     */ 
    public function fetchStatusHistory( $JobID ) {
        
        $sql = "select * from status_history where JobID = :JobID";
        $args = array( 'JobID' => $JobID );
        $this->status_history =  $this->Query($this->conn, $sql, $args);
        return $this->status_history;
    }
    
    
    
    /**
    * Short Description.
    * 
    * Description.
    * 
    * @param int $JobID
    * @return int $result
    */    
    
    public function getDaysFromBooking($JobID, $RepairCompleteDate) {
        
	/*if ($this->status_history == null) $this->fetchStatusHistory( $JobID );

	return 0;*/
	
        if(isset($this->current_record['DateBooked']))
        {
            $date_booked = new DateTime($this->current_record['DateBooked']);
        }
        else
        {
            $date_booked = new DateTime(date('Y-m-d'));
        }
        
	if($RepairCompleteDate) {
	    $today = new DateTime($RepairCompleteDate);
	} else {
	    $today = new DateTime(date('Y-m-d'));
	}
	
	return ($date_booked->diff($today)->days);
	
    }
    
   
    
/************
 
     public function fetchModelResult($args) {
        
        $table = 'model AS t1 LEFT JOIN product  AS t2 ON t1.ModelID = t2.ModelID
        LEFT JOIN unit_type             AS t3 ON t1.UnitTypeID = t3.UnitTypeID
        LEFT JOIN manufacturer          AS t4 ON t1.ManufacturerID = t4.ManufacturerID';
        
         $columns = array('t2.ProductNo',
            't1.ModelNumber',
            't1.ModelDescription',
            't3.UnitTypeID',
            't1.ManufacturerID');

        if (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '') {
           
            $args['where'] = '(';
            
           
            for ( $i=0 ; $i<count($columns) ; $i++ ) {

                 $args['where'] .= $columns[$i] .' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                 
            }
            
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        $output = $this->ServeDataTables($this->conn, $table, $columns, $args);
       
        return  $output;
        
    }
    
    *************/
    
    
    
    /*
    public function fetchProductResult($args) {
        
        $table = 'product AS t1 LEFT JOIN model  AS t2 ON t1.ModelID = t2.ModelID
        LEFT JOIN unit_type             AS t3 ON t2.UnitTypeID = t3.UnitTypeID
        LEFT JOIN manufacturer          AS t4 ON t2.ManufacturerID = t4.ManufacturerID';
        
         $columns = array('t1.ProductNo',
            't2.ModelNumber',
            't2.ModelDescription',
            't3.UnitTypeID',
            't2.ManufacturerID');

        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')) {
           
            $args['where'] = '(';
                
            for ( $i=0 ; $i<count($columns) ; $i++ ) {

                //list($field, $alias) = explode(' AS ', $columns[$i]);
                $args['where'] .= $columns[$i] .' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
            }
                
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        $output = $this->ServeDataTables($this->conn, $table, $columns, $args);
       
        return  $output;
        
    }*/
       
    
    
    public function fetchAll( $args='' ) {

        $columns = $this->dbColumns;
        
        // currently the $columns uses SQL alias but the AJAX JSON doesn't not
        #this code correct this bug
        foreach($columns as $c){
            $fields = explode('.', $c);
            if(isset($fields[1]) && isset($args[$fields[1]])){
                $args['where'] = $c . ' LIKE "%' . $args[$fields[1]] .'%"';
                
            }
        }
        
        $args['where'] .=  $this->UserJobFilter();
        
        $data = $this->ServeDataTables($this->conn, $this->tables, $columns, $args);

        return  $data;
        
    }   
    
    
    
    public function fetchAllOverdue($args,$user,$page) {
         $dataTable=$this->controller->loadModel('DataTable');
	/*
        $GeneralDefaultModel = $this->controller->loadModel('GeneralDefault');
        $JobTurnaroundTime = $GeneralDefaultModel->getValue( $this->controller->user->DefaultBrandID, 1); //Here 1 is IDNo for the Turnaround Time in general defualt table.
        if(!$JobTurnaroundTime) {
            $JobTurnaroundTime = 14;//As Neil suggested.
        }
        */
	
	//$JobTurnaroundTime
           
        $table = 'job AS t1 
                    LEFT JOIN status                AS js ON t1.StatusID = js.StatusID
                    LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                    LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                    LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                    LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID
                    LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                    LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                    LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                    LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                    LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                    LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                    LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                    LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                    LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                    LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                    LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                    LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                    LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                    LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                    LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                    LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                    LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
                   LEFT JOIN ra_status             AS ras on t1.RAStatusID = ras.RAStatusID
                   LEFT JOIN client             AS cl on t1.ClientID = cl.ClientID
                 
                  '; 
                    
                    
        
                
        

	/*
        $columns = array( "t1.JobID AS ID", 
                          array('t1.DateBooked',"DATE_FORMAT (t1.DateBooked, '%d/%m/%Y')"), 
                          "t6.CompanyName AS ServiceCentre", 
                          "t1.ServiceCentreJobNo", 
                          "t4.UnitTypeName AS UnitType", 
                          "t5.ManufacturerName as ManufacturerName", 
                          "IF (ISNULL(stm.`ServiceTypeName`), t7.ServiceTypeName, stm.`ServiceTypeName`) AS ServiceType", 
                          "js.StatusName AS SystemStatus", 
                          "t1.ItemLocation", 
                          "t1.JobSite AS FieldService" );
        */
	
        
        if($args['tatType']=='s')
        {
            $overdueColumn = 'statustat(t1.JobID)';
        }   
        else
        {
            $overdueColumn = "
	    CASE
		WHEN @days > (@turnaround := turnaround(t1.JobID))
		THEN (@days - @turnaround)
		ELSE 0
	    END
	    ";
        }
        
        
	
	
        $columns=$dataTable->getAllFieldNames('job',$user,$page,false,$overdueColumn);
        $columns_heading = $columns[1];
        $columns=$columns[0];
        
	
	/*
        $args['where'] = 't1.ClosedDate IS NULL AND t1.DateBooked < DATE_SUB(NOW(), INTERVAL '.$JobTurnaroundTime.' DAY) ' 
                       . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js'); 
        */
	
         
        if($args['tatType']=='s')
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            } 
        }
        else
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            } 

        }
        
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                if($args['tatType']=='s')
                {   
                    $args['where'] .= " AND statustat(t1.JobID)<".$args['btnValue']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<".$args['btnValue']." ";
                }   
                    
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                
                if($args['tatType']=='s')
                {   
                   $args['where'] .= " AND statustat(t1.JobID)>=".$args['btnValue']." AND statustat(t1.JobID)<=".$args['btnValue2']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>=".$args['btnValue']." AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<=".$args['btnValue2']." ";
                } 
                
                
                
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                if($args['tatType']=='s')
                {   
                   $args['where'] .= " AND statustat(t1.JobID)>".$args['btnValue']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>".$args['btnValue']." ";
                } 
                
                
            }
        }
        
         if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }  
        
        
        
        
        
        
         // Brand  Filter
        if (isset($args['brand']) && $args['brand']) {
           $args['where'] .= ' AND t8.BrandID='.$args['brand']; 
        }      
        
        // Status Type Filter
        if (isset($args['sType']) && $args['sType']) {
            $args['where'] .= ' AND t1.StatusID='.$this->conn->quote( $args['sType'] );
        }
        
        // Service Centre Type Filter
//        if (isset($args['scType']) && $args['scType']) {
//            $args['where'] .= ' AND t1.ServiceCentreID='.$this->conn->quote( $args['scType'] );
//        }
        
        // Manufacturer Filter
        if (isset($args['manufacturer'])) {
           $args['where'] .= ' AND t1.ManufacturerID='.$this->conn->quote( $args['manufacturer'] ); 
        }
        
         // Service Provider Filter
        if (isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID='.$this->conn->quote( $args['serviceProvider'] ); 
        }

        // Client Filter
        if (isset($args['client'])) {
           $args['where'] .= ' AND t1.ClientID='.$this->conn->quote( $args['client'] ); 
        }
        
        //Network Filter
        if(isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID=' . $this->conn->quote($args['network']);
        }
        
        //Branch Filter
        if(isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID=' . $this->conn->quote($args['branch']);
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND t4.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
        //Skill Set Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND t10.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
        
        
        
        $SystemStatusesModel   = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('overdueJobs', 'js');
        
        if(count($UserStatusPreferences))
        {
            $sIDStr = '';
            $sep    = '';
            
            foreach($UserStatusPreferences AS $usp)
            {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep    = ',';
            }
            $args['where'] .= ' AND js.StatusID IN ('.$sIDStr.')';
        }
        
        //$args['where'] .= " GROUP BY t1.JobID";
        $args['groupby'] = "t1.JobID";
	
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args, $columns_heading);
               
//        foreach ($data['aaData'] as $dI=>$dV) {
//            $data['aaData'][$dI][] = '<input type="checkbox"  value="'.$data['aaData'][$dI][0].'" name="check'.$data['aaData'][$dI][0].'">';
//        }
        $data_new = array('aaData'=>array());
        foreach($data['aaData'] as $dI => $dV){ 
            $initial =0;
            foreach($columns_heading as $heading){
                $data_new['aaData'][$dI][$heading] = $dV[$initial];
                $initial++;
            }
            
            $data_new['aaData'][$dI][] = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="check' . $data['aaData'][$dI][0] . '">';
        }
        
        $data['aaData'] = $data_new['aaData'];
        
        return  $data;
        
    }
    
    
    
    public function fetchAllOverdueSummary($args,$user,$page,$TatResult) {
           
        $table = 'job AS t1 
                    LEFT JOIN status                AS js ON t1.StatusID = js.StatusID
                    LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                    LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                    LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                    LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID
                    LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                    LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                    LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                    LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                    LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                    LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                    LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                    LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  '; 
                    
        $columns=$this->getSummaryFields($page,$TatResult,"overdueJobs",$args);
        $columns=$columns[0];
    
        if($args['tatType']=='s')
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            } 
        }
        else
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            } 

        }
        
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                if($args['tatType']=='s')
                {   
                    $args['where'] .= " AND statustat(t1.JobID)<".$args['btnValue']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<".$args['btnValue']." ";
                }   
                    
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                
                if($args['tatType']=='s')
                {   
                   $args['where'] .= " AND statustat(t1.JobID)>=".$args['btnValue']." AND statustat(t1.JobID)<=".$args['btnValue2']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>=".$args['btnValue']." AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<=".$args['btnValue2']." ";
                } 
                
                
                
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                if($args['tatType']=='s')
                {   
                   $args['where'] .= " AND statustat(t1.JobID)>".$args['btnValue']." ";
                } 
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>".$args['btnValue']." ";
                } 
                
                
            }
        }
        
         if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }  
        
        
        
        
        
        
         // Brand  Filter
        if (isset($args['brand']) && $args['brand']) {
           $args['where'] .= ' AND t8.BrandID='.$args['brand']; 
        }      
        
        // Status Type Filter
        if (isset($args['sType']) && $args['sType']) {
            $args['where'] .= ' AND t1.StatusID='.$this->conn->quote( $args['sType'] );
        }
        
        // Service Centre Type Filter
//        if (isset($args['scType']) && $args['scType']) {
//            $args['where'] .= ' AND t1.ServiceCentreID='.$this->conn->quote( $args['scType'] );
//        }
        
        // Manufacturer Filter
        if (isset($args['manufacturer'])) {
           $args['where'] .= ' AND t1.ManufacturerID='.$this->conn->quote( $args['manufacturer'] ); 
        }
        
         // Service Provider Filter
        if (isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID='.$this->conn->quote( $args['serviceProvider'] ); 
        }

        // Client Filter
        if (isset($args['client'])) {
           $args['where'] .= ' AND t1.ClientID='.$this->conn->quote( $args['client'] ); 
        }
        
        //Network Filter
        if(isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID=' . $this->conn->quote($args['network']);
        }
        
        //Branch Filter
        if(isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID=' . $this->conn->quote($args['branch']);
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND t4.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
        //Skill Set Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND t10.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
        
        
        
        $SystemStatusesModel   = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('overdueJobs', 'js');
        
        if(count($UserStatusPreferences))
        {
            $sIDStr = '';
            $sep    = '';
            
            foreach($UserStatusPreferences AS $usp)
            {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep    = ',';
            }
            $args['where'] .= ' AND js.StatusID IN ('.$sIDStr.')';
        }
        
        
        if(isset($args['serviceProvidecheck']) && is_array($args['serviceProvidecheck'])){
            $args['where'] .= ' AND t1.ServiceProviderID IN (' . implode(',',$args['serviceProvidecheck']) . ')';
        }
        
        //$args['where'] .= " GROUP BY t1.JobID";
        $args['groupby'] = "t1.ServiceProviderID";
	
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
               
        foreach ($data['aaData'] as $dI=>$dV) {
            $data['aaData'][$dI][] = '<input type="checkbox"  value="'.$data['aaData'][$dI][0].'" name="serviceProvidecheck[]">';
        }
        
        return  $data;
        
    }
    
    
    
     /**
     * Description
     * 
     * This method is for to fetch jobs for given phone number.
     * 
     * @param array $args Its an associative array.
    
     * @return array $data   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
     public function fetchCallerJobs( $args ) {
        
        $callerNumber = isset($args['0'])?$args['0']:0; 
        
        
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN customer              AS cu ON t1.CustomerID = cu.CustomerID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID'; 
        
        $columns = array( "t1.JobID", 
                          array('t1.DateBooked',"DATE_FORMAT (t1.DateBooked, '%d/%m/%Y')"), 
                          "t1.ServiceCentreJobNo", 
                          "t5.ManufacturerName",
                          "t3.ModelNumber",
                          "t4.UnitTypeName", 
                          "t1.ReportedFault");
        
        $args['where'] = '(cu.ContactHomePhone='.$this->conn->quote( $callerNumber ).'  OR cu.ContactMobile='.$this->conn->quote( $callerNumber ).') ' . $this->UserJobFilter('t1');
        
       // $this->controller->log($args['where']);    
        
        $args['where'] .= " GROUP BY t1.JobID";
        
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
        
        
        foreach ($data['aaData'] as $dI=>$dV) {
            $data['aaData'][$dI][0]  = '<input type="radio" class="PreviousJobIDRadio"  value="'.$data['aaData'][$dI][0].'"  name="JobID" >'.$data['aaData'][$dI][0];
            
        }
       
        return  $data;
        
    }
    
    
    
     /**
     * Description
     * 
     * This method is for to fetch customer contact details for given phone number.
     * 
     * @param array $args Its an associative array.
    
     * @return array $data   
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
     public function fetchCallerDetails( $args ) {
        
        $callerNumber = isset($args['0'])?$args['0']:0; 
        $customerID   = (isset($args['1']) && $args['1'] !='')?$args['1']:false; 
        
        
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN customer              AS cu ON t1.CustomerID = cu.CustomerID
                '; 
        
        $columns = array( "cu.CustomerID", 
                          "CONCAT_WS(' ', cu.ContactFirstName, cu.ContactLastName) AS CustomerName", 
                          "cu.BuildingNameNumber",
                          "cu.Street",
                          "cu.LocalArea",
                          "cu.TownCity",
                          "cu.PostalCode",
                          "cu.ContactHomePhone",
                          "cu.ContactWorkPhoneExt", 
                          "cu.ContactMobile",
                          "cu.ContactEmail",
                          "t1.JobID" );
        
        $args['where'] = '';
        if($customerID)
        {
            $args['where'] = 'cu.CustomerID='.$this->conn->quote( $customerID ).' AND ';
        }    
        $args['where'] .= '(cu.ContactHomePhone='.$this->conn->quote( $callerNumber ).'  OR cu.ContactMobile='.$this->conn->quote( $callerNumber ).') ' . $this->UserJobFilter('t1');
        
       // $this->controller->log($args['where']);    
        
        $args['where'] .= " GROUP BY cu.CustomerID";
        
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
        
        
       
        return  $data;
        
    }
    
    
    
    public function bookingPerformance( $args ) {
        
       $SkylineModel = $this->controller->loadModel('Skyline');
       
       
       $tableRows = array();
       
       
      
      
       
//       if($this->controller->user->SuperAdmin)
//       {
//           $networks_details = $SkylineModel->getNetworks();
//           foreach($networks_details AS $nt)
//           {
//               $tableRows[] = array(0=>$nt['NetworkID'], 1=>$nt['CompanyName']); 
//           }    
//       }
//       else  if($this->controller->user->NetworkID)
//       {
//           $network_name = $SkylineModel->getNetworkName($this->controller->user->NetworkID);
//           if($network_name!='')
//           {    
//                 $tableRows[] = array(0=>$this->controller->user->NetworkID, 1=>$network_name);
//           }
//       }
//       
       $ActiveJobFilter = $this->ActiveJobFilter('js');
       /*
        * Added to show and hide the cancelled Jobs
        * @Author Soubhik
        */
       if(isset($args['displayCancelled'])){
            if($args['displayCancelled']=='cancelled')
            {
                $ActiveJobFilter = '';
            }
       }
       
       /*
        * End of code
        */
       
       if($args['NetworkID']=='-1')
       {
           $args['NetworkID'] = false;
       }    
      
       if($args['ClientID']=='-1')
       {
           $args['ClientID'] = false;
       }
       
       if($args['BranchID']=='-1')
       {
           $args['BranchID'] = false;
       }
       
       if($args['BrandID']=='-1')
       {
           $args['BrandID'] = false;
       }
       
       if($args['NetworkID'] && $args['ClientID'] && $args['BranchID'])
       {
           if($args['BrandID'])
           {    
                $brandsList = $SkylineModel->getBrand($args['BrandID']);
                
                $tableRows[] = array(0=>$brandsList[0]['BrandID'], 1=>$brandsList[0]['Name']);
           }
           else
           {    
                $brandsList = $SkylineModel->getBrands($args['NetworkID'], $args['ClientID'], $args['BranchID']);
                foreach($brandsList AS $bt)
                {
                    $tableRows[] = array(0=>$bt['BrandID'], 1=>$bt['BrandName']); 
                } 
           }    
       }
       else if($args['NetworkID'] && $args['ClientID'])
       {
           if($args['BranchID'])
           {    
                $BranchModel    = $this->controller->loadModel('Branches');
                $BranchDetails  = $BranchModel->fetchRow(array('BranchID'=>$args['BranchID']));
                
                $tableRows[] = array(0=>$BranchDetails['BranchID'], 1=>$BranchDetails['BranchName']);
           }
           else
           {    
                $branchList = $SkylineModel->getBranches(null, null, $args['ClientID'], $args['NetworkID']);
                foreach($branchList AS $bl)
                {
                    $tableRows[] = array(0=>$bl['BranchID'], 1=>$bl['BranchName']); 
                } 
           }    
       }
       else if($args['NetworkID'])
       {
           if($args['ClientID'])
           {    
                $clientName  = $SkylineModel->getClientName($args['ClientID']);
                
                $tableRows[] = array(0=>$args['ClientID'], 1=>$clientName);
           }
           else
           {    
                $ClientList  = $SkylineModel->getNetworkClients($args['NetworkID']);
                foreach($ClientList AS $cl)
                {
                    $tableRows[] = array(0=>$cl['ClientID'], 1=>$cl['ClientName']); 
                } 
           }    
       }
       else 
       {
           if($args['NetworkID'])
           {    
                $network_name = $SkylineModel->getNetworkName($args['NetworkID']);
                if($network_name!='')
                {    
                      $tableRows[] = array(0=>$args['NetworkID'], 1=>$network_name);
                }
                
           }
           else
           {    
                $networks_details = $SkylineModel->getNetworks();
                foreach($networks_details AS $nt)
                {
                    $tableRows[] = array(0=>$nt['NetworkID'], 1=>$nt['CompanyName']); 
                }
           }    
       }
       
       
       
       
       
       
       $tableRowsCnt = count($tableRows);
       
       if($tableRowsCnt)
       {
               $tableRows[$tableRowsCnt][0] = '';
               $tableRows[$tableRowsCnt][1] = '<b>Totals</b>';
               
               for($n=2;$n<=7;$n++)
               {
                    $tableRows[$tableRowsCnt][$n] = 0;
               } 
       }
       
       
       $lastMonthStartDate = date("Y-m-01", strtotime("-1 month") ) ;
       $lastMonthEndDate   = date("Y-m-t", strtotime("-1 month") ) ;
       
       
       $last4MonthsStartDate = date("Y-m-01", strtotime("-4 month") ) ;
       
       
       for($i=0;$i<$tableRowsCnt;$i++)
       {
           if($tableRows[$i])
           {
               
               if($args['NetworkID'] && $args['ClientID'] && $args['BranchID'])
               {
                  
                   $sqlDay = "SELECT COUNT(DISTINCT t1.JobID) AS DayCount 
                       FROM job AS t1   LEFT JOIN status AS js ON t1.StatusID = js.StatusID 
                                        LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                                        LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID
		    
                       
                        WHERE t1.DateBooked=CURRENT_DATE AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLastMonth = "SELECT COUNT(DISTINCT t1.JobID) AS LastMonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID
                       LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                                        LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID

                    WHERE t1.DateBooked>='".$lastMonthStartDate."' AND t1.DateBooked<='".$lastMonthEndDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlMonth = "SELECT COUNT(DISTINCT t1.JobID) AS MonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID
                       LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                       LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID
                    WHERE t1.DateBooked>='".date("Y-m-")."01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlYear = "SELECT COUNT(DISTINCT t1.JobID) AS YearCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID
                       
                    LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                    LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID

                    WHERE t1.DateBooked>='".date("Y-")."-01-01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLast4Months = "SELECT COUNT(DISTINCT t1.JobID) AS LastFourMonthsCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID 
                       
                   LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                   LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID
                                        
                    WHERE t1.DateBooked>='".$last4MonthsStartDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlTotal = "SELECT COUNT(DISTINCT t1.JobID) AS TotalCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID
                       
                   LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                   LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID
                                        
                    WHERE  t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
               
                  
               }
               else if($args['NetworkID'] && $args['ClientID'])
               {
                   
                   $sqlDay = "SELECT COUNT(*) AS DayCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked=CURRENT_DATE AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLastMonth = "SELECT COUNT(*) AS LastMonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$lastMonthStartDate."' AND t1.DateBooked<='".$lastMonthEndDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlMonth = "SELECT COUNT(*) AS MonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-m-")."01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlYear = "SELECT COUNT(*) AS YearCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-")."-01-01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLast4Months = "SELECT COUNT(*) AS LastFourMonthsCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$last4MonthsStartDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlTotal = "SELECT COUNT(*) AS TotalCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE  t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
               
                   
               }
               else if($args['NetworkID'])
               {
                   $sqlDay = "SELECT COUNT(*) AS DayCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked=CURRENT_DATE AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLastMonth = "SELECT COUNT(*) AS LastMonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$lastMonthStartDate."' AND t1.DateBooked<='".$lastMonthEndDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlMonth = "SELECT COUNT(*) AS MonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-m-")."01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlYear = "SELECT COUNT(*) AS YearCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-")."-01-01' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLast4Months = "SELECT COUNT(*) AS LastFourMonthsCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$last4MonthsStartDate."' AND t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlTotal = "SELECT COUNT(*) AS TotalCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE  t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
               
               }
               else
               {
                   $sqlDay = "SELECT COUNT(*) AS DayCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked=CURRENT_DATE AND t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLastMonth = "SELECT COUNT(*) AS LastMonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$lastMonthStartDate."' AND t1.DateBooked<='".$lastMonthEndDate."' AND t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlMonth = "SELECT COUNT(*) AS MonthCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-m-")."01' AND t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlYear = "SELECT COUNT(*) AS YearCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".date("Y-")."-01-01' AND t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlLast4Months = "SELECT COUNT(*) AS LastFourMonthsCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE t1.DateBooked>='".$last4MonthsStartDate."' AND t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
                   
                   $sqlTotal = "SELECT COUNT(*) AS TotalCount FROM job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID WHERE  t1.NetworkID=".$this->conn->quote( $tableRows[$i][0] ).$ActiveJobFilter;
               
               }
               
               
               //Day count
               $recordset = $this->Query($this->conn, $sqlDay, $args);
               //$this->controller->log($recordset);
               $tableRows[$i][2] = isset($recordset[0]['DayCount'])?$recordset[0]['DayCount']:0;
               $tableRows[$tableRowsCnt][2] += $tableRows[$i][2];
               
               
               //Last Month count
               $recordset = $this->Query($this->conn, $sqlLastMonth, $args);
              // $this->controller->log($recordset);
               $tableRows[$i][3] = isset($recordset[0]['LastMonthCount'])?$recordset[0]['LastMonthCount']:0;
               $tableRows[$tableRowsCnt][3] += $tableRows[$i][3];
               
               
               //Month count
               $recordset = $this->Query($this->conn, $sqlMonth, $args);
              // $this->controller->log($recordset);
               $tableRows[$i][4] = isset($recordset[0]['MonthCount'])?$recordset[0]['MonthCount']:0;
               $tableRows[$tableRowsCnt][4] += $tableRows[$i][4];
               
               //Year count
               $recordset = $this->Query($this->conn, $sqlYear, $args);
               //$this->controller->log($recordset);
               $tableRows[$i][5] = isset($recordset[0]['YearCount'])?$recordset[0]['YearCount']:0;
               $tableRows[$tableRowsCnt][5] += $tableRows[$i][5];
               
               
               //Last 4 months
               $recordset = $this->Query($this->conn, $sqlLast4Months, $args);
               //$this->controller->log($recordset);
               $tableRows[$i][6] = isset($recordset[0]['LastFourMonthsCount'])?$recordset[0]['LastFourMonthsCount']:0;
               $tableRows[$tableRowsCnt][6] += $tableRows[$i][6];
               
               
               //Total count
               $recordset = $this->Query($this->conn, $sqlTotal, $args);
              // $this->controller->log($recordset);
               $tableRows[$i][7] = isset($recordset[0]['TotalCount'])?$recordset[0]['TotalCount']:0;
               $tableRows[$tableRowsCnt][7] += $tableRows[$i][7];
               
               
           }    
       }
      
       if($tableRowsCnt)
       {    
        
           for($i=2;$i<=7;$i++)
           {
               $tableRows[$tableRowsCnt][$i] = "<b>".$tableRows[$tableRowsCnt][$i]."</b>";
           }
       }
      //var_dump($args);
       //created by srinvas for displaying and non displaying of records with has totals '0' TBR-462          
       if(!isset($args['dispAll'])){
           $reorder=array();
           for($i = 0; $i < count($tableRows); $i++) {            
                  if( $tableRows[$i][7]!="0"){
                     $reorder[]=$tableRows[$i];                    
                  }  
                }
          $tableRows=$reorder; 
       }       
     //END 
      
       
       $data =  array (
                     'sEcho' => 0,
                     'iTotalRecords' => $tableRowsCnt,
                     'iTotalDisplayRecords' => $tableRowsCnt,
                     'aaData' => $tableRows );
        
        //$this->log($sqlTotal);
        //$this->log($args['displayCancelled']);
        return  $data;
        
    }
    
    
    
    public function fetchAllOpen($args,$user,$page) {
        //dt column proccsesing functions
        $dataTable=$this->controller->loadModel('DataTable');
        
        /*Add relation for Repair_skill table with unit_type to make filter if some one choose skillset.
         * Add condtions if some one choose networks,clients,branch, unit type and skillset.
         * 
         */
        
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID                 
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                  LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                  LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                  LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                  LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                  LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
                  LEFT JOIN ra_status             AS ras on t1.RAStatusID = ras.RAStatusID
                  LEFT JOIN client             AS cl on t1.ClientID = cl.ClientID
                 
                  '; 
        //$this->log($table);
	
        $columns=$dataTable->getAllFieldNames('job',$user,$page,$args['ojvBy']);
        $columns_heading = $columns[1];
        $columns=$columns[0];
       //$this->controller->log($columns,"columns_job____-_");
        //$this->log($columns);
	
        if(isset($args['ojBy']) && $args['ojBy']=='sp')
        {
             $args['where'] = 't1.ServiceProviderDespatchDate IS NULL  ' . $this->UserJobFilter('t1');  
        } /* Removed AND t1.ClosedDate IS NULL and  $this->ActiveJobFilter('js') from qury aboved based on Joe / Neil's definition of a Service provider Open Job - ajw 03/04/2013 */
        else
        {
            $args['where'] = 't1.ClosedDate IS NULL ' . $this->UserJobFilter('t1');       
        } 
        
        
       
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                $args['where'] .= " AND t1.DateBooked>DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                $args['where'] .= " AND t1.DateBooked<=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) AND t1.DateBooked>=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue2']." DAY)";
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                $args['where'] .= " AND t1.DateBooked<DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            }
        }
        
         if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }  
        
        
         if(isset($this->controller->session->hideNonSbJobs)&&$this->controller->session->hideNonSbJobs=="checked"){
             $args['where'].= ' AND t1.ServiceCentreJobNo!="" and t1.ServiceCentreJobNo is not null ';
        }
            
	//Brand Filter
        if(isset($args['brand']) && $args['brand']) {
           $args['where'] .= ' AND t8.BrandID=' . $args['brand']; 
        }    
        
        //Status Type Filter
        if(isset($args['sType']) && $args['sType']) {
            $args['where'] .= ' AND t1.StatusID=' . $this->conn->quote($args['sType']);
        }
        
        //Service Centre Type Filter
        if(isset($args['scType']) && $args['scType']) {
            $args['where'] .= ' AND t1.ServiceCentreID=' . $this->conn->quote($args['scType']);
        }

        //Manufacturer Filter
        if(isset($args['manufacturer']) && $args['manufacturer']) {
           $args['where'] .= ' AND t1.ManufacturerID=' . $this->conn->quote($args['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID=' . $this->conn->quote($args['serviceProvider']);
        }

        //Client Filter
        if(isset($args['client']) && $args['client']) {
           $args['where'] .= ' AND t1.ClientID=' . $this->conn->quote($args['client']);
        }
        
        //Network Filter
        if(isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID=' . $this->conn->quote($args['network']);
        }
        
        //Branch Filter
        if(isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID=' . $this->conn->quote($args['branch']);
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND ut.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
        //Skill Set Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND rs.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
     
        $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');
        
        if(count($UserStatusPreferences)) {
            $sIDStr = '';
            $sep = '';
            
            foreach($UserStatusPreferences as $usp) {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep = ',';
            }
            $args['where'] .= ' AND js.StatusID IN (' . $sIDStr . ')';
        }

        //$args['where'] .= " GROUP BY t1.JobID";
       $args['groupby'] = "t1.JobID";
        
       
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args,$columns_heading);
        
        $data_new = array('aaData'=>array());
        foreach($data['aaData'] as $dI => $dV){ 
            $initial =0;
            foreach($columns_heading as $heading){
                $data_new['aaData'][$dI][$heading] = $dV[$initial];
                $initial++;
            }
            
            $data_new['aaData'][$dI][] = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="check' . $data['aaData'][$dI][0] . '">';
        }
        
        $data['aaData'] = $data_new['aaData'];
        
        return  $data;
        
    }
    
    
    public function fetchAllOpenSummary($args,$user,$page,$TatResult) {
        //dt column proccsesing functions
        $dataTable=$this->controller->loadModel('DataTable');
        
        /*Add relation for Repair_skill table with unit_type to make filter if some one choose skillset.
         * Add condtions if some one choose networks,clients,branch, unit type and skillset.
         * 
         */
        
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID                  
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                  LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                  LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                  LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                  LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                  LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
              
                  '; 

        $columns=$this->getSummaryFields($page,$TatResult);
        $columns=$columns[0];
       //$this->controller->log($columns,"columns_job____-_");
        
	
        if(isset($args['ojBy']) && $args['ojBy']=='sp')
        {
             $args['where'] = 't1.ServiceProviderDespatchDate IS NULL  ' . $this->UserJobFilter('t1');  
        } /* Removed AND t1.ClosedDate IS NULL and  $this->ActiveJobFilter('js') from qury aboved based on Joe / Neil's definition of a Service provider Open Job - ajw 03/04/2013 */
        else
        {
            $args['where'] = 't1.ClosedDate IS NULL ' . $this->UserJobFilter('t1');       
        } 
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                $args['where'] .= " AND t1.DateBooked>DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                $args['where'] .= " AND t1.DateBooked<=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) AND t1.DateBooked>=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue2']." DAY)";
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                $args['where'] .= " AND t1.DateBooked<DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            }
        }
        
         if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }  
        
        
         if(isset($this->controller->session->hideNonSbJobs)&&$this->controller->session->hideNonSbJobs=="checked"){
             $args['where'].= ' AND t1.ServiceCentreJobNo!="" and t1.ServiceCentreJobNo is not null ';
        }
            
	//Brand Filter
        if(isset($args['brand']) && $args['brand']) {
           $args['where'] .= ' AND t8.BrandID=' . $args['brand']; 
        }    
        
        //Status Type Filter
        if(isset($args['sType']) && $args['sType']) {
            $args['where'] .= ' AND t1.StatusID=' . $this->conn->quote($args['sType']);
        }
        
        //Service Centre Type Filter
        if(isset($args['scType']) && $args['scType']) {
            $args['where'] .= ' AND t1.ServiceCentreID=' . $this->conn->quote($args['scType']);
        }

        //Manufacturer Filter
        if(isset($args['manufacturer']) && $args['manufacturer']) {
           $args['where'] .= ' AND t1.ManufacturerID=' . $this->conn->quote($args['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID=' . $this->conn->quote($args['serviceProvider']);
        }

        //Client Filter
        if(isset($args['client']) && $args['client']) {
           $args['where'] .= ' AND t1.ClientID=' . $this->conn->quote($args['client']);
        }
        
        //Network Filter
        if(isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID=' . $this->conn->quote($args['network']);
        }
        
        //Branch Filter
        if(isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID=' . $this->conn->quote($args['branch']);
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND ut.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
        //Skill Set Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND rs.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
     
        $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');
        
        if(count($UserStatusPreferences)) {
            $sIDStr = '';
            $sep = '';
            
            foreach($UserStatusPreferences as $usp) {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep = ',';
            }
            $args['where'] .= ' AND js.StatusID IN (' . $sIDStr . ')';
        }
        
        
        

        //$args['where'] .= " GROUP BY t1.JobID";
       $args['groupby'] = "t1.ServiceProviderID";
        
       
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
               
        foreach($data['aaData'] as $dI => $dV) {
            $data['aaData'][$dI][]  = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="serviceProvidecheck[]">';
        }
    
        return  $data;
        
    }

    
    
    /**
     * fetchJobStatusPeriod
     * 
     * Return a list of jobs together with their status
     * 
     * @param integer $days     Number of days to look back for booked date
     * 
     * @return Assoicative array containing recorset
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function fetchJobStatusPeriod($days) { 
        
        /*
         * Change j.`NetworkRefNo` AS `WrRefNo` to j.`AgentRefNo` AS `WrRefNo`
         * Similiar to job Update page
         */
        
        $sql = "
                SELECT DISTINCT
                        j.`ServiceCentreJobNo` AS `SB Job No`,
                        j.`RMANumber` AS `RMA No`,
                        j.`JobID` AS `Skyline No`,
                        DATE_FORMAT(j.`DateBooked`,'%d/%m/%Y') AS `Date Booked`,
                        n.`CompanyName` AS `Network`,
                        clt.`ClientName` AS `Client`,
                        IF (ISNULL(brnd.`BrandName`),
				'SKYLINE'
			,
				brnd.`BrandName`
			) AS `Brand`,
                        brch.`BranchName` AS `Branch`,
                        s.`StatusName` AS `Current Status`,
                        DATE_FORMAT(MAX(sh.`Date`),'%d/%m/%Y') AS `Current Status Change Date`,
                        sp.`CompanyName` AS `Service Provider`,
                        j.`GuaranteeCode` AS `Guarantee Code`,
                        j.`ClosedDate` AS `Completion Date`,
                        DATE_FORMAT(j.`ServiceProviderDespatchDate`,'%d/%m/%Y') AS `Despatched Date`,
                        j.`JobSite` AS `JobSite`,
                        IF (ISNULL(mft.`ManufacturerName`),
				j.`ServiceBaseManufacturer`
                        ,
				mft.`ManufacturerName`
			) AS `Manufacturer`,
			st.`ServiceTypeName` AS `Charge Type`,
			n.`CompanyName` AS `Warrantor`,
			u.`Username` AS `Originator`,
			j.`OriginalRetailer` AS `Retailer`,			
                        j.`AgentRefNo` AS `WrRefNo`,
			IF (ISNULL(mdl.`ModelNumber`),
				j.`ServiceBaseModel`
                        ,
				mdl.`ModelNumber`
			) AS `Model Number`,
			IF (ISNULL(ut.`UnitTypeName`),
				j.`ServiceBaseUnitType`
                        ,
				ut.`UnitTypeName`
			) AS `UnitType`,
			j.`SerialNo` AS `Serial Number`,
			DATE_FORMAT(j.`DateOfPurchase`,'%d/%m/%Y') AS `DOP`,
			j.`PolicyNo` AS `Policy Number`,
			j.`ConditionCode` AS `Condition Code`,
			j.`SymptomCode` AS `Symptom Code`,
			j.`GuaranteeCode` AS `Guarantee Code`,
			DATE_FORMAT(j.`DateUnitReceived`,'%d/%m/%Y') AS `Date Unit Received`,
			DATE_FORMAT(j.`RepairCompleteDate`,'%d/%m/%Y') AS `Date Repair Completed`,
			DATE_FORMAT(j.`ClosedDate`,'%d/%m/%Y') AS `Closed Date`,
			DATE_FORMAT(j.`ETDDate`,'%d/%m/%Y') AS `ETD Date`,
			j.`AgentStatus` AS `Agent Status`,
			j.`CompletionStatus` AS `Completion Status`,
			j.`EngineerName` AS `Engineer Name`,
			j.`ClaimNumber` AS `Claim Number`,
			j.`AuthorisationNo` AS `Authorisation Number`,
			j.`ChargeableLabourCost` AS `Chargeable Labour Cost`,
			COUNT(`PartID`) AS `Number of Parts`,
			COUNT(`AppointmentID`) AS `Number of Appointments`,
			COUNT(`ContactHistoryID`) AS `Number of Contact History Entries`,
                        DATE_FORMAT(j.`DateReturnedToCustomer`,'%d/%m/%Y') AS `Despatched From Store`
                FROM
                        `job` j LEFT JOIN `branch` brch ON j.`BranchID` = brch.`BranchID`        
                                LEFT JOIN `brand_branch` brndbrch ON brch.`BranchID` = brndbrch.`BranchID`
                                LEFT JOIN `brand` brnd ON brndbrch.`BrandID` = brnd.`BrandID`
                                LEFT JOIN `client` clt ON j.`ClientID` = clt.`ClientID`
                                LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
                                LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
                                LEFT JOIN `status_history` sh ON s.`StatusID` = sh.`StatusID`
                                                                AND j.`JobID` = sh.`JobID`
                                LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`
				LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID` 
        		        LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`
                                LEFT JOIN `part` p ON j.`JobID` = p.`JobID`
        		        LEFT JOIN `appointment` a ON j.`JobID` = a.`AppointmentID`
        		        LEFT JOIN `contact_history` ch ON j.`JobID` = ch.`JobID`
        		        LEFT JOIN `network` n ON j.`NetworkID` = n.`NetworkID`
        		        LEFT JOIN `user` u ON j.`BookedBy` = u.`UserID`,
        		`service_type` st
                WHERE
                        j.`DateBooked` > DATE_SUB(NOW(), INTERVAL $days DAY)
                        AND j.`ServiceTypeID` = st.`ServiceTypeID`
                GROUP BY
                        j.`JobID`
                ORDER BY
                        j.`DateBooked`
               ";
        
       
        $result = $this->Query($this->conn, $sql);                              /* Get first few columns - current status and service rpovider */        
        
        return($result);
    }
    

    
    /**
    * Description
    * 
    * This method is for to fetch ra jobs for query table.
    * 
    * @param array $args Its an associative array.

    * @return array $data   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    
    public function fetchAllRA($args) {
        $dataTable=$this->controller->loadModel('DataTable');

         $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN ra_status             AS t9 on t1.RAStatusID = t9.RAStatusID
                  LEFT JOIN network               AS t11 on t1.NetworkID = t11.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                  LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                  LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                  LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                  LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                  LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
                  LEFT JOIN ra_status             AS ras on t1.RAStatusID = ras.RAStatusID
                  LEFT JOIN client                AS cl on t1.ClientID = cl.ClientID
                 
                  '; 
        $columns=$dataTable->getAllFieldNames('job',  $this->controller->user->UserID,23);
        $columns=$columns[0];
        
        $args['where'] = 't1.ClosedDate IS NULL ' . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');
        
        //RA Type filter
        if (isset($args['atType']) && $args['atType']) {
           $args['where'] .= ' AND t1.RAType=' . $this->conn->quote($args['atType']); 
        } else {
            
            if(isset($args['RATypes'])) {
		if(is_array($args['RATypes'])) {
		    $sep = '';
		    $inValues = '';
		    foreach($args['RATypes'] as $rKey => $rValue) {
			$inValues = $inValues . $sep . "'" . $rValue['Name'] . "'";
			$sep = ',';
		    }

		    if($inValues != '') {
			$args['where'] .= ' AND t1.RAType IN (' . $inValues . ')'; 
		    } else {
			$args['where'] .= " AND t1.JobID='0' ";
		    }
		}     
            } else {
                $args['where'] .= ' AND t1.RAType IS NOT NULL'; 
            }
            
        }
      
        //RA Status Type filter
        if (isset($args['sType']) && $args['sType']) {
           $args['where'] .= ' AND t1.RAStatusID=' . $this->conn->quote($args['sType']); 
        }

        $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('raJobs', 'ra');
        
        if(count($UserStatusPreferences)) {
            $sIDStr = '';
            $sep = '';
            
            foreach($UserStatusPreferences AS $usp) {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep = ',';
            }
            $args['where'] .= " AND t9.RAStatusCode IN (SELECT RAStatusCode FROM ra_status WHERE RAStatusID IN ($sIDStr))";
            //$args['where'] .= ' AND t1.RAStatusID IN (' . $sIDStr . ')';
        }
        else
        {
            $brandID = $this->controller->user->DefaultBrandID;
            
            $q = "	SELECT  RAStatusCode

		FROM    ra_status

		WHERE   CASE
			    WHEN @stat > 0

			    THEN BrandID = $brandID

			    ELSE BrandID = 1000
			END
	     ";
            
            $userID=$this->controller->user->UserID;
             $args['where'] .= " 
				AND t9.RAStatusCode IN (
				    SELECT	ra_status.RAStatusCode
				    FROM	status_preference
				    LEFT JOIN	ra_status ON status_preference.StatusID = ra_status.RAStatusID
				    WHERE	status_preference.UserID=$userID
				)				
				AND t9.RAStatusCode IN ($q)";
        }
        
        
        $args['where'] .= " GROUP BY t1.JobID";
        
        $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
        
        foreach ($data['aaData'] as $dI=>$dV) {
            $data['aaData'][$dI][]  = '<input class="checkBoxDataTable" style="margin:0!important" type="checkbox"  value="' . $data['aaData'][$dI][0] . '" name="RAJobsTagged[]" >';
        }
       
        return  $data;
        
    }
    
    
    
    /**
    * Description
    * 
    * This method is for to fetch ra hisotry for query table.
    * 
    * @param int $JobID 
    * @param boolean $count 
    * @return array $data   
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchRaHistory($args, $JobID, $count = false) {
        
	$table = "  ra_history AS t1 
                    LEFT JOIN user AS t2 ON t1.ModifiedUserID = t2.UserID
                    LEFT JOIN ra_status AS t3 ON t1.RAStatusID = t3.RAStatusID
		    LEFT JOIN authorisation_types AS at ON at.AuthorisationTypeID = t3.AuthorisationTypeID
                 ";
	$args["where"] = "t1.JobID='" . $JobID . "'";
        
	if (isset($args["type"])) {
	    $args["where"] .= " AND at.AuthorisationTypeName LIKE '{$args["type"]}'";
	}
	
        if ($count) {
	    
            /* Execute a prepared statement by passing an array of values */
            $sql = "SELECT COUNT(*) FROM " . $table . " WHERE " . $args["where"];
            $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $fetchQuery->execute();
            $result = $fetchQuery->fetch();
            return (isset($result[0])) ? $result[0] : 0;
	    
        } else {
	    
	    $columns = [ 
		"t1.RAHistoryID", 
		["t1.ModifiedDate", "DATE_FORMAT (t1.ModifiedDate, '%d/%m/%Y')"], 
		"DATE_FORMAT (t1.ModifiedDate, '%H:%i')",
		"CONCAT(t2.ContactFirstName, ' ', t2.ContactLastName)",
		"t3.RAStatusName",
		"at.AuthorisationTypeName",
		"t1.Notes",
		"t1.Image"
	    ];
	    
            $data = $this->ServeDataTables($this->conn, $table, $columns, $args);
	    
            return  $data;
	    
        }
        
    }
    
    
    
    /**
    * fetchServiceProviderOpenStats
    * 
    * This method is for to get service provider statistics.
    * 
    * @param array $args Its an associative array.
    * @return array   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     public function fetchServiceProviderOpenStats( $ServiceProviderID )
     {
         $result = array();
         $where = 't1.ClosedDate IS NULL ' . $this->ActiveJobFilter('js');
         
         $sql = 'SELECT count(*) AS JobCount
                    FROM job AS t1
                    LEFT JOIN service_provider AS t2 ON t1.ServiceProviderID=t2.ServiceProviderID
                    LEFT JOIN status AS js ON t1.StatusID = js.StatusID ';
         
         
         $sql1 = $sql." WHERE t1.ServiceProviderID='".$ServiceProviderID."' AND t1.DateBooked >= DATE_SUB(NOW(), INTERVAL 7 DAY) AND ". $where;
     
         $data1 = $this->Query($this->conn, $sql1, null, PDO::FETCH_NUM);
         
         $result['7days']   = isset($data1[0][0])?$data1[0][0]:0;
         
         
         $sql2 = $sql." WHERE t1.ServiceProviderID='".$ServiceProviderID."' AND t1.DateBooked < DATE_SUB(NOW(), INTERVAL 7 DAY) AND t1.DateBooked >= DATE_SUB(NOW(), INTERVAL 14 DAY) AND ". $where;
     
         $data2 = $this->Query($this->conn, $sql2, null, PDO::FETCH_NUM);
         
         $result['7_14days'] = isset($data2[0][0])?$data2[0][0]:0;
         
         
         $sql3 = $sql." WHERE t1.ServiceProviderID='".$ServiceProviderID."' AND t1.DateBooked < DATE_SUB(NOW(), INTERVAL 14 DAY) AND ". $where;
     
         $data3 = $this->Query($this->conn, $sql3, null, PDO::FETCH_NUM);
         
         $result['14days']   = isset($data3[0][0])?$data3[0][0]:0;
         
         return $result;
         //$this->log($result);
     }        
    
    
    
    /**
    * Description
    * 
    * This method is used for to get service provider open jobs count.
    * 
    * @param int $ServiceProviderID 
    * @param int $NetworkID  
    * 
    * @return int   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
     
     public function getServiceProviderOpenJobs( $ServiceProviderID, $NetworkID=false, $aType=false ) {
         
         
        // $args['where'] = "t1.ClosedDate IS NULL AND t1.ServiceProviderID='".$ServiceProviderID."' ". $this->ActiveJobFilter('js');
         
         
         $args['where'] = "t1.ServiceProviderDespatchDate IS NULL AND t1.ServiceProviderID='".$ServiceProviderID."'  " . $this->UserJobFilter('t1');  
         
         if($NetworkID)
         {
            if($NetworkID==2)//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
            {    
               $args['where'] = $args['where']." AND t1.NetworkID='".$NetworkID."' AND t1.ManufacturerID='106'";
            }
            else
            {
               $args['where'] = $args['where']." AND t1.NetworkID='".$NetworkID."'";
            }
                
            
         }    
         
         if($aType=="av" || $aType=="ha" || $aType=="os")
         {
             $aTypeCondition = '';
             
             if($aType=="av")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
             } 
             else if($aType=="ha")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
             }
             else if($aType=="os")
             {
                 $aTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
             }
             
             $sql = 'SELECT count(*) AS JobCount 
                       FROM job AS t1
                       LEFT JOIN `model` AS mdl on t1.`ModelID`=mdl.`ModelID` 
                       LEFT JOIN `unit_type` AS ut on mdl.`UnitTypeID`=ut.`UnitTypeID` 
                       LEFT JOIN `repair_skill` AS rs on ut.`RepairSkillID`=rs.`RepairSkillID` 

                       LEFT JOIN status AS js ON t1.StatusID=js.StatusID WHERE ' . $args['where'].$aTypeCondition;
             
            
         }   
         else
         {
         
            $sql = 'SELECT count(*) AS JobCount 
                       FROM job AS t1
                       LEFT JOIN status AS js ON t1.StatusID=js.StatusID WHERE ' . $args['where'];
         }
         
          $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
          
          
//          $this->log("ssssssssssope");
//          $this->log($data);
          
          if(isset($data[0][0]))
          {
              return (int) $data[0][0];
          }    
          else
          {
               return 0;     
          }
          
     }
     
     
    
    public function fetchAllOpenStats( $args ) {
       
        $sql = null;
        
        
        
        if(isset($args['ojBy']) && $args['ojBy']=='sp')
        {
             $args['where'] = 't1.ServiceProviderDespatchDate IS NULL  ' . $this->UserJobFilter('t1');  
        }
        else
        {
            $args['where'] = 't1.ClosedDate IS NULL ' . $this->UserJobFilter('t1');      
        }
        
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                $args['where'] .= " AND t1.DateBooked>DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                $args['where'] .= " AND t1.DateBooked<=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) AND t1.DateBooked>=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue2']." DAY)";
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                $args['where'] .= " AND t1.DateBooked<DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            }
        }
        
        
        if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND t4.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
         //skillset Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND t10.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
        
         // Branch Filter
        if (isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID='.$this->conn->quote( $args['branch'] ); 
        }
        
        // Network Filter
        if (isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID='.$this->conn->quote( $args['network'] ); 
        }
        
         // Manufacturer Filter
        if (isset($args['manufacturer']) && $args['manufacturer']) {
           $args['where'] .= ' AND t1.ManufacturerID='.$this->conn->quote( $args['manufacturer'] ); 
        }
        
        
         // Service Provider Filter
        if (isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID='.$this->conn->quote( $args['serviceProvider'] ); 
        }
        

        // Client Filter
        if (isset($args['client']) && $args['client']) {
           $args['where'] .= ' AND t1.ClientID='.$this->conn->quote( $args['client'] ); 
        }
        
        // Brand  Filter
        if (isset($args['brand']) && $args['brand']) {
           $args['where'] .=  ' AND t8.BrandID='.$this->conn->quote( $args['brand'] ); 
        }
        
        // Status  Filter
        if (isset($args['sType']) && $args['sType']) {
           $args['where'] .=  ' AND t1.StatusID='.$this->conn->quote( $args['sType'] ); 
        }
        
        
        $SystemStatusesModel   = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');
       
        if(count($UserStatusPreferences))
        {
            $sIDStr = '';
            $sep    = '';
            
            foreach($UserStatusPreferences AS $usp)
            {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep    = ',';
            }
            $args['where'] .= ' AND js.StatusID IN ('.$sIDStr.')';
        }
        
        
        if (isset($args['scType']) && $args['scType']) {
            $sql = 'SELECT t2.CompanyName, count(*) AS JobCount, t1.ServiceProviderID 
                    FROM job AS t1
                    LEFT JOIN service_provider AS t2 ON t1.ServiceProviderID=t2.ServiceProviderID
                    LEFT JOIN status AS js ON t1.StatusID = js.StatusID ';
                    
                    if (isset($args['brand']) && $args['brand'])
                    {
                        $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
                    }
                    
                    $sql_ut = "";
                    if (isset($args['unitType']) && $args['unitType'])
                    {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';

                    }

                    if(isset($args['skillSet']) && $args['skillSet']) {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
                    }
                    
                    
                    
            $sql = $sql.$sql_ut.'
                    WHERE ' . $args['where']
                 . ' GROUP BY t1.ServiceProviderID';
        }
        else //if (isset($args['sType']) && $args['sType']) 
        {
            $sql = 'SELECT js.StatusName, count(*) AS JobCount, t1.StatusID, js.Colour 
                    FROM job AS t1
                    LEFT JOIN status AS js ON t1.StatusID=js.StatusID ';
            if (isset($args['brand']) && $args['brand'])
            {
                $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
            }
            
            if (isset($args['client']) && $args['client'])
            {
                $sql = $sql.'LEFT JOIN client  AS cl on t1.ClientID = cl.ClientID';
            }
            
            
            $sql_ut = "";
            if (isset($args['unitType']) && $args['unitType'])
            {
                $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';
                
            }
            
            if(isset($args['skillSet']) && $args['skillSet']) {
                $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
            }
            
            $sql = $sql.$sql_ut.'
                
                    WHERE ' . $args['where']
                 . ' GROUP BY t1.StatusID
                     ORDER BY js.StatusName';
        }
            
        /*
       if (isset($args['Brand']) && $args['Brand']) {
            $sql = 'SELECT t2.CompanyName, count(*) AS JobCount 
                    FROM job AS t1
                    LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID
                    WHERE t1.ClosedDate IS NULL ' . $this->UserJobFilter('t1')
                 . ' GROUP BY t1.ServiceProviderID';
        }*/
  //$this->controller->log($sql,"lo__");
        
//        echo $sql."<BR/>";
        
        
        $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        if (count($data) == 0)
           $data[] = array('', 0, 0, 'ffffff');
        $this->log($data);
        return $data;
    }
    
    
    
    /**
    * Description
    * 
    * This method is for to get all ra job statistics.
    * 
    * @param array $args Its an associative array contains atType etc.
    * @return array   
    * 
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchAllRAStats($args) {
       
	$sql = null;
        
        $args['where'] = 'job.ClosedDate IS NULL ' . $this->UserJobFilter('job') . $this->ActiveJobFilter('js');
       
        //RA Type filter
        if(isset($args['atType']) && $args['atType']) {
	    $args['where'] .= ' AND job.RAType=' . $this->conn->quote($args['atType']); 
        } else {
	    if(isset($args['RATypes'])) {
                 if(is_array($args['RATypes'])) {
                     $sep = '';
                     $inValues = '';
                     foreach($args['RATypes'] as $rKey => $rValue) {
                         $inValues = $inValues . $sep . "'" . $rValue['Name'] . "'";
                         $sep = ',';
                     }
                     
                     if($inValues != '') {
                         $args['where'] .= ' AND job.RAType IN (' . $inValues . ')'; 
                     } else {
                         $args['where'] .= " AND job.JobID='0' ";
                     }
                     
                 }     
            } else {
                $args['where'] .= ' AND job.RAType IS NOT NULL'; 
            }
        }
        
        $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('raJobs', 'ra');
        
	$brandID = $this->controller->user->DefaultBrandID;
	
	/*
	$q = "	SELECT  RAStatusCode

		FROM    ra_status

		WHERE   CASE
			    WHEN (  SELECT  COUNT(*)
				    FROM    ra_status
				    WHERE   BrandID = $brandID AND Status = 'Active'
				 ) > 0

			    THEN BrandID = $brandID

			    ELSE BrandID = 1000
			END	
	    ";	
	*/
	
	$q = "	SELECT  RAStatusCode

		FROM    ra_status

		WHERE   CASE
			    WHEN @stat > 0

			    THEN BrandID = $brandID

			    ELSE BrandID = 1000
			END
	     ";
	
        //if(count($UserStatusPreferences)) {
            $sIDStr = '';
            $sep = '';
            
            foreach($UserStatusPreferences AS $usp) {
                $sIDStr = $sIDStr . $sep . $usp['StatusID'];
                $sep = ',';
            }
            //$args['where'] .= ' AND rs.RAStatusID IN (' . $sIDStr . ')';
            
            if($sIDStr)
            {    
                $args['where'] .= " AND rs.RAStatusCode IN (SELECT RAStatusCode FROM ra_status WHERE RAStatusID IN ($sIDStr))";
            }
            else
            {
                $args['where'] .= " 
				AND rs.RAStatusCode IN (
				    SELECT	ra_status.RAStatusCode
				    FROM	status_preference
				    LEFT JOIN	ra_status ON status_preference.StatusID = ra_status.RAStatusID
				    WHERE	status_preference.UserID = :userID
				)				
				AND rs.RAStatusCode IN ($q)";
            }
            
        //}

      
        //RA Status Type filter
	//if(isset($args['sType']) && $args['sType']) {
	//     $args['where'] .= ' AND t1.RAStatusID='.$this->conn->quote($args['sType']); 
	//}
       
	//echo("<pre>"); print_r($args); echo("</pre>");
	//echo($args["where"]);
	
        $sql = "SELECT	    CASE
				WHEN (
					@stat := (
						    SELECT  COUNT(*)
						    FROM    ra_status
						    WHERE   BrandID = $brandID AND Status = 'Active'
						 )
				     ) > 0
					 
				THEN (
					SELECT 	ra_status.RAStatusName
					FROM	ra_status
					WHERE	ra_status.BrandID = $brandID AND
						ra_status.RAStatusCode = rs.RAStatusCode
				     )				
					 
				ELSE (
					SELECT 	ra_status.RAStatusName
					FROM	ra_status
					WHERE	ra_status.BrandID = 1000 AND
						ra_status.RAStatusID = rs.RAStatusCode
				     )
			    END AS RAStatusName, 
			    
			    count(*) AS JobCount, 
			    job.RAStatusID, 
			    rs.Colour 
			    
                FROM	    job
		
                LEFT JOIN   ra_status AS rs ON job.RAStatusID = rs.RAStatusID
                LEFT JOIN   status AS js ON job.StatusID = js.StatusID
		
                WHERE	    " . $args['where'] . "
		    
		GROUP BY    rs.RAStatusCode
		
                ORDER BY    rs.ListOrder";
       
	//$this->controller->log("111111111"); 
	//$this->controller->log(var_export($sql, true)); 

	$values = ["userID" => $this->controller->user->UserID];
	
        //$data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        $data = $this->Query($this->conn, $sql, $values, PDO::FETCH_NUM);
        
	//$this->controller->log(var_export($data, true));
	//$this->controller->log("sssssssssss");
        
        if(count($data) == 0) {
           $data[] = ['', 0, 0, 'ffffff'];
	}
        
	//$this->controller->log(var_export($data, true));
        
        return $data;
	
    }
    
    
    
    public function fetchAllOverdueStats( $args ) {
        
        $sql = null;
        
	/*
        $GeneralDefaultModel = $this->controller->loadModel('GeneralDefault');       
        $JobTurnaroundTime   = $GeneralDefaultModel->getValue( $this->controller->user->DefaultBrandID, 1); //Here 1 is IDNo for the Turnaround Time in general defualt table.        
        if(!$JobTurnaroundTime)
        {
            $JobTurnaroundTime = 14;//As Neil suggested.
        }        
	*/

	/*
	$args['where'] = 't1.ClosedDate IS NULL AND t1.DateBooked < DATE_SUB(NOW(), INTERVAL '.$JobTurnaroundTime.' DAY)  ' 
		       . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');
        */
	
        
        if($args['tatType']=='s')
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND statustat(t1.JobID)>=0 ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            }
        }   
        else
        {
            if(isset($args['ojBy']) && $args['ojBy']=='sp')
            {
                 $args['where'] = 't1.ServiceProviderDespatchDate IS NULL AND t1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');  
            }
            else
            {
                $args['where'] = 't1.ClosedDate IS NULL AND DATEDIFF(CURRENT_DATE, t1.DateBooked) > turnaround(t1.JobID) ' 
                           . $this->UserJobFilter('t1') . $this->ActiveJobFilter('js');      
            }
        }
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                if($args['tatType']=='s')
                {
                    $args['where'] .= " AND statustat(t1.JobID)<".$args['btnValue']." ";
                }   
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<".$args['btnValue']." ";
                }
                
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                
                 if($args['tatType']=='s')
                 {
                     $args['where'] .= " AND statustat(t1.JobID)>=".$args['btnValue']." AND statustat(t1.JobID)<=".$args['btnValue2']." ";
                 }   
                 else
                 {
                     $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>=".$args['btnValue']." AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))<=".$args['btnValue2']." ";
                 }
                
                
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                if($args['tatType']=='s')
                {
                    $args['where'] .= " AND statustat(t1.JobID)>".$args['btnValue']." ";
                }   
                else
                {
                    $args['where'] .= " AND (DATEDIFF(CURRENT_DATE, t1.DateBooked)-turnaround(t1.JobID))>".$args['btnValue']." ";
                }
                    
            }
        }
        
        
        if(isset($args['rcDate']) && $args['rcDate']) {
             
           $args['where'] .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
           
        }
        
	
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND t4.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
         //skillset Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND t10.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
        
         // Branch Filter
        if (isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID='.$this->conn->quote( $args['branch'] ); 
        }
        
        // Network Filter
        if (isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID='.$this->conn->quote( $args['network'] ); 
        }
        
         // Manufacturer Filter
        if (isset($args['manufacturer']) && $args['manufacturer']) {
           $args['where'] .= ' AND t1.ManufacturerID='.$this->conn->quote( $args['manufacturer'] ); 
        }
        
        
         // Service Provider Filter
        if (isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID='.$this->conn->quote( $args['serviceProvider'] ); 
        }
        

        // Client Filter
        if (isset($args['client']) && $args['client']) {
           $args['where'] .= ' AND t1.ClientID='.$this->conn->quote( $args['client'] ); 
        }
        
        // Brand  Filter
        if (isset($args['brand']) && $args['brand']) {
           $args['where'] .=  ' AND t8.BrandID='.$this->conn->quote( $args['brand'] ); 
        }
        
        // Status  Filter
        if (isset($args['sType']) && $args['sType']) {
           $args['where'] .=  ' AND t1.StatusID='.$this->conn->quote( $args['sType'] ); 
        }
        
        
        $SystemStatusesModel   = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('overdueJobs', 'js');
        
        if(count($UserStatusPreferences))
        {
            $sIDStr = '';
            $sep    = '';
            
            foreach($UserStatusPreferences AS $usp)
            {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep    = ',';
            }
            $args['where'] .= ' AND js.StatusID IN ('.$sIDStr.')';
        }
        
        
        if (isset($args['scType']) && $args['scType']) {
            $sql = 'SELECT t2.CompanyName, count(*) AS JobCount, t1.ServiceProviderID 
                    FROM job AS t1
                    LEFT JOIN service_provider AS t2 ON t1.ServiceProviderID=t2.ServiceProviderID
                    LEFT JOIN status AS js ON t1.StatusID=js.StatusID ';
            
                    if (isset($args['brand']) && $args['brand'])
                    {
                        $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
                    }
                    
                    $sql_ut = "";
                    if (isset($args['unitType']) && $args['unitType'])
                    {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';

                    }

                    if(isset($args['skillSet']) && $args['skillSet']) {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
                    }
            
            
            $sql = $sql.$sql_ut.'
                WHERE ' . $args['where']
             . ' GROUP BY t1.ServiceProviderID';
        }
        else //if (isset($args['sType'])) 
        {
                $sql = 'SELECT js.StatusName, count(*) AS JobCount, t1.StatusID, js.Colour 
                    FROM job AS t1
                    LEFT JOIN status AS js ON t1.StatusID=js.StatusID ';
                
                if (isset($args['brand']) && $args['brand'])
                {
                    $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
                }

                $sql_ut = "";
                if (isset($args['unitType']) && $args['unitType'])
                {
                    $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';

                }

                if(isset($args['skillSet']) && $args['skillSet']) {
                    $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
                }
                
                $sql = $sql.$sql_ut.' WHERE ' . $args['where']
                 . ' GROUP BY t1.StatusID
                     ORDER BY js.StatusName';
        }
        
//        echo $sql."<BR>";
        
        $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);        
        //$this->controller->log(var_export($data,true));
        
        if (count($data) == 0)
           $data[] = array('', 0, 0, 'ffffff');
        return $data;
    }
    

    
    
    
    
    public function bookingPerformanceStats( $args, $UserStatusPreferencesList = NULL ) {
        
        $sql = null;
        $SkylineModel = $this->controller->loadModel('Skyline');
        $from_date = false;
        $to_date  = false;
        
		$ActiveJobFilter = $this->ActiveJobFilter('js');
       
       /*
        * Added $UserStatusPreferencesList to execute the query 
        * @Author Soubhik
        */
       if($UserStatusPreferencesList != NULL){
            $ActiveJobFilter = 'AND t1.StatusID IN ('.rtrim($UserStatusPreferencesList,",").')';
            if(isset($args['displayCancelled'])){
                if($args['displayCancelled']=='cancelled')
                {
                    $sqlCancel = "SELECT	StatusID 
                            FROM	status 
                            WHERE	StatusName='29 JOB CANCELLED' OR StatusName = '29A JOB CANCELLED - NO CONTACT'
                           ";
                    $selectQuery = $this->conn->prepare($sqlCancel, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                    $selectQuery->execute();

                    $dataCancel = $selectQuery->fetchAll();
                    $UserStatusPreferencesCancelList = "";
                    foreach($dataCancel as $usf)
                    {
                        $UserStatusPreferencesCancelList .=  $usf['StatusID'].",";
                    }
                    $ActiveJobFilter = 'AND t1.StatusID IN ('.rtrim($UserStatusPreferencesList.$UserStatusPreferencesCancelList, ",").')';
                }
            }
       }
        /* End of Code*/
        if($args['NetworkID']=='-1')
        {
            $args['NetworkID'] = false;
        }    

        if($args['ClientID']=='-1')
        {
            $args['ClientID'] = false;
        }

        if($args['BranchID']=='-1')
        {
            $args['BranchID'] = false;
        }

        if($args['BrandID']=='-1')
        {
            $args['BrandID'] = false;
        }
        
        

        if($args['NetworkID'] && $args['ClientID'] && $args['BranchID'] && $args['BrandID'])
        {
            $tables = 'job AS t1   LEFT JOIN status AS js ON t1.StatusID = js.StatusID 
                                        LEFT JOIN brand_branch AS t17		ON t1.BranchID = t17.BranchID
                                        LEFT JOIN brand AS t18 ON t17.BrandID = t18.BrandID';
            
            $args['where'] = "t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] )." AND t18.BrandID=".$this->conn->quote( $args['BrandID'] ).$ActiveJobFilter;
            
        }
        else if($args['NetworkID'] && $args['ClientID'] && $args['BranchID'])
        {
            $tables = 'job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID';
            $args['where'] = "t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] )." AND t1.BranchID=".$this->conn->quote( $args['BranchID'] ).$ActiveJobFilter;
        }
        else if($args['NetworkID'] && $args['ClientID'])
        {
            $tables = 'job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID';
            $args['where'] = "t1.NetworkID=".$this->conn->quote( $args['NetworkID'] )." AND t1.ClientID=".$this->conn->quote( $args['ClientID'] ).$ActiveJobFilter;
        }
        else if($args['NetworkID'])
        {
            $tables = 'job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID';
            $args['where'] = "t1.NetworkID=".$this->conn->quote( $args['NetworkID'] ).$ActiveJobFilter;
        }
        else
        {
            
            $tables = 'job AS t1 LEFT JOIN status AS js ON t1.StatusID = js.StatusID';
            
            $args['where'] = 't1.JobID IS NOT NULL '.$ActiveJobFilter;
            
        }
        
        $sql = "SELECT t1.JobID, count(*) AS JobCount, DATE_FORMAT(t1.DateBooked, '%d/%m/%Y') AS DateBooked
            FROM 

            ".$tables;
        
        
        
        
        if($args['cvt']=='mtd')
        {    
            $from_date = date("Y-m-01");
            $to_date   = date("Y-m-d"); 
        }
        else if($args['cvt']=='lm')
        {    
            $from_date = date("Y-m-01", strtotime("-1 month") ) ;
            $to_date   = date("Y-m-t", strtotime("-1 month") ) ;
        }
        else if($args['cvt']=='ytd')
        {    
            $from_date = date("Y-01-01");
            $to_date   = date("Y-m-d"); 
        }
        else if($args['cvt']=='dr')
        {    
            $fromArray = explode('/', $args['date_from']);
            $toArray   = explode('/', $args['date_to']);
            
            
            $from_date = $fromArray[2].'-'.$fromArray[1].'-'.$fromArray[0];
            $to_date   = $toArray[2].'-'.$toArray[1].'-'.$toArray[0];
        }
        
        
        if($from_date && $to_date)
        {    
            if($args['cdt']=="w")
            {
                $minWeekNum = date("W", strtotime($from_date));
                $maxWeekNum = date("W", strtotime($to_date));
                $data = array();
                
                $cnt = 0;
                for($w=$minWeekNum;$w<=$maxWeekNum;$w++)
                {
                    $nodays = $cnt*7;
                    $cnt++;
                    
                    
                    $weekDate  = date("Y-m-d", strtotime("+".$nodays." day", strtotime($from_date)));
                    $weekRange = $SkylineModel->rangeWeek($weekDate);
                    
                    if($weekRange['start']<$from_date)
                    {
                        $weekRange['start'] = $from_date;
                    }
                    if($weekRange['end']>$to_date)
                    {
                        $weekRange['end'] = $to_date;
                    }
                    
                    
                    
                    $args['where1'] = "t1.DateBooked>='".$weekRange['start']."' AND t1.DateBooked<='".$weekRange['end']."'  AND ".$args['where'];
                    $sql1  = $sql.' WHERE ' . $args['where1'];
                    $data1 = $this->Query($this->conn, $sql1, null, PDO::FETCH_NUM); 
                    
                    if(is_array($data1))
                    {    
                        foreach($data1 as $dIndex=>$dData)
                        {
                            if($data1[$dIndex][1])
                            {
                                $data1[$dIndex][2] = date('d/m/Y', strtotime($weekRange['start'])).' - '.date('d/m/Y', strtotime($weekRange['end']));
                                $data1[$dIndex][0] = $data1[$dIndex][2].' ('.$data1[$dIndex][1].') '; 

                                $data[] = $data1[$dIndex];
                            }    
                        }    
                    }
                }
                
            }
            else if($args['cdt']=="m")
            {
                $d1 = new DateTime($from_date);
                $d2 = new DateTime($to_date);
                $diffMonths = $d1->diff($d2)->m;
                
                $data = array();
                
                $m = 0;
                $breakLoop = false;
                while(1)
                {
                    $MonthStart = date("Y-m-01", strtotime("+".$m." month", strtotime($from_date)) );
                    $MonthEnd = date("Y-m-t", strtotime("+".$m." month", strtotime($from_date)) );
                    
                    if($MonthStart<$from_date)
                    {
                       $MonthStart = $from_date;
                    } 
                    
                    
                    if($MonthEnd>$to_date)
                    {
                       $MonthEnd = $to_date;
                       $breakLoop = true;
                    } 
                    
                    $args['where1'] = "t1.DateBooked>='".$MonthStart."' AND t1.DateBooked<='".$MonthEnd."' AND ".$args['where'];
                    $sql1 = $sql.' WHERE ' . $args['where1'];
                    $data1 = $this->Query($this->conn, $sql1, null, PDO::FETCH_NUM);   

                    if(is_array($data1))
                    {    
                        foreach($data1 as $dIndex=>$dData)
                        {
                            if($data1[$dIndex][1])
                            {    
                                $data1[$dIndex][2] = date("d/m/Y", strtotime($MonthStart)).' - '.date('d/m/Y', strtotime($MonthEnd));
                                $data1[$dIndex][0] = $data1[$dIndex][2].' ('.$data1[$dIndex][1].') '; 
                                
                                $data[] = $data1[$dIndex];
                            }
                        }    
                    }
                    
                    if($breakLoop)
                    {
                        break;
                    }    
                    $m++;
                }
                
                
            }
            else
            {
                
                $args['where'] = "t1.DateBooked>='".$from_date."' AND t1.DateBooked<='".$to_date."' AND ".$args['where'].' GROUP BY t1.DateBooked';
                $sql = $sql.' WHERE ' . $args['where'];
                $this->controller->log(var_export($sql,true));
                $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);   
                
                if(is_array($data))
                {    
                    foreach($data as $dIndex=>$dData)
                    {
                        $data[$dIndex][0] = $data[$dIndex][2].' ('.$data[$dIndex][1].') '; 
                    }    
                }
               
            }
        
         }
       // echo $sql;
        //$this->controller->log(var_export($sql,true));
        
        if (count($data) == 0)
           $data[] = array('', 0, 0);
        return $data;
    }
    
    
    
    
    
    
    
    
    
    /**
     * 
     * getAppointments
     * @param string $JobID
     * @return Array
     ********************************************/
    public function getAppointments( $JobID ) {
        
        /*return array(
            array('10/02/12', '10:34AM', 'FIRST CALL', 'BW'),
            array('10/02/13', '10:50AM', 'FIRST CALL', 'BW'),
            array('10/02/14', '11:34AM', 'FIRST CALL', 'BW'),
            array('10/02/12', '10:34AM', 'FIRST CALL', 'BW'),
            array('10/02/13', '10:50AM', 'FIRST CALL', 'BW'),
            array('10/02/14', '11:34AM', 'FIRST CALL', 'BW'),
            array('10/02/12', '10:34AM', 'FIRST CALL', 'BW'),
            array('10/02/13', '10:50AM', 'FIRST CALL', 'BW'),
            array('10/02/14', '11:34AM', 'FIRST CALL', 'BW'),
            array('10/02/12', '10:34AM', 'FIRST CALL', 'BW')
        );*/
        
        $sql = 'select AppointmentDate, AppointmentTime, AppointmentType, EngineerCode from appointment where JobID=:JobID';
        $args = array( 'JobID' => $JobID );
        $this->appointments =  $this->Query($this->conn, $sql, $args);
        return $this->appointments;
        
    }


    
    /**
     * 
     * getProductDetails
     * @return Array
     ********************************************/
    public function getProductDetails( $JobID ) {
       /* 
        $sql = 'SELECT	    t2.ProductNo AS StockCode,
			    t3.ModelDescription AS Description,
			    t4.UnitTypeName AS ProductType,
			    t2.UnitTypeID,
			    t1.ModelID,
			    t3.ModelNumber,
			    t5.ManufacturerName AS Manufacturer,
			    t1.ManufacturerID,
			    t1.ServiceBaseManufacturer,
			    t1.RepairType,
			    t1.ProductLocation AS ItemLocation,
			    t1.ServiceTypeID,
			    t6.ServiceTypeName
		FROM	    job as t1
		LEFT JOIN   product	    AS t2 ON t1.ProductID = t2.ProductID
		LEFT JOIN   model	    AS t3 ON t2.ModelID = t3.ModelID
		LEFT JOIN   unit_type	    AS t4 ON t2.UnitTypeID = t4.UnitTypeID
		LEFT JOIN   manufacturer    AS t5 ON t1.ManufacturerID = t5.ManufacturerID
		LEFT JOIN   service_type    AS t6 ON t1.ServiceTypeID = t6.ServiceTypeID
		WHERE	    t1.JobID=:JobID ' . $this->UserJobFilter('t1');
        */
	
        $sql = 'SELECT	    t2.ProductNo AS StockCode,
			    t3.ModelDescription AS Description,
			    t4.UnitTypeName AS ProductType,
			    t2.UnitTypeID,
			    t1.ModelID,
			    t3.ModelNumber,
			    t5.ManufacturerName AS Manufacturer,
			    t1.ManufacturerID,
			    t1.ServiceBaseManufacturer,
			    t1.ServiceBaseModel,
			    t1.RepairType,
			    t1.ProductLocation AS ItemLocation,
			    t1.ServiceTypeID,
			    IF (ISNULL(stm.`ServiceTypeName`), t6.ServiceTypeName, stm.`ServiceTypeName`) AS ServiceTypeName,
                            stm.`ServiceTypeName` AS Mask
                            
		FROM	    job as t1
		LEFT JOIN   product	    AS t2 ON t1.ProductID = t2.ProductID
		LEFT JOIN   model	    AS t3 ON t1.ModelID = t3.ModelID
		LEFT JOIN   unit_type	    AS t4 ON t3.UnitTypeID = t4.UnitTypeID
		LEFT JOIN   manufacturer    AS t5 ON t1.ManufacturerID = t5.ManufacturerID
		LEFT JOIN   service_type    AS t6 ON t1.ServiceTypeID = t6.ServiceTypeID
                LEFT JOIN   service_type_alias    AS sta ON t6.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                LEFT JOIN   service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                
		WHERE	    t1.JobID=:JobID ' . $this->UserJobFilter('t1');
	
	
        $args = array( 'JobID' => $JobID );
        
        $this->product_details = $this->Query($this->conn, $sql, $args);  
        
        return $this->product_details;
        
    }
    
    
    
    /**
     * 
     * getAdditionalInformation
     * @global array $current_record
     * @return string
     ********************************************/
    public function getFullAddress( $JobID, $Delivery=false ){    

        if($this->current_record == null || $this->current_record['JobID'] != $JobID )
            $this->fetch( $JobID );
        
        if ($Delivery) {
         $address_fields = array(
                'ColAddCompanyName',
                'ColAddBuildingNameNumber',
                'ColAddStreet',
                'ColAddLocalArea',
                'ColAddTownCity',
                'ColAddPostcode');           
        } else {
         $address_fields = array(
                'BuildingNameNumber',
                'Street',
                'LocalArea',
                'TownCity',
                'CountyName',                
                'PostalCode',
                'CountryName');           
        }
        
         $address = '';
            
         foreach($address_fields as $f){
            $value = trim($this->current_record[$f]);
            if ($value != '') {
                /*if ($address != '')
                {
                    if($f=='PostalCode' || $f=='ColAddPostcode')
                    {
                         $address .= '. ';
                    }
                    else
                    {
                         $address .= ', ';
                    }    
                   
                }*/
                //$this->controller->log($f . '=' . var_export($value,true));
                if ($address != '') $address .= ', ';
                $address .= $value;
            }
         }
            
         return $address;

    }
    
    
    
    /**
     * 
     * getAdditionalInformation
     * @param string $JobID
     * @return Array
     ********************************************/
    public function getAdditionalInformation( $JobID ) {
        
        if($this->current_record == null || $this->current_record['JobID'] != $JobID )
            $this->fetch( $JobID );
                
        return array(
            'OriginalRetailer' => $this->current_record['OriginalRetailer'],
            'PurchaseDate' => $this->current_record['DateOfPurchase'],
            'SalesReceiptNo' => 0,   // NW doesn't know what SalesReceiptNo is so return zero for now
            'Insurer' => $this->current_record['Insurer'],
            'PolicyNo' => $this->current_record['PolicyNo'],
            'AuthorisationNo' => $this->current_record['AuthorisationNo'],
            'BrandReferralNo' => $this->current_record['AgentRefNo'],
            'parentJob' => $this->current_record['FromJobId'],
            'childJob' => $this->current_record['ToJobId']

        );
    }    
    
    
    
    /**
     * 
     * getIssueReport
     * @param string $JobID
     * @return Array
     ********************************************/
    public function getIssueReport( $JobID ) {
        
        if($this->current_record == null || $this->current_record['JobID'] != $JobID )
            $this->fetch( $JobID );
                
        return array(
            'IssueReport' => $this->current_record['ReportedFault']
        );
    }  

    
    
    public function getInvoiceCosts( $JobID ){
        
        $sql = "select
                ChargeablePartsCost as Parts,
                ChargeableLabourCost as Labour,
                ChargeableDeliveryCost as Carriage,
                ChargeableSubTotal as TotalNet,
                ChargeableVATCost as VAT,
                20.0 as VATRate,
                ChargeableTotalCost as Gross
                from job where JobID=:JobID";
        
        $params = array( 'JobID' => $JobID );
        $data = $this->Query($this->conn, $sql, $params);      
        return $data[0];
        
    } 
    

    
    public function getContactHistory( $JobID ){
        
        /*return array(
            array('10/01/12', '10:32', 'BOB WILLCOX', 'Note', 'PHOENED CUSTOMER', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit lobortis eleifend. Nunc id dui sit amet erat sollicitudin consectetur. Morbi ut luctus nibh. Donec sed pharetra dolor. Vestibulum egestas lacinia ante, non placerat risus placerat non. Quisque ornare nulla vitae urna sollicitudin non hendrerit neque pellentesque.'),
            array('10/01/12', '10:32', 'BOB WILLCOX', 'Email', 'UNit Awaiting Collection', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit lobortis eleifend. Nunc id dui sit amet erat sollicitudin consectetur. Morbi ut luctus nibh. Donec sed pharetra dolor. Vestibulum egestas lacinia ante, non placerat risus placerat non. Quisque ornare nulla vitae urna sollicitudin non hendrerit neque pellentesque.'),
            array('10/01/12', '10:32', 'BOB WILLCOX', 'SMS', 'PHOENED CUSTOMER', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit lobortis eleifend. Nunc id dui sit amet erat sollicitudin consectetur. Morbi ut luctus nibh. Donec sed pharetra dolor. Vestibulum egestas lacinia ante, non placerat risus placerat non. Quisque ornare nulla vitae urna sollicitudin non hendrerit neque pellentesque.')
        );*/
        
        $sql = 'SELECT 
                t1.ContactDate,
                t1.ContactTime,
                t1.UserCode,
                t3.Action,
                t1.Subject,
                t1.Note
                FROM contact_history as t1
                LEFT JOIN contact_history_action  AS t3 ON t1.ContactHistoryActionID = t3.ContactHistoryActionID
                WHERE t1.JobID=:JobID';
        
        $args = array( 'JobID' => $JobID );
        $this->contact_history = $this->Query($this->conn, $sql, $args); 
        return $this->contact_history;
    }

    
    
    public function getPartsUsed( $JobID ) {
        
        /*return array(
            array('LCD SCREEN', '56451553 5', '1', 'CPC', '56451553 5', '10/02/12', '11/02/12', '12/02/12', 'Delivered')
        );*/
        
        $sql = 'SELECT * from part WHERE JobID=:JobID';
        $args = array( 'JobID' => $JobID );
        $this->parts_used = $this->Query($this->conn, $sql, $args); 
        return $this->parts_used;
    }
    
    
    
    public function getPartsUsedDT($JobID,$chargeType) {

        
	$q = "	SELECT	PartID		    AS '0',
			PartDescription	    AS '1',
			PartNo		    AS '2',
			Quantity	    AS '3',
			sps.CompanyName	    AS '4',
			OrderNo		    AS '5',
			CASE
			    WHEN OrderDate
			    THEN DATE_FORMAT(OrderDate,'%d/%m/%y')
			    ELSE NULL
			END		    AS '6',
			CASE
			    WHEN DueDate
			    THEN DATE_FORMAT(DueDate,'%d/%m/%y')
			    ELSE NULL
			END		    AS '7',
			CASE
			    WHEN ReceivedDate
			    THEN DATE_FORMAT(ReceivedDate,'%d/%m/%y')
			    ELSE NULL
			END		    AS '8',
                          ChargeType as '9',
                        spsc.Colour as '10',
			ps.PartStatusName AS '11'
                      
                       
			
		FROM	part p
		left join service_provider_supplier sps on sps.ServiceProviderSupplierID= p.SupplierID
		left join part_status ps on ps.PartStatusID= p.PartStatusID
                left join sp_part_stock_template spst on spst.SpPartStockTemplateID=p.SpPartStockTemplateID
                left join sp_part_status_colour spsc on spsc.PartStatusID=ps.PartStatusID
		WHERE	JobID = $JobID
                    
                ORDER BY PartID DESC
	     ";
	
//        $this->log('ddddddddddddddddddddddd');
//        $this->log($q);
        
	$result = $this->query($this->conn, $q);
        
       $this->log($result,"cofee");
        
        
	return ["aaData" => $result];
	
	/*
	//$this->debug = true;
        $args = $_POST;
        $columns = [
	    "PartID",
	    "PartDescription", 
	    "PartNo", 
	    "Quantity",
	    "Supplier", 
	    "OrderNo",
	    ['OrderDate', "DATE_FORMAT(OrderDate,'%d/%m/%y')"], 
	    ['DueDate', "DATE_FORMAT(DueDate,'%d/%m/%y') AS ExpectedDate"], 
	    ['ReceivedDate', "DATE_FORMAT(ReceivedDate,'%d/%m/%y')"],
	    "PartStatus"
	];
        $tables = 'part';
        //$args['where'] = " JobID = $JobID AND (IsAdjustment!='Y' OR IsAdjustment is null)";
        $args['where'] = " JobID = $JobID";
        $args['DISTINCT'] = true; // KLUGE alert!!!! this is to remove invalid duplicate rows in the select.
                                  // In case you are wondering, there is no attempt to resolve the cause of 
                                  // the duplicate rows in the RMA Tracker API.
        
        $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);
        
        return($data);
	*/
    }
    
    
    
    public function getNumPartsUsed($JobID = null) { 
        
	$q = "SELECT COUNT(*) AS `count` FROM part WHERE JobID = :JobID";
        $args = ['JobID' => $JobID];
        $result = $this->query($this->conn, $q, $args); 
        return $result[0]["count"];
        
    }

    
    
    public function getNumPartsUsedNoAdjustment($JobID = null) { 
        
	$q = "SELECT COUNT(*) AS `count` FROM part WHERE UCASE(`PartDescription`) != 'ADJUSTMENT' AND JobID = :JobID";
        $args = ['JobID' => $JobID];
        $result = $this->query($this->conn, $q, $args); 
        return $result[0]["count"];
        
    }
    
    
    
    public function getNumContactHistory( $JobID = null ) {  
        
        /* 20/11/2012 Temporary fix to eliminate duplicate rows caused by error in API spec */
        
        //$sql = "select count(*) as num_rows from contact_history ch where ch.JobID=:JobID";
        //$args = array( 'JobID' => $JobID );
        //$result = $this->Query($this->conn, $sql, $args); 
        //return $result[0]['num_rows'];
        
        $sql = "SELECT	    COUNT(*) AS num_rows 
		FROM	    contact_history AS ch
		LEFT JOIN   contact_history_action AS ca ON ch.ContactHistoryActionID = ca.ContactHistoryActionID
		WHERE	    JobID = :JobID AND ca.Source != 'Remote Engineer'
		GROUP BY    ch.ContactDate, ch.ContactTime, ch.UserCode, ch.ContactHistoryActionID, ch.Subject, ch.Note
	       ";
	
        $args = array( 'JobID' => $JobID );
        $result = $this->Query($this->conn, $sql, $args); 
        return count($result);
    }
    
    
    
    public function getNumStatusChanges( $JobID = null ) { 
        
        //$sql = "select count(*) as num_rows from status_history sh where sh.JobID=:JobID";
        //$args = array( 'JobID' => $JobID );
        //$result = $this->Query($this->conn, $sql, $args); 
        //return $result[0]['num_rows'];
                       
        $sql = "select count(*) as num_rows from status_history where JobID=:JobID group by DATE_FORMAT(Date, '%d/%m/%y'), TIME_FORMAT(Date, '%H:%i'), UserID, StatusID, AdditionalInformation";
        $args = array( 'JobID' => $JobID );
        $result = $this->Query($this->conn, $sql, $args); 
        return count($result);                
        
    }
    
    
    
    /**
     * getServiceCentre
     * 
     * Returns the service centre ID of a job.
     * 
     * @param $jId  The ID of the job
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getServiceCentre($jId) {
        $sql = "
                SELECT
			`ServiceProviderID`
		FROM
			`job`
		WHERE
			`JobID` = '$jId'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['ServiceProviderID']);                            /* Job exists os return Service Centre ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * getServiceCentreName
     * 
     * Returns the service centre of a job.
     * 
     * @param $jId  The ID of the job
     * 
     * @return string  The Service Centre name
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getServiceCentreName($jId) {
        $sql = "
                SELECT
			`CompanyName`
		FROM
			`job` j LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
			j.`JobID` = ".$jId;
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['CompanyName']);                                  /* Job exists os return Service Centre ID */
        } else {
            return('None assigned');                                            /* Not found return null */
        }
    }
    
    
    
    /**
     * 
     * A sample function getServiceCentreContacts
     * @param int $ServiceCentreID
     * @return array
     ********************************************/
    public function getServiceCentreContacts( $ServiceCentreID ) {
        
        $sql = "SELECT * FROM user WHERE ServiceCentreID = :ServiceCentreID"; 
        $args = array( 'ServiceCentreID' => $ServiceCentreID );
        $this->service_centre_contacts = $this->Query($this->conn, $sql, $args); 
        return $this->service_centre_contacts;
    }    
    
    
    
    /**
     * getStatusChanges based on the audit db table
     * 
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     ********************************************/    
    public function getStatusChanges( $JobID ){
               
        $sql = "
                SELECT DATE_FORMAT(t1.Date, '%d/%m/%y') as Date, TIME_FORMAT(t1.Date, '%h:%i') AS Time, t3.UserName, t2.StatusName as Status, t1.AdditionalInformation  
                FROM status_history AS t1
                LEFT JOIN status AS t2 ON t1.StatusID=t2.StatusID
                LEFT JOIN user AS t3 ON t1.UserID=t3.UserID
                WHERE t1.JobID = :JobID
                ORDER BY t1.Date"; 
        $args = array( 'JobID' => $JobID );
        $this->status_changes = $this->Query($this->conn, $sql, $args); 
        return $this->status_changes;
    }    

    
    
    public function getStatusChangesDT($JobID) {
        
        $args = $_POST;
//changed display date format by thirumal..%d/%m/%y replace with %d/%/m%Y
        $columns = [ 
	    ['t1.Date', "DATE_FORMAT(t1.Date, '%d/%m/%Y')"], 
	    "TIME_FORMAT(t1.Date, '%H:%i')", 
	    "t1.UserCode", 
	    "t2.StatusName AS Status", 
	    "t1.AdditionalInformation"
	];
	
        $args['where'] = "t1.JobID = " . $this->conn->quote( $JobID );
        $args['DISTINCT'] = true; // KLUGE alert!!!! this is to remove invalid duplicate rows in the select.
                                  // In case you are wondering, there is no attempt to resolve the cause of 
                                  // the duplicate rows in the RMA Tracker API.
        
        $tables = " status_history AS t1 
		    LEFT JOIN status AS t2 ON t1.StatusID=t2.StatusID 
		    LEFT JOIN user AS t3 ON t1.UserID=t3.UserID
		  ";
        
        $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);

        return  $data;

    }
    
    

    /**
     * 
     * 
     * @param array $args
     * @return array
     *
     * @author  Simon Tsang <tsang.kuen@gmail.com>
     * 
     * note: audit history
     * 
     ********************************************/    
    public function getStatusHistory( $JobID ){
//        $select = "*, DATE_FORMAT(Date, '%d/%m/%y') AS Date, DATE_FORMAT(Date, '%H:%i') AS Time";
//        $data = $this->collect('status_history AS t1 LEFT JOIN status AS t2 ON t1.StatusID = t2.StatusID', array('JobID' => '34589120'), $select, 'Date DESC');
        //$select = "*, DATE_FORMAT(CreatedDate, '%d/%m/%y') AS Date, DATE_FORMAT(CreatedDate, '%H:%i') AS Time";
        //$data = $this->collect('audit AS t1 LEFT JOIN audit_trail_action AS t2 ON t1.AuditTrailActionID = t2.AuditTrailActionID', array('JobID' => '34589120'), $select, 'Date DESC');        
        #$this->controller->log('Job->getStatusHistory :'.var_export($data, true));
        
        // TODO:
        // Don't undertsand what this method is return -- so for now return an empty reslt set
        return array();

    }
    
    /*public function PartsUsed(){
        $data = array(
            array('Description'=>'LCD SCREEN', 'PartNo'=>'45628921 1', 'Quantity'=>'1', 'Supplie'=>'CPC',   'OrderNo'=>'', 'OrderDate'=>'12/02/12', 'Status'=>'Delivered')
        );
        
        return $data;
    }
            
    public function ContactHistoryNotes(){
        $data = array(
            array('Date'=>'10/02/12', 'Time'=>'10:32', 'UserName'=>'BOB WILLCOX', 'Type'=>'Note',   'Subject'=>'PHONED CUSTOMER ', 'Note'=>'Phoned the customer to book a Job. Spoke to Mr Berry and advised him that we had an available appointment date on Friday 17th February.  He agreed to this time.'),
            array('Date'=>'10/02/12', 'Time'=>'10:32', 'UserName'=>'BOB WILLCOX', 'Type'=>'Email',  'Subject'=>'AWAITING COLLECTION', 'Note'=>'<a href="#">View full email message</a>'),
            array('Date'=>'10/02/12', 'Time'=>'10:32', 'UserName'=>'BOB WILLCOX', 'Type'=>'SMS',    'Subject'=>'PHONED CUSTOMER ', 'Note'=>'SMS Messaged the customer to book a AFN-Job. Messaged Mr Berry and advised him that we had an available appointment date on Friday 17th February.  He agreed to this time.')
        );
        
        return $data;
    }*/
    
    /**
     * 
     * A sample function docblock
     *
     * @return array
     * @example return array(
     *       'DaysFromBooking' => 2,
     *       'TurnaroundTime' => 10,
     *       'DaysRemaining' => 8,
     *       'TurnaroundTimeType' => '' #-red for over estimated turnaround time
     *   );
     ********************************************/
    public function getTurnaroundTimeline(){
        
        // TODO:
        // Don't understand what this does - leave as is for now
        return array(
            'DaysFromBooking' => 2,
            'TurnaroundTime' => 10,
            'DaysRemaining' => 8,
            'TurnaroundTimeType' => '-red'#-red
        );
        
    }
    
    
    
     /**
     * fetchUnallocatedJobs
     * It fetches unallocated jobs.
     * 
     * @param  array $args
     * @return $data array
     * @author Nageswara Rao Kanteti nag.phpdeveloper@gmail.com	 
     ********************************************/
    public function fetchUnallocatedJobs($args){
        
      
        $columns = array( 't1.JobID', 
                          't3.ClientName', 
                          'b.BranchName',  
                          'cu.ContactLastName', 
                          'cu.PostalCode', 			  
                          'mft.ManufacturerName', 
                          't9.UnitTypeName', 
                          't10.RepairSkillName', 
                          array('t1.DateBooked','DATE_FORMAT (t1.DateBooked, "%d/%m/%Y")') );
        
        $args['where'] = "t1.ClosedDate IS NULL AND t4.StatusName!='29 JOB CANCELLED' AND t1.ServiceProviderID IS NULL ".$this->UserJobFilter('t1');

        if($args['firstArg'])
        {
            $args['where'] = $args['where']." AND t1.NetworkID='".$args['firstArg']."'";
        }           
      
        $tables = 'job AS t1 
            
                        LEFT JOIN client                AS t3  ON t1.ClientID = t3.ClientID
                        LEFT JOIN branch                AS b   ON t1.BranchID = b.BranchID
                        LEFT JOIN customer              AS cu  ON t1.CustomerID = cu.CustomerID
                        LEFT JOIN status                AS t4  ON t1.StatusID = t4.StatusID
                        LEFT JOIN customer_title        AS t7  ON cu.CustomerTitleID = t7.CustomerTitleID
                        LEFT JOIN manufacturer          AS mft ON t1.ManufacturerID = mft.ManufacturerID
                        LEFT JOIN model                 AS t8  ON t1.ModelID = t8.ModelID
                        LEFT JOIN unit_type             AS t9  ON t8.UnitTypeID = t9.UnitTypeID
                        LEFT JOIN repair_skill          AS t10 ON t9.RepairSkillID = t10.RepairSkillID
                        
        ';
        
        //$this->controller->log(var_export($args, true));
        
        $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);
        
        

        return  $data;
        
        
        
    }
    

    
    /**
    * 
    * A sample function docblock
    * @param array $args
    * @return $data array
    * @author Simon Tsang s.tsang@pccsuk.com	 
    ********************************************/
    
    public function search($args) {
                      	
        $this->super_filter = '{}';
        
        $args['jobSearch'] = (isset($args['jobSearch'])) ? $args['jobSearch'] : '';

        $args['batchID'] = (isset($args['batchID'])) ? $args['batchID'] : '';
	
        $this->search_value = $args['jobSearch'];
               
        $columns = [  
	    'DATE_FORMAT(t1.DateBooked,"%d/%m/%y")', 
	    't1.JobID', 
	    't1.ServiceCentreJobNo', 
	    'CONCAT_WS(", ", t2.ContactLastName, CONCAT_WS(" ", t3.Title, t2.ContactFirstName))', 
	    't2.PostalCode',	    	    
	    't1.ServiceBaseUnitType',
	    'sp.CompanyName', 
	    'js.StatusName'
	];
       
        
        if($args['jobSearch']) {
	    
            $searchStrLength = strlen($args['jobSearch']);
            $actualSearchString = false;
            if($searchStrLength >2 && $args['jobSearch'][0]=='*' && $args['jobSearch'][$searchStrLength-1]=='*')
            {
                $actualSearchString = '%'.substr($args['jobSearch'], 1, $searchStrLength-2).'%';
            }
            else if($searchStrLength >1 && $args['jobSearch'][0]=='*')
            {
                $actualSearchString = '%'.substr($args['jobSearch'], 1);
            }
            else if($searchStrLength >1 && $args['jobSearch'][$searchStrLength-1]=='*')
            {
                $actualSearchString = substr($args['jobSearch'], 0, $searchStrLength-1).'%';
            }
            else if($searchStrLength ==1 && $args['jobSearch']=='*')
            {
                $actualSearchString = '%%';
            }  
            
            if($actualSearchString) {
                
		if($actualSearchString=='%%') {
                    //$args['where'] = "(t1.JobID!='0')" . $this->UserJobFilter('t1','brdbrch');
                    $args['where'] = $this->UserJobFilter('t1','brdbrch');
                } else {   
                    
                    $pcTemp = str_replace(" ", "", $actualSearchString);
                
                    $pcLength = strlen($pcTemp);

                    if($pcLength>3)
                    {
                        $pcTemp2  = substr($pcTemp, 0, $pcLength-3)." ".substr($pcTemp, -3);
                        $pcWhere  = " OR t2.PostalCode LIKE ".$this->conn->quote( $pcTemp )." OR t2.PostalCode LIKE ".$this->conn->quote( $pcTemp2 )."  OR t1.ColAddPostcode LIKE ".$this->conn->quote( $pcTemp )." OR t1.ColAddPostcode LIKE ".$this->conn->quote( $pcTemp2 );
                    }
                    else
                    {
                        $pcWhere  = " OR t2.PostalCode LIKE ".$this->conn->quote( $actualSearchString )." OR t1.ColAddPostcode LIKE ".$this->conn->quote( $actualSearchString );
                    }
                    
                    
                    
                    $args['where'] = "(	t1.JobID LIKE ".$this->conn->quote( $actualSearchString ) . 
				     "	OR CAST(t1.ServiceCentreJobNo AS CHAR) LIKE CAST(" . $this->conn->quote( $actualSearchString ) . " AS CHAR)" .
                                     "	OR t1.AgentRefNo LIKE " . $this->conn->quote( $actualSearchString ) . 
                                     "	OR t1.AuthorisationNo LIKE " . $this->conn->quote( $actualSearchString ) .
                                     "	OR CAST(t1.RMANumber AS CHAR) LIKE CAST(" . $this->conn->quote( $actualSearchString ) . " AS CHAR)" .
                                     "	OR t1.PolicyNo LIKE " . $this->conn->quote( $actualSearchString ) .
				     "	OR t1.ImeiNo   LIKE " . $this->conn->quote( $actualSearchString ) .
                            	     $pcWhere. 
				     "	OR t2.ContactLastName LIKE " . $this->conn->quote( $actualSearchString ) . 
				     "	OR t2.ContactFirstName LIKE " . $this->conn->quote( $actualSearchString ).")" . 
                                     
				     $this->UserJobFilter('t1','brdbrch');
                }    
		
            } else {
                
                $actualSearchString = $args['jobSearch'];
                
                $pcTemp = str_replace(" ", "", $actualSearchString);
                
                $pcLength = strlen($pcTemp);

                if($pcLength>3)
                {
                    $pcTemp2  = substr($pcTemp, 0, $pcLength-3)." ".substr($pcTemp, -3);
                    $pcWhere  = " OR t2.PostalCode=".$this->conn->quote( $pcTemp )." OR t2.PostalCode=".$this->conn->quote( $pcTemp2 )." OR t1.ColAddPostcode=".$this->conn->quote( $pcTemp )." OR t1.ColAddPostcode=".$this->conn->quote( $pcTemp2 );
                }
                else
                {
                    $pcWhere  = " OR t2.PostalCode=".$this->conn->quote( $actualSearchString )." OR t1.ColAddPostcode=".$this->conn->quote( $actualSearchString );
                }
                
               
		
		$args['where'] =    "(t1.JobID=".$this->conn->quote( $actualSearchString ) . 
				    " OR CAST(t1.ServiceCentreJobNo AS CHAR) = CAST(" . $this->conn->quote( $actualSearchString ) . " AS CHAR)" .
                                    " OR t1.AgentRefNo=" . $this->conn->quote( $actualSearchString ) . 
                                    " OR t1.AuthorisationNo=" . $this->conn->quote( $actualSearchString ) . 
                                    " OR CAST(t1.RMANumber AS CHAR) = CAST(" . $this->conn->quote( $actualSearchString ) . " AS CHAR)" .
                                    " OR t1.PolicyNo=" . $this->conn->quote( $actualSearchString ) . 
				    " OR t1.ImeiNo=" . $this->conn->quote( $actualSearchString ) . 
				    $pcWhere . 
				    " OR t2.ContactLastName=" . $this->conn->quote( $actualSearchString ) . 
				    " OR t2.ContactFirstName=" . $this->conn->quote( $actualSearchString ).")" . 
                                    $this->UserJobFilter('t1','brdbrch');    
            }
            
            
            //$args['where'] = "t1.JobID LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t1.ServiceCentreJobNo LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t2.PostalCode LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' )." OR t2.ContactLastName LIKE ".$this->conn->quote( '%'.$args['jobSearch'].'%' );
                
        } else  {
             
	    //$args['where'] = "(t1.JobID!='0')".$this->UserJobFilter('t1','brdbrch');
            $args['where'] = $this->UserJobFilter('t1','brdbrch');
	    
        }
        
	/*
        // currently the $columns uses SQL alias but the AJAX JSON doesn't not
        #this code correct this bug
        foreach($columns as $c){
            $fields = explode('.', $c);
            if(isset($fields[1]) && isset($args[$fields[1]])){
                $args['where'] = $c . ' LIKE "%' . $args[$fields[1]] .'%"';
            }
        }*/
        
	if(isset($args["activeFilter"]) && isset($args["filterID"])) {
	    if($args["activeFilter"]!="" && $args["filterID"]!="") {
		switch ($args["activeFilter"]) {
		    case "network" : {
			$args["where"] .= " AND t1.NetworkID=" . $this->conn->quote($args["filterID"]);
			break;
		    }
		    case "client" : {
			$args["where"] .= " AND t1.ClientID=" . $this->conn->quote($args["filterID"]);
			break;
		    }
		    case "branch" : {
			$args["where"] .= " AND t1.BranchID=" . $this->conn->quote($args["filterID"]);
			break;
		    }
		    case "service" : {
			$args["where"] .= " AND t1.ServiceProviderID=" . $this->conn->quote($args["filterID"]);
			break;
		    }
		}
	    }
	}
		
	if($args["batchID"]!="" && $args["batchID"]!="undefined") {
	    $args["where"] .= " AND t1.batchID=" . $this->conn->quote($args["batchID"]);
	}
	
	//$this->controller->log("LINE " . __LINE__ . " WHERE: " . $args["where"]);
	
/*        $tables = 'job AS t1 LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
                  LEFT JOIN customer_title AS t3 ON t3.CustomerTitleID = t2.CustomerTitleID
                  LEFT JOIN status AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN brand_branch AS brdbrch ON t1.`BranchID` = brdbrch.`BranchID`
		  LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
        '; */

	//Merging conflict starts here
	
	/*
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID'; 
	*/
	
        if($this->controller->user->BranchJobSearchScope=="Brand") {    
            $tables = '	job AS t1 
			LEFT JOIN customer AS t2	    ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3	    ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js		    ON js.StatusID = t1.StatusID
			LEFT JOIN brand_branch AS brdbrch   ON t1.`BranchID` = brdbrch.`BranchID`
			LEFT JOIN service_provider AS sp    ON t1.ServiceProviderID=sp.ServiceProviderID
			
			LEFT JOIN product 		    ON t1.ProductID = product.ProductID
			LEFT JOIN model			    ON t1.ModelID = model.ModelID
			LEFT JOIN unit_type		    ON model.UnitTypeID = unit_type.UnitTypeID
			LEFT JOIN manufacturer		    ON t1.ManufacturerID = manufacturer.ManufacturerID
			LEFT JOIN service_type		    ON t1.ServiceTypeID = service_type.ServiceTypeID
			LEFT JOIN customer		    ON t1.CustomerID = customer.CustomerID
			LEFT JOIN customer_title	    ON customer.CustomerTitleID = customer_title.CustomerTitleID
                      ';
        } else {
            $tables = '	job AS t1 
			LEFT JOIN customer AS t2	    ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3	    ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js		    ON js.StatusID = t1.StatusID
			LEFT JOIN service_provider AS sp    ON t1.ServiceProviderID=sp.ServiceProviderID
			
			LEFT JOIN product 		    ON t1.ProductID = product.ProductID
			LEFT JOIN model			    ON t1.ModelID = model.ModelID
			LEFT JOIN unit_type		    ON model.UnitTypeID = unit_type.UnitTypeID
			LEFT JOIN manufacturer		    ON t1.ManufacturerID = manufacturer.ManufacturerID
			LEFT JOIN service_type		    ON t1.ServiceTypeID = service_type.ServiceTypeID
			LEFT JOIN customer		    ON t1.CustomerID = customer.CustomerID
			LEFT JOIN customer_title	    ON customer.CustomerTitleID = customer_title.CustomerTitleID
                      ';
        }
	
	//End
        
      
        if(isset($args['searchTypeCount']) && $args['searchTypeCount'] == 1) {
            
            //$sql  = "SELECT COUNT(*) FROM ".$tables." WHERE ";
            //$sql1 = "SELECT t1.JobID FROM ".$tables." WHERE ";        
            
            //if(isset($args["where"])) {
                //$sql  .= $args["where"];
                //$sql1 .= $args["where"];  
            //} else {
                //$sql  .= "1";
                //$sql1 .= "1"; 
            //}
            
            if(empty($args["where"])) {
                $sql  = "SELECT COUNT(*) FROM job";
                $sql1 = "SELECT t1.JobID FROM " . $tables;              
            } else { 
                if(substr($args['where'], 0, 5) == ' AND ') {
		    $args['where'] = substr($args['where'], 5);
		}
                $sql = "SELECT COUNT(*) FROM " . $tables . " WHERE " . $args["where"];
                $sql1 = "SELECT t1.JobID FROM " . $tables . " WHERE " . $args["where"];                
            }

            $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $fetchQuery->execute();
            $result = $fetchQuery->fetch();
                                 
            $data = [];
            
	    if($result['0'] == '1') {
                                               
		$fetchQuery1 = $this->conn->prepare($sql1, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
                $fetchQuery1->execute();
                $result1 = $fetchQuery1->fetch();
                               
                $JobID = (isset($result1[0])) ? $result1[0] : 0;
                
                $data = ["resultRows" => 1, "JobID" => $JobID];
		
            } else {
                              		
                $data = ["resultRows" => $result['0']];
		
            }
            
        } else {
                        
            if(substr($args['where'], 0, 5) == ' AND ') {
		$args['where'] = substr($args['where'], 5);
	    }

	    /*
            $tables .= 'LEFT JOIN general_default AS gd1 ON (gd1.BrandID=t1.BrandID and gd1.IDno=1)
                        LEFT JOIN general_default AS gd2 ON (gd2.BrandID=' . Constants::SKYLINE_BRAND_ID . ' and gd2.IDno=1)';
	    */
	    
	    /*
            $columns = [
		['t1.DateBooked', 'DATE_FORMAT(t1.DateBooked,"%d/%m/%y")'], 
		"
		(@days :=   (
				CASE 
				    WHEN t1.DateReturnedToCustomer IS NOT NULL THEN DATEDIFF(t1.DateReturnedToCustomer, t1.DateBooked)
				    ELSE    CASE
						WHEN t1.ClosedDate IS NOT NULL THEN DATEDIFF(t1.ClosedDate, t1.DateBooked)
						ELSE DATEDIFF(CURRENT_DATE, t1.DateBooked)
					    END
				END
			    )
		)
		",
		't1.JobID', 
		't1.ServiceCentreJobNo', 
		'CONCAT_WS(", ", t2.ContactLastName, CONCAT_WS(" ", t3.Title, t2.ContactFirstName))', 
		't2.PostalCode', 
		't1.ServiceBaseUnitType',
		'sp.CompanyName', 
		'js.StatusName',
		"
		CASE
		    WHEN @days > @turnaround :=	turnaround(t1.JobID)
		    THEN 1
		    ELSE 0
		END
		",
		"@turnaround"
	    ];
           */
	    
	    $columns = [
		"t1.JobID",
		"DATE_FORMAT(t1.DateBooked, '%d/%m/%Y')",
		"
		CASE
		    WHEN t1.ClosedDate IS NOT NULL
		    THEN @days := DATEDIFF(t1.ClosedDate, t1.DateBooked)
		    ELSE @days := DATEDIFF(CURRENT_DATE, t1.DateBooked)
		END
		",
		"
		CASE
		    WHEN (@days - turnaround(t1.JobID)) > 0
		    THEN (@days - turnaround(t1.JobID))
		    ELSE 0
		END
		",
		"sp.Acronym",
		"t1.ServiceCentreJobNo",
		"CONCAT(customer.ContactLastName, ', ', customer_title.Title, ' ', customer.ContactFirstName)",
		"customer.PostalCode",
		"js.StatusName",
		"
		CASE
		    WHEN t1.JobSite = 'field call' THEN 'F'
		    WHEN t1.JobSite = 'workshop' THEN 'W'
		    ELSE NULL
		END
		",
		"'<input type=\'checkbox\' value=\'\' />'",
		"turnaround(t1.JobID)"
	    ];
	    
	    
	    $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);
	    
	}
        
	return  $data;
         
    }
    
    
    
    /**
    * serviceProviderOpenJobs
    *
    *  This is method to get service provider open jobs..
    * 
    * @param array $args
    * @return $data array
    * @author Nageswara Rao Kanteti  <nag.phpdeveloper@gmail.com>
    ********************************************/
    
    public function serviceProviderOpenJobs($args) {
        
        
            if($args['ServiceProviderID'] && $args['ServiceProviderID']!='-1')
            {
                 $service_provider_condition = " AND t1.ServiceProviderID='".$args['ServiceProviderID']."' ";
            } 
            else
            {
                 if($args['NetworkID']!='')
                 {    
                    $ServiceProviderIDList = '-1';
                    $SkylineModel = $this->controller->loadModel('Skyline'); 
                    $ServiceProvidersArray  =  $SkylineModel->getNetworkServiceProviders($args['NetworkID'], true, $args['dType']);
                    $spCount = count($ServiceProvidersArray);
                    for($s=0;$s<$spCount;$s++)
                    {
                        $ServiceProviderIDList .= ",".$ServiceProvidersArray[$s]['ServiceProviderID'];
                    }

                    $service_provider_condition = " AND t1.ServiceProviderID IN (".$ServiceProviderIDList.") ";
                 }
                 else
                 {
                     $service_provider_condition = "";
                 }
            }
            
            
            $dTypeCondtion = '';

            if($args['dType']=='via')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='FullViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='FullViamente')) ";
            }
            else  if($args['dType']=='sa')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='NoViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='NoViamente')) ";
            }
            else  if($args['dType']=='ao')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='AllocationOnly' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='AllocationOnly')) ";
            }
            
            
        
           // $args['where'] = "t1.ClosedDate IS NULL ".$service_provider_condition.$dTypeCondtion. $this->ActiveJobFilter('js');
            
            $args['where'] = 't1.ServiceProviderDespatchDate IS NULL  '. $service_provider_condition.$dTypeCondtion . $this->UserJobFilter('t1');  
            
         
            if($args['NetworkID']!='')
            {
                if($args['NetworkID']==2)//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
                {    
                   $args['where'] = $args['where']." AND t1.NetworkID='".$args['NetworkID']."' AND t1.ManufacturerID='106'";
                }
                else
                {
                   $args['where'] = $args['where']." AND t1.NetworkID='".$args['NetworkID']."'";
                }
            } 
            
            // Status Type Filter
            if (isset($args['sType']) && $args['sType']) {
                $args['where'] .= ' AND t1.StatusID='.$this->conn->quote( $args['sType'] );
            }
            
            
            if(isset($args['aType']) && ($args['aType']=="av" || $args['aType']=="ha" || $args['aType']=="os"))
            {
                $aTypeCondition = '';

                 if($args['aType']=="av")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
                 } 
                 else if($args['aType']=="ha")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
                 }
                 else if($args['aType']=="os")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
                 }

                 $args['where'] =  $args['where'].$aTypeCondition;

                 $tables = '	job AS t1 
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t2.CustomerTitleID=t3.CustomerTitleID 
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN appointment AS app ON t1.JobID=app.JobID
                        LEFT JOIN `model` AS mdl on t1.`ModelID`=mdl.`ModelID` 
                        LEFT JOIN `unit_type` AS ut on mdl.`UnitTypeID`=ut.`UnitTypeID` 
                        LEFT JOIN `repair_skill` AS rs on ut.`RepairSkillID`=rs.`RepairSkillID` 
                      ';
            }
            else
            {
                $tables = ' job AS t1 
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t2.CustomerTitleID=t3.CustomerTitleID 
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN appointment AS app ON t1.JobID=app.JobID
                      ';
            }
            
	    
            $args['where']  = $args['where']." GROUP BY t1.JobID";        
            
            $columns = array(array('t1.DateBooked', 'DATE_FORMAT(t1.DateBooked,"%d/%m/%y")'), 
                            "
                            (@days :=   (
                                            CASE 
                                                WHEN t1.DateReturnedToCustomer IS NOT NULL THEN DATEDIFF(t1.DateReturnedToCustomer, t1.DateBooked)
                                                ELSE    CASE
                                                            WHEN t1.ClosedDate IS NOT NULL THEN DATEDIFF(t1.ClosedDate, t1.DateBooked)
                                                            ELSE DATEDIFF(CURRENT_DATE, t1.DateBooked)
                                                        END
                                            END
                                        )
                            ) AS Days
                            ",
			    't1.JobID', 
			    'sp.CompanyName', 
			    'CONCAT_WS(", ", t2.ContactLastName, CONCAT_WS(" ", t3.Title, t2.ContactFirstName))', 
			    't2.PostalCode', 
			    'CONCAT_WS(" ", t1.ServiceBaseUnitType, mf.ManufacturerName)',
			    'js.StatusName',
                            '(SELECT DATE_FORMAT(app2.AppointmentDate,"%d/%m/%y") FROM appointment app2 WHERE app2.JobID=t1.JobID AND app2.AppointmentDate>=CURRENT_DATE ORDER BY app2.AppointmentDate DESC LIMIT 0,1 ) AS AppointmentDate',
                            "
                            CASE
                                WHEN @days > (@turnaround := turnaround(t1.JobID))
                                THEN 1
                                ELSE 0
                            END AS overdue
                            ",
                            "@turnaround"
			);
            
            
	$data = $this->ServeDataTables($this->conn, $tables, $columns, $args);
        
	return  $data;
         
    }
    
    
    
    /**
    * serviceProviderOpenJobsWallboardChartStats
    *
    *  This is method is for to get service provider open jobs stats to display in diary wallboard graph..
    * 
    * @param array $args
    * @return $data array
    * @author Nageswara Rao Kanteti  <nag.phpdeveloper@gmail.com>
    ********************************************/
    
    public function serviceProviderOpenJobsWallboardChartStats( $args ) {
       
        
        if($args['ServiceProviderID'] && $args['ServiceProviderID']!='-1')
        {
             $service_provider_condition = " AND t1.ServiceProviderID='".$args['ServiceProviderID']."' ";
        } 
        else
        {
             if($args['NetworkID']!='')
             {    
                $ServiceProviderIDList = '-1';
                $SkylineModel = $this->controller->loadModel('Skyline'); 
                $ServiceProvidersArray  =  $SkylineModel->getNetworkServiceProviders($args['NetworkID'], true, $args['dType']);
                $spCount = count($ServiceProvidersArray);
                for($s=0;$s<$spCount;$s++)
                {
                    $ServiceProviderIDList .= ",".$ServiceProvidersArray[$s]['ServiceProviderID'];
                }

                $service_provider_condition = " AND t1.ServiceProviderID IN (".$ServiceProviderIDList.") ";
             }
             else
             {
                 $service_provider_condition = "";
             }
        }
        
        
        $dTypeCondtion = '';

        if($args['dType']=='via')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='FullViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='FullViamente')) ";
        }
        else  if($args['dType']=='sa')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='NoViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='NoViamente')) ";
        }
        else  if($args['dType']=='ao')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='AllocationOnly' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='AllocationOnly')) ";
        }
        
        
        
       // $args['where'] = "t1.ClosedDate IS NULL ".$service_provider_condition.$dTypeCondtion. $this->ActiveJobFilter('js');
         
        $args['where'] = 't1.ServiceProviderDespatchDate IS NULL  '. $service_provider_condition.$dTypeCondtion . $this->UserJobFilter('t1');  
        
        if($args['NetworkID']!='')
        {
            if($args['NetworkID']==2)//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
            {    
               $args['where'] = $args['where']." AND t1.NetworkID='".$args['NetworkID']."' AND t1.ManufacturerID='106'";
            }
            else
            {
               $args['where'] = $args['where']." AND t1.NetworkID='".$args['NetworkID']."'";
            }
        } 

        
        if(isset($args['aType']) && ($args['aType']=="av" || $args['aType']=="ha" || $args['aType']=="os"))
        {
            $aTypeCondition = '';
             
             if($args['aType']=="av")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
             } 
             else if($args['aType']=="ha")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
             }
             else if($args['aType']=="os")
             {
                 $aTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
             }
            
             $args['where'] =  $args['where'].$aTypeCondition;

             $sql = 'SELECT js.StatusName, count(DISTINCT t1.JobID) AS JobCount, t1.StatusID, js.Colour 
                     FROM    job AS t1 
                             LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
                             LEFT JOIN customer_title AS t3 ON t2.CustomerTitleID=t3.CustomerTitleID 
                             LEFT JOIN status AS js ON t1.StatusID=js.StatusID
                             LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                             LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                             LEFT JOIN appointment AS app ON t1.JobID=app.JobID
                             LEFT JOIN `model` AS mdl on t1.`ModelID`=mdl.`ModelID` 
                             LEFT JOIN `unit_type` AS ut on mdl.`UnitTypeID`=ut.`UnitTypeID` 
                             LEFT JOIN `repair_skill` AS rs on ut.`RepairSkillID`=rs.`RepairSkillID` 
                            ';
        }
        else
        {
            $sql = 'SELECT js.StatusName, count(DISTINCT t1.JobID) AS JobCount, t1.StatusID, js.Colour 
                    FROM    job AS t1 
                            LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
                            LEFT JOIN customer_title AS t3 ON t2.CustomerTitleID=t3.CustomerTitleID 
                            LEFT JOIN status AS js ON t1.StatusID=js.StatusID
                            LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                            LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                            LEFT JOIN appointment AS app ON t1.JobID=app.JobID ';


        }    
            
            $sql = $sql.'

                    WHERE ' . $args['where']
                 . ' GROUP BY t1.StatusID
                     ORDER BY js.StatusName';
        
        
//        $this->log("---rrrrr---");
//        $this->log($sql);
        
        
        $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        if (count($data) == 0)
           $data[] = array('', 0, 0, 'ffffff');
        return $data;
    }
    
    
    
    /**
    * serviceProviderAppJobsWallboardChartStats
    *
    *  This is method is for to get service provider appointment jobs stats to display in diary wallboard graph..
    * 
    * @param array $args
    * @return $data array
    * @author Nageswara Rao Kanteti  <nag.phpdeveloper@gmail.com>
    ********************************************/
    
    public function serviceProviderAppJobsWallboardChartStats( $args ) {
       
        
        if($args['ServiceProviderID'] && $args['ServiceProviderID']!='-1')
        {
             $service_provider_condition = " AND app.ServiceProviderID='".$args['ServiceProviderID']."' ";
        } 
        else
        {
             if($args['NetworkID']!='')
             {    
                $ServiceProviderIDList = '-1';
                $SkylineModel = $this->controller->loadModel('Skyline'); 
                $ServiceProvidersArray  =  $SkylineModel->getNetworkServiceProviders($args['NetworkID'], true, $args['dType']);
                $spCount = count($ServiceProvidersArray);
                for($s=0;$s<$spCount;$s++)
                {
                    $ServiceProviderIDList .= ",".$ServiceProvidersArray[$s]['ServiceProviderID'];
                }

                $service_provider_condition = " AND app.ServiceProviderID IN (".$ServiceProviderIDList.") ";
             }
             else
             {
                 $service_provider_condition = "";
             }
        }
        
        $dTypeCondtion = '';
        
        if($args['dType']=='via')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='FullViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='FullViamente')) ";
        }
        else  if($args['dType']=='sa')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='NoViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='NoViamente')) ";
        }
        else  if($args['dType']=='ao')
        {
            $dTypeCondtion = " AND ((sp.DiaryType='AllocationOnly' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='AllocationOnly')) ";
        }
        
        
        
        $args['where'] = "app.AppointmentDate='".$args['AppointmentDate']."' ".$service_provider_condition.$dTypeCondtion;
         
        if($args['NetworkID']!='')
        {
                 if($args['NetworkID']==2)//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
                 { 
                      $args['where'] = $args['where']." AND ((t1.NetworkID='".$args['NetworkID']."' AND t1.ManufacturerID='106' ".$this->ActiveJobFilter('js').") OR ((nsj.NetworkID='".$args['NetworkID']."' OR nsj.NetworkRefNo IS NOT NULL OR nsj.ServiceType LIKE 'warranty') AND nsj.Manufacturer LIKE 'SAMSUNG'))";
                 }
                 else
                 {
                      $args['where'] = $args['where']." AND ((t1.NetworkID='".$args['NetworkID']."' ".$this->ActiveJobFilter('js').") OR nsj.NetworkID='".$args['NetworkID']."')";
                 }
            
        }
        
        
        if(isset($args['aType']) && ($args['aType']=="av" || $args['aType']=="ha" || $args['aType']=="os"))
        {
            $aTypeCondition = '';
             
             if($args['aType']=="av")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
             } 
             else if($args['aType']=="ha")
             {
                 $aTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
             }
             else if($args['aType']=="os")
             {
                 $aTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
             }
            
             $args['where'] =  $args['where'].$aTypeCondition;

             $sql = 'SELECT js.StatusName, (count(app.JobID) + count(app.NonSkylineJobID)) AS JobCount, t1.StatusID, js.Colour 
                FROM    appointment AS app 
                        LEFT JOIN job AS t1 ON app.JobID=t1.JobID
                        LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN service_provider AS sp2 ON nsj.ServiceProviderID=sp2.ServiceProviderID
                        LEFT JOIN service_provider_skillset sps on app.ServiceProviderSkillsetID=sps.ServiceProviderSkillsetID
                        LEFT JOIN skillset sk on sps.SkillsetID=sk.SkillsetID
                        LEFT JOIN repair_skill rs on sk.RepairSkillID=rs.RepairSkillID
                       ';
        }
        else
        {
             $sql = 'SELECT js.StatusName, (count(app.JobID) + count(app.NonSkylineJobID)) AS JobCount, t1.StatusID, js.Colour 
                FROM    appointment AS app 
                        LEFT JOIN job AS t1 ON app.JobID=t1.JobID
                        LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN service_provider AS sp2 ON nsj.ServiceProviderID=sp2.ServiceProviderID ';
        }    
        
        
        $sql = $sql.'

                WHERE ' . $args['where']
             . ' GROUP BY t1.StatusID
                 ORDER BY js.StatusName';

	
        $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        if (count($data) == 0)
           $data[] = array('', 0, 0, 'ffffff');
        
       for($i=0;$i<count($data);$i++)
       {
           if(!$data[$i][3])
           {
              $data[$i][0] = 'Non Skyline Jobs'; 
              $data[$i][2] = -1;
              $data[$i][3] = 'FF0000'; 
           }   
       }
        
        return $data;
    }
    
    
    
    
     /**
    * serviceProviderAppJobs
    *
    *  This is method to get jobs which have appointment..
    * 
    * @param array $args
    * @return $data array
    * @author Nageswara Rao Kanteti  <nag.phpdeveloper@gmail.com>
    ********************************************/
    
    public function serviceProviderAppJobs($args) {
        
            if($args['ServiceProviderID'] && $args['ServiceProviderID']!='-1')
            {
                 $service_provider_condition = " AND app.ServiceProviderID='".$args['ServiceProviderID']."' ";
            } 
            else
            {
                 if($args['NetworkID']!='')
                 {    
                    $ServiceProviderIDList = '-1';
                    $SkylineModel = $this->controller->loadModel('Skyline'); 
                    $ServiceProvidersArray  =  $SkylineModel->getNetworkServiceProviders($args['NetworkID'], true, $args['dType']);
                    $spCount = count($ServiceProvidersArray);
                    for($s=0;$s<$spCount;$s++)
                    {
                        $ServiceProviderIDList .= ",".$ServiceProvidersArray[$s]['ServiceProviderID'];
                    }

                    $service_provider_condition = " AND app.ServiceProviderID IN (".$ServiceProviderIDList.") ";
                 }
                 else
                 {
                     $service_provider_condition = "";
                 }
            }
            
            
            $dTypeCondtion = '';

            if($args['dType']=='via')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='FullViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='FullViamente')) ";
            }
            else  if($args['dType']=='sa')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='NoViamente' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='NoViamente')) ";
            }
            else  if($args['dType']=='ao')
            {
                $dTypeCondtion = " AND ((sp.DiaryType='AllocationOnly' AND sp2.DiaryType IS NULL) OR (sp.DiaryType IS NULL AND sp2.DiaryType='AllocationOnly')) ";
            }
        
        
        
        
            $args['where'] = "app.AppointmentDate='".$args['AppointmentDate']."' ".$service_provider_condition.$dTypeCondtion;
         
            if($args['NetworkID']!='')
            {
                 if($args['NetworkID']==2)//If network is samsung we also filtering jobs samsung manufacturer...(ID 106)//Chris has requested this featuer on 12/03/2013
                 { 
                      $args['where'] = $args['where']." AND ((t1.NetworkID='".$args['NetworkID']."' AND t1.ManufacturerID='106' ".$this->ActiveJobFilter('js').") OR ((nsj.NetworkID='".$args['NetworkID']."'  OR nsj.NetworkRefNo IS NOT NULL OR nsj.ServiceType LIKE 'warranty') AND nsj.Manufacturer LIKE 'SAMSUNG') )";
                 }
                 else
                 {
                      $args['where'] = $args['where']." AND ((t1.NetworkID='".$args['NetworkID']."' ".$this->ActiveJobFilter('js').") OR nsj.NetworkID='".$args['NetworkID']."')";
                 }
            } 
            
             // Status Type Filter
            if (isset($args['sType']) && $args['sType']) {
                
                if($args['sType']==-1)
                {    
                    $args['where'] .= ' AND t1.StatusID IS NULL';
                }
                else
                {
                    $args['where'] .= ' AND t1.StatusID='.$this->conn->quote( $args['sType'] );
                }
            }
            
            
            
            if(isset($args['aType']) && ($args['aType']=="av" || $args['aType']=="ha" || $args['aType']=="os"))
            {
                $aTypeCondition = '';

                 if($args['aType']=="av")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName = 'BROWN GOODS' ";
                 } 
                 else if($args['aType']=="ha")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName = 'WHITE GOODS' ";
                 }
                 else if($args['aType']=="os")
                 {
                     $aTypeCondition = " AND rs.RepairSkillName != 'WHITE GOODS' AND rs.RepairSkillName != 'BROWN GOODS' ";
                 }

                 $args['where'] =  $args['where'].$aTypeCondition;

                 $tables = '	
                        appointment AS app 
                        LEFT JOIN job AS t1 ON app.JobID=t1.JobID
                        LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN service_provider AS sp2 ON nsj.ServiceProviderID=sp2.ServiceProviderID
                        LEFT JOIN service_provider_skillset sps on app.ServiceProviderSkillsetID=sps.ServiceProviderSkillsetID
                        LEFT JOIN skillset sk on sps.SkillsetID=sk.SkillsetID
                        LEFT JOIN repair_skill rs on sk.RepairSkillID=rs.RepairSkillID
                        
                      ';
            }
            else
            {
                $tables = '	
                        appointment AS app 
                        LEFT JOIN job AS t1 ON app.JobID=t1.JobID
                        LEFT JOIN non_skyline_job AS nsj ON app.NonSkylineJobID=nsj.NonSkylineJobID
			LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
			LEFT JOIN customer_title AS t3 ON t3.CustomerTitleID = t2.CustomerTitleID
			LEFT JOIN status AS js ON t1.StatusID=js.StatusID
			LEFT JOIN service_provider AS sp ON t1.ServiceProviderID=sp.ServiceProviderID
                        LEFT JOIN manufacturer AS mf ON t1.ManufacturerID=mf.ManufacturerID
                        LEFT JOIN service_provider AS sp2 ON nsj.ServiceProviderID=sp2.ServiceProviderID
                        
                      ';
            }    
            
           // $this->log('------tttt----------');
           // $this->log($tables);
           // $this->log($args['where']);        
	   
           // $args['where']  = $args['where'];
            
            $columns = array(array('t1.DateBooked', 'DATE_FORMAT(t1.DateBooked,"%d/%m/%y")'), 
                            "
                            (@days :=   (
                                            CASE 
                                                WHEN t1.DateReturnedToCustomer IS NOT NULL THEN DATEDIFF(t1.DateReturnedToCustomer, t1.DateBooked)
                                                ELSE    CASE
                                                            WHEN t1.ClosedDate IS NOT NULL THEN DATEDIFF(t1.ClosedDate, t1.DateBooked)
                                                            ELSE DATEDIFF(CURRENT_DATE, t1.DateBooked)
                                                        END
                                            END
                                        )
                            ) AS Days
                            ",
			    'IF(t1.JobID IS NULL, CONCAT_WS(" ", "NSJ No.", nsj.NonSkylineJobID), t1.JobID)', 
			    'IF(t1.JobID IS NULL, sp2.CompanyName, sp.CompanyName)', 
			    'IF(t1.JobID IS NULL, CONCAT_WS(" ", nsj.CustomerTitle, nsj.CustomerSurname), CONCAT_WS(", ", t2.ContactLastName, CONCAT_WS(" ", t3.Title, t2.ContactFirstName)))', 
			    'IF(t1.JobID IS NULL, nsj.Postcode, t2.PostalCode)', 
			    'IF(t1.JobID IS NULL, CONCAT_WS(" ", nsj.ProductType, nsj.Manufacturer), CONCAT_WS(" ", t1.ServiceBaseUnitType, mf.ManufacturerName))',
			    'js.StatusName',
                            'DATE_FORMAT(app.AppointmentDate,"%d/%m/%y")',
                            "
                            CASE
                                WHEN @days > (@turnaround := turnaround(t1.JobID))
                                THEN 1
                                ELSE 0
                            END AS overdue
                            ",
                            "@turnaround"    
			);
                  
	    $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);
            	    
	
        
	return  $data;
         
    }
    
    
    
    public function AdvancedSearch( $args ){
       
        if ($_SERVER['REQUEST_METHOD'] == 'POST') 
        {
        
            $tables = "job AS t1 LEFT JOIN customer AS t2 ON t1.CustomerID = t2.CustomerID
                            LEFT JOIN status AS t3 ON t1.StatusID = t3.StatusID
                            LEFT JOIN product AS t4 ON t1.ProductID = t4.ProductID
                            LEFT JOIN model AS t5 ON t1.ModelID = t5.ModelID
                            LEFT JOIN brand_branch AS brdbrch ON t1.`BranchID` = brdbrch.`BranchID`
                           ";
            $args['where'] = '';
            $sep = '';
          
            if(isset($_POST['ContactLastName']) && $_POST['ContactLastName'])
            {
                $args['where'] .= "t2.ContactLastName=".$this->conn->quote( $_POST['ContactLastName'] );
                $sep = " AND ";
            }
            if(isset($_POST['BuildingNameNumber']) && $_POST['BuildingNameNumber'])
            {
                $args['where'] .= $sep."t2.BuildingNameNumber LIKE ".$this->conn->quote( '%'.$_POST['BuildingNameNumber'].'%' );
                $sep = " AND ";
            }
            if(isset($_POST['Street']) && $_POST['Street'])
            {
                $args['where'] .= $sep."t2.Street=".$this->conn->quote( $_POST['Street'] );
                $sep = " AND ";
            }
            if(isset($_POST['TownCity']) && $_POST['TownCity'])
            {
                $args['where'] .= $sep."t2.TownCity=".$this->conn->quote( $_POST['TownCity'] );
                $sep = " AND ";
            }
            
            if(isset($_POST['PostalCode']) && $_POST['PostalCode'])
            {
                $pcTemp = str_replace(" ", "", $_POST['PostalCode']);
                
                $pcLength = strlen($pcTemp);
                
                if($pcLength>3)
                {
                    $pcTemp2 = substr($pcTemp, 0, $pcLength-3)." ".substr($pcTemp, -3);
                    $args['where'] .= $sep."(t2.PostalCode=".$this->conn->quote( $pcTemp )." OR t2.PostalCode=".$this->conn->quote( $pcTemp2 ).")";
                }
                else
                {
                    $args['where'] .= $sep."t2.PostalCode=".$this->conn->quote( $_POST['PostalCode'] );
                }
                $sep = " AND ";
            }
            
            if(isset($_POST['ColAddPostcode']) && $_POST['ColAddPostcode'])
            {
                $pcTemp = str_replace(" ", "", $_POST['ColAddPostcode']);
                
                $pcLength = strlen($pcTemp);
                
                if($pcLength>3)
                {
                    $pcTemp2 = substr($pcTemp, 0, $pcLength-3)." ".substr($pcTemp, -3);
                    $args['where'] .= $sep."(t1.ColAddPostcode=".$this->conn->quote( $pcTemp )." OR t1.ColAddPostcode=".$this->conn->quote( $pcTemp2 ).")";
                }
                else
                {
                    $args['where'] .= $sep."t1.ColAddPostcode=".$this->conn->quote( $_POST['ColAddPostcode'] );
                }
                $sep = " AND ";
            }
            
            
            
            if(isset($_POST['OlderThan']) && $_POST['OlderThan'])
            {
                $_POST['OlderThan'] = date("Y-m-d", strtotime($_POST['OlderThan']));
                $args['where'] .= $sep."t1.DateBooked<=".$this->conn->quote( $_POST['OlderThan'] );
                $sep = " AND ";
            }
            if(isset($_POST['NewerThan']) && $_POST['NewerThan'])
            {
                $_POST['NewerThan'] = date("Y-m-d", strtotime($_POST['NewerThan']));
                $args['where'] .= $sep."t1.DateBooked>=".$this->conn->quote( $_POST['NewerThan'] );
                $sep = " AND ";
            }
            if(isset($_POST['JobStatus']) && $_POST['JobStatus'])
            {
                $args['where'] .= $sep."t1.StatusID=".$this->conn->quote( $_POST['JobStatus'] );
                $sep = " AND ";
            }
            if(isset($_POST['NetworkID']) && $_POST['NetworkID'])
            {
                $args['where'] .= $sep."t1.NetworkID=".$this->conn->quote($_POST['NetworkID']);
                $sep = " AND ";
            }
            if(isset($_POST['ClientID']) && $_POST['ClientID'])
            {
                $args['where'] .= $sep."t1.ClientID=".$this->conn->quote($_POST['ClientID']);
                $sep = " AND ";
            }
            if(isset($_POST['BranchID']) && $_POST['BranchID'])
            {
                $args['where'] .= $sep."t1.BranchID=".$this->conn->quote($_POST['BranchID']);
                $sep = " AND ";
            }
            if(isset($_POST['BookingPerson']) && $_POST['BookingPerson'])
            {
                $args['where'] .= $sep."t1.BookedBy=".$this->conn->quote($_POST['BookingPerson']);
                $sep = " AND ";
            }
            if(isset($_POST['ProductNo']) && $_POST['ProductNo'])
            {
                $args['where'] .= $sep."t4.ProductNo = ".$this->conn->quote($_POST['ProductNo']);
                $sep = " AND ";
            }
            if(isset($_POST['ManufacturerID']) && $_POST['ManufacturerID'])
            {
                $args['where'] .= $sep."t5.ManufacturerID = ".$this->conn->quote($_POST['ManufacturerID']);
                $sep = " AND ";
            }
            if(isset($_POST['UnitTypeID']) && $_POST['UnitTypeID'])
            {
                $args['where'] .= $sep."t5.UnitTypeID = ".$this->conn->quote($_POST['UnitTypeID']);
                $sep = " AND ";
            }
            if(isset($_POST['ModelNumber']) && $_POST['ModelNumber'])
            {
                $args['where'] .= $sep."t5.ModelNumber = ".$this->conn->quote($_POST['ModelNumber']);
                $sep = " AND ";
            }
            if(isset($_POST['SerialNo']) && $_POST['SerialNo'])
            {
                $args['where'] .= $sep."t1.SerialNo = ".$this->conn->quote($_POST['SerialNo']);
                $sep = " AND ";
            }
            if(isset($_POST['ServiceProviderID']) && $_POST['ServiceProviderID'])
            {
                $args['where'] .= $sep."t1.ServiceProviderID = ".$this->conn->quote($_POST['ServiceProviderID']);
                $sep = " AND ";
            }
            if(isset($_POST['ProductLocation']) && $_POST['ProductLocation'])
            {
                $args['where'] .= $sep."t1.ProductLocation = ".$this->conn->quote($_POST['ProductLocation']);
                $sep = " AND ";
            }
            
            
            if($args['where']!='')
            {
                $args['where'] .= $this->UserJobFilter('t1','brdbrch');
            }
            else
            {
                $args['where'] = "t1.JobID!='0'".$this->UserJobFilter('t1','brdbrch');
            }
            

           // $this->controller->log($args['where']);
            
            $columns = array('t1.JobID', 't1.ServiceCentreJobNo', 't2.ContactLastName',  't2.PostalCode', 't1.RepairDescription', 't3.StatusName');


            $data = $this->ServeDataTables($this->conn, $tables, $columns, $args);


            return  $data;
        }
        

        /*****
        # assign values
        foreach($args as $key => $a){
            $this->advanced_search_fields[$key]['value'] = $a;
        }
        
        #$this->controller->log('JobBooking AdvancedSearch using : ' . var_export($this->advanced_search_fields, true));
        $this->super_filter = json_encode($args);
        
        $table = 'job, customer';
        $where  = 'WHERE job.CustomerID = customer.CustomerID';      
        
        foreach($args as $field => $value){
            if($value !=''){
                $where .= ($where == '' ? "WHERE $field LIKE '%$value%'" : " AND $field LIKE '%$value%'");
            }
        }
        
	$query = "SELECT * FROM $table $where";
       // $this->controller->log($query);


        $query = $this->conn->query( $query );
        $query->setFetchMode( PDO::FETCH_ASSOC );     
        $this->search_result = $query->fetchAll() or die('Query error');      
        
        $this->matches = count( $this->search_result );           
        
        **********/
    }

    
    
    public function ValidateAdvancedJobForm(){
        $raw_js = '';
        
        foreach ($this->advanced_search_fields as $key => $f){
            $raw_js .= ($raw_js == '' ? '' : ' && ') . '$("#' . $key. '").val() == "" ';
        }
        
        # $this->controller->log('JobBooking Model : ' . $raw_js);
        
        return ($raw_js == '' ? 'false' :$raw_js);
    }
    
    
    
    public function GetValue($dbFieldName=''){       
        return (isset($this->advanced_search_fields[$dbFieldName]['value']) ? $this->advanced_search_fields[$dbFieldName]['value'] : '');
    }
    
    
    
    private function ActiveJobFilter( $statusalias='status') {
        //return ' AND '.$statusalias.".StatusName != '29 JOB CANCELLED'";
        return Functions::ActiveJobFilter( $statusalias );
    }
    
    
    
    private function CancelledJobFilter( $statusalias='status') {
        //return ' AND '.$statusalias.".StatusName='29 JOB CANCELLED'";
        return Functions::CancelledJobFilter( $statusalias );
    }
    
    
    
    private function UserJobFilter( $jobalias='job', $branchbrandalias='branch_brand') {
        
        return Functions::UserJobFilter( $this->controller->user, $jobalias, $branchbrandalias );
                
        //$this->controller->log(var_export($this->controller->session->UserID, true));
             
//        switch ($this->controller->user->UserType) {                            /* Set job filter based on user type */
//            case 'Admin':
//                return '' ;
//                break;
//            
//            case 'ServiceProvider':
//               return ' AND '.$jobalias.'.ServiceProviderID='.$this->controller->user->ServiceProviderID;
//                break;
//            
//            case 'Branch':
//              switch ($this->controller->user->BranchJobSearchScope) {        /* User is branch so filter is based on Nrancj Job Scope Serach */
//                  case 'Brand':
//                      return (" AND $branchbrandalias.`BrandID` = {$this->controller->user->DefaultBrandID}");
//                      break;
//                  
//                  case 'Client':
//                      return ' AND '.$jobalias.'.ClientID='.$this->controller->user->ClientID;
//                      break;
//                  
//                  default:                                                    /* Branch Only */
//                      return ' AND '.$jobalias.'.BranchID='.$this->controller->user->BranchID;
//              }                
//              break;
//          
//          case 'Client':
//              return ' AND '.$jobalias.'.ClientID='.$this->controller->user->ClientID;
//              break;
//          
//          case 'Network':
//              return ' AND '.$jobalias.'.NetworkID='.$this->controller->user->NetworkID;
//              break;
//          
//          case 'Manufacturer':
//              return ' AND '.$jobalias.'.ManufacturerID='.$this->controller->user->ManufacturerID;
//              break;
//          
//           case 'ExtendedWarrantor':
//              return '';
//              break;
//          
//          default:
//              //$this->controller->log(var_export($this->controller->user,true));
//              throw new exception('Unrecognised User Type');
//      } 
    }
    
    
    
     /**
     * doesNetworkRefExist
     * 
     * This returns if a job with the given network reference number exists
     *
     * @param string $nId     NetworkID
     *   
     * @return integer      User ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     **************************************************************************/ 
    public function doesNetworkRefExist($netRef) {
        
         $sql = "
                SELECT
			COUNT(`JobID`) AS count
		FROM
			`job`
		WHERE
			`NetworkRefNo` = '$netRef'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return ( $result[0]['count'] );
    }
    
    
    
    /**
     * fetchAllDetails
     * 
     * Get all the fields form the jobs table associated with a job
     * 
     * @param $jId - The Job ID to return the details for
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function fetchAllDetails($jId) {
        
        $sql = "
                SELECT
			*
		FROM
			`job`
		WHERE
			`JobID` = '$jId'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return ( $result[0] ); 
        } else {
            return (null);
        }
                
    }
    
    
    
    /**
     * getNetworkRefNo
     * 
     * Return the NetworkRefNo associated with a job
     * 
     * @param $jId - The Job ID to return the details for
     * 
     * @return string   The Network reference Number
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/

    public function getNetworkRefNo($jId) {
        $sql = "
                SELECT
			`NetworkRefNo`
		FROM
			`job`
		WHERE
			`JobID` = '$jId'
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return ( $result[0]['NetworkRefNo'] ); 
        } else {
            return (null);
        }
    }

    
    
    /**
     * getIdFromRmaNo
     * 
     * Returns the JobID from the RMA number (if the job exists) 
     * 
     * @param integer $rNo  The RMA number of the job
     * 
     * @return integer  The ID of the Job
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getIdFromRmaNo($rNo) {
        $sql = "
                SELECT
			`JobID`
		FROM
			`job`
		WHERE
			`RMANumber` = $rNo
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['JobID']);                                        /* Job exists so return Job ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    /**
     * getIdFromNetworkRef
     * 
     * Returns the JobID from the Network Ref (if the job exists) 
     * 
     * @param integer $rNo  The Network Reference number of the job
     * 
     * @return integer  The ID of the Job
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getIdFromNetworkRef($nRef) {
        $sql = "
                SELECT
			`JobID`, `NetworkRefNo`
		FROM
			`job`
		WHERE
			`NetworkRefNo` = '$nRef'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['JobID']);                                        /* Job exists so return Job ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    public function getModelID($modelNumber) {
	
	$q="SELECT * FROM model WHERE ModelNumber=:ModelNumber";
	$args=array("ModelNumber" => $modelNumber);
	$result=$this->Query($this->conn,$q,$args);
	if(count($result)>0) {
	    return $result[0]["ModelID"];
	} else {
	    return false;
	}
	
    }
    
    
    
    public function getProductID($productNumber) {

	$q="SELECT * FROM product WHERE ProductNo=:productNumber";
	$values=array("productNumber" => $productNumber);
	$result=$this->Query($this->conn,$q,$values);
	if(count($result)>0) {
	    return $result[0]["ProductID"];
	} else {
	    return false;
	}
	
    }

    
    
    public function getManufacturerID($modelID) {

	$q="SELECT * FROM model WHERE ModelID=:modelID";
	$values=array("modelID" => $modelID);
	$result=$this->query($this->conn,$q,$values);
	if(count($result)>0) {
	    return $result[0]["ManufacturerID"];
	} else {
	    return false;
	}
	
    }
    
    
    
    public function getBranch($branchName, $clientID) {
	
	$q='SELECT	branch.BranchName,branch.BranchID,brand.ClientID
	    FROM	branch
	    LEFT JOIN 	brand_branch ON branch.BranchID=brand_branch.BranchID
	    LEFT JOIN 	brand ON brand.BrandID=brand_branch.BrandID
	    WHERE	branch.BranchName LIKE :branchName AND brand.ClientID=:clientID';
	
	$branchName.="%";
	
	$args=array("branchName" => $branchName, "clientID" => $clientID);
	$result=$this->Query($this->conn,$q,$args);
	if(count($result)>0) {
	    return $result[0]["BranchID"];
	} else {
	    return false;
	}
	
    }
    
    
    
    public function getClientID($clientName) {
	
	$q="SELECT * FROM client WHERE ClientName=:clientName";
	$args=array("clientName" => $clientName);
	$result=$this->Query($this->conn,$q,$args);
	if(count($result)>0) {
	    return $result[0]["ClientID"];
	} else {
	    return false;
	}
	
    }
    
    
    
    public function getLatestBatchRecord($clientID) {
	
	$q="SELECT * FROM job_import_batch_data WHERE clientID=:clientID ORDER BY batchNumber DESC LIMIT 1";
	$values=array("clientID" => $clientID);
	$result=$this->query($this->conn,$q,$values);
	if(count($result)>0) {
	    return $result[0];
	} else {
	    return false;
	}
	
    }


    
    public function getLatestBatchRecord_Hash($clientID,$batchHash) {
	
	$q="SELECT * FROM job_import_batch_data WHERE clientID=:clientID AND batchHash=:batchHash LIMIT 1";
	$values=array("clientID" => $clientID, "batchHash" => $batchHash);
	$result=$this->query($this->conn,$q,$values);
	if(count($result)>0) {
	    return $result[0];
	} else {
	    return false;
	}
	
    }
    
    
    
    public function createBatchRecord($batchNumber,$clientID,$batchRowCount,$description,$batchHash) {
	
	$q="INSERT INTO	    job_import_batch_data (batchNumber,clientID,batchRowCount,batchDescription,batchHash)
	    VALUES	    (:batchNumber,:clientID,:batchRowCount,:batchDescription,:batchHash)";
	$values=array(	"batchNumber" => $batchNumber, 
			"clientID" => $clientID, 
			"batchRowCount" => $batchRowCount,
			"batchDescription" => $description,
			"batchHash" => $batchHash
		    );
	$result=$this->execute($this->conn,$q,$values);
	return $result;
	
    }
 
   
    
    public function getImportedJobs($batchID) {
	
	$q="SELECT COUNT(*) AS jobs FROM job WHERE batchID=:batchID";
	$values=array("batchID" => $batchID);
	$result=$this->query($this->conn,$q,$values);
	
	return $result[0]["jobs"];
	
    }
 
    
    
    public function changeBatchState($batchID,$batchState) {
	
	$q="UPDATE job_import_batch_data SET batchState=:batchState WHERE batchID=:batchID";
	$values=array("batchID" => $batchID, "batchState" => $batchState);
	$result=$this->execute($this->conn,$q,$values);

	return $result;
	
    }
    
   
    
    public function getServiceProviderByJobID($jobID) {
	
	$q="SELECT ServiceProviderID FROM job WHERE JobID=:jobID";
	$values=array("jobID" => $jobID);
	$result=$this->query($this->conn,$q,$values);

	return ($result[0]["ServiceProviderID"] && $result[0]["ServiceProviderID"]!=0) ? $result[0]["ServiceProviderID"] : false;
	
    }
    

    
    //Communicates the job to Service Base, RMA Tracker and Service Center
    //2012-12-11  Vic <v.rutkunas@pccsuk.com>
    
    public function pushJobToSC($jobID) {

	$serviceProviderID = $this->getServiceProviderByJobID($jobID);
	
	if($serviceProviderID) {

	    $ServiceProvidersModel = $this->controller->loadModel('ServiceProviders');
	    $ServiceProvidersDetails = $ServiceProvidersModel->fetchRow(["ServiceProviderID" => $serviceProviderID]);

	    if(is_array($ServiceProvidersDetails)) {
		
		$api = $this->controller->loadModel('APIJobs');

		$rmaResponse = $api->rmaPutNewJob($jobID); 

		if($rmaResponse && $rmaResponse == "SC0001") {  //If Rma traker call responds with success.
		    
		    if($ServiceProvidersDetails['Platform'] == "ServiceBase") {
			$api->putNewJob($jobID);
			return true;
		    } else if($ServiceProvidersDetails['Platform'] == "API") {
			$this->update(["DownloadedToSC" => 0, "JobID" => $jobID]);
			return true;
		    } else if($ServiceProvidersDetails['Platform'] == "Website" || $ServiceProvidersDetails['Platform'] == "RMA Tracker") {
			$this->update(["DownloadedToSC" => 1, "JobID" => $jobID]);
			return true;
		    }
		    
		}

	    }
	    
	}
	
	return false;
	
    }
    
    
    
    public function getUnsentJobIDs() {

	$q="SELECT	JobID
	    FROM	job
	    INNER JOIN 	service_provider ON job.serviceproviderid = service_provider.ServiceProviderID
	    WHERE	(service_provider.Platform = 'ServiceBase' AND (job.DownloadedToSC <> 1 OR job.RMANumber IS NULL)) 
			OR (service_provider.Platform = 'RMA Tracker' AND job.RMANumber IS NULL)";
	
	$result=$this->query($this->conn,$q);
	
	return $result;	
	
    }
    
    
    
    public function getBatchList($clientID = null) {
	
	$clientID = null;
	$clientID = ($clientID) ? $clientID : $this->getClientID("Shop Direct Ltd");
	//$clientID = ($clientID) ? $clientID : $this->getClientID("Shop Direct");
	
	$q = "	SELECT	    DATE_FORMAT(batchTime, '%d/%m/%Y') AS '0',
			    DATE_FORMAT(batchTime, '%H:%i') AS '1',
			    batchNumber AS '2',
			    batchDescription AS '3',
			    'Camtronics' AS '4',
			    (batchRowCount - 1) AS '5',
			    batchID AS '6'
		FROM	    job_import_batch_data WHERE clientID=:clientID
		ORDER BY    batchNumber DESC
		";
	
	$values = array("clientID" => $clientID);
	$result = $this->query($this->conn, $q, $values);
	
	$return = array("aaData" => $result);

	return $return;
	
    }
    
    
    
    public function getDeliveryDetails($jobID) {
	
	$q = "	SELECT	ColAddCompanyName AS CompanyName,
			ColAddBuildingNameNumber AS BuildingNameNumber,
			ColAddStreet AS Street,
			ColAddLocalArea AS Area,
			ColAddTownCity AS City,
			ColAddCountyID AS CountyID,
			ColAddCountryID AS CountryID,
			ColAddPostcode AS PostalCode,
                        ColAddEmail,
                        ColAddPhone,
                        ColAddPhoneExt
                        
		FROM	job
		WHERE	JobID=:jobID
	    ";
	
	$values = array("jobID" => $jobID);
	$result = $this->query($this->conn, $q, $values);

	return (count($result)>0) ? $result[0] : false;
	
    }
    
  
    
    public function updateDeliveryDetails($data,$jobID) {
	
	$q = "	UPDATE	job 
		SET	ColAddCompanyName = :CompanyName,
			ColAddBuildingNameNumber = :BuildingNameNumber,
			ColAddStreet = :Street,
			ColAddLocalArea = :LocalArea,
			ColAddTownCity = :City,
			ColAddCountyID = :CountyID,
			ColAddPostcode = :PostalCode,
                        ColAddEmail    = :ColAddEmail,
                        ColAddPhone    = :ColAddPhone,
                        ColAddPhoneExt = :ColAddPhoneExt,
                        ModifiedUserID = :ModifiedUserID,
                        ModifiedDate = :ModifiedDate
		WHERE	JobID = :jobID
	     ";
	
	$values = array(
			    "CompanyName" => $data["CompanyName"],
			    "BuildingNameNumber" => $data["BuildingNameNumber"],
			    "Street" => $data["Street"],
			    "LocalArea" => $data["LocalArea"],
			    "City" => $data["City"],
			    "CountyID" => ($data["CountyID"]!="") ? $data["CountyID"] : null,
                            "PostalCode" => $data["PostalCode"],
                            "ColAddEmail" => ($data["ColAddEmail"]!="") ? $data["ColAddEmail"] : null,
                            "ColAddPhone" => ($data["ColAddPhone"]!="") ? $data["ColAddPhone"] : null,
                            "ColAddPhoneExt" => ($data["ColAddPhoneExt"]!="") ? $data["ColAddPhoneExt"] : null,
                            "ModifiedUserID" => $this->controller->user->UserID,
                            "ModifiedDate" => date('Y-m-d H:i:s'),
			    "jobID" => $jobID
		       );
	
	$result = $this->execute($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    public function toCaps($data) {
	
	$return = array();
	
	foreach($data as $key => $value) {
	    if(!stristr($key,"email") && !stristr($key,"RAType")) {
		$return[$key] = strtoupper($value);
	    } else {
		$return[$key] = $value;
	    }
	}
	
	return $return;
	
    }

    
    
    public function getServiceTypes() {
	
	$q = "SELECT ServiceTypeID, ServiceTypeName FROM service_type";
	$result = $this->query($this->conn,$q);
	
	foreach($result as $key => $value) {
	    $result[$key]["ServiceTypeName"] = strtoupper($value["ServiceTypeName"]);
	}
	
	return $result;
	
    }
    

    
    public function getALtSCDetails($serviceProviderID,$clientID) {
	
	if($serviceProviderID==null || $clientID==null) {
	    return false;
	}
	
	$q = "SELECT * FROM service_provider_contacts WHERE ServiceProviderID = :serviceProviderID AND ClientID = :clientID";
	$values = array("serviceProviderID" => $serviceProviderID, "clientID" => $clientID);
	$result = $this->query($this->conn,$q,$values);
	
	if(count($result)>0) {
	    return $result[0];
	} else {
	    return false;
	}
	
    }
    
    
    
    /**
     * getLastStatus
     *  
     * Get the last status of a job
     * 
     * @param array $jId    Service Provider ID
     * 
     * @return string   Status name
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getLastStatus($jId) {
        $sql = "
                SELECT
                	s.`StatusName`
                FROM 
                	`job` j,
                        `status` s
                WHERE
                        s.`StatusID` = j.`StatusID`
                	AND j.`JobID` = $jId 
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['StatusName']);                                   /* Status exists so return ststus name */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    //Returns count of jobs with specific status (job.OpenJobStatus)
    //2012-11-16  Vic <v.rutkunas@pccsuk.com>
     
    public function getJobsByStatusNumber($user, $status) {
	
	/*
	$q = '	SELECT	    COUNT(*) AS number
		FROM	    job
		LEFT JOIN   manufacturer ON job.ManufacturerID = manufacturer.manufacturerID
		WHERE	    job.OpenJobStatus = :status AND 
			    (
				(manufacturer.AuthorisationRequired = "Yes" AND job.AuthorisationNo IS NOT NULL AND job.AuthorisationNo != "") OR
				(manufacturer.AuthorisationRequired = "No" OR manufacturer.AuthorisationRequired IS NULL)
			    )
	     ';
	*/

	$q = '	SELECT	    COUNT(*) AS number
		FROM	    job
		LEFT JOIN   manufacturer ON job.ManufacturerID = manufacturer.manufacturerID
		/*WHERE	    job.OpenJobStatus = :status AND 
			    ((job.RepairAuthStatus != "required" AND job.RepairAuthStatus != "rejected") OR job.RepairAuthStatus IS NULL)
		*/
		WHERE	    job.OpenJobStatus = :status AND 
			    (job.RepairAuthStatus != "required" OR job.RepairAuthStatus IS NULL) AND
			    job.StatusID != 44 AND
			    job.StatusID != 33 AND
			    job.StatusID != 41
	     ';

	$values = ["status" => $status];
	
	if($user->UserType == "Network") {
	    $q .= " AND NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}

	$result = $this->query($this->conn, $q, $values);

	return ($result[0]["number"]) ? $result[0]["number"] : 0;
	
    }

    
    
    //Returns count of jobs with authorisation required but not present (job.AuthorisationNo)
    //2012-11-16  Vic <v.rutkunas@pccsuk.com>
    
    public function getJobsAuthRequiredNumber($user) {
	
	$q = "	SELECT	    COUNT(*) AS number
		
		FROM	    job
		
		LEFT JOIN   ra_status AS ra ON job.RAStatusID = ra.RAStatusID
		
		WHERE	    job.RepairAuthStatus = 'required' AND
			    ra.RAStatusCode !=	(
						    SELECT  RAStatusCode
						    FROM    ra_status
						    WHERE   RAStatusName = 'General Query'
						) AND
			    job.StatusID != 33 AND
			    job.StatusID != 41
	    ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}

	$result = $this->query($this->conn,$q,$values);

	return ($result[0]["number"]) ? $result[0]["number"] : 0;
	
    }
    
    

    /**
    @action counts jobs on the system for the user requiring service appraisal
    @input  user data
    @return number of jobs
    @output void
    2012-11-16  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getJobsAppraisalRequiredNumber($user) {
	
	$q = "	SELECT	COUNT(*) AS number
		FROM	job
		WHERE	job.StatusID = 44 AND (job.OpenJobStatus != 'branch_repair' OR job.OpenJobStatus IS NULL)
	     ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}

	$result = $this->query($this->conn, $q, $values);

	return ($result[0]["number"]) ? $result[0]["number"] : 0;
	
    }
    
    
    
    public function getJobsQueryNumber($user) {
	
	$q = "	SELECT	    COUNT(*) AS number
		FROM	    job
		LEFT JOIN   ra_status AS ra ON ra.RAStatusID = job.RAStatusID
		WHERE	    ra.RAStatusName = 'General Query' AND
			    job.StatusID != 33 AND
			    job.StatusID != 41
	    ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}

	$result = $this->query($this->conn, $q, $values);

	return ($result[0]["number"]) ? $result[0]["number"] : 0;
	
    }
    

    
    public function getJobsBranchRepairNumber($user) {
	
	$q = "	SELECT	    COUNT(*) AS number
		FROM	    job
		WHERE	    OpenJobStatus = 'branch_repair'
	     ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}

	$result = $this->query($this->conn, $q, $values);

	return ($result[0]["number"]) ? $result[0]["number"] : 0;
	
    }

    
    
    //Returns number of overdue jobs for given user and status
    //2012-11-25  Vic <v.rutkunas@pccsuk.com>
    
    public function getOverdueJobsNumber($user, $status) {
	
        $networkID = ($user->NetworkID) ? $user->NetworkID : 'NULL';
        $clientID = ($user->ClientID) ? $user->ClientID : 'NULL';
        $branchID = ($user->BranchID) ? $user->BranchID : 'NULL';
        
	$q = "  /*Setting input variables separate from the big query*/
		
		SET @networkID := $networkID, @clntID := $clientID, @branchID := $branchID;
                
		/*First query - generating temp table with overdue jobs*/
		
		CREATE TEMPORARY TABLE IF NOT EXISTS temp AS (
		    SELECT	j.NetworkID,
				@clientID := j.ClientID,
				j.BranchID,
				j.ServiceProviderID,
				((DATEDIFF(CURDATE(), j.DateBooked)) - turnaround(j.JobID)) AS overdue
				
		    FROM	job AS j
		    
		    WHERE	CASE
                                    WHEN (@networkID IS NOT NULL) THEN j.NetworkID = @networkID
                                    ELSE TRUE
		    		END
		    		
		    		AND
		    		
		    		CASE
                                    WHEN (@clntID IS NOT NULL) THEN j.ClientID = @clntID
                                    ELSE TRUE
		    		END
		    		
                                AND

		    		CASE
                                    WHEN (@branchID IS NOT NULL) THEN j.BranchID = @branchID
                                    ELSE TRUE
		    		END
		    		
		    		AND
                                
                                j.ClosedDate IS NULL AND
				j.StatusID != 33 AND
				j.StatusID != 41
		    
		    HAVING	overdue > 0
		);


		/*Second query - filtering out overdue jobs with specific time range*/

		SELECT 	COUNT(*) AS number
		FROM 	temp
        ";

        if($status == "1to5") {
	    $q .= "WHERE overdue >= 1 AND overdue <= 5 AND overdue != 0";
	}
	if($status == "6to10") {
	    $q .= "WHERE overdue >= 6 AND overdue <= 10 AND overdue != 0";
	}
	if($status == "over10") {
	    $q .= "WHERE overdue > 10 AND overdue != 0";
	}
	

	//Run MySQLi multi query
        $this->mysqli->multi_query($q);
               
        $result = [];
        
	//Process multiple results
        do {
            if($res = $this->mysqli->store_result()) {
                $result[] = $res->fetch_all(MYSQLI_ASSOC);
                $res->free();
            }
        } while ($this->mysqli->more_results() && $this->mysqli->next_result());        
        
	return (isset($result[0][0]["number"])) ? $result[0][0]["number"] : 0;
	
    }

    
    
    //Returns overdue jobs for given user and status
    //2012-11-25  Vic <v.rutkunas@pccsuk.com>
    
    public function getOverdueJobs($user, $status) {
        
        $networkID = ($user->NetworkID) ? $user->NetworkID : "NULL";
        $clientID = ($user->ClientID) ? $user->ClientID : "NULL";
        $branchID = ($user->BranchID) ? $user->BranchID : "NULL";
	
	$q = file_get_contents("application/sql/overdue_jobs.sql");
	
	$q = str_replace(["[networkID]", "[clientID]", "[branchID]"], [$networkID, $clientID, $branchID], $q);
	
        if($status == "1to5") {
	    $q .= " WHERE job.overdue >= 1 AND job.overdue <= 5 AND job.overdue != 0";
	}
	if($status == "6to10") {
	    $q .= " WHERE job.overdue >= 6 AND job.overdue <= 10 AND job.overdue != 0";
	}
	if($status == "over10") {
	    $q .= " WHERE job.overdue > 10 AND job.overdue != 0";
	}
        
	
	//Run MySQLi multi query
        $this->mysqli->multi_query($q);
        
        $result = [];
        
        do {
            if($res = $this->mysqli->store_result()) {
		$result[] = $res->fetch_all(MYSQLI_ASSOC);
		$res->free();
            }
        } while ($this->mysqli->more_results() && $this->mysqli->next_result());        

	if($this->mysqli->error) {
	    throw new Exception("Overdue Jobs query error: " . $this->mysqli->error);
	}
	
        return ["aaData" => $result[0]];
	
    }
    
    
    
    /**
    @action gets jobs with appraisal required data for user
    @input  user data
    @return jobs data
    @output void
    */
    
    public function getJobsAppraisalRequired($user) {
	
	$q = "	SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
			    
			    CASE
				WHEN	(@days :=   CASE
							WHEN job.RepairCompleteDate != ''
							THEN DATEDIFF(job.RepairCompleteDate, job.DateBooked)
							ELSE DATEDIFF(CURDATE(), job.DateBooked)
						    END
					) > turnaround(job.JobID)
				THEN	CONCAT('<span style=\'color:red; font-weight:bold;\'>', @days, '</span>')
				ELSE	CONCAT('<span style=\'font-weight:bold;\'>', @days, '</span>')
			    END					    AS '1',			    -- Column Days
			    
			    CASE
				WHEN (@stat := (SELECT TimelineStatus FROM status AS ts WHERE ts.StatusID = job.StatusID)) = 'booked' 
				THEN 'Job Booked'
			
				WHEN @stat = 'viewed' THEN 'Job Viewed'
				WHEN @stat = 'in_progress' THEN 'In Progress'
				WHEN @stat = 'parts_ordered' THEN 'Parts Ordered'
				WHEN @stat = 'parts_received' THEN 'Parts Received'
				WHEN @stat = 'site_visit' THEN 'Site Visit'
				WHEN @stat = 'revisit' THEN 'Site Revisit'
				WHEN @stat = 'closed' THEN 'Job Closed'
			
			    END					    AS '2',			    -- Column Status

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
					    IF(customer.Street != '', customer.Street, ''),
					    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
					    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
					    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
				    ),
				    '\'>',
				    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
				    '</span>'
			    )					     AS '3',			    -- Column Customer
			    
			    CONCAT( '<span class=\'hoverCell\' ',
				    CASE
					WHEN customer.ContactMobile != '' AND customer.ContactMobile IS NOT NULL
					THEN	CASE
						    WHEN customer.ContactHomePhone != '' AND customer.ContactHomePhone IS NOT NULL
						    THEN CONCAT('title=\'Alternative No: ', customer.ContactHomePhone, '\'')
						    ELSE 'title=\'No alternative phone number\''
						END
					ELSE 'title=\'No alternative phone number\''
				    END,
				    '>',
				    IF(customer.ContactMobile != '', customer.ContactMobile, customer.ContactHomePhone),
				    '</span>'
			    )					    AS '4',			    -- Column Phone

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
				    '\'>',
				    unit.UnitTypeName,
				    '</span>'
			    )					    AS '5',			    -- Column Product Type
			    
			    job.JobID				    AS '6',			    -- Column Skyline No
			    service.Acronym			    AS '7',			    -- Column Service Provider
			    job.ServiceCentreJobNo		    AS '8',			    -- Column Job No
                            '<input type=\'checkbox\' />'           AS '9'			    -- Column Select
			    
		FROM	    job
		
		LEFT JOIN   customer			    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title	    ON customer.CustomerTitleID = title.CustomerTitleID
		LEFT JOIN   product			    ON job.ProductID = product.ProductID
		LEFT JOIN   model			    ON job.ModelID = model.ModelID
		LEFT JOIN   manufacturer		    ON job.ManufacturerID = manufacturer.manufacturerID
		LEFT JOIN   unit_type AS unit		    ON model.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service	    ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   status			    ON job.StatusID = status.StatusID
		
		WHERE	    job.StatusID = 44 AND (job.OpenJobStatus != 'branch_repair' OR job.OpenJobStatus IS NULL)
	     ";
	
	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}
		
	$result = $this->query($this->conn, $q, $values);
	
	return ["aaData" => $result];
	
    }
    
    
    
    //Returns jobs with provided status (job.OpenJobStatus) for user
    //2012-11-16  Vic <v.rutkunas@pccsuk.com>
   
    public function getJobsByStatus($user, $status) {
	
        //changed display date format by thirumal..%Y-%m-%d replace with %d/%/m%Y
        
	$q = "	SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
			    
			    CASE
				WHEN	(@days :=   CASE
							WHEN job.RepairCompleteDate != ''
							THEN DATEDIFF(job.RepairCompleteDate, job.DateBooked)
							ELSE DATEDIFF(CURDATE(), job.DateBooked)
						    END
					) > turnaround(job.JobID)
				THEN	CONCAT('<span style=\'color:red; font-weight:bold;\'>', @days, '</span>')
				ELSE	CONCAT('<span style=\'font-weight:bold;\'>', @days, '</span>')
			    END					    AS '1',			    -- Column Days
			    
			    CASE
				WHEN (@stat := (SELECT TimelineStatus FROM status AS ts WHERE ts.StatusID = job.StatusID)) = 'booked' 
				THEN 'Job Booked'
			
				WHEN @stat = 'viewed' THEN 'Job Viewed'
				WHEN @stat = 'in_progress' THEN 'In Progress'
				WHEN @stat = 'parts_ordered' THEN 'Parts Ordered'
				WHEN @stat = 'parts_received' THEN 'Parts Received'
				WHEN @stat = 'site_visit' THEN 'Site Visit'
				WHEN @stat = 'revisit' THEN 'Site Revisit'
				WHEN @stat = 'closed' THEN 'Job Closed'
			
			    END					    AS '2',			    -- Column Status

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
					    IF(customer.Street != '', customer.Street, ''),
					    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
					    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
					    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
				    ),
				    '\'>',
				    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
				    '</span>'
			    )					     AS '3',			    -- Column Customer
			    
			    CONCAT( '<span class=\'hoverCell\' ',
				    CASE
					WHEN customer.ContactMobile != '' AND customer.ContactMobile IS NOT NULL
					THEN	CASE
						    WHEN customer.ContactHomePhone != '' AND customer.ContactHomePhone IS NOT NULL
						    THEN CONCAT('title=\'Alternative No: ', customer.ContactHomePhone, '\'')
						    ELSE 'title=\'No alternative phone number\''
						END
					ELSE 'title=\'No alternative phone number\''
				    END,
				    '>',
				    IF(customer.ContactMobile != '', customer.ContactMobile, customer.ContactHomePhone),
				    '</span>'
			    )					    AS '4',			    -- Column Phone

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
				    '\'>',
				    unit.UnitTypeName,
				    '</span>'
			    )					    AS '5',			    -- Column Product Type
			    
			    job.JobID				    AS '6',			    -- Column Skyline No
			    service.Acronym			    AS '7',			    -- Column Service Provider
			    job.ServiceCentreJobNo		    AS '8',			    -- Column Job No
                            '<input type=\'checkbox\' />'           AS '9',                         -- Column Select
			    
			    CASE
				WHEN job.RepairAuthStatus = 'rejected'
				THEN 1
				ELSE 0
			    END					    AS '10'
		
		FROM	   job
		
		LEFT JOIN   customer			    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title	    ON customer.CustomerTitleID = title.CustomerTitleID
		LEFT JOIN   product			    ON job.ProductID = product.ProductID
		LEFT JOIN   model			    ON job.ModelID = model.ModelID
		LEFT JOIN   manufacturer		    ON job.ManufacturerID = manufacturer.manufacturerID
		LEFT JOIN   unit_type AS unit		    ON model.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service	    ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   status			    ON job.StatusID = status.StatusID
	     ";

	$values = ["status" => $status];
	
	$q .= ' WHERE	job.OpenJobStatus = :status AND 
			(job.RepairAuthStatus != "required" OR job.RepairAuthStatus IS NULL) AND
			job.StatusID != 44 AND
			job.StatusID != 33 AND
			job.StatusID != 41
	      ';
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}
		
	$result = $this->query($this->conn,$q,$values);
	
	return ["aaData" => $result];
	
    }
    

    
    //Returns jobs with pending RA Query
    //2012-12-21  Vic <v.rutkunas@pccsuk.com>
    
    public function getJobsQuery($user) {

	$q = "	SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
			    
			    CASE
				WHEN	(@days :=   CASE
							WHEN job.RepairCompleteDate != ''
							THEN DATEDIFF(job.RepairCompleteDate, job.DateBooked)
							ELSE DATEDIFF(CURDATE(), job.DateBooked)
						    END
					) > turnaround(job.JobID)
				THEN	CONCAT('<span style=\'color:red; font-weight:bold;\'>', @days, '</span>')
				ELSE	CONCAT('<span style=\'font-weight:bold;\'>', @days, '</span>')
			    END					    AS '1',			    -- Column Days
			    
			    CASE
				WHEN (@stat := (SELECT TimelineStatus FROM status AS ts WHERE ts.StatusID = job.StatusID)) = 'booked' 
				THEN 'Job Booked'
			
				WHEN @stat = 'viewed' THEN 'Job Viewed'
				WHEN @stat = 'in_progress' THEN 'In Progress'
				WHEN @stat = 'parts_ordered' THEN 'Parts Ordered'
				WHEN @stat = 'parts_received' THEN 'Parts Received'
				WHEN @stat = 'site_visit' THEN 'Site Visit'
				WHEN @stat = 'revisit' THEN 'Site Revisit'
				WHEN @stat = 'closed' THEN 'Job Closed'
			
			    END					    AS '2',			    -- Column Status

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
					    IF(customer.Street != '', customer.Street, ''),
					    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
					    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
					    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
				    ),
				    '\'>',
				    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
				    '</span>'
			    )					     AS '3',			    -- Column Customer
			    
			    CONCAT( '<span class=\'hoverCell\' ',
				    CASE
					WHEN customer.ContactMobile != '' AND customer.ContactMobile IS NOT NULL
					THEN	CASE
						    WHEN customer.ContactHomePhone != '' AND customer.ContactHomePhone IS NOT NULL
						    THEN CONCAT('title=\'Alternative No: ', customer.ContactHomePhone, '\'')
						    ELSE 'title=\'No alternative phone number\''
						END
					ELSE 'title=\'No alternative phone number\''
				    END,
				    '>',
				    IF(customer.ContactMobile != '', customer.ContactMobile, customer.ContactHomePhone),
				    '</span>'
			    )					    AS '4',			    -- Column Phone

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
				    '\'>',
				    unit.UnitTypeName,
				    '</span>'
			    )					    AS '5',			    -- Column Product Type
			    
			    job.JobID				    AS '6',			    -- Column Skyline No
			    service.Acronym			    AS '7',			    -- Column Service Provider
			    job.ServiceCentreJobNo		    AS '8',			    -- Column Job No
                            '<input type=\'checkbox\' />'           AS '9',                         -- Column Select
			    
			    CASE
				WHEN job.RepairAuthStatus = 'rejected'
				THEN 1
				ELSE 0
			    END					    AS '10'
		
		FROM	   job
		
		LEFT JOIN   customer			    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title	    ON customer.CustomerTitleID = title.CustomerTitleID
		LEFT JOIN   product			    ON job.ProductID = product.ProductID
		LEFT JOIN   model			    ON product.ModelID = model.ModelID
		LEFT JOIN   manufacturer		    ON job.ManufacturerID = manufacturer.manufacturerID
		LEFT JOIN   unit_type AS unit		    ON model.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service	    ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   status			    ON job.StatusID = status.StatusID
		LEFT JOIN   ra_status AS ra		    ON job.RAStatusID = ra.RAStatusID
		
		WHERE	    ra.RAStatusName = 'General Query'
	     ";

	
	if($user->UserType == "Admin") {
	    
	    $result = $this->query($this->conn, $q);
	    
	} else {
	    
	    if($user->UserType == "Network") {
		$q .= " AND job.NetworkID = :networkID";
		$values["networkID"] = $user->NetworkID;
	    }
	    if($user->UserType == "Client") {
		$q .= " AND job.ClientID = :clientID";
		$values["clientID"] = $user->ClientID;
	    }
	    if($user->UserType == "Branch") {
		$q .= " AND job.BranchID = :branchID";
		$values["branchID"] = $user->BranchID;
	    }
	    if($user->UserType == "ServiceProvider") {
		$q .= " AND job.ServiceProviderID = :serviceProviderID";
		$values["serviceProviderID"] = $user->ServiceProviderID;
	    }

	    $result = $this->query($this->conn, $q, $values);
	
	}
	
	return ["aaData" => $result];
	
    }
    
 
    
    public function getJobsBranchRepair($user) {

	$q = "	SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
			    
			    CASE
				WHEN	(@days :=   CASE
							WHEN job.RepairCompleteDate != ''
							THEN DATEDIFF(job.RepairCompleteDate, job.DateBooked)
							ELSE DATEDIFF(CURDATE(), job.DateBooked)
						    END
					) > turnaround(job.JobID)
				THEN	CONCAT('<span style=\'color:red; font-weight:bold;\'>', @days, '</span>')
				ELSE	CONCAT('<span style=\'font-weight:bold;\'>', @days, '</span>')
			    END					    AS '1',			    -- Column Days
			    
			    CASE
				WHEN (@stat := (SELECT TimelineStatus FROM status AS ts WHERE ts.StatusID = job.StatusID)) = 'booked' 
				THEN 'Job Booked'
			
				WHEN @stat = 'viewed' THEN 'Job Viewed'
				WHEN @stat = 'in_progress' THEN 'In Progress'
				WHEN @stat = 'parts_ordered' THEN 'Parts Ordered'
				WHEN @stat = 'parts_received' THEN 'Parts Received'
				WHEN @stat = 'site_visit' THEN 'Site Visit'
				WHEN @stat = 'revisit' THEN 'Site Revisit'
				WHEN @stat = 'closed' THEN 'Job Closed'
			
			    END					    AS '2',			    -- Column Status

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
					    IF(customer.Street != '', customer.Street, ''),
					    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
					    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
					    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
				    ),
				    '\'>',
				    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
				    '</span>'
			    )					     AS '3',			    -- Column Customer
			    
			    CONCAT( '<span class=\'hoverCell\' ',
				    CASE
					WHEN customer.ContactMobile != '' AND customer.ContactMobile IS NOT NULL
					THEN	CASE
						    WHEN customer.ContactHomePhone != '' AND customer.ContactHomePhone IS NOT NULL
						    THEN CONCAT('title=\'Alternative No: ', customer.ContactHomePhone, '\'')
						    ELSE 'title=\'No alternative phone number\''
						END
					ELSE 'title=\'No alternative phone number\''
				    END,
				    '>',
				    IF(customer.ContactMobile != '', customer.ContactMobile, customer.ContactHomePhone),
				    '</span>'
			    )					    AS '4',			    -- Column Phone

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
				    '\'>',
				    unit.UnitTypeName,
				    '</span>'
			    )					    AS '5',			    -- Column Product Type
			    
			    job.JobID				    AS '6',			    -- Column Skyline No
			    service.Acronym			    AS '7',			    -- Column Service Provider
			    job.ServiceCentreJobNo		    AS '8',			    -- Column Job No
                            '<input type=\'checkbox\' />'           AS '9',                         -- Column Select
			    
			    CASE
				WHEN job.RepairAuthStatus = 'rejected'
				THEN 1
				ELSE 0
			    END					    AS '10'
		
		FROM	    job
		
		LEFT JOIN   customer			    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title	    ON customer.CustomerTitleID = title.CustomerTitleID
		LEFT JOIN   product			    ON job.ProductID = product.ProductID
		LEFT JOIN   model			    ON product.ModelID = model.ModelID
		LEFT JOIN   manufacturer		    ON job.ManufacturerID = manufacturer.manufacturerID
		LEFT JOIN   unit_type AS unit		    ON model.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service	    ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   status			    ON job.StatusID = status.StatusID
		
		WHERE	    job.OpenJobStatus = 'branch_repair'
	     ";

	
	if($user->UserType == "Admin") {
	    
	    $result = $this->query($this->conn, $q);
	    
	} else {
	    
	    if($user->UserType == "Network") {
		$q .= " AND job.NetworkID = :networkID";
		$values["networkID"] = $user->NetworkID;
	    }
	    if($user->UserType == "Client") {
		$q .= " AND job.ClientID = :clientID";
		$values["clientID"] = $user->ClientID;
	    }
	    if($user->UserType == "Branch") {
		$q .= " AND job.BranchID = :branchID";
		$values["branchID"] = $user->BranchID;
	    }
	    if($user->UserType == "ServiceProvider") {
		$q .= " AND job.ServiceProviderID = :serviceProviderID";
		$values["serviceProviderID"] = $user->ServiceProviderID;
	    }

	    $result = $this->query($this->conn, $q, $values);
	
	}
	
	return ["aaData" => $result];
	
    }
    
    
    
    //Returns jobs where authorisation is required, but authorisation number is null (job.AuthorisationNo) for user
    //2012-11-29  Vic <v.rutkunas@pccsuk.com>
   
    public function getJobsAuthRequired($user) {
	
	$q = "	SELECT	    DATE_FORMAT(job.DateBooked, '%d/%m/%Y') AS '0',			    -- Column Date
			    
			    CASE
				WHEN	(@days :=   CASE
							WHEN job.RepairCompleteDate != ''
							THEN DATEDIFF(job.RepairCompleteDate, job.DateBooked)
							ELSE DATEDIFF(CURDATE(), job.DateBooked)
						    END
					) > turnaround(job.JobID)
				THEN	CONCAT('<span style=\'color:red; font-weight:bold;\'>', @days, '</span>')
				ELSE	CONCAT('<span style=\'font-weight:bold;\'>', @days, '</span>')
			    END					    AS '1',			    -- Column Days
			    
			    CASE
				WHEN (@stat := (SELECT TimelineStatus FROM status AS ts WHERE ts.StatusID = job.StatusID)) = 'booked' 
				THEN 'Job Booked'
			
				WHEN @stat = 'viewed' THEN 'Job Viewed'
				WHEN @stat = 'in_progress' THEN 'In Progress'
				WHEN @stat = 'parts_ordered' THEN 'Parts Ordered'
				WHEN @stat = 'parts_received' THEN 'Parts Received'
				WHEN @stat = 'site_visit' THEN 'Site Visit'
				WHEN @stat = 'revisit' THEN 'Site Revisit'
				WHEN @stat = 'closed' THEN 'Job Closed'
			
			    END					    AS '2',			    -- Column Status

			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT( IF(customer.BuildingNameNumber != '', CONCAT(customer.BuildingNameNumber, ', '), ''),
					    IF(customer.Street != '', customer.Street, ''),
					    IF(customer.LocalArea != '', CONCAT(', ', customer.LocalArea), ''),
					    IF(customer.TownCity != '', CONCAT(', ', customer.TownCity), ''),
					    IF(customer.PostalCode != '', CONCAT(', ', customer.PostalCode), '')
				    ),
				    '\'>',
				    CONCAT( customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName),
				    '</span>'
			    )					    AS '3',			    -- Column Customer
			    
			    job.AuthorisationNo			    AS '4',			    -- Column Auth No
			    
			    CONCAT( '<span class=\'hoverCell\' title=\'',
				    CONCAT(manufacturer.manufacturerName, ' Model No: ', model.modelNumber),
				    '\'>',
				    unit.UnitTypeName,
				    '</span>'
			    )					    AS '5',			    -- Column Product Type
			    
			    job.JobID				    AS '6',			    -- Column Skyline No
			    service.Acronym			    AS '7',			    -- Column Service Provider
			    job.ServiceCentreJobNo		    AS '8',			    -- Column Job No
                            '<input type=\'checkbox\' />'           AS '9'			    -- Column Select
		
		FROM	   job
		
		LEFT JOIN   customer			    ON job.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title	    ON customer.CustomerTitleID = title.CustomerTitleID
		LEFT JOIN   product			    ON job.ProductID = product.ProductID
		LEFT JOIN   model			    ON job.ModelID = model.ModelID
		LEFT JOIN   manufacturer		    ON job.ManufacturerID = manufacturer.manufacturerID
		LEFT JOIN   unit_type AS unit		    ON model.UnitTypeID = unit.UnitTypeID
		LEFT JOIN   service_provider AS service	    ON job.ServiceProviderID = service.ServiceProviderID
		LEFT JOIN   status			    ON job.StatusID = status.StatusID
		LEFT JOIN   service_type AS st		    ON job.ServiceTypeID = st.ServiceTypeID
		LEFT JOIN   ra_status AS ra		    ON job.RAStatusID = ra.RAStatusID
		
		/*WHERE	    (job.AuthorisationNo IS NULL OR job.AuthorisationNo = '') AND 
			    manufacturer.AuthorisationRequired = 'Yes' AND
			    job.OpenJobStatus IS NOT NULL AND
			    st.ServiceTypeName = 'Warranty'*/
			    
		WHERE	    job.RepairAuthStatus = 'required' AND
			    ra.RAStatusCode !=	(
						    SELECT  RAStatusCode
						    FROM    ra_status
						    WHERE   RAStatusName = 'General Query'
						) AND
			    job.StatusID != 44
	     ";

	$values = [];
	
	if($user->UserType == "Network") {
	    $q .= " AND job.NetworkID = :networkID";
	    $values["networkID"] = $user->NetworkID;
	}
	if($user->UserType == "Client") {
	    $q .= " AND job.ClientID = :clientID";
	    $values["clientID"] = $user->ClientID;
	}
	if($user->UserType == "Branch") {
	    $q .= " AND job.BranchID = :branchID";
	    $values["branchID"] = $user->BranchID;
	}
	if($user->UserType == "ServiceProvider") {
	    $q .= " AND job.ServiceProviderID = :serviceProviderID";
	    $values["serviceProviderID"] = $user->ServiceProviderID;
	}
		
	$result = $this->query($this->conn,$q,$values);
	
	return array("aaData" => $result);
	
    }

    
    
    /**
    Updates job status (job.OpenJobStatus) for given jobs
    2012-11-16  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function updateOpenJobsStatus($data) {

	$jobs = json_decode($data["jobs"]);
	$status = $data["status"];
        
        $EmailModel = $this->controller->loadModel('Email');  
	
	foreach($jobs as $id) {
	    
	    if($status == "closed") {
		$q = "	UPDATE	job 
			SET	OpenJobStatus = :status, 
				ModifiedUserID = :ModifiedUserID, 
				ModifiedDate = NOW(),
				ClosedDate = NOW(),
				StatusID = 25
			WHERE	JobID = :id";
                
                
                $EmailModel->sendJobCompleteEmail($id);
                
	    } else {
		$q = "UPDATE job SET OpenJobStatus = :status, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :id";
	    }
	    
	    $values = ["status" => $status, "id" => $id, "ModifiedUserID" => $this->controller->user->UserID];
	    $this->execute($this->conn, $q, $values);
            
	    /*
            $contactModel = $this->loadModel('ContactHistory');
            $contact = ['JobID' => $id,
                        'ContactHistoryActionID' => 16,
                        'BrandID' => $this->controller->user->DefaultBrandID,
                        'ContactDate' => date('Y-m-d'), 
                        'ContactTime' => date('H:i'),
                        'UserCode' => $this->controller->user->UserID,
                        'Note' => $data['status'] 
                       ];
            $result = $contactModel->create($contact);
	    */
	    
	}
	
    }

    
    
    /**
    Updates job status (job.OpenJobStatus)
    2012-12-14  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function updateOpenJobStatus($jobID, $status) {
	
	$q = "UPDATE job SET OpenJobStatus = :status, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :id";
	$values = ["status" => $status, "id" => $jobID, "ModifiedUserID" => $this->controller->user->UserID];
	$this->execute($this->conn, $q, $values);
	
    }
    
    
    
   /**
    * getPercentageJobsOverDays
    * 
    * Returns the percentage of jobs for a sepecific usertyoe over a certain
    *  number of days
    * 
    * @param integer $days  The number of days to calculare open jobs for
    *        string $filter
    * 
    * @return integer  The ID of the Service Centre
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getPercentageJobsOverDays($days,$filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			COUNT(`JobID`) / (SELECT COUNT(`JobID`) FROM `job`) * 100 AS Percent
		FROM
			`job` j,
                        `service_provider` sp
		WHERE
			`DateBooked` < DATE_SUB(NOW(), INTERVAL $days DAY)
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Percent']);                                      /* Job exists so return Job ID */
        } else {
            return(0);                                                       /* Not found return 0 */
        }
    }
    
    
    
   /**
    * getNumberJobsBetweenDays
    * 
    * Returns the percentage of jobs for a sepecific usertyoe over a certain
    *  number of days
    * 
    * @param integer $daysFrom  Lower bound of days search range
    *        integer $daysTo    Upper bound of days search range
    *        string $filter
    * 
    * @return integer  The ID of the Service Centre
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getNumberJobsBetweenDays($daysFrom, $daysTo,$filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND j.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND j.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND j.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			COUNT(`JobID`) AS `Count`
		FROM
			`job` j LEFT JOIN `status` s ON s.`StatusID` = j.`StatusID`
                                LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
			`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL $daysTo DAY) AND DATE_SUB(NOW(), INTERVAL $daysFrom DAY)
                        $filter
                        $type
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                      /* Got a result*/
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
   /**
    * getNumberComnpletedJobsBetweenDays
    * 
    * Returns the nuber of jobs for a sepecific usertyoe over a certain
    *  number of days range for completion date
    * 
    * @param integer $daysFrom  Lower bound of days search range
    *        integer $daysTo    Upper bound of days search range
    *        string $filter
    * 
    * @return integer  The ID of the Service Centre
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getNumberCompletedJobsBetweenDays($daysFrom, $daysTo, $filter = "", $closedfield = 'ClosedDate',$data = array()) {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND j.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND j.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND j.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        
        //Brand Filter
        if(isset($data['brand']) && $data['brand']) {
          $type .= ' AND t8.BrandID=' . $data['brand']; 
        }    
        
        //Status Type Filter
        if(isset($data['sType']) && $data['sType']) {
            $type .= ' AND j.StatusID=' . $this->conn->quote($data['sType']);
        }
        
        //Service Centre Type Filter
        if(isset($data['scType']) && $data['scType']) {
            $type .= ' AND j.ServiceCentreID=' . $this->conn->quote($data['scType']);
        }

        //Manufacturer Filter
        if(isset($data['manufacturer']) && $data['manufacturer']) {
           $type .= ' AND j.ManufacturerID=' . $this->conn->quote($data['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($data['serviceProvider']) && $data['serviceProvider']) {
           $type .= ' AND j.ServiceProviderID=' . $this->conn->quote($data['serviceProvider']);
        }

        //Client Filter
        if(isset($data['client']) && $data['client']) {
           $type .= ' AND j.ClientID=' . $this->conn->quote($data['client']);
        }
        
        //Network Filter
        if(isset($data['network']) && $data['network']) {
           $type .= ' AND j.NetworkID=' . $this->conn->quote($data['network']);
        }
        
        //Branch Filter
        if(isset($data['branch']) && $data['branch']) {
           $type .= ' AND j.BranchID=' . $this->conn->quote($data['branch']);
        }
        
        //Unit Type Filter
        if(isset($data['unitType']) && $data['unitType']) {
           $type .= ' AND t4.UnitTypeID=' . $this->conn->quote($data['unitType']);
        }
        
        //Skill Set Filter
        if(isset($data['skillSet']) && $data['skillSet']) {
           $type .= ' AND t10.RepairSkillID=' . $this->conn->quote($data['skillSet']);
        }
        
        $sql = "
                SELECT
			COUNT(`JobID`) AS `Count`
		FROM
			`job` j 
                        LEFT JOIN `status` s ON s.`StatusID` = j.`StatusID`
                        LEFT JOIN product               AS t2 ON j.ProductID = t2.ProductID
                        LEFT JOIN model                 AS t3 ON j.ModelID = t3.ModelID
                        LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                        LEFT JOIN manufacturer          AS t5 ON j.ManufacturerID = t5.ManufacturerID
                        LEFT JOIN service_type          AS t7 ON j.ServiceTypeID = t7.ServiceTypeID
                        LEFT JOIN brand_branch          AS t8 on j.BranchID = t8.BranchID
                        LEFT JOIN network               AS t9 on j.NetworkID = t9.NetworkID
                        LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID
                        LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
			ABS(DATEDIFF($closedfield, `DateBooked`)) < $daysTo 
                        AND ABS(DATEDIFF($closedfield, `DateBooked`)) >= $daysFrom 
                        $filter
                        $type
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                      /* Got a result*/
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
    /* Add new function getNumberCompletedJobs to filter Closed jobs according to gauage preferences
     * added by user.
     * 
     */
    public function getNumberCompletedJobs($guage, $filter = "", $closedfield = 'ClosedDate',$data = array(),$gaugePrefs=array()) {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND j.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND j.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND j.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $filter .= " AND j.CompletionStatus IS NOT NULL";
        
        //Brand Filter
        if(isset($data['brand']) && $data['brand']) {
          $type .= ' AND t8.BrandID=' . $data['brand']; 
        }    
        
        //Service Centre Type Filter
        if(isset($data['scType']) && $data['scType']) {
            $type .= ' AND j.ServiceCentreID=' . $this->conn->quote($data['scType']);
        }

        //Manufacturer Filter
        if(isset($data['manufacturer']) && $data['manufacturer']) {
           $type .= ' AND j.ManufacturerID=' . $this->conn->quote($data['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($data['serviceProvider']) && $data['serviceProvider']) {
           $type .= ' AND j.ServiceProviderID=' . $this->conn->quote($data['serviceProvider']);
        }

        //Client Filter
        if(isset($data['client']) && $data['client']) {
           $type .= ' AND j.ClientID=' . $this->conn->quote($data['client']);
        }
        
        //Network Filter
        if(isset($data['network']) && $data['network']) {
           $type .= ' AND j.NetworkID=' . $this->conn->quote($data['network']);
        }
        
        //Branch Filter
        if(isset($data['branch']) && $data['branch']) {
           $type .= ' AND j.BranchID=' . $this->conn->quote($data['branch']);
        }
        
        //Unit Type Filter
        if(isset($data['unitType']) && $data['unitType']) {
           $type .= ' AND t4.UnitTypeID=' . $this->conn->quote($data['unitType']);
        }
        
        //Skill Set Filter
        if(isset($data['skillSet']) && $data['skillSet']) {
           $type .= ' AND t10.RepairSkillID=' . $this->conn->quote($data['skillSet']);
        }
        
//        echo $guage."<BR>";
        if($guage !== "0" && strtolower($guage) !== strtolower('All Others')){
            $filter .= " AND j.CompletionStatus='{$guage}'";
        }
        
        if($guage === 0 || strtolower($guage) === strtolower('All Others')){ 
            
            if(count($gaugePrefs) > 0){ 
                $gaugePrefString="";
                 foreach($gaugePrefs as $ini=>$gaugePref){ 
                     if($ini < (count($gaugePrefs)-1)){
                         $gaugePrefString .= "'".$gaugePref['GaDialStatus']."',";
                     }
                }
                $gaugePrefString = substr($gaugePrefString,0,-1);
                
                if($gaugePrefString <>""){
                    $filter .= " AND j.CompletionStatus NOT IN ({$gaugePrefString})";
                }
            }
            
            //Status Type Filter
            if(isset($data['sType']) && $data['sType'] && strtolower($data['sType']) != strtolower("All Others")) {
                $type .= ' AND j.CompletionStatus=' . $this->conn->quote($data['sType']);
            }
            
        }
        
        $type .= " AND s.CompletionStatusID IS NOT NULL";
        
        $sql = "
                SELECT
			COUNT(`JobID`) AS `Count`
		FROM
			`job` j 
                        LEFT JOIN `completion_status` s ON s.`CompletionStatus` = j.`CompletionStatus`
                        LEFT JOIN product               AS t2 ON j.ProductID = t2.ProductID
                        LEFT JOIN model                 AS t3 ON j.ModelID = t3.ModelID
                        LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                        LEFT JOIN manufacturer          AS t5 ON j.ManufacturerID = t5.ManufacturerID
                        LEFT JOIN service_type          AS t7 ON j.ServiceTypeID = t7.ServiceTypeID
                        LEFT JOIN brand_branch          AS t8 on j.BranchID = t8.BranchID
                        LEFT JOIN network               AS t9 on j.NetworkID = t9.NetworkID
                        LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID
                        LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE 1
                        $filter
                        $type
               ";
        
//        echo $sql."<BR>";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                      /* Got a result*/
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
   /**
    * getTotalMatchingJobs
    * 
    * Returns the total number of jobs matching the criteria passed for a specific
    * user type.
    * 
    * @param string $filter
    * 
    * @return integer  The ID of the Service Centre
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getTotalMatchingJobs($filter = "",$data = array()) {
        
        //if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND j.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND j.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND j.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        //Brand Filter
        if(isset($data['brand']) && $data['brand']) {
          $type .= ' AND t8.BrandID=' . $data['brand']; 
        }    
        
        //Service Centre Type Filter
        if(isset($data['scType']) && $data['scType']) {
            $type .= ' AND j.ServiceCentreID=' . $this->conn->quote($data['scType']);
        }

        //Manufacturer Filter
        if(isset($data['manufacturer']) && $data['manufacturer']) {
           $type .= ' AND j.ManufacturerID=' . $this->conn->quote($data['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($data['serviceProvider']) && $data['serviceProvider']) {
           $type .= ' AND j.ServiceProviderID=' . $this->conn->quote($data['serviceProvider']);
        }

        //Client Filter
        if(isset($data['client']) && $data['client']) {
           $type .= ' AND j.ClientID=' . $this->conn->quote($data['client']);
        }
        
        //Network Filter
        if(isset($data['network']) && $data['network']) {
           $type .= ' AND j.NetworkID=' . $this->conn->quote($data['network']);
        }
        
        //Branch Filter
        if(isset($data['branch']) && $data['branch']) {
           $type .= ' AND j.BranchID=' . $this->conn->quote($data['branch']);
        }
        
        //Unit Type Filter
        if(isset($data['unitType']) && $data['unitType']) {
           $type .= ' AND t4.UnitTypeID=' . $this->conn->quote($data['unitType']);
        }
        
        //Skill Set Filter
        if(isset($data['skillSet']) && $data['skillSet']) {
           $type .= ' AND t10.RepairSkillID=' . $this->conn->quote($data['skillSet']);
        }
        
        $type .= ' AND s.CompletionStatusID IS NOT NULL';
        
        $sql = "
                SELECT
			COUNT(j.`JobID`) AS `Count`
                FROM
                                   
                        `job` j 
                        LEFT JOIN completion_status `s` ON s.CompletionStatus = j.CompletionStatus
                        LEFT JOIN product               AS t2 ON j.ProductID = t2.ProductID
                        LEFT JOIN model                 AS t3 ON j.ModelID = t3.ModelID
                        LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                        LEFT JOIN manufacturer          AS t5 ON j.ManufacturerID = t5.ManufacturerID
                        LEFT JOIN service_type          AS t7 ON j.ServiceTypeID = t7.ServiceTypeID
                        LEFT JOIN brand_branch          AS t8 on j.BranchID = t8.BranchID
                        LEFT JOIN network               AS t9 on j.NetworkID = t9.NetworkID
                        LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID
                        LEFT JOIN service_provider      AS sp ON j.ServiceProviderID = sp.ServiceProviderID
                        LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=j.NetworkID AND sta.ClientID=j.ClientID
                        LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status='Active'
                WHERE
                        $filter
                        $type
               ";

        
//        echo $sql."<BR/>";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                        /* Got a result*/
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    /*
     *                   LEFT JOIN status                AS js ON js.StatusID = j.StatusID
                  LEFT JOIN product               AS t2 ON j.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON j.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN manufacturer          AS t5 ON j.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON j.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON j.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on j.BranchID = t8.BranchID'; 
     */
    
    
    /**
     * getJobsByDatesByServiceCentreCount
     * 
     * Returns the total number of jobs by Service Centre booked between two dates
     * 
     * @param integer $dayFrom nymber of days booked before today start range
     * @param integer $dayTo nymber of days  booked before today end range
     * @param string $filter
     * @param string $orderby Column to order by
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getJobsByDatesByServiceCentreCount($dayFrom, $dayTo, $filter = "", $orderby = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND j.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			sp.`CompanyName` AS `Name`,
			sp.`ServiceProviderID`,
			COUNT(j.`JobID`) AS `Count`,
			(
                            SELECT
                                    COUNT(j.`JobID`) AS `CountTotal`
                            FROM
                                    `job` j
                            WHERE 
                                    sp.`ServiceProviderID` =  j.`ServiceProviderID`
                                    $filter
                                    $type
			) AS `Total`
                FROM
			`service_provider` sp LEFT JOIN `job` j ON  sp.`ServiceProviderID` = j.`ServiceProviderID`
                                              LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` 
	        WHERE
			j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL $dayFrom DAY) AND DATE_SUB(NOW(), INTERVAL $dayTo DAY)
                        $filter
                        $type
                GROUP BY
			sp.`CompanyName`
               ";
        
        if ($orderby != "") $sql.=" ORDER BY $orderby";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Got a result*/
        } else {
            return(array());                                                    /* Not found return blank array */
        }
    }

    
    
    /**
     * getJobsByDatesByManufacturer
     * 
     * Returns the total number of jobs by Service Centre booked between two dates
     * 
     * @param integer $dayFrom nymber of days booked before today start range
     * @param integer $dayTo nymber of days  booked before today end range
     * @param string $filter
     * @param string $orderby Column to order by
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getJobsByDatesByManufacturerCount($dayFrom, $dayTo, $filter = "", $orderby = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			mft.`ManufacturerName` AS `Name`,
                        mft.`ManufacturerID`,
			COUNT(j.`JobID`) AS `Count`,
			(
                            SELECT
                                    COUNT(j.`JobID`) AS `CountTotal`
                            FROM
                                    `job` j
                            WHERE 
                                    mft.`ManufacturerID` = j.`ManufacturerID`
                                    ".str_replace('sp.','j.',$filter)."
                                    $type
			) AS `Total`
                FROM
			`manufacturer` mft LEFT JOIN `job` j ON  mft.`ManufacturerID` = j.`ManufacturerID`
                                           LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` 
	        WHERE
			j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL $dayFrom DAY) AND DATE_SUB(NOW(), INTERVAL $dayTo DAY)
                        ".str_replace('sp.','j.',$filter)."
                        $type
                GROUP BY
			mft.`ManufacturerName`
               ";
        

        
        if ($orderby != "") $sql.=" ORDER BY $orderby";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Got a result*/
        } else {
            return(array());                                                    /* Not found return blank array */
        }
    }
    
    
    
    /**
     * getJobsByDatesBytworkCount
     * 
     * Returns the total number of jobs by Network booked between two dates
     * 
     * @param integer $dayFrom nymber of days booked before today start range
     * @param integer $dayTo nymber of days  booked before today end range
     * @param string $filter
     * @param string $orderby Column to order by
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getJobsByDatesByNetworkCount($dayFrom, $dayTo, $filter = "", $orderby = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			n.`CompanyName` AS `Name`,
                        n.`NetworkID`,
			COUNT(j.`JobID`) AS `Count`,
			(
                            SELECT
                                    COUNT(j.`JobID`) AS `CountTotal`
                            FROM
                                    `job` j                                     
                            WHERE 
                                    n.`NetworkID` = j.`NetworkID`
                                    ".str_replace('sp.','j.',$filter)."
                                    $type
			) AS `Total`
                FROM
			`network` n LEFT JOIN `job` j ON  n.`NetworkID` = j.`NetworkID`
                                    LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` 

	        WHERE
			j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL $dayFrom DAY) AND DATE_SUB(NOW(), INTERVAL $dayTo DAY)
                        ".str_replace('sp.','j.',$filter)."
                        $type
                GROUP BY
			n.`CompanyName`
               ";
        
        
        if ($orderby != "") $sql.=" ORDER BY $orderby";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Got a result*/
        } else {
            return(array());                                                    /* Not found return blank array */
        }
    }
    
    
    
   /**
     * getJobsByDatesByClientCount
     * 
     * Returns the total number of jobs by Network booked between two dates
     * 
     * @param integer $dayFrom nymber of days booked before today start range
     * @param integer $dayTo nymber of days  booked before today end range
     * @param string $filter
     * @param string $orderby Column to order by
     * 
     * @return integer  The ID of the Service Centre
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getJobsByDatesByClientCount($dayFrom, $dayTo, $filter = "", $orderby = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND `ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			clt.`ClientName` AS `Name`,
                        clt.`ClientID`,
			COUNT(j.`JobID`) AS `Count`,
			(
                            SELECT
                                    COUNT(j.`JobID`) AS `CountTotal`
                            FROM
                                    `job` j                                     
                            WHERE 
                                    clt.`ClientID` = j.`ClientID`
                                    ".str_replace('sp.','j.',$filter)."
                                    $type
			) AS `Total`
                FROM
			`client` clt LEFT JOIN `job` j ON  clt.`ClientID` = j.`ClientID`
                                     LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` 
	        WHERE
			j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL $dayFrom DAY) AND DATE_SUB(NOW(), INTERVAL $dayTo DAY)
                        ".str_replace('sp.','j.',$filter)."
                        $type
                GROUP BY
			clt.`ClientName`
               ";
        
        
        if ($orderby != "") $sql.=" ORDER BY $orderby";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Got a result*/
        } else {
            return(array());                                                    /* Not found return blank array */
        }
    }
    
    
    
   /**
    * getPercentageJobsAwaitingParts
    * 
    * Get the number of jobs awaiting parts
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return integer   Number of jobs awaiting parts     
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsAwaitingParts($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			COUNT(`JobID`) AS `Count`
		FROM
			`job` j,
			`status` s,
                        `service_provider` sp
		WHERE
			j.`StatusID` = s.`StatusID`
			AND (
				s.`StatusName` = '11 PARTS REQUIRED'
				OR s.`StatusName` = '12 PARTS ORDERED'
			)
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                        /* Job exists so return number of jobs */
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
    
   /**
    * getAverageDaysFromBookingDate
    * 
    * Get the average number of days from the booking date until tofay if still
    * open or the closed date if closed.
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return integer  The ID of the Service Centre
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getAverageDaysFromBookingDate($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			AVG(DATEDIFF(IF (ISNULL(j.ClosedDate),NOW(),j.ClosedDate), j.`DateBooked`)) AS `AvCount`
		FROM
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp
		WHERE
			j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
               ";
       
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['AvCount']);                                      /* Job exists so return average number days */
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }

    
    
   /**
    * getJobsAwaitingAppointment
    * 
    * Get the number of jobs awaiting an appointment
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return integer   Number of jobs awaiting an appointment    
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsAwaitingAppointment($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			COUNT(`JobID`) AS `Count`
		FROM
			`job` j,
			`status` s,
                        `service_provider` sp
		WHERE
			j.`StatusID` = s.`StatusID`
			AND (
				s.`StatusName` = '05 FIELD CALL REQUIRED'
				OR s.`StatusName` = '14 2ND FIELD CALL REQ.'
			)
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                        /* Job exists so return number of jobs */
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
    
    public function saveTableState($tableID, $data, $user) {
	
	$q = "SELECT * FROM table_state WHERE UserID = :userID AND TableID = :tableID";
	$values = array("userID" => $user->UserID, "tableID" => $tableID);
	$result = $this->query($this->conn, $q, $values);
        
	if(count($result)>0) {
	    //update
	    
	    //for some reason PDO executes query (data written to the database) but also throws a "General Error"
	    //MySQLi used instead, because it doesn't seem to cause the same problem
	    
	    //$q = "UPDATE table_state SET StateData = :data WHERE UserID = :userID AND TableID = :tableID";
            //$values = array("data" => $data, "userID" => $user->UserID, "tableID" => $tableID);
            //$this->query($this->conn, $q, $values);
	    $q = "UPDATE table_state SET StateData = ? WHERE UserID = ? AND TableID = ?";
	    $stmt = $this->mysqli->prepare($q);
	    $stmt->bind_param("sis", $data, $user->UserID, $tableID);
	    $stmt->execute();
            //$this->mysqli->query($q);
	} else {
	    //insert
            //$q = "INSERT INTO table_state VALUES (:userID, :tableID, :data)";
            //$values = array("data" => $data, "userID" => $user->UserID, "tableID" => $tableID);
            //$this->query($this->conn, $q, $values);
            $q = "INSERT INTO table_state VALUES (?, ?, ?)";
	    $stmt = $this->mysqli->prepare($q);
	    $stmt->bind_param("ssi", $user->UserID, $tableID, $data);
	    $stmt->execute();
	}
	
    }
    
  
    
    public function loadTableState($tableID, $user) {
        
        
        $q = "SELECT * FROM table_state WHERE UserID = :userID AND TableID = :tableID LIMIT 1";
        $values = array("userID" => $user->UserID, "tableID" => $tableID);
        $result = $this->query($this->conn, $q, $values);
        
        return(count($result)>0 ? json_encode($result[0]["StateData"]) : false);
        
    }
    
    
 
   /**
    * getJobsAwaitingAppointment
    * 
    * Get the number of jobs awaiting an appointment
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return integer   Number of jobs awaiting an appointment    
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsAverageEstimateOutsanding($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			AVG(DATEDIFF(IF (ISNULL(j.ClosedDate),NOW(),j.ClosedDate), j.`DateBooked`)) AS `AvCount`
		FROM
			`job` j,
			`status` s,
                        `service_provider` sp
		WHERE
			j.`StatusID` = s.`StatusID`
			AND s.`StatusName` = '08 ESTIMATE SENT'
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['AvCount']);                                      /* Job exists so return number of jobs */
        } else {
            return(0);                                                          /* Not found return 0 */
        }
    }
    
    
    
   /**
    * getJobsByManufacturer
    * 
    * Get the number of jobs by manufacturer
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs by manufacturer   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByManufacturer($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        mft.`ManufacturerName` AS `Name`,
			COUNT(j.`ManufacturerID`) AS `Count`
		FROM
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
			`manufacturer` mft,
                        `service_provider` sp
		WHERE
			j.`ManufacturerID` = mft.`ManufacturerID`
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                GROUP BY
			mft.`ManufacturerName`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsByStatusCount
    * 
    * Get the number of jobs by status
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByStatusCount($filter = "") {
        
        //if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        IF (ISNULL(s.`StatusName`),'01 UNALLOCATED JOB',s.`StatusName`) AS `Name`,
                        IF (ISNULL(s.`Colour`),'ef1a1a',s.`Colour`) AS `Colour`,
			COUNT(j.`StatusID`) AS `Count`
		FROM
			`job` j LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
                                LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
                        
                        $filter
                        $type
                GROUP BY
                        s.`StatusName`
               ";

        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
   /**
    * getJobsByCompletionCount
    * 
    * Get the number of jobs by status
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByCompletionCount($filter = "") {
        
        //if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        j.`CompletionStatus` AS `Name`,
			COUNT(j.`CompletionStatus`) AS `Count`
		FROM
			`job` j LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
                                LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
                        $filter
                        $type
                GROUP BY
                        j.`CompletionStatus`
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }    
    
   /**
    * countJobsGraphicalAnalysisFilter
    * 
    * Get a count of the number of jobs matching a filter. This is used by the 
    * graphical analysis screens to get a total for the matching jobs.
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Total job matching the filter   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function countJobsGraphicalAnalysisFilter($filter = "") {
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			COUNT(j.`JobID`) AS `Count`
		FROM
			`job` j LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
                        LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
		WHERE
                        $filter
                        $type
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['Count']);                                           /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsByCountyCount
    * 
    * Get the number of jobs by county / unitary authority
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByCountyCount($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        cty.`Name` AS `Name`,
                        ctry.`InternationalCode` AS `Country`,
			COUNT(cty.`CountyCode`) AS `Count`
		FROM			
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp,
                        `customer` c,
                        `county` cty,
                        `country` ctry
		WHERE
			j.`CustomerID` = c.`CustomerID`
			AND c.`CountyID` = cty.`CountyID`
                        AND cty.`CountryID` = ctry.`CountryID`
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                GROUP BY
			cty.`Name`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsByCountryCount
    * 
    * Get the number of jobs by county / unitary authority
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByCountryCount($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        ctry.`Name` AS `Name`,
			COUNT(ctry.`CountryCode`) AS `Count`
		FROM			
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp,
                        `customer` c,
                        `county` cty,
                        `country` ctry
		WHERE
			j.`CustomerID` = c.`CustomerID`
			AND c.`CountyID` = cty.`CountyID`
			AND cty.`CountryID` = ctry.`CountryID`
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                GROUP BY
			ctry.`Name`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsByCountryCount
    * 
    * Get the number of jobs by county / unitary authority
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getCountrysByJobs($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT DISTINCT
                        ctry.`Name` AS `Name`,
			ctry.`InternationalCode`
		FROM			
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp,
                        `customer` c,
                        `county` cty,
                        `country` ctry
		WHERE
			j.`CustomerID` = c.`CustomerID`
			AND c.`CountyID` = cty.`CountyID`
			AND cty.`CountryID` = ctry.`CountryID`
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                ORDER BY
			ctry.`Name`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsByAppointmentEngineer
    * 
    * Get the number of jobs by engineer serrviced
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsByAppointmentEngineer($filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`) AS `Name`,
                        spe.`RouteColour` AS `Colour`,
			COUNT(spe.`ServiceProviderEngineerID`) AS `Count`
		FROM			
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp,
                        `appointment` a,
                        `service_provider_engineer` spe
		WHERE
			j.`JobID` = a.`JobID`
			AND a.`ServiceProviderEngineerID` = spe.`ServiceProviderEngineerID`
                        AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                GROUP BY
			CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`)
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
   /**
    * getJobsBookedInWeekEnding
    * 
    * Get the number of jobs by engineer serrviced
    * 
    * @param mysqldate $date    Date of the week ending 
    *        string $filter     Addition query clasue to filter
    * 
    * @return array   Number of jobs count   
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsBookedInWeekEnding($date, $filter = "") {
        
        if ($filter != "") $filter = " AND $filter";
        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND `BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND `ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND `NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
                        DATE_FORMAT(j.`DateBooked`,'%W %D')  AS `Name`,
			COUNT(j.`JobID`) AS `Count`
		FROM			
			`job` j LEFT JOIN `status` AS s ON s.`StatusID` = j.`StatusID` ,
                        `service_provider` sp
		WHERE
			j.`DateBooked` >= DATE_SUB('$date', INTERVAL 7 DAY)
			AND j.`ServiceProviderID` = sp.`ServiceProviderID`
                        $filter
                        $type
                GROUP BY
			j.`DateBooked`
                ORDER BY
                        j.`DateBooked`
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }    
    
 
    
   /**
    * exportJobsGraphicalAnalysis
    * 
    * Get a recordset of job details for export to Excel. This was developed for
    * export functionality of Graphical Analysis
    * 
    * @param string $filter     Addition query clasue to filter
    * 
    * @return array   Output of query 
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function exportJobsGraphicalAnalysis($filter = "") {        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND brch.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND clt.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND n.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT
			j.`DateBooked` AS `Booked`,
			DATEDIFF(NOW(), j.`DateBooked`) AS `Days`,
			j.`JobID` AS `SL No`,
			sp.`CompanyName` AS `Service Provider`,
			j.`ServiceCentreJobNo` AS `Job No`,
			IF (ISNULL(CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`)),
				j.`EngineerName`
			,
				CONCAT(spe.`EngineerFirstName`,' ',spe.`EngineerLastName`)
			) AS `Engineer`,
			mft.`ManufacturerName` AS `Manufacturer`,
			ut.`UnitTypeName` AS `Unit Type`,
			mdl.`ModelNumber` AS `Model`,
			st.`ServiceTypeName` AS `Service Type`,
			j.`JobSite` AS `Site`,
			s.`StatusName` AS `Current Status`,
			n.`CompanyName` AS `Network`,
			clt.`ClientName` AS `Client`,
			brnd.`BrandName` AS `Brand`,
			brch.`BranchName` AS `Branch`
		FROM
			`job` j LEFT JOIN `status` s ON j.`StatusID` = s.`StatusID`
                                LEFT JOIN `service_provider` sp ON j.`ServiceProviderID` = sp.`ServiceProviderID`
                                LEFT JOIN `appointment` a ON j.`JobID` = a.`JobID`
                                LEFT JOIN `service_provider_engineer` spe ON a.`ServiceProviderEngineerID` = spe.`ServiceProviderEngineerID`
                                LEFT JOIN `manufacturer` mft ON j.`ManufacturerID` = mft.`ManufacturerID`
                                LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`
                                LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`
				LEFT JOIN `service_type` st ON j.`ServiceTypeID` = st.`ServiceTypeID`
				LEFT JOIN `network` n ON j.`NetworkID` = n.`NetworkID`
				LEFT JOIN `client` clt ON j.`ClientID` = clt.`ClientID`
				LEFT JOIN `brand` brnd ON j.`BrandID` = brnd.`BrandID`
				LEFT JOIN `branch` brch ON j.`BranchID` = brch.`BranchID`
                WHERE
                        $filter
                        $type
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
   /**
    * exportClosedJobsGraphicalAnalysis
    * 
    * Get a recordset of job details based on the Closed Jobs DataTable  for 
    * export to Excel. This was developed for export functionality of Graphical 
    * Analysis
    * 
    * @param string $filter         Addition query clasue to filter
    *        string $closedField    The field to use as the closed date (varies depending on SP or Branch perspective 
    * 
    * @return array   Output of query 
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function exportClosedJobsGraphicalAnalysis($filter = "", $closedField = '`ClosedDate`') {        
        if (isset($this->controller->user->ServiceProviderID)) {                /* Add clause to filetr query accourding to suer type - only want to report on own data */
            $type = " AND sp.`ServiceProviderID` = {$this->controller->user->ServiceProviderID} ";
        } elseif (isset($this->controller->user->BranchID)) {
            $type = " AND j.`BranchID` = {$this->controller->user->BranchID} ";    
        } elseif (isset($this->controller->user->ClientID)) {
            $type = " AND j.`ClientID` = {$this->controller->user->ClientID} ";
        } elseif (isset($this->controller->user->NetworkID)) {
            $type = " AND j.`NetworkID` = {$this->controller->user->NetworkID} ";
        } else {
            $type = "";
        }
        
        $sql = "
                SELECT	    j.JobID		    AS 'SL No',
			    DATE_FORMAT(j.DateBooked,'%d%m/%Y')	    AS 'Date Booked',
			    DATE_FORMAT(j.$closedField,'%d%m/%Y')	    AS '".( $closedField == '`ClosedDate`' ? 'Closed' : 'Despatched' )."',
			    DATEDIFF(j.$closedField, j.DateBooked) AS 'Days',
			    sp.Acronym		    AS 'ServiceCentre',
			    j.ServiceCentreJobNo  AS 'Job No',
			    st.ServiceTypeName	    AS 'Service',
			    CONCAT(customer.ContactLastName, ', ', title.Title, ' ', customer.ContactFirstName) AS 'Customer Name',
			    CASE
				WHEN j.JobSite = 'workshop' THEN 'W'
				WHEN j.JobSite = 'field call' THEN 'F'
			    END			    AS 'F/S'


		FROM	    job j

		LEFT JOIN   service_provider AS sp ON j.ServiceProviderID = sp.ServiceProviderID
		LEFT JOIN   service_type AS st ON j.ServiceTypeID = st.ServiceTypeID
		LEFT JOIN   status ON j.StatusID = status.StatusID
		LEFT JOIN   customer ON j.CustomerID = customer.CustomerID
		LEFT JOIN   customer_title AS title ON customer.CustomerTitleID = title.CustomerTitleID
		
		WHERE	    $filter
                            $type
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    
    //Inserts Authorisation Number into job.AuthorisationNo
    //2012-12-01  Vic <v.rutkunas@pccsuk.com>
    
    public function setAuthNo($jobID, $authNo) {
        
        $q = "UPDATE job SET AuthorisationNo = :authNo, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :jobID";
        $values = ["jobID" => $jobID, "authNo" => $authNo, "ModifiedUserID" => $this->controller->user->UserID];
        $result = $this->execute($this->conn, $q, $values);
        return $result;
        
    }

    
    
    //Checks whether RA is required and if job.AuthorisationNo is not null
    //Returns true or false
    //2012-12-06  Vic <v.rutkunas@pccsuk.com>

    public function checkRARequired($serviceTypeID, $manufacturerID, $authNo) {
	
	$q = "SELECT * FROM service_type WHERE ServiceTypeID = :serviceTypeID";
	$values = ["serviceTypeID" => $serviceTypeID];
	$result = $this->query($this->conn, $q, $values);
	$serviceTypeName = isset($result[0]["ServiceTypeName"]) ? $result[0]["ServiceTypeName"] : null;
	
	$q = "SELECT * FROM manufacturer WHERE ManufacturerID = :manufacturerID";
	$values = ["manufacturerID" => $manufacturerID];
	$result = $this->query($this->conn, $q, $values);
	$authorisationRequired = isset($result[0]["AuthorisationRequired"]) ? $result[0]["AuthorisationRequired"] : null;

	if($serviceTypeName == "Warranty" && $authorisationRequired == "Yes" && ($authNo == "" || $authNo == null)) {
	    return true;
	} else {
	    return false;
	}	
	
    }
    
    
    
    //Sets job.RepairAuthStatus value to given $status
    //2012-12-10  Vic <v.rutkunas@pccsuk.com>
    
    public function setRAStatus($jobID, $status) {
	
	$q = "UPDATE job SET RepairAuthStatus = :status, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :jobID";
	$values = ["status" => $status, "jobID" => $jobID, "ModifiedUserID" => $this->controller->user->UserID];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    //Sets job.StatusID relevant to status.StatusName
    //2012-12-10  Vic <v.rutkunas@pccsuk.com>
    
    public function setJobStatusByName($job, $status) {
	
	$q = "UPDATE job SET StatusID = (SELECT StatusID FROM status WHERE StatusName = :status), ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :jobID";
	$values = ["jobID" => $job["JobID"], "status" => $status, "ModifiedUserID" => $this->controller->user->UserID];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    

    
    /**
    @action sets job.StatusID
    @input  job ID, status ID
    @return boolean
    @output void
    2013-02-11  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function setJobStatusByID($jobID, $statusID) {
	
	$q = "	UPDATE	job 
		SET	StatusID = :statusID, 
			ModifiedUserID = :ModifiedUserID, 
			ModifiedDate = NOW() 
		WHERE	JobID = :jobID
	     ";
	$values = ["jobID" => $jobID, "statusID" => $statusID, "ModifiedUserID" => $this->controller->user->UserID];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    

    
    //Sets job.RAType with given $type
    //2012-12-10  Vic <v.rutkunas@pccsuk.com>
    
    public function setRAType($job, $type) {
	
	$q = "UPDATE job SET RAType = :type, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :jobID";
	$values = ["type" => $type, "jobID" => $job["JobID"], "ModifiedUserID" => $this->controller->user->UserID];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    //Sets job.RAStatusID relevant to given $status
    //2012-12-10  Vic <v.rutkunas@pccsuk.com>
    
    public function setRAStatusID($job, $status, $brandID = null) {
	
	$q = "	UPDATE 	job 

		SET	RAStatusID =	CASE
					    
					    WHEN    @statusID := (
								    SELECT  RAStatusID
								    FROM    ra_status
								    WHERE   RAStatusCode = (	
										SELECT 	RAStatusCode
										FROM	ra_status 
										WHERE 	RAStatusName = :status
									    ) AND BrandID = :brandID AND Status = 'Active'
								 )
					 				
					    THEN    @statusID
									
					    ELSE    (
							SELECT 	RAStatusID 
							FROM 	ra_status 
							WHERE 	RAStatusName = :status AND
								BrandID = 1000
						    )
						    
					END,
                                        
                ModifiedUserID = :ModifiedUserID,
                ModifiedDate = NOW()

		WHERE 	JobID = :jobID";
	
	$values = ["status" => $status, "jobID" => $job["JobID"], "brandID" => $brandID, 
                   "ModifiedUserID" => $this->controller->user->UserID];
	$result = $this->execute($this->conn, $q, $values);
	return $result;
	
    }
    
    
    
    //Perform actions when RA request is accepted
    //2012-12-11  Vic <v.rutkunas@pccsuk.com>
    
    public function processRAAccepted($jobID) {
	
	$job = self::fetch($jobID);
	
	//Setting new RepairAuthStatus
	$raStat = self::setRAStatus($jobID, "confirmed");
	
	//Changing job.OpenJobStatus
	if($job["OpenJobStatus"] != "in_store") {
	    self::updateOpenJobStatus($jobID, "with_supplier");
	}
	
	//Setting new job status
	if(isset($job["ServiceProviderID"]) && $job["ServiceProviderID"] != null && $job["ServiceProviderID"] != "") {
	    self::setJobStatusByName($job, "02 ALLOCATED TO AGENT");
	} else {
	    self::setJobStatusByName($job, "01 UNALLOCATED JOB");
	}
	
	$skylineModel = $this->controller->loadModel("Skyline");
	$spID = $skylineModel->findServiceCentre($job);
        
        
	$q = "UPDATE job SET ServiceProviderID = :spID WHERE JobID = :jobID";
	$values = ["jobID" => $jobID, "spID" => (($spID === 0) ? null : $spID)];
	$result = $this->execute($this->conn, $q, $values);
	
	//Communicating to SC, SB and RMA
	$com = self::pushJobToSC($jobID);
	
	//Setting new RA Status
	$setRAStatus = self::setRAStatusID($job, "Authorisation Accepted", $this->controller->user->DefaultBrandID);
	
	if($job["AutoSendEmails"] == "1") {
	
	    //Sending confirmation email
	    $emailModel = $this->controller->loadModel("Email");

	    if($job['EmailType'] == 'CRM') {    
		$email = $emailModel->fetchRow(['EmailCode' => "job_booking_crm"]);
	    } else if($job['EmailType']=='Tracker') {    
		$email = $emailModel->fetchRow(['EmailCode' => "job_booking_smart_service_tracker"]);
	    } else {
		$email = $emailModel->fetchRow(['EmailCode' => "job_booking"]);
	    }

	    $email["toEmail"] = $job["ContactEmail"];
	    $email["fromName"] = "Skyline";
	    $email["fromEmail"] = "no-reply@skylinecms.co.uk";
	    $email["replyEmail"] = ($job['ServiceReplyEmail']) ? $job['ServiceReplyEmail'] : false;
	    $email["replyName"] = ($job['ServiceCentreName']) ? $job['ServiceCentreName'] : false;
	    $email["ccEmails"] = false;
	    $emailModel->processEmail($email, $job);
	    
	}
	
    }
    

    
    //Perform actions if RA request is rejected
    //2012-12-18  Vic <v.rutkunas@pccsuk.com>
    
    public function authRejected($jobID, $text) {

	$job = self::fetch($jobID);
	
	self::updateOpenJobStatus($jobID, "awaiting_collection");
	
	self::setRAStatus($jobID, "rejected");
	
	self::setRAStatusID($job, "Authorisation Declined", $this->controller->user->DefaultBrandID);

	$q = "UPDATE job SET CancelReason = :text, ModifiedUserID = :ModifiedUserID, ModifiedDate = NOW() WHERE JobID = :jobID";
	$values = ["text" => $text, "jobID" => $jobID, "ModifiedUserID" => $this->controller->user->UserID];
	$this->execute($this->conn, $q, $values);
	
        $emailModel = $this->controller->loadModel("Email");
        $emailModel->sendAuthorisationEmail($JobID, 'unable_to_authorise_repair');
         
	//Sending rejection email
	/*
	$emailModel = $this->controller->loadModel("Email");
	$manufacturerModel = $this->controller->loadModel("Manufacturers");
	$manufacturer = $manufacturerModel->getManufacturer($job["ManufacturerID"]);
	$email = $emailModel->fetchRow(['EmailCode' => "ra_rejected_branch"]);
	$email["toEmail"] = $job["ContactEmail"];
	$email["fromName"] = "Skyline";
	$email["fromEmail"] = "no-reply@skylinecms.co.uk";
	$email["replyEmail"] = ($manufacturer["AuthorisationManagerEmail"]) ? $manufacturer["AuthorisationManagerEmail"] : false;
	$email["replyName"] = ($manufacturer["AuthorisationManager"]) ? $manufacturer["AuthorisationManager"] : false;
	$email["ccEmails"] = false;
	$emailModel->processEmail($email, $job);
	*/
	
    }
    
    
    
    //Returns list of ServiceProviders from current search
    //2012-12-20  Vic <v.rutkunas@pccsuk.com>
    
    public function getServiceCentersFromSearch($args) {
	
	$result = self::search($args);
	
	foreach($result["aaData"] as $row) {
	    if($row[6] != null && $row[6] != "") {
		$sc[] = $row[6];
	    }
	}
	
	$ids = array_unique($sc);
	
	foreach($ids as $id) {
	    $q = "  SELECT	ServiceProviderID, 
				CompanyName 
		    FROM	service_provider
		    WHERE	CompanyName = :id
		 ";
	    $values = ["id" => $id];
	    $result = $this->query($this->conn, $q, $values);
	    $return[] = $result[0];
	}
	return $return;
	
    }
    
    
    
    //Checks if RA is required for given manufacturer
    //2012-12-20  Vic <v.rutkunas@pccsuk.com>
    
    public function getRARequired($id) {
	
	$q = "	SELECT 	CASE
			    WHEN AuthorisationRequired = 'Yes'
			    THEN 'Yes'
			    ELSE 'No'
			END	AS response
		FROM 	manufacturer 
		WHERE 	ManufacturerID = :id";
	$values = ["id" => $id];
	$result = $this->query($this->conn, $q, $values);
	return ($result[0]["response"] == "Yes") ? true : false;
	
    }

    
    
   /**
    * fetchOpenTAT
    * 
    * Get a list of jobbs to pass to the matching jobs table on the 
    * 
    * @param array $args    Array
    * 
    * @return json  Output for datatable
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function fetchOpenTAT( $args ) {

        $filters = array (                                                      /* Array of filters to return correct jobs (open closed etc) passed to quries getting data */
                  'OpenJobs' => 'ISNULL(`ClosedDate`) 
                                 AND j.`Status` != "29 JOB CANCELLED"
                                 AND j.`Status` != "29 JOB CANCELLED"
                                 AND j.`Status` != "26 INVOICE RECONCILED"
                                 AND (ISNULL(j.CompletionStatus) OR j.CompletionStatus = "0")',             
                  'ClosedJobs' => 'NOT ISNULL(`ClosedDate`)',
                  'OpenTAT' => 'ISNULL(`ServiceProviderDespatchDate`)',
                  'ClosedTAT' => 'NOT ISNULL(`ServiceProviderDespatchDate`)'
                 );
        
        $datefields = array (                                                      /* Array of filters to return correct jobs (open closed etc) passed to quries getting data */
                  'OpenJobs' => '`DateBooked`',             
                  'ClosedJobs' => 'NOT ISNULL(`ClosedDate`)',
                  'OpenTAT' => '`DateBooked`',
                  'ClosedTAT' => '`ServiceProviderDespatchDate`'
                 );
        
        $filter = $filters[$args['tab']];
        $datefield = $datefields[$args['tab']];
        
        if(isset($args['openby'])) {                                            /* Set which perception eg open by we need */
            if ($args['openby'] == 'branch') {
                if ($args['tab'] == 'ClosedTAT') {
                    $filter = "NOT ISNULL(`ClosedDate`)";
                } else {
                    $filter = "ISNULL(`ClosedDate`)";
                    $datefield = `ClosedDate`;
                }
            }
        }   
        
        if ($args['tab'] == 'OpenTAT') {                                        /* System Staus show only user prefered for Open TAT */
            $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
            $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');

            if(count($UserStatusPreferences)) {
                $sIDStr = '';
                $sep = '';

                foreach($UserStatusPreferences as $usp) {
                    $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                    $sep = ',';
                }
                $filter .= ' AND s.StatusID IN (' . $sIDStr . ')';
            }
        }
        
        if ( isset($args['datefrom'])) {
            $datefrom = $args['datefrom'];
        } else {
            $year = date('Y');                                                  /* Current Year */
            if (date('m') == '01') {                                            /* If Januray */
                $year--;                                                        /* Last year */
            }
            $datefrom = "$year-01-01";
        } /* fi isset $args['display'] */

        if ( isset($args['dateto'])) {
            $dateto = $args['dateto'];
        } else {
            $dateto = date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))); /*Last day of last month */
        } /* fi isset $args['display'] */;
        
        if ($args['tab'] == 'ClosedTAT') {                                              /* If closed TAT */
            
            switch ($args['display']) {
                case 'ytd':
                    $filter .= " AND YEAR(j.$datefield) = YEAR(CURDATE())";
                    break;
                
                case '30':
                    $filter.= " AND j.$datefield > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                    break;

                case 'dr':
                    $filter .= " AND j.$datefield >= '$datefrom' AND j.$datefield <= '$dateto'";
                    break;

                default: /* mtd */
                    $filter.= " AND YEAR(j.$datefield) = YEAR(CURDATE()) AND MONTH(j.$datefield) = MONTH(CURDATE())";
            }
        } /* fi tab == 'ClosedTAT' */
        unset($args);
        $args['where'] = $filter;
        
            
        if (isset($args['spid']) && $args['spid']) {                            /* Service Provider Filter */
           $args['where'] .= ' AND j.ServiceProviderID='.$args['spid']; 
        }    
        
        if (isset($args['nid']) && $args['nid']) {                              /* Newtork Filter */
            $args['where'] .= ' AND j.NetworkID='.$args['nid'];
        }
        
        if (isset($args['cid']) && $args['cid']) {                              /* Client Filter */
            $args['where'] .= ' AND j.ClientID='.$args['cid'];
        }
        
        if (isset($args['bid']) && $args['bid']) {                              /* Branch Filter */
            $args['where'] .= ' AND j.BranchID='.$args['bid'];
        }
        
        if ( isset($args['daysFrom']) && isset($args['daysTo']) )  {            /* Guage (days) Filter */
            $args['where'] .= " AND j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL {$args['daysFrom']} DAY) AND DATE_SUB(NOW(), INTERVAL {$args['daysTo']} DAY)";
        }
        
        //$args['where'] .=
        
        $table = "job AS j LEFT JOIN `status` AS s ON s.StatusID = j.StatusID 
                           LEFT JOIN `model` AS mdl ON j.ModelID = mdl.ModelID
                           LEFT JOIN `unit_type` AS ut ON mdl.UnitTypeID = ut.UnitTypeID
                           LEFT JOIN `manufacturer` AS mft ON j.ManufacturerID = mft.ManufacturerID
                           LEFT JOIN `service_provider` AS sp ON j.ServiceProviderID = sp.ServiceProviderID
                           LEFT JOIN `service_type` AS st ON j.ServiceTypeID = st.ServiceTypeID
                           LEFT JOIN `service_type_alias` AS sta ON st.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=j.NetworkID AND sta.ClientID=j.ClientID
                           LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status='Active'
                           ";
        
        $columns = array("j.JobID AS `ID`", 
                        "DATE_FORMAT (j.DateBooked, '%d/%m/%Y')", 
                        "DATEDIFF(NOW(),`DateBooked`)", 
                        "IF(ISNULL(sp.`Acronym`),sp.`CompanyName`,sp.`Acronym`) AS `ServiceCentre`", 
                        "j.`ServiceCentreJobNo`", 
                        "ut.UnitTypeName AS UnitType", 
                        "IF(ISNULL(mft.`Acronym`),mft.`ManufacturerName`,mft.`Acronym`) AS ManufacturerName", 
                        "st.ServiceTypeName AS ServiceType", 
                        "s.StatusName AS SystemStatus", 
                        "j.ItemLocation", 
                        "j.JobSite AS FieldService"
                  );
        
        $result = $this->ServeDataTables($this->conn, $table, $columns, $args);
        
        foreach ($result['aaData'] as $dI=>$dV) {
            $result['aaData'][$dI][]  = '<input type="checkbox"  value="'.$result['aaData'][$dI][0].'" name="check'.$result['aaData'][$dI][0].'">';
        }
        
        return($result);                                                        /* Job exists so return recordset */
        
    }
    

    
    /**
    @return jobs for the provided job ID list.
    2012-01-31  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getJobs($data) {
	
	$idList = $data;
	$ids = implode(",", $idList);
	
	$q = "SELECT * FROM job WHERE JobID IN($ids)";
	
	$result = $this->query($this->conn, $q);
	
	return $result;
	
    }

    
    
    /**
    @action updates job status (Job Update page)
    @input  job ID, status ID
    @output void
    @return void
    */
    
    public function updateJobStatus($data, $closed) {
	
	if($closed) {
	    $q = "  UPDATE  job 
		    SET	    StatusID = :statusID, 
			    ClosedDate = DATE_FORMAT(NOW(), '%Y-%m-%d'),
			    RepairCompleteDate = DATE_FORMAT(NOW(), '%Y-%m-%d')
		    WHERE   JobID = :jobID";
	} else {
	    $q = "UPDATE job SET StatusID = :statusID WHERE JobID = :jobID";
	}
	
	$values = ["statusID" => $data["status"], "jobID" => $data["jobID"]];
	$result = $this->execute($this->conn, $q, $values);
	
        
	$q = "	INSERT INTO status_history 
			    (
				JobID, 
				StatusID, 
				UserID, 
				Date
			    )
		VALUES
			    (
				:jobID,
				:statusID,
				:userID,
				NOW()
			    )
	     ";

	$values["userID"] = $this->controller->user->UserID;
	$result = $this->execute($this->conn, $q, $values);
	
    }    

    
    
    /**
    @action checks if Service Appraisal Required is true for given branch
    @input  branch ID
    @return boolean
    @output void
    */
    
    function isServiceAppraisalRequired($branchID) {
	
	if(!$branchID) {
	    return false;
	}
	
	$q = "SELECT ServiceAppraisalRequired FROM branch WHERE branchID = :branchID";
	$values = ["branchID" => $branchID];
	$result = $this->query($this->conn, $q, $values);
	if(isset($result[0]) && $result[0]["ServiceAppraisalRequired"] == "Yes") {
	    return true;
	} else {
	    return false;
	}
	
    }
    
   /**
    * getUnitModelType
    * 
    * Get the UnitType & Model of a job based on the ServicebaseXXX field if it 
    * exists or the appropriate Skyline Table if not
    * 
    * @param integer $jId   JobID
    * 
    * @return string    Name of UnitType
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getUnitModelType( $jId ) {
        $sql = "
                SELECT
			IF(ISNULL(j.`ServiceBaseModel`) OR (j.`ServiceBaseModel` = 0),
				mdl.`ModelNumber`
			,
				j.`ServiceBaseModel`
			) AS `Model`,
			IF(ISNULL(j.`ServiceBaseUnitType`),
				ut.`UnitTypeName`
			, 
				j.`ServiceBaseUnitType`
			) AS `UnitType`, ut.`ImeiRequired`
		FROM
			`job` j LEFT JOIN `model` mdl ON j.`ModelID` = mdl.`ModelID`
				LEFT JOIN `unit_type` ut ON mdl.`UnitTypeID` = ut.`UnitTypeID`
		WHERE
			j.`JobID` = $jId
               ";
        //$this->log($sql,"xxxx");
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Job exists so return model & unit type */
        } else {
            return(array('Model'=>'','UnitType'=>''));                          /* Not found return null */
        }
    }
     /**
    * getImeiRequired
    * 
    * Get the ImeiRequired Status of a UnityType based on the UnitTypeID field 
      *     * 
    * @param integer $utId   UnitTypeID
    * 
    * @return string  Name of ImeiRequired
    * 
    * Praveen Kumar N <p.nakka@pccsuk.com> 
    ***************************************************************************/
    
     public function getImeiRequired( $utId ) {
        $sql = "
		    SELECT 
			    ImeiRequired 
		    FROM 
			    unit_type 
		    WHERE 
			    UnitTypeID = $utId
	    ";
	
	$result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]);                                        /* UnitTypeID exists so return ImeiRequired */
        } else {
            return(array('ImeiRequired'=>''));                         /* Not found return null */
        }
     }
    
        
    /**
    @action end Service Appraisal procedure
    @input  job data
    @return void
    @output void
    */
    
    function endAppraisal($data) {

	/*
	$serviceActions = [];
	$serviceActions["SoftwareUpdate"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "SoftwareUpdate";
	$serviceActions["Education"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "CustomerEducation";
	$serviceActions["Settings"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "SettingsSetup";
	$serviceActions["RepairRequired"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "RepairRequired";
	$serviceActions["DamagedBeyondRepair"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "DamagedBeyondRepair";
	$serviceActions["FaultyAccessory"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "FaultyAccessory";
	$serviceActions["NonUKModel"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "NonUKModel";
	$serviceActions["Unresolvable"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "Unresolvable";
	$serviceActions["BranchRepair"] = isset($data["ServiceAction"]) && $data["ServiceAction"] == "BranchRepair";
	*/
	
	$softService = false;
	
	$serviceAction = null;
	
	switch ($data["ServiceAction"]) {
	    case "SoftwareUpdate" : {
		$serviceAction = "software_update";
		break;
	    }
	    case "CustomerEducation" : {
		$serviceAction = "education";
		break;
	    }
	    case "SettingsSetup" : {
		$serviceAction = "setup";
		break;
	    }
	    case "RepairRequired" : {
		$serviceAction = "repair_required";
		break;
	    }
	    case "DamagedBeyondRepair" : {
		$serviceAction = "damaged";
		break;
	    }
	    case "FaultyAccessory" : {
		$serviceAction = "faulty_acc";
		break;
	    }
	    case "NonUKModel" : {
		$serviceAction = "non_uk_mod";
		break;
	    }
	    case "Unresolvable" : {
		$serviceAction = "unresolvable";
		break;
	    }
	    case "BranchRepair" : {
		$serviceAction = "branch_repair";
		break;
	    }
	    default: {
		$serviceAction = null;
		break;
	    }
	}
	
	if($serviceAction) {
	    $softService = true;
	}
	
	
	$q = "	UPDATE	job
	    
		SET	ModelID =   CASE
					WHEN (@model := (SELECT COUNT(*) FROM model WHERE ModelNumber = :modelNo LIMIT 1)) > 0
					THEN (SELECT ModelID FROM model WHERE ModelNumber = :modelNo LIMIT 1)
					ELSE ModelID
				    END,
			ServiceBaseModel =  CASE
						WHEN @model = 1
						THEN ServiceBaseModel
						ELSE :modelNo
					    END,
			ManufacturerID =    CASE
						WHEN :manufacturerID AND :manufacturerID != ''
						THEN :manufacturerID
						ELSE ManufacturerID
					    END,
			ServiceBaseUnitType =	CASE
						    WHEN @model = 1
						    THEN ServiceBaseUnitType
						    ELSE (SELECT UnitTypeName FROM unit_type WHERE UnitTypeID = :productType)
						END,
			ServiceTypeID =	CASE
					    WHEN (@stype := :serviceTypeID) AND @stype != ''
					    THEN @stype
					    ELSE ServiceTypeID
					END,
			SerialNo = :serialNo,
			ImeiNo = :imeiNo,
			ReportedFault = :reportedFault,
			RepairDescription = :repairDescription,
			ServiceAction = :serviceAction,
			ConditionCodeID = :conditionCodeID,
			SymptomCodeID = :symptomCodeID,
			WarrantyServiceTypeID = :warrantyServiceTypeID,
			WarrantyDefectTypeID = :warrantyDefectTypeID,
			JobCategory = :jobCategory,
			ServiceCategory = :serviceCategory,
			CompletionStatusID = :completionStatusID,
			InboundFaultCodeID =	CASE
						    WHEN :inboundFaultCodeID IS NOT NULL AND :inboundFaultCodeID != ''
						    THEN :inboundFaultCodeID
						    ELSE InboundFaultCodeID
						END
		
		WHERE	JobID = :jobID
	     ";
	
	$values = [
	    "modelNo" => $data["ModelNo"],
	    "manufacturerID" => $data["Manufacturer"],
	    "productType" => $data["ProductType"],
	    "serialNo" => $data["SerialNo"],
	    "imeiNo" => $data["ImeiNo"],
	    "serviceTypeID" => ((isset($data["ServiceType"]) && $data["ServiceType"] != "") ? $data["ServiceType"] : null),
	    "reportedFault" => $data["ServiceRequest"],
	    "repairDescription" => $data["ServiceReport"],
	    "serviceAction" => $serviceAction,
	    "jobID" => $data["sbJobID"],
	    "conditionCodeID" => ((isset($data["conditionCode"]) && $data["conditionCode"] != "") ? $data["conditionCode"] : null),
	    "symptomCodeID" => ((isset($data["symptomCode"]) && $data["symptomCode"] != "") ? $data["symptomCode"] : null),
	    "warrantyServiceTypeID" => ((isset($data["warrantyServiceType"]) && $data["warrantyServiceType"] != "") ? $data["warrantyServiceType"] : null),
	    "warrantyDefectTypeID" => ((isset($data["warrantyDefectType"]) && $data["warrantyDefectType"] != "") ? $data["warrantyDefectType"] : null),
	    "jobCategory" => (isset($data["jobCategory"]) ? $data["jobCategory"] : null),
	    "serviceCategory" => (isset($data["serviceCategory"]) ? $data["serviceCategory"] : null),
	    "completionStatusID" => ((isset($data["completionStatus"]) && $data["completionStatus"] != "") ? $data["completionStatus"] : null),
	    "inboundFaultCodeID" => ((isset($data["InboundFault"]) && $data["InboundFault"] != "") ? $data["InboundFault"] : null),
	];
	
	$this->execute($this->conn, $q, $values);
	
	
	if(isset($data["qaQuestions"])) {
	    $q = "DELETE FROM quality_assurance_data WHERE JobID = :jobID";
	    $values = ["jobID" => $data["sbJobID"]];
	    $result = $this->execute($this->conn, $q, $values);
	    foreach($data["qaQuestions"] as $question) {
		$q = "	INSERT INTO quality_assurance_data
				    (
					QAQuestionID,
					JobID,
					QAData,
					ModifiedUserID
				    )
			VALUES	    
				    (
					:questionID,
					:jobID,
					1,
					:userID
				    )
		     ";
		$values = ["questionID" => $question, "jobID" => $data["sbJobID"], "userID" => $this->controller->user->UserID];
		$result = $this->execute($this->conn, $q, $values);
	    }
	}
	
	if($serviceAction == "branch_repair") {
	    self::updateOpenJobStatus($data["sbJobID"], "branch_repair");
	} else {
	    self::updateOpenJobStatus($data["sbJobID"], null);
	}
	
	//user is only saving the job, not completing it.
	if(!isset($data["complete"])) {
	    exit();
	}
	
	
	if($softService && $serviceAction != "repair_required") {

	    if($data["endOptions"] == "service_complete" || $data["endOptions"] == "customer_notified") {
				
		$q = "  UPDATE  job 
			SET	StatusID = 25,
				RepairCompleteDate = CURDATE(),
				ClosedDate = CURDATE(),
				OpenJobStatus = :openJobStatus
			WHERE   JobID = :jobID
		     ";
		
	    } else if($data["endOptions"] == "returned_to_customer") {
		
		$q = "  UPDATE  job 
			SET	StatusID = 25,
				RepairCompleteDate = CURDATE(),
				ClosedDate = CURDATE(),
				DateReturnedToCustomer = CURDATE(),
				OpenJobStatus = :openJobStatus
			WHERE   JobID = :jobID
		     ";
		
	    }
	    
	    $values = ["jobID" => $data["sbJobID"]];
	    
	    if($data["endOptions"] == "service_complete") {
		$values["openJobStatus"] = "awaiting_collection";
	    }
	    if($data["endOptions"] == "customer_notified") {
		$values["openJobStatus"] = "customer_notified";
	    }
	    if($data["endOptions"] == "returned_to_customer") {
		$values["openJobStatus"] = "closed";
	    }
	    
	    $result = $this->execute($this->conn, $q, $values);
            
            //Chris asked Nag to send this email when ClosedDate is set. - 11/04/2013
            $EmailModel = $this->controller->loadModel('Email');  
            $EmailModel->sendJobCompleteEmail($data["sbJobID"]);
            
	} else if($serviceAction == "repair_required") {
	    
	    $courierModel = $this->controller->loadModel("Couriers");
	    $result = $courierModel->updateJobShippingData($data, $data["sbJobID"]);
	    
	    $job = self::fetch($data["sbJobID"]);
	    
	    if($job["BranchID"]) {
		
		//If RA is not required then proceed with normal Service Provicer allocation
		if(!self::checkRARequired($job["ServiceTypeID"], $job["ManufacturerID"], $job["AuthorisationNo"])) {
		    $skylineModel = $this->controller->loadModel("Skyline");
		    $spID = $skylineModel->findServiceCentre($job);
		    $q = "UPDATE job SET ServiceProviderID = :spID WHERE JobID = :jobID";
		    $values = ["jobID" => $data["sbJobID"], "spID" => (($spID === 0) ? null : $spID)];
		    $result = $this->execute($this->conn, $q, $values);
		}
		
		$q = "  UPDATE  job 
			SET	StatusID = 2,
				OpenJobStatus = :openJobStatus
			WHERE   JobID = :jobID
		     ";
		$values = ["jobID" => $data["sbJobID"]];

		if($data["endOptions"] == "awaiting_collection") {
		    $values["openJobStatus"] = "in_store";
		}
		if($data["endOptions"] == "with_service_provider") {
		    $values["openJobStatus"] = "with_supplier";
		}

		$result = $this->execute($this->conn, $q, $values);
		
	    }
	    
	}
	
	
    }
    

    
    /**
    @action updates job.Accessories
    @input  accessories text, job ID
    @output void
    @return void
    2013-02-19  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function editAccessories($jobID, $text) {
	
	$q = "UPDATE job SET Accessories = :text WHERE JobID = :jobID";
	$values = ["jobID" => $jobID, "text" => $text];
	$result = $this->execute($this->conn, $q, $values);
	
    }
    
    
    
    /**
    @action updates job.UnitCondition
    @input  condition text, job ID
    @output void
    @return void
    2013-02-19  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function editCondition($jobID, $text) {
	
	$q = "UPDATE job SET UnitCondition = :text WHERE JobID = :jobID";
	$values = ["jobID" => $jobID, "text" => $text];
	$result = $this->execute($this->conn, $q, $values);
	
    }
    
    
    
    /**
    @action gets unit turnaround time
    @input  job data
    @output void
    @return job turnaround time
    2013-02-19  Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getTurnaroundTime($job) {
	
	$q = "SELECT @turnaround := turnaround(:jobID) AS turnaround";
	
	$values = ["jobID" => $job["JobID"]];
	
	$result = $this->query($this->conn, $q, $values);
	
	return (isset($result[0]) ? $result[0]["turnaround"] : false);
	
    }
    

    
    public function getClosedJobs($data,$user,$page,$gaugePrefs=array(),$completionStatus=array()) {
       
	$args=$data;
        if(isset($data['openby'])) {                                            /* Get which perception of closed by we need */                                        
            if ($data['openby']== 'branch') {                                   /* Branch selected */
               // $filter = "NOT ISNULL(`ClosedDate`)";
                $closedField = '`ClosedDate`';                                  /* Field in database we check for closed */
            } else {                                                            /* Otherwise service provider */
               // $filter = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
                $closedField = '`ServiceProviderDespatchDate`';                 /* Field in database we check for closed */
            }
        } else {
//            $filter = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
            $closedField = '`ServiceProviderDespatchDate`';                     /* Field in database we check for closed */
        }
        
        $filter = $closedField." IS NOT NULL";
        $filter .= " AND t1.CompletionStatus IS NOT NULL";
        
        if ( isset($data['datefrom'])) {
            $datefrom = $data['datefrom'];
        } else {
            $year = date('Y');                                                  /* Current Year */
            if (date('m') == '01') {                                            /* If Januray */
                $year--;                                                        /* Last year */
            }
            $datefrom = "$year-01-01";
        } /* fi isset $args['display'] */

        if ( isset($data['dateto'])) {
            $dateto = $data['dateto'];
        } else {
            $dateto = date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))); /*Last day of last month */
        } /* fi isset $args['display'] */
        
        if(isset($data['display'])) {
            switch ($data['display']) {
                case 'ytd':
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE())";
                    break;

                case 'lm':
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH($closedField) = MONTH(CURDATE() - INTERVAL 1 MONTH)";
                    break;

                case '30':
                    $filter .= " AND $closedField > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                    break;
                
                case 'dr':
                    $filter .= " AND Date($closedField) >= '$datefrom' AND Date($closedField) <= '$dateto'";
                    break;

                default: /* mtd */
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE()) AND MONTH($closedField) = MONTH(CURDATE())";
            }
        } else {
            $filter .= " AND YEAR($closedField) = YEAR(CURDATE()) AND MONTH($closedField) = MONTH(CURDATE())";
        }
        
        if ( isset($data['daysFrom']) && isset($data['daysTo']) )  {            /* Guage (days) Filter */
            $filter .= " 
			AND DATEDIFF(t1.$closedField, t1.`DateBooked`) < {$data['daysTo']}
                        AND DATEDIFF(t1.$closedField, t1.`DateBooked`) >= {$data['daysFrom']}                 
                        ";
        }
        
	$dataTable=$this->controller->loadModel('DataTable');
        $columns=$dataTable->getAllFieldNames('closedJob',$user,$page,false,$closedField);
        $columns_heading = $columns[1];
        $columns=$columns[0];
		    
		 $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN completion_status     AS cs ON cs.CompletionStatus = t1.CompletionStatus
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                  LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                  LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                  LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                  LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                  LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
                 '; 

            $q=" AND cs.CompletionStatusID IS NOT NULL";

            //Brand Filter
            if(isset($data['brand']) && $data['brand']) {
              $q .= ' AND t8.BrandID=' . $data['brand']; 
            }    

            //Status Type Filter
            
            if(isset($data['sType']) && $data['sType'] != '0') {
                if($data['sType']=="All Others"){
                    $q .= " AND t1.CompletionStatus IS NOT NULL";
                    if(count($gaugePrefs) > 0){ 
                        
                        
                        $gaugePrefString="";
                        foreach($gaugePrefs as $ini=>$gaugePref){ 
                            if($ini < (count($gaugePrefs)-1)){
                                $gaugePrefString .= "'".$gaugePref['GaDialStatus']."',";
                            }
                        }
                        
                        $gaugePrefString = substr($gaugePrefString,0,-1);
                        
                        if($gaugePrefString <>""){
                            $q .= " AND t1.CompletionStatus NOT IN ({$gaugePrefString})";
                        }
                        
                        
                    }
                }
                else{
                    $q .= ' AND t1.CompletionStatus=' . $this->conn->quote($data['sType']);
                }
            }
            
            //Service Centre Type Filter
            if(isset($data['scType']) && $data['scType']) {
                $q .= ' AND t1.ServiceCentreID=' . $this->conn->quote($data['scType']);
            }

            //Manufacturer Filter
            if(isset($data['manufacturer']) && $data['manufacturer']) {
               $q .= ' AND t1.ManufacturerID=' . $this->conn->quote($data['manufacturer']);
            }

            //Service Provider Filter
            if(isset($data['serviceProvider']) && $data['serviceProvider']) {
               $q .= ' AND t1.ServiceProviderID=' . $this->conn->quote($data['serviceProvider']);
            }

            //Client Filter
            if(isset($data['client']) && $data['client']) {
               $q .= ' AND t1.ClientID=' . $this->conn->quote($data['client']);
            }

            //Network Filter
            if(isset($data['network']) && $data['network']) {
               $q .= ' AND t1.NetworkID=' . $this->conn->quote($data['network']);
            }

            //Branch Filter
            if(isset($data['branch']) && $data['branch']) {
               $q .= ' AND t1.BranchID=' . $this->conn->quote($data['branch']);
            }

            //Unit Type Filter
            if(isset($data['unitType']) && $data['unitType']) {
               $q .= ' AND ut.UnitTypeID=' . $this->conn->quote($data['unitType']);
            }

            //Skill Set Filter
            if(isset($data['skillSet']) && $data['skillSet']) {
               $q .= ' AND rs.RepairSkillID=' . $this->conn->quote($data['skillSet']);
            }

            if(isset($data['btnName']) && isset($data['btnValue']) && $data['btnName'] && $data['btnValue'])
            {
                if($data['btnName']=="b1")
                {
                    $q .= " AND t1.".$closedField." > DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) ";
//                    $q .= " AND DATEDIFF(t1.$closedField, t1.`DateBooked`) < ".$data['btnValue']." AND  DATEDIFF(t1.$closedField, t1.`DateBooked`) > 0";
                } 
                else if($data['btnName']=="b2" && isset($data['btnValue2']) && $data['btnValue2'])
                {
                    if($data['btnValue2']<$data['btnValue'])
                    {
                        $tempValue = $data['btnValue'];
                        $data['btnValue'] = $data['btnValue2'];
                        $data['btnValue2'] = $tempValue;
                    }
                    $q .= " AND t1.".$closedField." <=DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) AND t1.".$closedField." >=DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue2']." DAY)";
//                    $q .= " AND DATEDIFF(t1.$closedField, t1.`DateBooked`) >= ".$data['btnValue']." AND  DATEDIFF(t1.$closedField, t1.`DateBooked`) <= ".$data['btnValue2'];
                }
                else if($data['btnName']=="b3" || $data['btnName']=="b4")
                {
                    $q .= " AND t1.".$closedField." < DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) ";
//                    $q .= " AND DATEDIFF(t1.$closedField, t1.`DateBooked`) > ".$data['btnValue'];
                }
            }

            if($this->controller->user->UserType == "Network") {
                $q .= " AND t1.NetworkID =".$this->controller->user->NetworkID;
            }
            if($this->controller->user->UserType == "Client") {
                $q .= " AND t1.ClientID =".$this->controller->user->ClientID;
             }

            if($this->controller->user->UserType == "Branch") {
                $q .= " AND t1.BranchID =".$this->controller->user->BranchID;


            }
            if($this->controller->user->UserType == "ServiceProvider") {
                $q .= "AND t1.ServiceProviderID =".$this->controller->user->ServiceProviderID;;
            }
	
	
        $args['where'] =$filter;
        $args['where'] .=$q;
	  
	$args['groupby'] = "t1.JobID";
	$data = $this->ServeDataTables($this->conn, $table, $columns, $args, $columns_heading);
        
        
//        foreach($data['aaData'] as $dI => $dV) {
//            $data['aaData'][$dI][]  = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="check' . $data['aaData'][$dI][0] . '">';
//        }

        $data_new = array('aaData'=>array());
        foreach($data['aaData'] as $dI => $dV){ 
            $initial =0;
            foreach($columns_heading as $heading){
                $data_new['aaData'][$dI][$heading] = $dV[$initial];
                $initial++;
            }
            
            $data_new['aaData'][$dI][] = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="check' . $data['aaData'][$dI][0] . '">';
        }
        
        $data['aaData'] = $data_new['aaData'];
        
	return $data;
	
    }
    
    
    public function getClosedJobsSummary($data,$user,$page,$TatResult,$gaugePrefs=array(),$completionStatus=array()) {
      
        if(isset($data['openby'])) {                                            /* Get which perception of closed by we need */                                        
            if ($data['openby']== 'branch') {                                   /* Branch selected */
               // $filter = "NOT ISNULL(`ClosedDate`)";
                $closedField = '`ClosedDate`';                                  /* Field in database we check for closed */
            } else {                                                            /* Otherwise service provider */
               // $filter = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
                $closedField = '`ServiceProviderDespatchDate`';                 /* Field in database we check for closed */
            }
        } else {
//            $filter = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
            $closedField = '`ServiceProviderDespatchDate`';                     /* Field in database we check for closed */
        }
        
        $filter = $closedField." IS NOT NULL";
        $filter .= " AND t1.CompletionStatus IS NOT NULL";
        
        if ( isset($data['datefrom'])) {
            $datefrom = $data['datefrom'];
        } else {
            $year = date('Y');                                                  /* Current Year */
            if (date('m') == '01') {                                            /* If Januray */
                $year--;                                                        /* Last year */
            }
            $datefrom = "$year-01-01";
        } /* fi isset $args['display'] */

        if ( isset($data['dateto'])) {
            $dateto = $data['dateto'];
        } else {
            $dateto = date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))); /*Last day of last month */
        } /* fi isset $args['display'] */
        
        if(isset($data['display'])) {
            switch ($data['display']) {
                case 'ytd':
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE())";
                    break;

                case 'lm':
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH($closedField) = MONTH(CURDATE() - INTERVAL 1 MONTH)";
                    break;

                case '30':
                    $filter .= " AND $closedField > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                    break;
                
                case 'dr':
                    $filter .= " AND Date($closedField) >= '$datefrom' AND Date($closedField) <= '$dateto'";
                    break;

                default: /* mtd */
                    $filter .= " AND YEAR($closedField) = YEAR(CURDATE()) AND MONTH($closedField) = MONTH(CURDATE())";
            }
        } else {
            $filter .= " AND YEAR($closedField) = YEAR(CURDATE()) AND MONTH($closedField) = MONTH(CURDATE())";
        }
        
        if ( isset($data['daysFrom']) && isset($data['daysTo']) )  {            /* Guage (days) Filter */
            $filter .= " 
			AND DATEDIFF(t1.$closedField, t1.`DateBooked`) < {$data['daysTo']}
                        AND DATEDIFF(t1.$closedField, t1.`DateBooked`) >= {$data['daysFrom']}                 
                        ";
        }
		    
            $columns=$this->getSummaryFields($page,$TatResult,"closedJobs",$data);
            $columns=$columns[0];
                           
		 $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN completion_status     AS cs ON cs.CompletionStatus = t1.CompletionStatus
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID
                  LEFT JOIN manufacturer          AS ma on ma.ManufacturerID = t1.ManufacturerID
                  LEFT JOIN network               AS ne on ne.NetworkID = t1.NetworkID
                  LEFT JOIN model                 AS md on md.ModelID = t1.ModelID
                  LEFT JOIN unit_type             AS ut on ut.UnitTypeID = md.UnitTypeID
                  LEFT JOIN repair_skill          AS rs ON rs.RepairSkillID = ut.RepairSkillID
                 '; 
                 
	$q=" AND cs.CompletionStatusID IS NOT NULL";
	
        //Brand Filter
        if(isset($data['brand']) && $data['brand']) {
          $q .= ' AND t8.BrandID=' . $data['brand']; 
        }    
        
        //Status Type Filter
        if(isset($data['sType']) && $data['sType'] != '0') {
            if($data['sType']=="All Others"){
                if(count($gaugePrefs) > 0){ 
                    
                    $gaugePrefString="";
                        foreach($gaugePrefs as $ini=>$gaugePref){ 
                            if($ini < (count($gaugePrefs)-1)){
                                $gaugePrefString .= "'".$gaugePref['GaDialStatus']."',";
                            }
                        }
                        
                        $gaugePrefString = substr($gaugePrefString,0,-1);
                        
                        if($gaugePrefString <>""){
                            $q .= " AND t1.CompletionStatus NOT IN ({$gaugePrefString})";
                        }
                }
            }
            else{
                $q .= ' AND t1.CompletionStatus=' . $this->conn->quote($data['sType']);
            }
        }
        
        //Service Centre Type Filter
        if(isset($data['scType']) && $data['scType']) {
            $q .= ' AND t1.ServiceCentreID=' . $this->conn->quote($data['scType']);
        }

        //Manufacturer Filter
        if(isset($data['manufacturer']) && $data['manufacturer']) {
           $q .= ' AND t1.ManufacturerID=' . $this->conn->quote($data['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($data['serviceProvider']) && $data['serviceProvider']) {
           $q .= ' AND t1.ServiceProviderID=' . $this->conn->quote($data['serviceProvider']);
        }

        //Client Filter
        if(isset($data['client']) && $data['client']) {
           $q .= ' AND t1.ClientID=' . $this->conn->quote($data['client']);
        }
        
        //Network Filter
        if(isset($data['network']) && $data['network']) {
           $q .= ' AND t1.NetworkID=' . $this->conn->quote($data['network']);
        }
        
        //Branch Filter
        if(isset($data['branch']) && $data['branch']) {
           $q .= ' AND t1.BranchID=' . $this->conn->quote($data['branch']);
        }
        
        //Unit Type Filter
        if(isset($data['unitType']) && $data['unitType']) {
           $q .= ' AND ut.UnitTypeID=' . $this->conn->quote($data['unitType']);
        }
        
        //Skill Set Filter
        if(isset($data['skillSet']) && $data['skillSet']) {
           $q .= ' AND rs.RepairSkillID=' . $this->conn->quote($data['skillSet']);
        }
        
        //---------------change condition of closed jobs display according to Tat Alert Filter
        
        if(isset($data['btnName']) && isset($data['btnValue']) && $data['btnName'] && $data['btnValue'])
        {
            if($data['btnName']=="b1"){
                $q .= " AND t1.".$closedField." > DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) ";
            }                
            else if($data['btnName']=="b2" && isset($data['btnValue2']) && $data['btnValue2']){
                if($data['btnValue2']<$data['btnValue'])
                {
                    $tempValue = $data['btnValue'];
                    $data['btnValue'] = $data['btnValue2'];
                    $data['btnValue2'] = $tempValue;
                }
                $q .= " AND t1.".$closedField." <=DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) AND t1.".$closedField." >=DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue2']." DAY)";
            }
            else if($data['btnName']=="b3" || $data['btnName']=="b4"){
                $q .= " AND t1.".$closedField." < DATE_SUB( CURDATE( ) , INTERVAL ".$data['btnValue']." DAY) ";
            }
        }
        
	if($this->controller->user->UserType == "Network") {
	    $q .= " AND t1.NetworkID =".$this->controller->user->NetworkID;
	}
	if($this->controller->user->UserType == "Client") {
	    $q .= " AND t1.ClientID =".$this->controller->user->ClientID;
	 }
         
	if($this->controller->user->UserType == "Branch") {
	    $q .= " AND t1.BranchID =".$this->controller->user->BranchID;
	   
	}
	if($this->controller->user->UserType == "ServiceProvider") {
	    $q .= "AND t1.ServiceProviderID =".$this->controller->user->ServiceProviderID;;
	}
        
        if(isset($data['serviceProvidecheck']) && is_array($data['serviceProvidecheck'])){
            $q .= ' AND t1.ServiceProviderID IN (' . implode(',',$data['serviceProvidecheck']) . ')';
        }
        
        
        $args['where'] = $filter;
        $args['where'] .=$q;
	  
	$args['groupby'] = "t1.ServiceProviderID";
	$data = $this->ServeDataTables($this->conn, $table, $columns, $args);
	 foreach($data['aaData'] as $dI => $dV) {
            $data['aaData'][$dI][]  = '<input class="taggedRec" type="checkbox" onclick="countTagged()"  value="' . $data['aaData'][$dI][0] . '" name="serviceProvidecheck[]">';
        }

	return $data;
	
    }

    
    
    public function despatchJobs($data) {
	
	$jobs = json_decode($data["jobs"]);
	$consignmentDate = $data["consignmentDate"];
	
	if(!$consignmentDate) {
	    $consignmentDate = date("d/m/Y");
	}
	
	foreach($jobs as $job) {

	    $q = "UPDATE job SET OpenJobStatus = 'with_supplier' WHERE JobID = :jobID";
	    $values = ["jobID" => $job[0]];
	    $result = $this->execute($this->conn, $q, $values);
	    
	    $q = "SELECT * FROM shipping WHERE JobID = :jobID";
	    $result = $this->query($this->conn, $q, $values);
	    
	    if(count($result) != 0) {
		
		$q = "  UPDATE  shipping
			SET	ConsignmentNo = :consignmentNo,
				ConsignmentDate = :consignmentDate
			WHERE	JobID = :jobID
		     ";
		$values = ["consignmentNo" => $job[1], "consignmentDate" => "DATE_FORMAT($consignmentDate,'%d/%m/%Y')", "jobID" => $job[0]];
		$this->execute($this->conn, $q, $values);
		
	    } else {
		
		$q = "	INSERT INTO shipping
				    (
					JobID,
					ConsignmentNo,
					ConsignmentDate
				    )
			VALUES	    
				    (
					:jobID,
					:consignmentNo,
					:consignmentDate
				    )
		     ";
		$values = ["consignmentNo" => $job[1], "consignmentDate" => $consignmentDate, "jobID" => $job[0]];
		$this->execute($this->conn, $q, $values);
		
	    }
	    
	}
	
    }
    
    /**
    * getJobsFailedRmaSend()
    * 
    * Get the jobs whos updates failed being sent to RMA
    * 
    * @param none
    * 
    * @return array   List of jobs which failed sending to RMA
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getJobsFailedRmaSend() {
        $sql = "
                SELECT
                        `JobID`
		FROM
			`job` j
		WHERE
                        `RMASendFail` = 1
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    } 
    
    
    public function updateNotes ( $JobID, $text ) {
        
        $sql = "select Notes from job where JobID=:JobID";
        $params = array('JobID' => $JobID);
        $result = $this->Query($this->conn, $sql, $params);
        
        if (count($result) > 0) {
            
            $job_table = TableFactory::Job(); 
            $notes = $result[0]['Notes'];
            if (!empty($notes)) $notes .= "\n";
            $notes .= $text;
            
            $job_params = array('JobID' => $JobID, 'Notes' => $notes );
            
            $this->UpdateRow( $this->conn, $job_table, $job_params );
            
        }
        
    }
    
    
   /**
    * getWhereNotDowloadedToSc
    * 
    * Get all job IDs for a specific network which have not bwwn downloaded to
    * the serice centre.
    * (This is mainly used for UTL and teh UTL API but could work with any network.
    * 
    * @param string $nName  - Network Name
    * 
    * @return array   Job records matching query
    * 
    * @author Andrew Williams <a.williams@pccsuk.com> 
    ***************************************************************************/
    
    public function getWhereNotDowloadedToSc($nName) {
        $sql = "
                SELECT
                        j.`JobID`
		FROM
			`job` j,
			`network` n
		WHERE
                        j.`NetworkID` = n.`NetworkID`
                        AND j.`DownloadedToSC` = 0
                        AND n.`CompanyName` = '$nName'
               ";

        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result);                                                    /* Job exists so return recordset */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }  
    

    
    public function getProductLocation($jobID) {
	
	$q = "SELECT ProductLocation FROM job WHERE JobID = :jobID";
	
	$values = ["jobID" => $jobID];
	
	$result = $this->query($this->conn, $q, $values);
	
	if(count($result) == 1) {
	    return $result[0]["ProductLocation"];
	} else {
	    return null;
	}
	
    }
    // function to get columns, and text for columns for Summary display for open/over-due jobs
    public function getSummaryFields($page,$tatResult,$pagetitle = null,$args = array()){
      
        $tat_txt[] =  "ServiceProviderID";
        $tat_txt[] =  "Service Provider";
        
        
        $tat_columns[]=  "t1.ServiceProviderID";
        $tat_columns[]=  "t6.CompanyName";
        
//        echo '<pre>';
//        print_r($page);
//        echo '</pre>';
        
        
        if(!empty($tatResult)){ 
            $tat_txt[] =  "All Jobs";
           if(strtolower($pagetitle) == strtolower("overdueJobs")){
               
               if(isset($args['tatType']) && $args['tatType']=='s')
                {
                    $overdueColumn = 'statustat(t1.JobID)';
                }   
                else
                {
                    $overdueColumn = "
                    CASE
                        WHEN (@days := DATEDIFF( CURRENT_DATE, t1.DateBooked )) > (@turnaround := turnaround(t1.JobID))
                        THEN (@days - @turnaround)
                        ELSE 0
                    END
                    ";
                }
                
                $tat_columns[]=  " 
                    Sum(IF(($overdueColumn)  <  ".$tatResult['Button1'].",1,0)) + 
                    Sum(IF((($overdueColumn) >= ".$tatResult['Button1']." && ($overdueColumn) <  ".$tatResult['Button3']."),1,0)) +
                    Sum(IF((($overdueColumn) >= ".$tatResult['Button3']." && ($overdueColumn) <  ".$tatResult['Button4']."),1,0)) +
                    Sum(IF(($overdueColumn)  >=  ".$tatResult['Button4']." ,1,0))
                ";
               
                $tat_columns[]=  " Sum(IF(($overdueColumn)  <  ".$tatResult['Button1'].",1,0))";
                $tat_columns[]=  " Sum(IF(($overdueColumn) <  ".$tatResult['Button1']." && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT' ,1,0))";

                $tat_columns[]=  " Sum(IF((($overdueColumn) >= ".$tatResult['Button1']." && ($overdueColumn) <  ".$tatResult['Button3']."),1,0))";
                $tat_columns[]=  " Sum(IF(((($overdueColumn) >= ".$tatResult['Button1']." && ($overdueColumn) <  ".$tatResult['Button3'].") && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";

                $tat_columns[]=  " Sum(IF((($overdueColumn) >= ".$tatResult['Button3']." && ($overdueColumn) <  ".$tatResult['Button4']."),1,0))";
                $tat_columns[]=  " Sum(IF(( (($overdueColumn) >= ".$tatResult['Button3']." && ($overdueColumn) <  ".$tatResult['Button4'].") && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";

                $tat_columns[]=  " Sum(IF(($overdueColumn)  >=  ".$tatResult['Button4']." ,1,0))";
                $tat_columns[]=  " Sum(IF(( ($overdueColumn)  >=  ".$tatResult['Button4']." && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";

                $tat_columns[]=  " ROUND(AVG($overdueColumn))";
           }
           elseif(strtolower($pagetitle) == strtolower("closedJobs")){
               
                if(isset($args['openby'])) {                                            /* Get which perception of closed by we need */                                        
                    if ($args['openby']== 'branch') {                                   /* Branch selected */
                     $closedField = '`ClosedDate`';                                  /* Field in database we check for closed */
                    } 
                    else {                                                            /* Otherwise service provider */
                     $closedField = '`ServiceProviderDespatchDate`';                 /* Field in database we check for closed */
                    }
                } 
                else {
                    $closedField = '`ServiceProviderDespatchDate`';                     /* Field in database we check for closed */
                }
               
               
               
               $tat_columns[]=  " 
                    Sum(IF(t1.".$closedField." > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) ,1,0)) + 
                    Sum(IF( (t1.".$closedField." <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY)) ,1,0)) +
                    Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) ) ,1,0)) +
                    Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY)) ,1,0))
                ";
               
                $tat_columns[]=  " Sum(IF(t1.".$closedField." > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.".$closedField." IS NOT NULL && js.TimelineStatus='closed') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY)) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.".$closedField." IS NOT NULL && js.TimelineStatus='closed') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) ) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.".$closedField." >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) && t1.".$closedField." IS NOT NULL && js.TimelineStatus='closed') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY)) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.".$closedField." < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) && t1.".$closedField." IS NOT NULL && js.TimelineStatus='closed') ,1,0))";
                $tat_columns[]=  " ROUND(AVG(DATEDIFF(CURDATE(),t1.".$closedField.")))";
           }
           else{
               
                $tat_columns[]=  " 
                    Sum(IF(t1.DateBooked > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) ,1,0)) + 
                    Sum(IF( (t1.DateBooked <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY)) ,1,0)) +
                    Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) ) ,1,0)) +
                    Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY)) ,1,0))
                ";
               
                $tat_columns[]=  " Sum(IF(t1.DateBooked > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked > DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY)) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked <= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button1']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) ) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button3']." DAY) && t1.DateBooked >= DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY)) ,1,0))";
                $tat_columns[]=  " Sum(IF( (t1.DateBooked < DATE_SUB(CURDATE(),INTERVAL ".$tatResult['Button4']." DAY) && t1.RepairCompleteDate IS NOT NULL && js.StatusName!='29 JOB CANCELLED' && js.StatusName!='29A JOB CANCELLED - NO CONTACT') ,1,0))";
                $tat_columns[]=  " ROUND(AVG(DATEDIFF(CURDATE(),t1.DateBooked)))";
           }
            
            
            $tat_txt[] =  "< ".$tatResult['Button1']." ".$page['Text']['days_text'];
            $tat_txt[] =  "[C]";
            $tat_txt[] =  $tatResult['Button1']." ".$page['Text']['to_text']." ".$tatResult['Button3']." ".$page['Text']['days_text'];
            $tat_txt[] =  "[C]";
            $tat_txt[] =  "> ".$tatResult['Button3']." ".$page['Text']['days_text'];
            $tat_txt[] =  "[C]";
            $tat_txt[] =  "> ".$tatResult['Button4']." ".$page['Text']['days_text'];
            $tat_txt[] =  "[C]";
            $tat_txt[] =  $page['Text']['average_days_text'];
        }
        
        
        $res = $tat_columns;
        $saDatabaseNames=$tat_txt;
        
        return array($res,$saDatabaseNames);
    }
    
    
    public function fetchAllOpenSummary_excel($args,$user,$page,$TatResult){
        $dataTable=$this->controller->loadModel('DataTable');
        
        /*Add relation for Repair_skill table with unit_type to make filter if some one choose skillset.
         * Add condtions if some one choose networks,clients,branch, unit type and skillset.
         * 
         */
        
        $table = 'job AS t1 
                  LEFT JOIN status                AS js ON js.StatusID = t1.StatusID
                  LEFT JOIN product               AS t2 ON t1.ProductID = t2.ProductID
                  LEFT JOIN model                 AS t3 ON t1.ModelID = t3.ModelID
                  LEFT JOIN unit_type             AS t4 ON t3.UnitTypeID = t4.UnitTypeID
                  LEFT JOIN repair_skill          AS t10 ON t4.RepairSkillID = t10.RepairSkillID            
                  LEFT JOIN manufacturer          AS t5 ON t1.ManufacturerID = t5.ManufacturerID
                  LEFT JOIN service_provider      AS t6 ON t1.ServiceProviderID = t6.ServiceProviderID
                  LEFT JOIN service_type          AS t7 ON t1.ServiceTypeID = t7.ServiceTypeID
                  LEFT JOIN service_type_alias    AS sta ON t7.ServiceTypeID = sta.ServiceTypeID AND sta.NetworkID=t1.NetworkID AND sta.ClientID=t1.ClientID
                  LEFT JOIN service_type          AS stm ON sta.ServiceTypeMask=stm.ServiceTypeID AND stm.Status=\'Active\'
                  LEFT JOIN brand_branch          AS t8 on t1.BranchID = t8.BranchID
                  LEFT JOIN network               AS t9 on t1.NetworkID = t9.NetworkID
                  LEFT JOIN user                  AS u on u.UserID = t1.BookedBy
                  LEFT JOIN branch                AS b on b.BranchID = t1.BranchID
                  LEFT JOIN courier               AS co on co.CourierID = t1.CourierID
                  LEFT JOIN customer              AS cu on cu.CustomerID = t1.CustomerID
                  LEFT JOIN job_type              AS jt on jt.JobTypeID = t1.JobTypeID'; 
        
	
        $columns=$this->getSummaryFields($page,$TatResult);
        $columns=$columns[0];
      
        
	
        if(isset($args['ojBy']) && $args['ojBy']=='sp')
        {
             $where = 't1.ServiceProviderDespatchDate IS NULL  ' . $this->UserJobFilter('t1');  
        } /* Removed AND t1.ClosedDate IS NULL and  $this->ActiveJobFilter('js') from qury aboved based on Joe / Neil's definition of a Service provider Open Job - ajw 03/04/2013 */
        else
        {
            $where = 't1.ClosedDate IS NULL ' . $this->UserJobFilter('t1');       
        } 
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                $where .= " AND t1.DateBooked>DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                $where .= " AND t1.DateBooked<=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) AND t1.DateBooked>=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue2']." DAY)";
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                $where .= " AND t1.DateBooked<DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            }
        }
        
         if(isset($args['rcDate']) && $args['rcDate']) {
           $where .= " AND t1.RepairCompleteDate IS NOT NULL AND js.StatusName!='29 JOB CANCELLED' AND js.StatusName!='29A JOB CANCELLED - NO CONTACT' "; 
        }  
        
         if(isset($this->controller->session->hideNonSbJobs)&&$this->controller->session->hideNonSbJobs=="checked"){
             $where.= ' AND t1.ServiceCentreJobNo!="" and t1.ServiceCentreJobNo is not null ';
        }
            
	//Brand Filter
        if(isset($args['brand']) && $args['brand']) {
           $where .= ' AND t8.BrandID=' . $args['brand']; 
        }    
        
        //Status Type Filter
        if(isset($args['sType']) && $args['sType']) {
            $where .= ' AND t1.StatusID=' . $this->conn->quote($args['sType']);
        }
        
        //Service Centre Type Filter
        if(isset($args['scType']) && $args['scType']) {
            $where .= ' AND t1.ServiceCentreID=' . $this->conn->quote($args['scType']);
        }

        //Manufacturer Filter
        if(isset($args['manufacturer']) && $args['manufacturer']) {
           $where .= ' AND t1.ManufacturerID=' . $this->conn->quote($args['manufacturer']);
        }
        
        //Service Provider Filter
        if(isset($args['serviceProvider']) && $args['serviceProvider']) {
           $where .= ' AND t1.ServiceProviderID=' . $this->conn->quote($args['serviceProvider']);
        }

        //Client Filter
        if(isset($args['client']) && $args['client']) {
           $where .= ' AND t1.ClientID=' . $this->conn->quote($args['client']);
        }
        
        //Network Filter
        if(isset($args['network']) && $args['network']) {
           $where .= ' AND t1.NetworkID=' . $this->conn->quote($args['network']);
        }
        
        //Branch Filter
        if(isset($args['branch']) && $args['branch']) {
           $where .= ' AND t1.BranchID=' . $this->conn->quote($args['branch']);
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $where .= ' AND ut.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
        //Skill Set Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $where .= ' AND rs.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
     
        $SystemStatusesModel = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');
        
        if(count($UserStatusPreferences)) {
            $sIDStr = '';
            $sep = '';
            
            foreach($UserStatusPreferences as $usp) {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep = ',';
            }
            $where .= ' AND js.StatusID IN (' . $sIDStr . ')';
        }
        
        if(isset($args['serviceProvidecheck']) && is_array($args['serviceProvidecheck'])){
            $where .= ' AND t1.ServiceProviderID IN (' . implode(',',$args['serviceProvidecheck']) . ')';
        }

        //$where .= " GROUP BY t1.JobID";
       $groupby = " GROUP BY  t1.ServiceProviderID";
        
       $sql = "Select ".implode(",",$columns)." From ".$table." Where ".$where.$groupby;
       
       $result = $this->Query($this->conn, $sql);

       return  $result;
    }
    
    //--------------function for closed jobs stats-----------------------------------------------------
    public function fetchAllClosedStats( $args, $gaugePrefs,$completionStatus ) {
       
        $sql = null;
        if(isset($args['openby'])) {                                            /* Get which perception of closed by we need */                                        
            if ($args['openby']== 'branch') {                                   /* Branch selected */
                $columnname = '`ClosedDate`';                                  /* Field in database we check for closed */
            } else {                                                            /* Otherwise service provider */
                $columnname = '`ServiceProviderDespatchDate`';                 /* Field in database we check for closed */
            }
        } else {
            $columnname = '`ServiceProviderDespatchDate`';                     /* Field in database we check for closed */
        }
        
         $args['where'] = $columnname.' IS NOT NULL  ' . $this->UserJobFilter('t1');
         $args['where'] .= ' AND t1.CompletionStatus IS NOT NULL';
        
        if ( isset($args['datefrom'])) {
            $datefrom = $args['datefrom'];
        } else {
            $year = date('Y');                                                  /* Current Year */
            if (date('m') == '01') {                                            /* If Januray */
                $year--;                                                        /* Last year */
            }
            $datefrom = "$year-01-01";
        } /* fi isset $args['display'] */

        if ( isset($args['dateto'])) {
            $dateto = $args['dateto'];
        } else {
            $dateto = date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))); /*Last day of last month */
        } /* fi isset $args['display'] */
        
        if(isset($args['display'])) {
            switch ($args['display']) {
                case 'ytd':
                    $args['where'] .= " AND YEAR($columnname) = YEAR(CURDATE())";
                    break;

                case 'lm':
                    $args['where'] .= " AND YEAR($columnname) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH($columnname) = MONTH(CURDATE() - INTERVAL 1 MONTH)";
                    break;

                case '30':
                    $args['where'] .= " AND $columnname > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                    break;
                
                case 'dr':
                    $args['where'] .= " AND Date($columnname) >= '$datefrom' AND Date($columnname) <= '$dateto'";
                    break;

                default: /* mtd */
                    $args['where'] .= " AND YEAR($columnname) = YEAR(CURDATE()) AND MONTH($columnname) = MONTH(CURDATE())";
            }
        } else {
            $args['where'] .= " AND YEAR($columnname) = YEAR(CURDATE()) AND MONTH($columnname) = MONTH(CURDATE())";
        }
        
        if ( isset($args['daysFrom']) && isset($args['daysTo']) )  {            /* Guage (days) Filter */
            $args['where'] .= " 
			AND DATEDIFF(t1.$columnname, t1.`DateBooked`) < {$args['daysTo']}
                        AND DATEDIFF(t1.$columnname, t1.`DateBooked`) >= {$args['daysFrom']}                 
                        ";
        }
        
        if(isset($args['btnName']) && isset($args['btnValue']) && $args['btnName'] && $args['btnValue'])
        {
            if($args['btnName']=="b1")
            {
                $args['where'] .= " AND t1.".$columnname." > DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            } 
            else if($args['btnName']=="b2" && isset($args['btnValue2']) && $args['btnValue2'])
            {
                if($args['btnValue2']<$args['btnValue'])
                {
                    $tempValue = $args['btnValue'];
                    $args['btnValue'] = $args['btnValue2'];
                    $args['btnValue2'] = $tempValue;
                }
                $args['where'] .= " AND t1.".$columnname." <=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) AND t1.".$columnname." >=DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue2']." DAY)";
            }
            else if($args['btnName']=="b3" || $args['btnName']=="b4")
            {
                $args['where'] .= " AND t1.".$columnname." < DATE_SUB( CURDATE( ) , INTERVAL ".$args['btnValue']." DAY) ";
            }
        }
        
        //Unit Type Filter
        if(isset($args['unitType']) && $args['unitType']) {
           $args['where'] .= ' AND t4.UnitTypeID=' . $this->conn->quote($args['unitType']);
        }
        
         //skillset Filter
        if(isset($args['skillSet']) && $args['skillSet']) {
           $args['where'] .= ' AND t10.RepairSkillID=' . $this->conn->quote($args['skillSet']);
        }
        
         // Branch Filter
        if (isset($args['branch']) && $args['branch']) {
           $args['where'] .= ' AND t1.BranchID='.$this->conn->quote( $args['branch'] ); 
        }
        
        // Network Filter
        if (isset($args['network']) && $args['network']) {
           $args['where'] .= ' AND t1.NetworkID='.$this->conn->quote( $args['network'] ); 
        }
        
         // Manufacturer Filter
        if (isset($args['manufacturer']) && $args['manufacturer']) {
           $args['where'] .= ' AND t1.ManufacturerID='.$this->conn->quote( $args['manufacturer'] ); 
        }
        
        
         // Service Provider Filter
        if (isset($args['serviceProvider']) && $args['serviceProvider']) {
           $args['where'] .= ' AND t1.ServiceProviderID='.$this->conn->quote( $args['serviceProvider'] ); 
        }
        

        // Client Filter
        if (isset($args['client']) && $args['client']) {
           $args['where'] .= ' AND t1.ClientID='.$this->conn->quote( $args['client'] ); 
        }
        
        // Brand  Filter
        if (isset($args['brand']) && $args['brand']) {
           $args['where'] .=  ' AND t8.BrandID='.$this->conn->quote( $args['brand'] ); 
        }
        
        //Status Type Filter
        if(isset($args['sType']) && $args['sType'] != '0') {
            if($args['sType']=="All Others"){
                if(count($gaugePrefs) > 0){ 
                    $gaugePrefString="";
                    foreach($gaugePrefs as $ini=>$gaugePref){ 
                        if($ini < (count($gaugePrefs)-1)){
                            $gaugePrefString .= "'".$gaugePref['GaDialStatus']."',";
                        }
                    }
                    $gaugePrefString = substr($gaugePrefString,0,-1);
                    if($gaugePrefString <>""){
                        $args['where'] .= " AND t1.CompletionStatus NOT IN ({$gaugePrefString})";
                    }
                }
            }
            else{
                $args['where'] .= ' AND t1.CompletionStatus =' . $this->conn->quote($args['sType']);
            }
        }
        
        $SystemStatusesModel   = $this->controller->loadModel('SystemStatuses');
        $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('closedJobs', 'js');
        
        if(count($UserStatusPreferences))
        {
            $sIDStr = '';
            $sep    = '';
            
            foreach($UserStatusPreferences AS $usp)
            {
                $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                $sep    = ',';
            }
            $args['where'] .= ' AND cs.CompletionStatusID IN ('.$sIDStr.')';
        }
        
        
        $args['where']  .= " AND cs.CompletionStatusID IS NOT NULL";
        
        if (isset($args['scType']) && $args['scType']) {
            $sql = 'SELECT t2.CompanyName, count(*) AS JobCount, t1.ServiceProviderID 
                    FROM job AS t1
                    LEFT JOIN service_provider AS t2 ON t1.ServiceProviderID=t2.ServiceProviderID
                    LEFT JOIN completion_status AS cs ON t1.CompletionStatus = cs.CompletionStatus ';
                    
                    if (isset($args['brand']) && $args['brand'])
                    {
                        $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
                    }
                    
                    $sql_ut = "";
                    if (isset($args['unitType']) && $args['unitType'])
                    {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';

                    }

                    if(isset($args['skillSet']) && $args['skillSet']) {
                        $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
                    }
                    
            
                    
            $sql = $sql.$sql_ut.'
                    WHERE ' . $args['where']
                 . ' GROUP BY t1.ServiceProviderID';
        }
        else //if (isset($args['sType']) && $args['sType']) 
        {
            $sql = 'SELECT t1.CompletionStatus, count(*) AS JobCount, t1.CompletionStatus FROM job AS t1 
                    LEFT JOIN completion_status AS cs ON t1.CompletionStatus=cs.CompletionStatus';
//            $sql = 'SELECT js.StatusName, count(*) AS JobCount, t1.StatusID, js.Colour 
//                    FROM job AS t1
//                    LEFT JOIN status AS js ON t1.StatusID=js.StatusID ';
            if (isset($args['brand']) && $args['brand'])
            {
                $sql = $sql.'LEFT JOIN brand_branch  AS t8 on t1.BranchID = t8.BranchID';
            }
            
            
            $sql_ut = "";
            if (isset($args['unitType']) && $args['unitType'])
            {
                $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID';
                
            }
            
            if(isset($args['skillSet']) && $args['skillSet']) {
                $sql_ut = ' LEFT JOIN model AS t3 ON t1.ModelID = t3.ModelID LEFT JOIN unit_type AS t4 ON t3.UnitTypeID = t4.UnitTypeID LEFT JOIN repair_skill as t10 on t4.RepairSkillID = t10.RepairSkillID';
            }
            
            $sql = $sql.$sql_ut.'
                
                    WHERE ' . $args['where']
                 . ' GROUP BY t1.CompletionStatus
                     ORDER BY t1.CompletionStatus';
        }
            
  //$this->controller->log($sql,"lo__");
        
//        echo $sql."<BR/>";
        
        
        $data = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        if (count($data) == 0)
           $data[] = array('', 0, 0, 'ffffff');
        
        return $data;
    }
    
    public function changeJobModel($spModelID,$jobid){
        $sql="update job set ModelID=(select ModelID from service_provider_model where ServiceProviderModelID=$spModelID) where JobID=$jobid";
        $this->Execute($this->conn,$sql);
    }
    /* Added by Raju India On 16th July 2013 for Tracker Base Log 534 code Starts Here */
    public function getSkylineJobNumber($spId,$sbJobId)
    {
        $sql = "SELECT JobID FROM job WHERE ServiceProviderID=:ServiceProviderID AND ServiceCentreJobNo=:ServiceCentreJobNo"; 
        $params = array( 'ServiceProviderID' => $spId, 'ServiceCentreJobNo'=>$sbJobId);
        $result = $this->query($this->conn, $sql, $params, PDO::FETCH_CLASS  );
        if(isset($result[0]))
        {
            return $result[0];
        }
        else
        {
            return false;
        }
    }
    /* Added by Raju India On 16th July 2013 for Tracker Base Log 534 code Ends Here */
}
?>


