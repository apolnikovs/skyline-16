<?php

/**
 * Short Description of Wallboard DataModel.
 * 
 * Long description of Wallboard DataModel.
 *
 * @author     Brian Etherington 
 * @copyright  2012 PC Control systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 */

require_once('CustomModel.class.php');

class Wallboard extends CustomModel {  
    
    /*
     * Database Connection Object
     * @access public
     */
    public $conn;
        
    /*
     * Constructor
     * 
     * @param  CustomController $Controller  The owning controller class instance
     */
    public function  __construct(CustomController $Controller) {
                  
        parent::__construct($Controller); 
        
        /*$this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );*/
        
    }
    
    /*
     * Description
     * 
     * @return array    $result           xxxxxxxxxxx
     */
    public function TradeAccounts( $filter ) {
        
        $result = array (
                      'Account 01',
                      'Account 02',
                      'Account 03',
                      'Account 04',
                      'Account 05'
                   );
        
        return $result;
        
    }
    
    /*
     * Description
     * 
     * @return array    $result           xxxxxxxxxxx
     */
    public function Manufacturers( $filter ) {
        
        $result = array (
                      'Manufacturer 01',
                      'Manufacturer 02',
                      'Manufacturer 03',
                      'Manufacturer 04',
                      'Manufacturer 05'
                   );
        
        return $result;
        
    }
    
    /*
     * Description
     * 
     * @return array    $result           xxxxxxxxxxx
     */
    public function ProductGroups( $filter ) {
        
        $result = array (
                      'Product Group 01',
                      'Product Group 02',
                      'Product Group 03',
                      'Product Group 04',
                      'Product Group 05'
                   );
        
        return $result;
        
    }
    
    /*
     * Description
     * 
     * @return array    $result           xxxxxxxxxxx
     */
    public function ServiceProviders( $filter ) {
        
        $result = array (
                      'Service Provider 01',
                      'Service Provider 02',
                      'Service Provider 03',
                      'Service Provider 04',
                      'Service Provider 05'
                   );
        
        return $result;
        
    }
    
    /*
     * Description
     * 
     * @return array    $result           xxxxxxxxxxx
     */
    public function Engineers( $filter ) {
        
        $result = array (
                      'Engineer 01',
                      'Engineer 02',
                      'Engineer 03',
                      'Engineer 04',
                      'Engineer 05'
                   );
        
        return $result;
        
    }
    
    public function ServiceCentresCompletedByTAT () {
        
        $result = array (
            'Electronic Services',
            'Forth Electronics',
            'Martin Dawes',
            'D K Audio Visual',
            'Repair Guys',
            'DSC Solutions',
            'DK Audio Visual',
            'Startec',
            'Genserve',
            'Birmingham Specialised Services',
            'Electronic Services',
            'Forth Electronics',
            'Martin Dawes',
            'D K Audio Visual',
            'Repair Guys',
            'DSC Solutions',
            'DK Audio Visual',
            'Startec',
            'Genserve',
            'Birmingham Specialised Services',
            'D K Audio Visual',
            'Repair Guys',
            'DSC Solutions',
            'DK Audio Visual',
            'Startec',
            'Genserve',
            'Birmingham Specialised Services',
            'Electronic Services',
            'Forth Electronics',
            'Martin Dawes',
            'D K Audio Visual',
            'Repair Guys',
            'Martin Dawes',
            'D K Audio Visual',
            'Repair Guys',
            'DSC Solutions',
            'DK Audio Visual',
            'Startec',
            'Genserve',
            'Birmingham Specialised Services',
            'D K Audio Visual',
            'Repair Guys',
            'DSC Solutions',
            'DK Audio Visual',
            'Startec',
            'Genserve',
            'Birmingham Specialised Services',
            'Electronic Services',
            'Forth Electronics',            
            'Genserve'          
        );
        
        return $result;
        
    }

    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function TurnaroundTime( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return 6;
        
    }

    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function AppointmentLeadTime( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function AverageCostPerJob( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {

        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function FirstTimeFix( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        $this->controller->log('TurnaroundTime');
        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function ReRepairs( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function NetPromoterScore( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function CompletedJobsByTat( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return 6;
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function ServiceCentreRepairCapacity( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0, 400);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function JobsWaitingParts( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0, 50);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function JobsOlderThan14Days( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0,20);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function AverageDaysFromBooking( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0,20);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function Estimates( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0, 20);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function Appointments( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return rand(0,100);
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function WorkInProgressByVolume( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return array();
        
    }
    
    /*
     * Description
     * 
     * @param  string $TradeaAccount   xxxxxxxxxxxxx
     * @param  string $Manufacturer    xxxxxxxxxxxxx
     * @param  string $ProductGroup    xxxxxxxxxxxxx
     * @param  string $ServiceProvider xxxxxxxxxxxxx
     * @param  string $Engineer        xxxxxxxxxxxxx
     * @return int    $value           xxxxxxxxxxx
     */
    public function AverageTimeByStatus( $TradeAccount, $Manufacturer, $ProductGroup, $ServiceProvider, $Engineer ) {
        
        return array();
        
    }
    
}
?>

