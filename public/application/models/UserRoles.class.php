<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');


/**
 * Description
 *
 * This class is used for handling database actions of Service Types Page in Lookup Tables section under System Admin
 *
 * @author Simon Tsang <s.tsang@pccsuk.com>
 * @version     1.0
 */

class UserRoles extends CustomModel {
    
    public $debug = false;
    private $conn;
    private $dbColumns = array('T1.RoleID', 'T1.Name', 'T1.UserType', 'T1.Status', 'T1.Comment', 'COUNT(T2.RoleID) as count', 'T1.InactivityTimeout');
    private $tables    = "role as T1 LEFT JOIN assigned_permission as T2 ON T1.RoleID=T2.RoleID ";
    private $table     = "role";
    public $messages;

      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       
        
        
    }

   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */  
    public function fetch($args) {
        
       
       
        if(isset($args['firstArg']) && ($args['firstArg']=='Active' || $args['firstArg']=='In-active'))
        {
            $args['where'] = "T1.Status='".$args['firstArg']."'";
        }
        $args['groupby'] = "T2.RoleID ";
        $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Simon Tsang <s.tsang@pccsuk.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['RoleID']) || !$args['RoleID']) {
               return $this->create($args);
         } else {
             return $this->update($args);
         }
     }
    
     
       /**
     * Description
     * 
     * This method finds the maximum code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum code if it finds in the database table otherwise it returns 0.
     * @author Simon Tsang <s.tsang@pccsuk.com> 
     */   
     public function getCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ServiceTypeCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY RoleID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0])) {
           return $result[0];
        } else {
             return 0;
        }
       
    }
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $ServiceTypeCode  
     * @param interger $BrandID.
     * @param interger $RoleID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
     public function isValidAction($RoleID, $RoleName, $UserType) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RoleID FROM '.$this->table.' WHERE Name=:RoleName AND UserType=:UserType AND RoleID!=:RoleID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':RoleName' => $RoleName, ':UserType' => $UserType, ':RoleID' => $RoleID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['RoleID']) {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
    public function create($args) {

        unset($args['search_results_test_length']);

        $fields = array();
        $add_permission = array();
        $dbColumns = array();
        foreach($this->dbColumns as $keys=>$vals)
        {
            if($keys != 5)
                $dbColumns[] = str_replace ("T1.", "", $vals);
        }
        foreach($args as $key => $a){
            //if(in_array($key, $this->dbColumns) && $key != 'RoleID')
            if(in_array($key, $dbColumns) && $key != 'RoleID')
                $fields[$key] = $a;
        }

        $fields['CreatedDate'] = date('Y-m-d H:i:s');

        #$this->controller->log("UserRoles->create : " . var_export($fields, true));        
        
        //$sql = TableFactory::Modify(
        //    $this->conn,
        //    'Role',
        //    'insert',
        //    $fields
        //    );
        
        $role_table = TableFactory::Role();
        $cmd = $role_table->insertCommand( $fields );
        if (!$this->Execute($this->conn, $cmd, $fields)) {
            
            
            
            
            $this->controller->log('Error Creating Role Record');
            $this->controller->log(var_export($fields, true));
            $this->controller->log($this->lastPDOError()); 
            return array('status' => 'FAIL',
                'message' => $this->controller->page['Errors']['data_error_msg']);
        }
        else
        {
            $RoleID = $this->conn->lastInsertId();
            
            foreach($args as $key => $a){
                
                //PermissionID_1
                if(preg_match('/HiddenPermissionID_/', $key)){
                    list($f, $PermissionID) = explode('_', $key);
                    #echo $PermissionID;
                    
                   if($args['HiddenPermissionID_'.$PermissionID]=="Active")
                   {          
                        $add_permission[] = array('RoleID' => $RoleID, 'PermissionID' => $PermissionID);
                   } 
                }
            }
            
            
            $assigned_permission_table = TableFactory::AssignedPermission();
            //
            // remove all assigned_permission for this role
            //
            $where = 'RoleID='.$RoleID;
            $delete_cmd = $assigned_permission_table->deleteCommand( $where );
            if ($this->Execute($this->conn, $delete_cmd)) {
                //
                // add assigned_permission(s)
                //
                foreach($add_permission as $row){
                    $insert_cmd = $assigned_permission_table->insertCommand( $row );
                    if (!$this->Execute($this->conn, $insert_cmd, $row)) {
                        $this->controller->log('Error Adding Assigned Permissions Record: '.$insert_cmd);
                        $this->controller->log(var_export($row, true));
                        $this->controller->log($this->lastPDOError());                      
                    }
                } 
                
            } else {
                $this->controller->log('Error Deleting Assigned Permissions Record: '.$delete_cmd);
                $this->controller->log(var_export($delete_args, true));
                $this->controller->log($this->lastPDOError());
            }
            
            
            
            
            
            

            return array('status' => 'OK',
                         'message' => $this->controller->page['Text']['data_inserted_msg']);
        }
        #get added ID and then update assigned_permissions

        #$this->controller->log("UserRoles->create : {$sql} \n" . var_export($fields, true));

        //return array('status' => 'OK',
        //        'message' => $this->controller->page['data_inserted_msg']);

//        if(!isset($args['ServiceTypeCode']) || !$args['ServiceTypeCode']) {
//            $args['ServiceTypeCode'] = $this->getCode($args['BrandID'])+1;//Preparing next name code.
//        }
       // $this->controller->log(var_export('tessss', true));
        
//        if($this->isValidAction($args['ServiceTypeCode'], $args['BrandID'], 0)) {
//            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
//          
//            
//            $insertQuery->execute(array(':ServiceTypeName' => $args['ServiceTypeName'], ':ServiceTypeCode' => $args['ServiceTypeCode'], ':Code' => $args['Code'], ':JobTypeID' => $args['JobTypeID'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));
//        
//        
//              return array('status' => 'OK',
//                        'message' => $this->controller->page['data_inserted_msg']);
//        } else {
//            
//            return array('status' => 'ERROR',
//                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
//        }
    }
    
    /*
     * 
     */
    public function createUserRole( $args ){
        
        $user_role_table = TableFactory::UserRole();
        $cmd = $user_role_table->insertCommand( $args ); 
        if (!$this->Execute($this->conn, $cmd, $args)) {
            $this->controller->log('Error Creating User Role Record');
            $this->controller->log(var_export($args, true));
            $this->controller->log($this->lastPDOError()); 
            return array('status' => 'FAIL',
                         'message' => $this->controller->page['Errors']['data_error_msg']);
        }

        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_inserted_msg']);
        
        //TableFactory::Modify(
        //    $this->conn,
       //     'UserRole',
       //     'insert',
       //     $args
       //     );  
        
       // return array('status' => 'OK',
        //        'message' => 'data_inserted_msg');        
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     */ 
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RoleID, Name, UserType, Comment, Status, InactivityTimeout FROM '.$this->table.' WHERE RoleID=:RoleID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':RoleID' => $args['RoleID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
     
    /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Simon Tsang <s.tsang@pccsuk.com>
     * 
     */ 
    public function update($args) {

        $fields = array();
        $ignore_permission = '';
        $add_permission = array();
        $dbColumns = array();
        foreach($this->dbColumns as $keys=>$vals)
        {
            if($keys != 5)
                $dbColumns[] = str_replace ("T1.", "", $vals);
        }
        if($this->isValidAction($args['RoleID'], $args['Name'], $args['UserType'])){

        foreach($args as $key => $a){
            //if(in_array($key, $this->dbColumns))
            if(in_array($key, $dbColumns))
                $fields[$key] = $a;
            
            //PermissionID_1
            if(preg_match('/HiddenPermissionID_/', $key)){
                list($f, $PermissionID) = explode('_', $key);
                #echo $PermissionID;
                
                if($args['HiddenPermissionID_'.$PermissionID]=="Active")
                {        
                    $add_permission[] = array('RoleID' => $args['RoleID'], 'PermissionID' => $PermissionID);
                }

            }
        }
        
        if($args['Status'] == 'In-active')
            $fields['EndDate'] = date('Y-m-d H:i:s');
        
        /*TableFactory::Modify(
            $this->conn,
            'Role',
            'update',
            $fields
            ); */
        
        $role_table = TableFactory::Role();
        $cmd = $role_table->updateCommand( $fields );
        if ($this->Execute($this->conn, $cmd, $fields)) {
            
            $assigned_permission_table = TableFactory::AssignedPermission();
            //
            // remove all assigned_permission for this role
            //
            $where = 'RoleID='.$args['RoleID'];
            $delete_cmd = $assigned_permission_table->deleteCommand( $where );
            if ($this->Execute($this->conn, $delete_cmd)) {
                //
                // add assigned_permission(s)
                //
                
              //  $this->log("-----------");
              //  $this->log($add_permission);
                
                foreach($add_permission as $row){
                    $insert_cmd = $assigned_permission_table->insertCommand( $row );
                    if (!$this->Execute($this->conn, $insert_cmd, $row)) {
                        $this->controller->log('Error Adding Assigned Permissions Record: '.$insert_cmd);
                        $this->controller->log(var_export($row, true));
                        $this->controller->log($this->lastPDOError());                      
                    }
                } 
                
            } else {
                $this->controller->log('Error Deleting Assigned Permissions Record: '.$delete_cmd);
                $this->controller->log(var_export($delete_args, true));
                $this->controller->log($this->lastPDOError());
            }
            
            
        } else {
            $this->controller->log('Error Updating Role Record: '.$cmd);
            $this->controller->log(var_export($fields, true));
            $this->controller->log($this->lastPDOError());
        }
        
        return array('status' => 'OK', 'message' => $this->controller->page['data_updated_msg']);
        
        #remove all assigned_permission for this role
        /*TableFactory::Modify(
            $this->conn,
            'assigned_permission',
            'delete',
            array(),
            'RoleID = ' . $args['RoleID']
            );
        

        # assigned_permission(s)
        foreach($add_permission as $row){

            TableFactory::Modify(
                $this->conn,
                'assigned_permission',
                'insert',
                $row
                );                  
        } */           

        //return array('status' => 'OK', 'message' => $this->controller->page['data_updated_msg']); 
        
        }else{
            return array('status' => 'ERROR', 'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }

    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    public function deleteUserRole( $args ){
        
        $where = 'UserID = ' . $args['UserID'] .' AND  RoleID = ' . $args['RoleID'];
        
        #$this->controller->log('UserRoles->deleteUserRole : ' . $where );
        
        $user_role_table = TableFactory::UserRole();
        $cmd = $user_role_table->deleteCommand( $where );
        if (!$this->Execute($this->conn, $cmd)) {
            $this->controller->log('Error Deleting User Role Record: '.cmd);
            $this->controller->log($this->lastPDOError());           
        }
        
        return array('status' => 'OK', 'message' => $this->controller->page['data_deleted_msg']);
        
        /*TableFactory::Modify(
            $this->conn,
            'UserRole',
            'delete',
            array(),
            $where
            );  
        
        return array('status' => 'OK',
                     'message' => 'data_deleted_msg');  */       
        
    }
    
    
    
}
?>
