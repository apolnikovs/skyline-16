<?php

require_once('CustomModel.class.php');

/**
 * DataTable.class.php
 *
 * This class is used for handling database actions for DataTables with custom settings
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @copyright   2013 PC Control Systems
 * @link       
 * @version     1.04
 * 
 * Changes
 * Date        Version Author                Reason
 * 4/04/2013  1.00    Andris Polnikovs      Initial Version
 * 14/04/2013  1.01    Andris Polnikovs     retrun only visible fieldnames
 * 22/01/2013  1.02    Andrew J. Williams   Data Integrity - Servicebase Refresh Added checkbox to open jobs
 * 04/06/2013  1.03    Andris Polnikovs     Added job fault code table
 * 06/06/2013  1.04    Andris Polnikovs     Added overdueJobs page in datakeys function (uses same table name as openjobs)
 * **************************************************************************** */
class DataTable extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);

        $this->debug = true;
    }

    /*     * ******************************************************************************
     * Description
     * 
     * This method is for getting  table fieldnames,
     * if 2nd param is passed this will return only specified visible columns
     *  for future use in datatables
     * 
     * @param 
     * @return
     * 
     * @author Andris Polnikovs <a.polnikovs@gmail.com>******************************
     */

    //public function getAllFieldNames($table,$visible=false,$order=false,$name=false,$SAname=false)
    public function getAllFieldNames($table, $userID = false, $pageID = false, $spid = false, $extraColumn = false) {

        $visible = false;
        if ($userID) { 
            $columnStrings = $this->getColumnStrings($userID, $pageID);
            $columnStringsSA = $this->getColumnStrings(1, $pageID, 'sa');
            
            if ($columnStrings) {
                $visible = explode(",", $columnStrings['ColumnDisplayString']);
                $order = explode(",", $columnStrings['ColumnOrderString']);
                $name = explode(",", $columnStrings['ColumnNameString']);
                $SAname = explode(",", $columnStringsSA['ColumnNameString']);

                /*Check for column from user preferences is visible or not
                 * if no column visible then it will show from default table view 
                 * set by Super Admin
                */                
                $visiblecounter = 0;
                
                foreach($visible as $vis){
                    if($vis > 0 && $vis <> ""){
                        $visiblecounter++;
                    }
                }
                
                if($visiblecounter > 0){
                    
                }
                else{
                    if ($columnStringsSA) {
                        $visible = explode(",", $columnStringsSA['ColumnDisplayString']);
                        $order = explode(",", $columnStringsSA['ColumnOrderString']);
                        $name = explode(",", $columnStringsSA['ColumnNameString']);
                        $SAname = explode(",", $columnStringsSA['ColumnNameString']);
                    }
                }
            } elseif ($columnStringsSA) {
                $visible = explode(",", $columnStringsSA['ColumnDisplayString']);
                $order = explode(",", $columnStringsSA['ColumnOrderString']);
                $name = explode(",", $columnStringsSA['ColumnNameString']);
                $SAname = explode(",", $columnStringsSA['ColumnNameString']);
            }
        }
        //$this->controller->log($order,"rr_log_");
        //$this->log($table);
        switch ($table) {

            case "service_provider_model": {
                    $id = "ServiceProviderModelID";
                    $ApproveStatus = "";
                }
            case "model": {
                    if (!isset($id) && !$spid) {
                        $id = "ModelID";
                        $ApproveStatus = "model.ApproveStatus";
                    } else {
                        $id = "ServiceProviderModelID";
                        $ApproveStatus = "1";
                    }
                    $res = [
                        "$table.$id",
                        "$table.ModelNumber",
                        "$table.ModelName",
                        "$table.ModelDescription",
                        "$table.CreatedDate",
                        "$table.EndDate",
                        "$table.Status",
                        "$table.Features",
                        "$table.IMEILengthFrom",
                        "$table.IMEILengthTo",
                        "$table.MSNLengthFrom",
                        "$table.MSNLengthTo",
                        "$table.WarrantyRepairLimit",
                        "$table.ExcludeFromRRCRepair",
                        "$table.AllowIMEIAlphaChar",
                        "$table.UseReplenishmentProcess",
                        "$table.HandsetWarranty1Year",
                        "$table.ReplacmentValue",
                        "$table.LoanSellingPrice",
                        "$table.ExchangeSellingPrice",
                        "$table.RRCOrderCap",
                        "mm.Acronym",
                        "mm.ManufacturerName",
                        "ut.UnitTypeName",
                        $ApproveStatus,
                    ];
                    $saDatabaseNames = [
                        $id,
                        "ModelNumber",
                        "ModelName",
                        "ModelDescription",
                        "CreatedDate",
                        "EndDate",
                        "Status",
                        "Features",
                        "IMEILengthFrom",
                        "IMEILengthTo",
                        "MSNLengthFrom",
                        "MSNLengthTo",
                        "WarrantyRepairLimit",
                        "ExcludeFromRRCRepair",
                        "AllowIMEIAlphaChar",
                        "UseReplenishmentProcess",
                        "HandsetWarranty1Year",
                        "ReplacmentValue",
                        "LoanSellingPrice",
                        "ExchangeSellingPrice",
                        "RRCOrderCap",
                        "Manufacturer Acronym",
                        "Manufacturer Name",
                        "Unit Type",
                        $ApproveStatus,
                    ];
                    break;
                }
            case "service_provider_supplier": {
                    $id = "ServiceProviderSupplierID";
                    $ApproveStatus = "";
                }
            case "supplier": {
                    if (!isset($id) && !$spid) {
                        $id = "SupplierID";
                        $ApproveStatus = "ApproveStatus";
                    } else {
                        $id = "ServiceProviderSupplierID";
                        $ApproveStatus = "1";
                    }
                    $saDatabaseNames = array(
                        $id,
                        "Company Name",
                        "Account Number",
                        "Telephone No",
                        "Post Code",
                        "Fax No",
                        "Email Address",
                        "Contact Name",
                        "Collect Supplier Order No",
                        "Postage Charge Prompt",
                        "Default Postage Charge",
                        "Normal Supply Period",
                        "Minimum Order Value",
                        "Usage History Period",
                        "Multiply By Factor",
                        "Tax Exempt",
                        "Order Permitted VariancePercent",
                        "Status",
                        "Building Name Number",
                        "Street",
                        "Local Area",
                        "Town City",
                        "Country Name",
                        "Currency Name",
                        $ApproveStatus
                    );
                    $res = array(
                        $id,
                        "CompanyName",
                        "AccountNumber",
                        "TelephoneNo",
                        "PostCode",
                        "FaxNo",
                        "EmailAddress",
                        "ContactName",
                        "CollectSupplierOrderNo",
                        "PostageChargePrompt",
                        "DefaultPostageCharge",
                        "NormalSupplyPeriod",
                        "MinimumOrderValue",
                        "UsageHistoryPeriod",
                        "MultiplyByFactor",
                        "TaxExempt",
                        "OrderPermittedVariancePercent",
                        "Status",
                        "BuildingNameNumber",
                        "Street",
                        "LocalArea",
                        "TownCity",
                        "country.Name",
                        "CurrencyName",
                        $ApproveStatus
                    );

                    break;
                }
            case "service_provider_manufacturer": {
                    $id = "ServiceProviderManufacturerID";
                    $ApproveStatus = "";
                }
            case "manufacturer": {
                    if (!isset($id) && !$spid) {
                        $id = "t1.ManufacturerID";
                        $ApproveStatus = "ApproveStatus";
                    } else {
                        $id = "ServiceProviderManufacturerID";
                        $ApproveStatus = "1";
                    }
                    $res = array(
                        $id,
                        "ManufacturerName",
                        "Acronym",
                        "BuildingNameNumber",
                        "Street",
                        "LocalArea",
                        "TownCity",
                        "AuthorisationRequired",
                        "AuthReqChangedDate",
                        "AuthorisationManager",
                        "AuthorisationManagerEmail",
                        "AccountNo",
                        "Postcode",
                        "TelNoSwitchboard",
                        "TelNoSpares",
                        "TelNoWarranty",
                        "TelNoTechnical",
                        "FaxNo",
                        "EmailAddress",
                        "WarrantyEmailAddress",
                        "SparesEmailAddress",
                        "ContactName",
                        "StartClaimNo",
                        "VATNo",
                        $ApproveStatus
                    );
                    $saDatabaseNames = [
                        "ID",
                        "Manufacturer Name",
                        "Acronym",
                        "Building Name/Number",
                        "Street",
                        "Local Area",
                        "Town City",
                        "Authorisation Required",
                        "Auth Req Changed Date",
                        "Authorisation Manager",
                        "Authorisation ManagerEmail",
                        "Account No",
                        "Postcode",
                        "TelNo Switchboard",
                        "TelNo Spares",
                        "TelNo Warranty",
                        "TelNo Technical",
                        "FaxNo",
                        "Email Address",
                        "Warranty EmailAddress",
                        "Spares Email Address",
                        "Contact Name",
                        "Start Claim No",
                        "VAT No",
                        "Approve Status"
                    ];

                    break;
                }
            case "closedJob": {
                    if (!$extraColumn) {
                        $extraColumn = "ClosedDate";
                    }
                    $res = [
                        "t1.JobID",
                        "t1.JobID		  ",
                        " DATE_FORMAT(t1.DateBooked,'%d/%m/%Y')	    ",
                        "DATE_FORMAT(t1.$extraColumn,'%d/%m/%Y')	    ",
                        "DATEDIFF(t1.$extraColumn, t1.DateBooked)",
                        "t6.Acronym		    ",
                        "t1.ServiceCentreJobNo  ",
                        "t7.ServiceTypeName	    ",
                        "CONCAT_WS(' ',cu.ContactLastName, cu.ContactFirstName) ",
                        "CASE
				WHEN t1.JobSite = 'workshop' THEN 'W'
				WHEN t1.JobSite = 'field call' THEN 'F'
			    END			    ",
                        "IF (t1.`ProductLocation` = 'Branch',
                                'B'
                            ,
                                'SP'
                            ) ",
                        "t1.AgentStatus",
                        "t1.AuthorisationNo",
                        "date_format(t1.ClosedDate,'%d/%m/%Y')",
                        "date_format(t1.CollectionDate,'%d/%m/%Y')",
                        "t1.CompletionStatus",
                        "t1.ConditionCode",
                        "date_format(t1.DateReturnedToCustomer,'%d/%m/%Y (%H:%i)')",
                        "date_format(t1.DateUnitReceived,'%d/%m/%Y')",
                        "
                CASE
		WHEN t1.DownloadedToSC = '1' THEN 'Yes'
		WHEN t1.DownloadedToSC = '0' THEN 'No'
		ELSE NULL
	    END",
                        "t1.eInvoiceStatus",
                        "t1.EngineerAssignedDate",
                        "t1.EngineerAssignedTime",
                        "t1.EngineerName",
                        "t1.JobSite",
                        "t1.OpenJobStatus",
                        "t1.ProductLocation",
                        "date_format(t1.RepairCompleteDate,'%d/%m/%Y')",
                        "t1.RMANumber",
                        "t1.SamsungRefNo",
                        "t1.ServiceBaseModel",
                        "t1.ServiceBaseUnitType",
                        "date_format(t1.ServiceProviderDespatchDate,'%d/%m/%Y (%H:%i)')",
                        "t1.TimeBooked",
                        "concat_ws(' ',u.ContactFirstName,u.ContactLastName)",
                        "concat_ws(' ',cu.ContactLastName,cu.ContactFirstName)",
                        "b.BranchName",
                        "co.CourierName",
                        "jt.Type",
                        "ma.ManufacturerName",
                        "t2.ProductNo",
                        "t1.NetworkRefNo",
                        "(select concat(date_format(max(Date),'%d/%m/%Y'),' (',date_format(max(Date),'%H:%i'),')') from status_history sh where sh.JobID=t1.JobID)",
                        "(select date_format(max(ContactDate),'%d/%m/%Y') from contact_history ctt where ctt.JobID=t1.JobID)",
                        "concat(date_format(t1.RefreshDate,'%d/%m/%Y'),' (',date_format(t1.RefreshDate,'%H:%i'),')')",
                        "t1.PolicyNo",                        
                        "t1.AgentRefNo"
                    ];
                    $saDatabaseNames = [
                        "JobID <span style='color:red'>System</span>",
                        "Job ID ",
                        "Booked",
                        "Despached",
                        "Days",
                        "ServiceProvider",
                        "SB Job No",
                        "Service",
                        "Customer Name",
                        "F/S",
                        "Type",
                        "Agent Status",
                        "Authorisation No",
                        "Closed Date",
                        "Collection Date",
                        "Completion Status",
                        "Condition Code",
                        "Date Returned To Customer",
                        "Date Unit Received",
                        "Downloaded To SC",
                        "eInvoice Status",
                        "Engineer Assigned Date",
                        "Engineer Assigned Time",
                        "Engineer Name",
                        "Job Site",
                        "Open Job Status",
                        "Product Location",
                        "Repair Complete Date",
                        "RMA Number",
                        "Samsung Ref No",
                        "Service Base Model",
                        "Service Base Unit Type",
                        "Service Provider Despatch Date",
                        "Time Booked",
                        "Booked by",
                        "Customer",
                        "Branch Name",
                        "Courier",
                        "Job Type",
                        "Manufacturer",
                        "Product",
                        "Network Ref. No.",
                        "Last Status Change Date",
                        "Last Contact History Date",
                        "Refresh Date",
                        "Policy No",                        
                        "Agent Ref. No"
                    ];
                    break;
                }
            case "job": {
                    if (!$extraColumn) {
                        $extraColumn = "CASE
		WHEN @days > (@turnaround := turnaround(t1.JobID))
		THEN (@days - @turnaround)
		ELSE 0
	    END";
                    }
                    $res = [
                        "t1.JobID",
                        "t1.JobID",
                        "DATE_FORMAT(t1.DateBooked, '%d/%m/%Y')",
                        "@days := DATEDIFF(CURRENT_DATE, t1.DateBooked)",
                        $extraColumn,
                        "t6.Acronym",
                        "t1.ServiceCentreJobNo",
                        "
	    CASE
		WHEN t4.UnitTypeName IS NOT NULL 
		THEN t4.UnitTypeName
		ELSE t1.ServiceBaseUnitType
	    END
	    ",
                        "
	    CASE
		WHEN t5.ManufacturerName IS NOT NULL
		THEN t5.ManufacturerName
		ELSE t1.ServiceBaseManufacturer
	    END
	    ",
                        "js.StatusName",
                        "
	    CASE
		WHEN t1.JobSite = 'field call' THEN 'F'
		WHEN t1.JobSite = 'workshop' THEN 'W'
		ELSE NULL
	    END
	    ",
                        "t1.AgentStatus",
                        "t1.AuthorisationNo",
                        "date_format(t1.ClosedDate,'%d/%m/%Y')",
                        "date_format(t1.CollectionDate,'%d/%m/%Y')",
                        "t1.CompletionStatus",
                        "t1.ConditionCode",
                        "date_format(t1.DateBooked,'%d/%m/%Y')",
                        "date_format(t1.DateReturnedToCustomer,'%d/%m/%Y (%H:%i)')",
                        "date_format(t1.DateUnitReceived,'%d/%m/%Y')",
                        "
                CASE
		WHEN t1.DownloadedToSC = '1' THEN 'Yes'
		WHEN t1.DownloadedToSC = '0' THEN 'No'
		ELSE NULL
	    END",
                        "t1.eInvoiceStatus",
                        "t1.EngineerAssignedDate",
                        "t1.EngineerAssignedTime",
                        "t1.EngineerName",
                        "t1.JobSite",
                        "t1.OpenJobStatus",
                        "t1.ProductLocation",
                        "date_format(t1.RepairCompleteDate,'%d/%m/%Y')",
                        "t1.RMANumber",
                        "t1.SamsungRefNo",
                        "t1.ServiceBaseModel",
                         "t1.ServiceBaseUnitType",
                        "t1.ServiceCentreJobNo",
                        "t7.ServiceTypeName",
                        "date_format(t1.ServiceProviderDespatchDate,'%d/%m/%Y (%H:%i)')",
                        "t1.TimeBooked",
                        "concat_ws(' ',u.ContactFirstName,u.ContactLastName)",
                        "concat_ws(' ',cu.ContactLastName,cu.ContactFirstName)",
                        "b.BranchName",
                        "co.CourierName",
                        "jt.Type",
                        "ne.CompanyName",
                        "t1.NetworkRefNo",
                        "(select concat(date_format(max(Date),'%d/%m/%Y'),' (',date_format(max(Date),'(%H:%i)'),')') from status_history sh where sh.JobID=t1.JobID)",
                        "(select date_format(max(ContactDate),'%d/%m/%Y') from contact_history ctt where ctt.JobID=t1.JobID)",
                        "concat(date_format(t1.RefreshDate,'%d/%m/%Y'),' (',date_format(t1.RefreshDate,'(%H:%i)'),')')",
                        "ma.ManufacturerName",
                        "t1.OriginalRetailer",
                        "t1.GuaranteeCode",
                        "t1.RetailerLocation",
                        "t1.SerialNo",
                        "date_format(t1.DateOfPurchase,'%d/%m/%Y')",
                        "t1.ConditionCode",
                        "t1.SymptomCode",
                        "t1.ChargeableLabourCost",
                        "t1.PolicyNo",
                        "t1.AuthorisationNo",
                        "t1.AgentRefNo",
                        "ras.RAStatusName",
                        "date_format(ras.ModifiedDate,'%d/%m/%Y (%H:%i)')",
                        "cl.ClientName"
                        
                        

                    ];
                    $saDatabaseNames = [
			"JobID <span style='color:red'>System</span>",
                        "JobID ",
                        "Booked",
                        "Days",
                        "Overdue",
                        "ServiceProvider",
                        "Job No",
                        "Product Type",
                        "Manufacturer",
                        "Status",
                        "F/S",
                        "Agent Status",
                        "Authorisation No",
                        "Closed Date",
                        "Collection Date",
                        "Completion Status",
                        "Condition Code",
                        "Date Booked",
                        "Date Returned To Customer",
                        "Date Unit Received",
                        "Downloaded To SC",
                        "eInvoice Status",
                        "Engineer Assigned Date",
                        "Engineer Assigned Time",
                        "Engineer Name",
                        "Job Site",
                        "Open Job Status",
                        "Product Location",
                        "Repair Complete Date",
                        "RMA Number",
                        "Samsung Ref No",
                        "Service Base Model",
                        "Service Base Unit Type",
                        "Service Centre Job No",
                        "Service Type Name",
                        "Service Provider Despatch Date",
                        "Time Booked",
                        "Customer",
                        "BookedBy",
                        "Branch Name",
                        "Courier",
                        "Job Type",
                        "Warrantor",
                        "Network Ref. No.",
                        "Last Status Change Date",
                        "Last Contact History Date",
                        "Refresh Date",
                        "Brand",
                        "Original Retailer",
                        "Guarantee Code",
                        "Retailer",
                        "Serial No",
                        "DOP",
                        "Condition Code",
                        "Symptom Code ",
                        "Chargeable Labour Cost",
                        "Policy No",
                        "Authorisation No",
                        "Agent Ref. No",
                        "RA Status",
                        "RA Modified Date",
                        "Client"
                        
                    ];
                    break;
                }
            case "sp_part_stock_template": {
                    $res = [
                        "$table.SpPartStockTemplateID",
                        "sp.Acronym",
                        "$table.PartNumber",
                        "format($table.PurchaseCost, 2)",
                        "$table.Description",
                        "$table.MinStock",
                        "$table.MakeUpTo",
                        "ColourName",
                        "$table.Status",
                        "u.Username",
                        "u2.Username",
                        "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$table.SPPartStockTemplateID
                and sps.Available='Y' and sps.InStock='Y'    
                )",
                        "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$table.SPPartStockTemplateID
                 and sps.InStock='Y'    
                )",
                        "sup.CompanyName",
                        "Sundry",
                        "OEMPartNo",
                        "OEMPartDesc",
                        "InitialQty",
                        "ShelfLocation",
                        "BinLocation",
                        "PartUsageLimit",
                        "Accessory",
                        "AccessoryCost",
                        "AccessoryWarrantyPeriod",
                        "AccessorySerialNumber",
                        "AllowDuplicatePartOnExchange",
                        "RetailItem",
                        "SuspendPart",
                        "RedundantPart",
                        "ReturnFaultySpare",
                        "ChargeablePartOnly",
                        "AttachBySolder",
                        "VATExempt",
                        "SerialisedStock",
                        "ShelfLifeExpiryDate",
                        "UseStockLevelTrigger",
                        "UseAutomaticReOrderPeriod",
                        "AutoReOrderPartEvery",
                        "AutoReOrderQty",
                        "PercentageMarkupMainStore",
                        "UsePercentageMarkupForWarranty",
                        "WarrantyPercentageMarkupValue",
                        "SalesPrice",
                        "UsePercentageMarkupForChargeable",
                        "TradeCost",
                        "ChargeablePercentageMarkupValue",
                        "VATRateRepair",
                        "VATRateSale"
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Service Provider",
                        "Part Number",
                        "Purchase Cost",
                        "Part Descripion",
                        "Min. Stock",
                        "Make Up To",
                        "Colour",
                        "Status",
                        "User Created",
                        "User Modified",
                        "Stock Available",
                        "Stock Total",
                        "Supplier",
                        "Sundry",
                        "OEM Part No",
                        "OEM Part Desc",
                        "Initial Qty",
                        "Shelf Location",
                        "Bin Location",
                        "Part Usage Limit",
                        "Accessory",
                        "Accessory Cost",
                        "Accessory Warranty Period",
                        "Accessory Serial Number",
                        "Allow Duplicate Part On Exchange",
                        "Retail Item",
                        "Suspend Part",
                        "Redundant Part",
                        "Return Faulty Spare",
                        "Chargeable Part Only",
                        "Attach By Solder",
                        "VAT Exempt",
                        "Serialised Stock",
                        "Shelf Life Expiry Date",
                        "Use StockLevel Trigger",
                        "Use Automatic Re-Order Period",
                        "Auto Re-Order Part Every",
                        "Auto Re-Order Qty",
                        "Percentage Markup Main Store",
                        "Use Percentage Markup For Warranty",
                        "Warranty Percentage Markup Value",
                        "Sales Price",
                        "Use Percentage Markup For Chargeable",
                        "Trade Cost",
                        "Chargeable Percentage Markup Value",
                        "VAT Rate Repair",
                        "VAT Rate Sale"
                    ];
                    break;
                }

            case "part": {
                    $res = [
                        "$table.PartID",
                        "sps.CompanyName",
                        "spst.PartNumber",
                        "spst.Description",
                        "spst.PurchaseCost",
                        "spst.MinStock",
                        "spst.MakeUpTo",
                        "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=spst.SPPartStockTemplateID
                and sps.Available='Y' and sps.InStock='Y'    
                )",
                        "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$table.SPPartStockTemplateID
                 and sps.InStock='Y'    
                )",
                        "spsta.PartStatusName",
                        "$table.Quantity",
                        "$table.JobID",
                        "$table.SPPartRequisitionID"
                    ];
                    $saDatabaseNames = [
                        "Part ID",
                        "Supplier",
                        "Stock Code",
                        "Description",
                        "Purchase Cost",
                        "Minimum Stock",
                        "Make Up To",
                        "Total Stock",
                        "Available Quantity",
                        "Status",
                        "Quantity Required",
                        "JobID",
                        "Requisition No"
                    ];
                    break;
                }
            case "job_fault_code": {
                    $res = [

                        "JobFaultCodeID",
                        "RepairTypeID",
                        "JobFaultCode",
                        "Status",
                        "FieldNo",
                        "FieldType",
                        "Lookup",
                        "NonFaultCode",
                        "MainInFault",
                        "MainOutFault",
                        "UseAsGenericDescription",
                        "RestrictLength",
                        "LengthFrom",
                        "LengthTo",
                        "ForceFormat",
                        "Format",
                        "ReplicateToFaultDescription",
                        "HideWhenRelatedFaultCodeBlank",
                        "FaultCodeNo",
                        "HideForThirdPartyJobs",
                        "NotReqForThirdPartyJobs",
                        "ReqAtBookingForChargeable",
                        "ReqAtCompletionForChargeable",
                        "ReqAtBookingForWarranty",
                        "ReqAtCompletionForWarranty",
                        "ReqIfExchangeIssued",
                        "ReqOnlyForRepairType",
                        "UseLookup",
                        "ForceLookup",
                    ];
                    $saDatabaseNames = [
                        "Job Fault Code ID",
                        "Repair Type ID",
                        "Job Fault Code",
                        "Status",
                        "Field No",
                        "Field Type",
                        "Lookup",
                        "Non Fault Code",
                        "Main In Fault",
                        "Main Out Fault",
                        "Use As Generic Description",
                        "Restrict Length",
                        "Length From",
                        "Length To",
                        "Force Format",
                        "Format",
                        "Replicate To Fault Description",
                        "Hide When Related Fault Code Blank",
                        "Fault Code No",
                        "Hide For Third Party Jobs",
                        "Not Req For Third Party Jobs",
                        "Req At Booking For Chargeable",
                        "Req At Completion For Chargeable",
                        "Req At Booking For Warranty",
                        "Req At Completion For Warranty",
                        "Req If Exchange Issued",
                        "Req Only For Repair Type",
                        "Use Lookup",
                        "Force Lookup",
                    ];
                    break;
                }
            case "job_fault_code_lookup": {
                    $res = [
                        "$table.JobFaultCodeLookupID",
                        "$table.LookupName",
                        "$table.Description",
                        "ManufacturerName",
                        "$table.ExcludeFromBouncerTable",
                        "$table.ForcePartFaultCode",
                        "$table.PartFaultCodeNo",
                        "$table.JobTypeAvailability",
                        "$table.PromptForExchange",
                        "$table.RelatedPartFaultCode",
                        "$table.RepairTypeIDForChargeable",
                        "$table.RepairTypeIDForWarranty",
                        "$table.RestrictLookup",
                        "$table.RestrictLookupTo",
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Lookup Name",
                        "Description",
                        "Manufacturer",
                        "Exclude From Bouncer Table",
                        "Force Part Fault Code",
                        "Part Fault Code No",
                        "Job Type Availability",
                        "Prompt For Exchange",
                        "Related Part FaultCode",
                        "Repair Type ID For Chargeable",
                        "Repair Type ID For Warranty",
                        "Restrict Lookup",
                        "Restrict Lookup To",
                    ];
                    break;
                }
            case "part_fault_code_lookup_model": {
                   $mid = "ModelName";
                }  
            case "part_fault_code_lookup": {
                 if (!isset($id) && !$spid) {
                    $mid = "ModelName";
                    }   
                $res = [
                        "PartFaultCodeLookupID" ,
                        "$table.LookupName",
                        "$table.FaultCodeDescription",
                        "$table.Status",
                        "ManufacturerName",
                        "$table.FieldNumber",
                        "$table.ForceJobFaultCode",
                        "$table.JobFaultCodeNo",
                        "$table.RestrictLookup",
                        "$table.RestrictLookupTo",
                        "$table.JobTypeAvailability",
                        
                        
                        
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Lookup Name",
                        "Fault Code Description",
                        "Status",
                        "ManufacturerID",
                        "Field Number",
                        "Force Job Fault Code",
                        "Job Fault Code No",
                        "Restrict Lookup",
                        "Restrict Lookup To",
                        "Job Type Availability",
                        
                       
                    ];
                    break;
                }
                // added status & field number  by thirumal
            case "part_fault_code": {
                    $res = [
                        "$table.PartFaultCodeID",
                        "$table.FieldName",
                        "ManufacturerName",
                        "$table.Status",
                        "$table.FieldNumber",
                        "$table.NonFaultCode",
                        "$table.MainOutFault",
                        "$table.KeyRepair",
                        "$table.CompulsoryAtCompletion",
                        "$table.CompulsoryForAdjustment",
                        "$table.FieldType",
                        "$table.Format",
                        "$table.ForceFormat",
                        "$table.CopyFromJobFaultCode",
                        "$table.JobFaultCodeNo",
                        "$table.RestrictLength",
                        "$table.LengthFrom",
                        "$table.LengthTo",
                        "$table.FillWithNAForAccessory",
                        "$table.FillWithNAForSoftwareUpgrade",
                        "$table.UseLookup",
                        "$table.ForceLookup",
                    ];
                    $saDatabaseNames = [
                        "Part FaultCode ID",
                        "Field Name",
                        "Manufacturer",
                        "Status",
                        "Field Number",
                        "Non Fault Code",
                        "Main Out Fault",
                        "Key Repair",
                        "Compulsory At Completion",
                        "Compulsory For Adjustment",
                        "Field Type",
                        "Format",
                        "Force Format",
                        "Copy From Job Fault Code",
                        "Job Fault Code No",
                        "Restrict Length",
                        "Length From",
                        "Length To",
                        "Fill With NA For Accessory",
                        "Fill With NA For Software Upgrade",
                        "Use Lookup",
                        "ForceLookup",
                    ];
                    break;
                }
            case "repair_type": {
                    $res = [
                        "RepairTypeID",
                        "RepairType",
                        "JobWeighting",
                        "RepairIndex",
                        "Chargeable",
                        "Warranty",
                        "WarrantyCode",
                        "CompulsoryFaultCoding",
                        "ExcludeFromEDI",
                        "ForceAdjustmentIfNoPartsUsed",
                        "ExcludeFromInvoicing",
                        "PromptForExchange",
                        "NoParts",
                        "ScrapExchange",
                        "ExcludeRRCHandlingFee",
                        "Unavailable",
                        "Type",
                        "Status",
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Repair Type",
                        "Job Weighting",
                        "Repair Index",
                        "Chargeable",
                        "Warranty",
                        "Warranty Code",
                        "Compulsory FaultCoding",
                        "Exclude From EDI",
                        "Force Adjustment If No Parts Used",
                        "Exclude From Invoicing",
                        "Prompt For Exchange",
                        "No Parts",
                        "Scrap Exchange",
                        "Exclude RRC Handling Fee",
                        "Unavailable",
                        "Type",
                        "Status",
                    ];
                    break;
                }
            case "service_provider_part_location": {
                    $res = [

                        "ServiceProviderPartLocationID",
                        "LocationName",
                        "MainStore",
                        "RepairSite",
                        "UseStockAllocation",
                        "OutWarrantyMarkupPerc",
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Name",
                        "Main Store",
                        "Repair Site",
                        "Use Stock Allocation",
                        "Out Warranty Markup %",
                    ];
                    break;
                }
            case "service_provider_shelf_location": {
                    $res = [

                        "ServiceProviderShelfLocationID",
                        "ShelfLocationName",
                        "Status",
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Name",
                        "Status",
                    ];
                    break;
                }
            case "service_provider_part_category": {
                    $res = [

                        "ServiceProviderPartCategoryID",
                        "CategoryName",
                        "$table.Status",
                        "sp.Acronym"
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Name",
                        "Status",
                        "ServiceProvider"
                    ];
                    break;
                }
            case "service_provider_part_sub_category": {
                    $res = [
                        "ServiceProviderPartSubCategoryID",
                        "SubCategoryName",
                        "CategoryName",
                        "$table.Status",
                        "sp.Acronym"
                    ];
                    $saDatabaseNames = [
                        "ID",
                        "Sub-Category Name",
                        "Category Name",
                        "Status",
                        "ServiceProvider"
                    ];
                    break;
                }
                
                case "currency": {
                    $res = [
                        "CurrencyID",
                        "CurrencyName",
                        "CurrencyCode",
                        "CurrencySymbol",
                        "AutomaticExchangeRateCalculation",
                        "ConversionCalculation",
                        "ExchangeRate",
                        "Status",
                    ];
                    $saDatabaseNames = [
                        "Currency ID",
                        "Currency Name",
                        "Currency Code",
                        "Currency Symbol",
                        "Automatic Exchange Rate Calculation",
                        "Conversion Calculation",
                        "Exchange Rate",
                        "Status",
                    ];
                    break;
                }
                case "accessory": {
                    $res = [
                        "$table.AccessoryID",
                        "$table.AccessoryName",
                        "$table.CreatedDate",
                        "$table.ModifiedDate",
                        "$table.ApproveStatus",
                        "UnitTypeName",
                        "$table.ModifiedUserID",
                        "$table.Status",
                    ];
                    $saDatabaseNames = [
                        "Accessory ID",
                        "Accessory Name",
                        "Created Date",
                        "Modified Date",
                        "Approve Status",
                        "Unit Type Name",
                        "Modified User ID",
                        "Status",
                    ];
                    break;
                }

                default: {

                    $sql = "SELECT distinct(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS
 WHERE table_name = '$table'
   ";
                    $res = $this->Query($this->conn, $sql);
                }
        }
        $rr = array();
        if ($visible) {
            //getting only visible columns +id as its always should be selected
            foreach ($visible as $k => $v) {
                if ($v == 1 || $k == 0) {

                    if ($k == 0) {//id exception
                        $res2[0] = $res[$k];
                        $names[0] = "ID";
                    } else {
                        //ordering
                        if (isset($res[$k])) {
                            $res2[$order[$k] + 1] = $res[$k];
                            if (!isset($SAname[$k])) {
                                $SAname[$k] = '';
                            }
                            //custom name use user name, if blank system admin default name, if blank use database column name
                            $names[$order[$k] + 1] = (isset($name[$k]) && $name[$k] != '' ? $name[$k] : ($SAname[$k] != '' ? $SAname[$k] : isset($saDatabaseNames[$k]) ? $saDatabaseNames[$k] : (isset($res[$k]['COLUMN_NAME']) ? $res[$k]['COLUMN_NAME'] : $res[$k])));
                        }
                    }
                }
            }



            //getting correct order

            $res = $res2;
        }
        foreach ($res as $r => $d) {
            if (isset($d['COLUMN_NAME'])) {
                $rr[$r] = $d['COLUMN_NAME'];
            } else {
                $rr[$r] = $d;
            }
        }
        ksort($rr, SORT_NATURAL);
        if ($visible) {
            ksort($names, SORT_NATURAL);
        }
        ///reordering array keys
        foreach ($rr as $r) {
            $re[] = $r;
        }

        if (isset($re)) {
            $rr = $re;
        };

        //Sa name column used if not posible display database column names due use of functions
        if (isset($saDatabaseNames) && !isset($names)) {
            foreach ($saDatabaseNames as $k) {
                $names[] = $k;
            }
        }
//     echo"<pre>";
//        print_r(array($rr,$names));
//        echo"</pre>";
        // $this->controller->log($rr);
        // print_r($rr);
        if (isset($names)) {
            //$this->controller->log(array($rr,$names),"laza");
            return array($rr, $names);
        } else {
            return $rr;
        }
    }

    /*     * ******************************************************************************
     * Description
     * 
     * This method is for fetching all tables for future use in datatables
     * 
     * @param 
     * @return
     * 
     * @author Andris Polnikovs <a.polnikovs@gmail.com>******************************
     */

    public function getColumnStrings($userID, $pageID, $mode = '') { 
        
        if($mode === "sa"){
            $table = "datatables_custom_default_columns";
        }
        else{
            $table = "datatables_custom_columns";
        }
//        switch ($mode) {
//            case "user":$table = "datatables_custom_columns";
//                break;
//            case "sa":$table = "datatables_custom_default_columns";
//                break;
//            case "":$table = "datatables_custom_columns";
//                break;
//        }

        $sql = "select * from $table where UserID='$userID' and PageID='$pageID'";

//        echo $sql."<BR/>";



        $res = $this->Query($this->conn, $sql);

        if (isset($res[0])) {
            return $res[0];
        } else {
            if ($mode == '') {
                $sql = "select * from datatables_custom_default_columns where UserID='$userID' and PageID='$pageID'";
                $res = $this->Query($this->conn, $sql);
            } else {
                return false;
            }
            if (isset($res[0])) {
                return $res[0];
            } else {
                return false;
            }
        }
    }

    /*     * ******************************************************************************
     * Description
     * 
     * This method is for saving datatable display preference
     * 
     * @param userid
     * @param page id 
     * @param names string ex.(test1,test2)
     * @param order string ex.(2,1)
     * @param visibility string ex.(0,1,0,1)
     * @return
     * 
     * @author Andris Polnikovs <a.polnikovs@gmail.com>******************************
     */

    public function saveDisplayPreferences($uID, $pID, $post) {

      
        $nameData = "";
        $orderData = "";
        $visData = "";
        $stData = "";
//       echo "<pre>";
//       print_r($post);
//       echo "</pre>";
        for ($i = 0; $i < $post['numberOfRecords']; $i++) {
            $nameData.=$post["columnName_$i"] . ",";

            //order fix
            while (strpos($orderData, "," . ($post["columnOrder_$i"] - 1) . ",")) {
                $post["columnOrder_$i"]++;
            }

            $orderData.=($post["columnOrder_$i"] - 1) . ",";
            //order fix

            if (isset($post["vis_$i"])) {
                $visData.="1,";
            } else {
                $visData.="0,";
            }
            if (isset($post["st_$i"])) {
                $stData.="1,";
            } else {
                $stData.="0,";
            }
        }

        if (!isset($post['sa'])) {
            $mode = "user";
        } else {
            $mode = "sa";
        }
        //   $n='',$o='',$v='',$mode="user",$s=null,
        $n = $nameData;
        $o = $orderData;
        $v = $visData;
        $mode;
        $s = $stData;
        switch ($mode) {
            case "user":$table = "datatables_custom_columns";
                $extraUpdatesql = "";
                $extraInsertSQl = "";
                $extraInsertSQl2 = "";
                $extraParams = array();
                break;
            case "sa":$table = "datatables_custom_default_columns";
                $extraUpdatesql = ",ColumnStatusString=:ColumnStatusString";
                $extraParams = array('ColumnStatusString' => $s);
                $extraInsertSQl2 = ",:ColumnStatusString";
                $extraInsertSQl = ",ColumnStatusString";
                break;
        }

        $sql = "select * from $table where UserID=$uID and PageID=$pID";
       
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            $sql = "update $table dcc set ColumnDisplayString=:ColumnDisplayString, 
            ColumnOrderString=:ColumnOrderString,ColumnNameString=:ColumnNameString 
            $extraUpdatesql where UserID=:UserID and PageID=:PageID";
        } else {
            $sql = "insert into $table (UserID,PageID,ColumnDisplayString,ColumnOrderString,ColumnNameString $extraInsertSQl)
            values
                (:UserID,:PageID,:ColumnDisplayString,:ColumnOrderString,:ColumnNameString $extraInsertSQl2)
            ";
        }

        $params = array
            (
            'ColumnDisplayString' => $v,
            'ColumnOrderString' => $o,
            'ColumnNameString' => $n,
            'UserID' => $uID,
            'PageID' => $pID
        );
        $params = array_merge($params, $extraParams);
        $this->Execute($this->conn, $sql, $params);

    }

    //datatable serverside 
    //to assign id to <tr> first of column selectors always should be ID
    //to add class to inactive rows  last column selectors always should be Status
    public function datatableSS($table, $columns, $args, $joins = "", $extraColumns = array(), $idpos = 0, $statusPos = false, $showInActive = false, $spID = false, $ApproveStatus = "", $groupBy = "", $where = "", $ignoreFiltered = false) {
      
        if ($showInActive) {
            if ($showInActive != "" && $showInActive !== true) {
                $statusWhere = "";
            } else {
                $statusWhere = " and $table.Status='In-active'";
            }
        } else {
            $statusWhere = " and $table.Status='Active'";
        }

        if ($spID) {
            $spIDWhere = "and $table.ServiceProviderID=$spID";
        } else {
            $spIDWhere = "";
        }
        if ($ApproveStatus != "" && $spID == false) {
            $ApproveStatusWhere = "and $table.ApproveStatus='$ApproveStatus'";
        } else {
            $ApproveStatusWhere = "";
        }

        //setting connection
        $dbtmp = explode(';', $this->controller->config['DataBase']['Conn']);
        $db = substr($dbtmp[1], 7);
        $dbhost = substr($dbtmp[0], 11);
        $dbc = mysql_connect($dbhost, $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']) or die($this->controller->log(mysql_error(), "dataTable_error___"));
        mysql_select_db($db);
      
// filtering
        $sql_where = "";
        if ($args['sSearch'] != "") {
            $extraWhereArg = "";
            $sql_where = "WHERE (";

            foreach ($columns as $column) {
                if ($column != "count(*)") {  //fixes invalid group use error
                    $sql_where .= $column . " LIKE '%" . mysql_real_escape_string($args['sSearch']) . "%' OR ";
                }
            }
            $sql_where = substr($sql_where, 0, -3);
            $sql_where.= ")";
        } else {
            $extraWhereArg = "where 1=1 ";
        }
        $extraWhereArg.="";
       
        
// ordering
        $sql_order = "";
        if (isset($args['iSortCol_0'])) {
            $sql_order = "ORDER BY  ";
            for ($i = 0; $i < mysql_real_escape_string($args['iSortingCols']); $i++) {
                $sql_order .= $columns[$args['iSortCol_' . $i]] . " " . mysql_real_escape_string($args['sSortDir_' . $i]) . ", ";
            }
            $sql_order = substr_replace($sql_order, "", -2);
        }

// paging
        $sql_limit = "";
        if (isset($args['iDisplayStart']) && $args['iDisplayLength'] != '-1') {
            $sql_limit = "LIMIT " . mysql_real_escape_string($args['iDisplayStart']) . ", " . mysql_real_escape_string($args['iDisplayLength']);
        }

       if($this->debug) $this->controller->log("SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $columns) . " FROM {$table} {$joins} {$sql_where}   $extraWhereArg $where $statusWhere $spIDWhere $ApproveStatusWhere $groupBy {$sql_order} {$sql_limit}", "dt_query2");
       
       $main_query = mysql_query("SELECT SQL_CALC_FOUND_ROWS " . implode(", ", $columns) . " FROM {$table} {$joins} {$sql_where} $extraWhereArg $where $statusWhere $spIDWhere $ApproveStatusWhere $groupBy {$sql_order} {$sql_limit}")
                or die($this->controller->log(mysql_error(), "dataTable_error___"));
                  // $this->log($joins); 
// get the number of filtered rows
        $filtered_rows_query = mysql_query("SELECT FOUND_ROWS()")
                or die($this->controller->log(mysql_error(), "dataTable_error___"));
        $row = mysql_fetch_array($filtered_rows_query);
        $response['iTotalDisplayRecords'] = $row[0];
        
// get the number of rows in total
     if($this->debug)   $this->controller->log("SELECT COUNT(*) FROM {$table} {$joins} where 1=1 $where $statusWhere $spIDWhere $groupBy $ApproveStatusWhere", "dt_query2");
        $total_query = mysql_query("SELECT COUNT(*) FROM {$table} {$joins} where 1=1 $where  $statusWhere $spIDWhere  $groupBy  $ApproveStatusWhere {$sql_order}")
                or die($this->controller->log(mysql_error(), "dataTable_error___"));
        $row = mysql_fetch_array($total_query);
        if (!$ignoreFiltered) {
            $response['iTotalRecords'] = $row[0];
        } else {
            $response['iTotalRecords'] = $response['iTotalDisplayRecords'];
        }

// send back the number requested
        $response['sEcho'] = intval($args['sEcho']);

        $response['aaData'] = array();

// finish getting rows from the main query
        while ($row = mysql_fetch_row($main_query)) {
            if (!$statusPos) {
                $rt = sizeof($row) - 1;
            } else {
                $rt = $statusPos;
            }
            if ($row[sizeof($row) - 1] == "In-active") {
                $row['DT_RowClass'] = "inactive";
            }
            $row['DT_RowId'] = $row[$idpos];
            foreach ($extraColumns as $d) {
                // add an extra column that has a print link
                $row[] = $d;
                $response['aaData'][] = $row;
                
               // $this->log($row);
            }
            if (sizeof($extraColumns) == 0) {
                $response['aaData'][] = $row;
            }

            if (!isset($args['sEcho'])) {
                $args['sEcho'] = '';
            }



            $response['sEcho'] = (int) $args['sEcho'];
        }
        if ($this->debug) {
            $this->controller->log($response, "dtss____");
        }
       // $this->log($response);
        return $response;
    }

    public function resetDisplayPreferences($uid, $pageid) {
        $sql = "delete from datatables_custom_columns where UserID=$uid and PageID=$pageid";
        $this->Execute($this->conn, $sql);
    }

    public function getPendingCount($table) {
        $sql = "select count(*) as c from $table where ApproveStatus='Pending' and Status='Active'";
        $res = $this->Query($this->conn, $sql);
        return $res[0]['c'];
    }

}
?>
  
