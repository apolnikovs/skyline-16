<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Accessories Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */

class Accessories extends CustomModel {
    
    private $conn;
    private $dbColumns = array('AccessoryID', 'AccessoryName', 'Status');
    private $table     = "accessory";
    public $page;
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to insert Accessories for given JobID.
     *
     * @param array $args  
     
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
        public function processJobAccessories($args, $JobID) {



            if($JobID && is_array($args))
            {
                //Deleting existing rows of job id
                
                $delete_sql = 'DELETE FROM job_accessory WHERE JobID=:JobID';
                $deleteQuery = $this->conn->prepare($delete_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $deleteQuery->execute(array(':JobID' => $JobID));
                
                
                /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO job_accessory (JobID, AccessoryID)
                    VALUES(:JobID, :AccessoryID)';

            
                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                foreach ($args as $aid)
                {
                    $insertQuery->execute(array(':JobID' => $JobID,  ':AccessoryID' => $aid ));
                }    

                  return array('status' => 'OK',
                            'message' => 'Success');
            }
            else
            {

                return array('status' => 'ERROR',
                            'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
            }
        }
    
    
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT AccessoryID, AccessoryName, Status, EndDate FROM '.$this->table.' WHERE AccessoryID=:AccessoryID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':AccessoryID' => $args['AccessoryID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch Job's Accessories from database.
     *
     * @param int $JobID
     * @return array 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchJobAccessories($JobID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT T2.AccessoryID, T2.AccessoryName FROM job_accessory AS T1 LEFT JOIN '.$this->table.' AS T2 ON T1.AccessoryID=T2.AccessoryID WHERE T1.JobID=:JobID AND T2.Status=:Status';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':JobID' => $JobID, ':Status'=>'Active'));
        $result = $fetchQuery->fetchAll();
        
        return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch active rows from database.
     *
     * @global $this->table  
     * @return array It contains rows.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchAll() {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT AccessoryID, AccessoryName FROM '.$this->table.' ';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':Status' => 'Active'));
        $result = $fetchQuery->fetchAll();
        
        return $result;
      // $this->execute( $this->conn, $sql,$params);  
    }
    
     public function getAccessoriesList($utID=false){       
        if(!$utID){
            //$sql="select * from accessory where Status='Active'";
            
            $sql = " SELECT	    AccessoryID,
                                    AccessoryName,
                                    DATE_FORMAT(CreatedDate, '%d/%m/%Y (%H:%i)') AS CreatedDate,
                                    DATE_FORMAT(EndDate, '%d/%m/%Y (%H:%i)') AS EndDate,
                                    Status,
                                    ApproveStatus,
                                    ModifiedUserID,
                                    DATE_FORMAT(ModifiedDate, '%d/%m/%Y (%H:%i)') AS ModifiedDate,
                                    UnitTypeID
			
		FROM	    accessory where status='Active'
	     ";
        }        
        else
        {
        $sql="select * from accessory where UnitTypeID=$utID ";
        }
      //  $this->log($sql);
        $res=$this->query( $this->conn, $sql); 
     
        return $res;
    }

    
    public function insertAccessory($p){
        $sql="insert into accessory (AccessoryName,CreatedDate,ModifiedUserID,UnitTypeID)
    values
    (:AccessoryName,now(),:ModifiedUserID,:UnitTypeID)
    ";
        $params=array(
            'AccessoryName'=>$p['AccessoryName'],
            'ModifiedUserID'=>$this->controller->user->UserID,
            'UnitTypeID'=>$p['UnitTypeID']
        );
        
       $this->execute( $this->conn, $sql,$params);  
    }
    
    public function updateAccessory($p){     
     $sql="update accessory set AccessoryName=:AccessoryName,ModifiedUserID=:ModifiedUserID,UnitTypeID=:UnitTypeID,Status=:Status
    where AccessoryID=:AccessoryID;
    ";
    $p['Status']=isset($p['Status'])? $p['Status'] : 'Active' ;
        $params=array(
            'AccessoryName'     =>  $p['AccessoryName'],
            'ModifiedUserID'    =>  $this->controller->user->UserID,
            'UnitTypeID'        =>  $p['UnitTypeID'],
            'AccessoryID'       =>  $p['AccessoryID'],
            'Status'            =>  $p['Status']
        );       
       $this->execute( $this->conn, $sql,$params); 
    }
    
    public function getAccessoryData($id){
        $sql="select * from accessory where AccessoryID=$id";
        $res=$this->query( $this->conn, $sql); 
        return $res[0];
    }
  
    public function deleteAccessory($id){
        $sql="update accessory set Status='In-Active' where AccessoryID=$id";
        $this->execute( $this->conn, $sql); 
    }
    
    
    
    
    
    
}
?>
