<?php

require_once('CustomBusinessModel.class.php');
require_once('Functions.class.php');
require_once('Constants.class.php');

/**
 * Short Description of Questionnaire Business Model.
 * 
 * Long description of Questionnaire Business Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.3
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 12/04/2013 1.0      Brian Etherington     Initial Version
 * **************************************************************************** */
class QuestionnaireBusinessModel extends CustomBusinessModel {
    
    private $model = null;

    public function __construct($controller) {
        parent::__construct($controller);        
        $this->model = $this->loadModel('Questionnaire');
    }
    
    public function CreateQuestionnaire( $args ) {

        $params = array(
            'QuestionnaireType'         => $args['_questionnaire_type'],
            'JobID'                     => $args['_job_id'],
            'CustomerID'                => $args['_customer_id'],
            'QuestionnaireBrandID'      => $args['_brand_id']
        );
        
        $id = $this->model->Add( $params);
        
        $this->CreateData($id, $args);
        
        // mark Questionnaire Log as "responded"...
        $this->model->MarkLog($args['_log']);
    }
    
    private function CreateData($id, $args, $variable_name=null) {
        foreach( $args as $key => $value ) {
            if (substr($key,0,1) != '_') {
                if (is_array($value)) 
                    $this->Createdata($id, $value, $key);
                else {
                    if ($variable_name == null)
                        $this->model->AddData( $id, $key, $value);
                    else
                        $this->model->AddData( $id, $variable_name, $value);
                        
                }
            }
        } 
    }
    

}

?>
