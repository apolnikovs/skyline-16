{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ClosedJobsPage}
{/block}


{block name=afterJqueryUI}
    
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>

    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
   
    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
{/block}


{block name=scripts}
    <style>
#serviceActionWrapper .ui-combobox-input {
width: 340px;
height: 20px;
}   
.hideTick{ display:none; }
</style>
<script type="text/javascript" src="{$_subdomain}/js/jquery-ui-1.9.1.custom.min.js"></script>
<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery-ui-1.9.1.custom.min.css" type="text/css" />
 
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>

    
<script>


    $(document).ready(function() {
    {* added by srinvas *}  
   $("#ContactAllow").click(function() {
                            if($('#ContactAllow').is(':checked'))
                            $(".hideTick").show();                            
                            else
                            $(".hideTick").hide();    
                    });      

    $("#CustomerTitleID").combobox({
	    create: function(event, ui) {
		var styles = {
                    width : "50px",
                    height : "18px"
                  };
                $(this).next("span").find("input").attr("name", "CustomerTitleInput").css( styles  );
	    }
	});
       {* end combo box creation for Title *}   
	var jbReportedFaultOptions = {  
	    maxCharacterSize:	2000,  
	    originalStyle:	"textCount",  
	    warningStyle:       "warningDisplayInfo",  
	    warningNumber:	1800,  
	    displayFormat:	"#input Characters | #left Characters Left | #words Words"
	};
	$("#reportedFault").textareaCount(jbReportedFaultOptions, function() {
	    if($("#reportedFault").val().length > 200) {
		$("#reportedFault").css("color", "red");
	    } else {
		$("#reportedFault").css("color", "inherit");
	    }
	});  
	
	$("#purchaseDate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
	
	$("#serviceType").combobox({
	    create: function(event, ui) {
		$(this).next("span").find("input").attr("name", "serviceTypeInput");
	    }
	});

	$("#modelNo").autocomplete({
	    source: function(request, response) {
		$.ajax({
		    url: "{$_subdomain}/Data/getModelNumbers/list=1/manufacturer=106/",
		    dataType: "json",
		    data: {
			featureClass:	    "P",
			style:		    "full",
			maxRows:	    12,
			name_startsWith:    request.term
		    },
		    success: function( data ) {
			response( $.map( data.models, function( item ) {
			    return {
				label: item.ModelNumber,
				value: item.ModelNumber
			    }
			}));
		    }
		});
	    },
	    minLength: 2
	});  
	
	
	$(document).on("click", "#finish", function() {
	
	    $("#serviceActionForm").validate = null;

	    $("#serviceActionForm").validate({
		rules: {
		    imeiNo:		{ required: true },
		    modelNo:		{ required: true },
                    serviceTypeInput:	{ required: true },
		    reportedFault:	{ required: true },
		    ServiceAction:	{ required: true },
                    CustomerTitleID:    { required: true },
                    ContactLastName:    { required: true },
                    ContactMobile:      { number: true ,  
                                          minlength: 5 ,      
                                            required : function(element) {
                                                    if($('#ContactAllow').is(':checked'))
                                                            {                                                                
                                                                if ($("#ContactEmail").val().length > 0 || $("#ContactMobile").val().length > 0) {
                                                                                                        return false;
                                                                                                    }
                                                                                                    else {
                                                                                                        return true;
                                                                                                    }
                                                                }
                                                            else
                                                               { return false; }
                                                }
		},
                    ContactEmail:       { email: true
                                            
                                        }      
		},
		messages: {
		    imeiNo:		{ required: "Imei No is required" },
		    modelNo:		{ required: "Model No is required" },
		    serviceTypeInput:	{ required: "Service Type is required" },
		    reportedFault:	{ required: "Reported Fault is required" },
		    ServiceAction:	{ required: "Service Action is required" },
                    CustomerTitleID:	{ required: "Title and Surname are required" },
                    ContactLastName:    { required: "Title and Surname are required" },
                    ContactMobile:      { number : "Provide Valid Phone No" ,   required : "Contact Phone No (or) Customer Email Address is required" , minlength : "Provide Valid Phone No" },
                    ContactEmail:       { email: "Provide Valid Email Address" }
		},
		errorPlacement: function(error, element) {
		    switch (element.attr("name")) {
			case "imeiNo":
			case "modelNo": {
			    error.css("padding-left", "135px");
			    error.insertAfter(element);
			    break;
			}
			case "serviceTypeInput": {
			    error.css("padding-left", "135px");
			    error.insertAfter($("#serviceType").next(".ui-combobox"));
			    break;
			}
			case "ServiceAction": {
			    error.css("padding-left", "60px");
			    error.insertAfter("#softService");
			    break;
			}
                        {* added by srinvas as per Tracker Base 479 *}  
                        case "CustomerTitleID": {
			    if($('#ContactLastName').next("p").length)
                                 $('#ContactLastName').next("p").remove();
			    error.css("padding-left", "135px");                            
			    error.insertAfter("#ContactLastName");
			    break;
			}
                        
                        case "ContactLastName": {
			    if($('#ContactLastName').next("p").length)
                                 $('#ContactLastName').next("p").remove();
                            error.css("padding-left", "135px");
			    error.insertAfter("#ContactLastName");
			    break;
			}
                        
                        case "ContactEmail": {
			    if($('#ContactLastName').next("p").length)
                                        $('#ContactLastName').next("p").remove();
			    error.css("padding-left", "170px");                            
			    error.insertAfter("#ContactEmail");
			    break;
			}
                        
                        case "ContactMobile": {			    
                            error.css("padding-left", "135px");
                             if($('#ContactLastName').next("p").length)
			    $('#ContactLastName').next("p").remove();
			    error.insertAfter(element);
			    break;
			}
                        {* end Tracker Base 479 *}  
			default: {
			    error.insertAfter(element);
			    break;
			}
		    }
		},
		ignore:		"",
		errorClass:	"fieldError",
		onkeyup:	false,
		onblur:		false,
		errorElement:   "p",
		submitHandler: function() {
		    $.post("{$_subdomain}/Job/saveServiceActionTracking", $("#serviceActionForm").serialize(), function(response) {
			location.href = "{$_subdomain}/index";
		    });
		    return false;
		}
	    });

	    $("#serviceActionForm").submit();

	    return false;

	});
	
    
    });


</script>
    

{/block}


{block name=body}

    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / &nbsp;{$page['Text']['page_title']|escape:'html'}
    </div>
</div>

{*include file='include/menu.tpl'*}


<div id="serviceActionWrapper" style="position:relative; float:left; width:100%; margin-top:30px;">

    <form id="serviceActionForm" name="serviceActionForm">
<br />
	<div style="width:55%; float:left;">
	    
	    <div>
		<label for="imeiNo" style="width:120px; text-align:left;">IMEI No: <sup>*</sup></label>
		<input type="text" name="imeiNo" id="imeiNo" value="" style="width:340px; height:20px;" />
	    </div>

	    <div style="margin-top:5px;">
		<label for="modelNo" style="width:120px; text-align:left;">Model No: <sup>*</sup></label>
		<input type="text" name="modelNo" id="modelNo" value="" style="width:340px; height:20px;" />
	    </div>

	    <div style="margin-top:5px;">
		<label for="purchaseDate" style="width:120px; text-align:left;">Purchase Date:</label>
		<input type="text" name="purchaseDate" id="purchaseDate" value="" style="width:340px; height:20px;" />
	    </div>

	    <div style="margin-top:5px;">
		<label for="serviceType" style="width:120px; text-align:left;">Service Type: <sup>*</sup></label>
		<select id="serviceType" name="serviceType">
		    <option value="">Select Service Type</option>
		    {foreach $serviceTypes as $type}
			<option value="{$type.ServiceTypeID}">{$type.ServiceTypeName}</option>
		    {/foreach}
		</select>
	    </div>
	    
             <div style="margin-top:5px;">
		<label style="width:110px; text-align:left;" >  </label>
                   &nbsp;&nbsp;  
                   <label style="width:50px; text-align:left;" > Title: </label>

                   <label style="width:130px; text-align:left;"> Forename: </label>

                   <label style="width:130px; text-align:left;"> Surname: </label>
            
	    </div>
           {* added by srinvas as per Tracker Base 479 *}     
           <div style="margin-top:5px;">
		<label class="fieldLabel" for="CustomerTitleID"  >Customer Name: <sup>*</sup></label>
                   &nbsp;&nbsp;  
                   <select  name="CustomerTitleID" id="CustomerTitleID" title="CustomerTitleID"  class="text auto-hint" style="width:60px;" >

                       <option value="" selected="selected"></option>
                    {foreach $selectTitle as $type}
			<option value="{$type.TitleID}">{$type.Name}</option>
		    {/foreach}
                        
                    </select>

                   <input type="text" style="width:130px;height: 19px;" class="text lowercaseText capitalizeText" name="ContactFirstName" value=""  id="ContactFirstName" >

                   <input type="text" style="width:130px;height: 19px;" class="text" name="ContactLastName" value="" id="ContactLastName" > 
            
	    </div>
            
           <div style="margin-top:5px;" class="hideTick" >
		<label for="ContactMobile" style="width:120px; text-align:left;">Customer Phone No: </label>
		<input type="text" name="ContactMobile" id="ContactMobile" value="" style="width:340px; height:20px;" />
	    </div>
	</div>

	<div style="width:45%; float:right;top: -37px;position: relative;">
	    <label for="ContactAllow" style="width:110px; text-align:left; display:block; position:relative; margin-top:4px; float:left;">Reported Fault: <sup>*</sup></label>
	    <textarea id="reportedFault" name="reportedFault" style="width:100%; margin-top:15px; height:144px;"></textarea>
            
	    <label for="ContactAllow" style="width:350px; text-align:left; display:block; position:relative; margin-top:10px; float:left;left:-5px">
                <input type="checkbox" value="Yes" name="ContactAllow" id="ContactAllow" /> Customer Contact Allowed </label>
	    
            <span class="hideTick">    
            <label for="ContactEmail" style="width:160px; text-align:left; position:relative; margin-top:18px; float:left;">Customer Email : </label>
            <input type="text" value="" name="ContactEmail" id="ContactEmail" style="width:240px; height:20px;margin-top:11px;" />
            </span>
	</div>         
               

        {* end added by srinvas as per Tracker Base 479 *}          
	<fieldset name="serviceActionFieldset" style="width:100%; clear:both; margin-top:15px; position:relative; display:block; float:left;">
	    
	    <legend style="font-weight:bold;">Service Actions</legend>
	    
	    <div id="softService" style="position:relative; float:left; width:100%;">

		<div id="actionLeft" style="width:33%; float:left; text-align:center; position:relative; display:block; margin-top:10px;">

		    <div style="display:inline-block; text-align:left;">
			<h3 style="margin-bottom:0; font-weight:bold; font-size:14px; margin-left:5px;">Soft Service</h3>

			<input type="radio" name="ServiceAction" value="SoftwareUpdate" id="SoftwareUpdate" style="margin-top:10px;" />
			<label for="ServiceAction">Software Update</label>
			<br/>

			<input type="radio" name="ServiceAction" value="CustomerEducation" id="CustomerEducation" style="margin-top:5px;" />
			<label for="ServiceAction">Customer Education/Tuition</label>
			<br/>

			<input type="radio" name="ServiceAction" value="SettingsSetup" id="SettingsSetup" style="margin-top:5px;" />
			<label for="ServiceAction">Settings/Setup</label>
			<br/>
		    </div>

		</div>

		<div id="actionMiddle" style="width:33%; float:left; text-align:center; position:relative; display:block; margin:10px 0px;">

		    <div style="display:inline-block; text-align:left;">
			<h3 style="margin-bottom:0; font-weight:bold; font-size:14px; margin-left:5px;">Technical Issue</h3>

			<input type="radio" name="ServiceAction" value="DamagedBeyondRepair" id="DamagedBeyondRepair" style="margin-top:10px;" />
			<label for="ServiceAction">Damaged Beyond Repair</label>
			<br/>

			<input type="radio" name="ServiceAction" value="FaultyAccessory" id="FaultyAccessory" style="margin-top:5px;" />
			<label for="ServiceAction">Faulty Accessory</label>
			<br/>

			<input type="radio" name="ServiceAction" value="NonUKModel" id="NonUKModel" style="margin-top:5px;" />
			<label for="ServiceAction">Non-UK Model</label>
			<br/>

			<input type="radio" name="ServiceAction" value="Unresolvable" id="Unresolvable" style="margin-top:5px;" />
			<label for="ServiceAction">Un-resolvable</label>
			<br/>
		    </div>
		    
		</div>

		<div id="actionRight" style="width:33%; float:left; text-align:center; position:relative; display:block; margin:10px 0px;">
		    
		    <div style="display:inline-block; text-align:left;">
			<h3 style="margin-bottom:0; font-weight:bold; font-size:14px; margin-left:5px;">Repair Required</h3>
			<input type="radio" name="ServiceAction" value="ThirdParty" id="ThirdParty" style="margin-top:10px;" />
			<label for="ServiceAction">3rd party repair required</label>
		    </div>
		    
		</div>
		
	    </div>    
		    
	</fieldset>
		
	<div style="width:100%; text-align:center; position:relative; float:left; clear:both; margin-top:20px;">
	    <a href="#" id="finish" class="btnConfirm" style="margin-right:10px; width:60px;">Finish</a>
	    <a href="{$_subdomain}/index" id="cancel" class="btnCancel" style="width:60px;">Cancel</a>
	</div>
    
    </form>	
	
</div>


{/block}
