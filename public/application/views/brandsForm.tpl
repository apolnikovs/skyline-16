{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
       
                
           
        
    $(document).ready(function() {
        $("#NetworkID").combobox({
            change: function() {
                $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';            
                var $NetworkID = $("#NetworkID").val();
                if($NetworkID && $NetworkID!='')
                {
                    $networkClients = $clients[$NetworkID];
                    if($networkClients)
                    {
                        for(var $i=0;$i<$networkClients.length;$i++)
                        {
                            $clientDropDownList += '<option value="'+$networkClients[$i][0]+'" >'+$networkClients[$i][1]+'</option>';
                        }
                    }
                  $("#ClientID").html($clientDropDownList);     
                }
            }
        });
        $("#ClientID").combobox();
             function autoHint()
             {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } );  
             }
             
             
             
             $(document).on('click', '#insert_save_btn', 
                                function() {
                                
                                autoHint();
                
               });
               
             $(document).on('click', '#update_save_btn', 
                                function() {
                                
                                autoHint();
                
               });   
                
                
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="BrandsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="BrandsForm" name="BrandsForm" method="post" enctype="multipart/form-data"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                            <p>
                                <label ></label>
                                <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                            </p>
                       
                         
                         
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['service_network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                            
                            
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $nClients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                         <p>
                            <label class="fieldLabel" for="NewBrandID" >{$page['Labels']['brandid']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="NewBrandID" value="{$datarow.NewBrandID|escape:'html'}" id="NewBrandID" >
                        
                         </p> 
                           
                            
                         <p>
                            <label class="fieldLabel" for="BrandName" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="BrandName" value="{$datarow.BrandName|escape:'html'}" id="BrandName" >
                        
                         </p>
                         
                         
                          <p>
                            <label class="fieldLabel" for="Acronym" >{$page['Labels']['acronym']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text" maxlength="30"  name="Acronym" value="{$datarow.Acronym|escape:'html'}" id="Acronym" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="BrandLogo" >{$page['Labels']['logo_location']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; 
                             
                             
                             <input  type="file" class="text" style="width:225px"  name="BrandLogo"  id="BrandLogo" >
                             {if $datarow.BrandLogo neq ''} 
                             
                               <a href="{$_subdomain}/{$brandLogosPath}{$datarow.BrandLogo|escape:'html'}" target="_blank" >{$page['Text']['view']|escape:'html'}</a>
                                 
                             {/if}
                             <input  type="hidden" class="text"  name="UploadedBrandLogo"  id="UploadedBrandLogo" value="{$UploadedBrandLogo}" >
                        
                         </p>
                         
                         
                         
                         <p>
                            <label class="fieldLabel" for="AutoSendEmails" >&nbsp;</label>
                           
                             &nbsp;&nbsp; 
                                
                                  <input  type="checkbox" name="AutoSendEmails"  value="0" {if $datarow.AutoSendEmails eq "0"} checked="checked" {/if}  /><span class="text" >{$page['Labels']['donot_auto_send_emails']|escape:'html'}</span> 

                                 
                          </p>
                          
                          
                          {*
                          <p>
                            <label class="fieldLabel" for="SendRepairCompleteTextMessage" >&nbsp;</label>
                           
                             &nbsp;&nbsp; 
                                
                                  <input  type="checkbox" name="SendRepairCompleteTextMessage"  value="Yes" {if $datarow.SendRepairCompleteTextMessage eq "Yes"} checked="checked" {/if}  /><span class="text" >{$page['Labels']['send_repair_complete_text_message']|escape:'html'}</span> 
                                 
                          </p>
                          
                          
                          <p>
                            <label class="fieldLabel" for="SMSID" >{$page['Labels']['sms']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp;  
                             <select name="SMSID" id="SMSID" class="text" >
                                <option value="" {if $datarow.SMSID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $smsMessages as $sms}

                                    <option value="{$sms.SMSID}" {if $datarow.SMSID eq $sms.SMSID}selected="selected"{/if}>{$sms.SMSName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          *}
                          
                          
                           <p>
                            <label class="fieldLabel" for="EmailType" >{$page['Labels']['email_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                

                             <input class="TrackerRadioButton"  type="radio" name="EmailType"  value="Generic" {if $datarow.EmailType eq 'Generic'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['generic']|escape:'html'}</span> 
                             <input class="TrackerRadioButton" type="radio" name="EmailType"  value="CRM" {if $datarow.EmailType eq 'CRM'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['crm']|escape:'html'}</span> 
                             {if $datarow.BrandID neq '2000'} {* Samsung does not want Tracker Type email..Requeted by Chris/Bill Walker on 11/04/2013 *}
                             <input class="TrackerRadioButton" type="radio" name="EmailType" id="TrackerEmailType"  value="Tracker" {if $datarow.EmailType eq 'Tracker'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['tracker']|escape:'html'}</span>
                             {/if}
                                  
                          </p>
                          
                          
                          
                           <p id="TrackerURLElement" {if $datarow.EmailType neq 'Tracker'} style="display:none" {/if} >
                                <label class="fieldLabel" for="TrackerURL" >{$page['Labels']['brand_tracker_url']|escape:'html'}:<sup>*</sup></label>

                                 &nbsp;&nbsp; <input  type="text" class="text" maxlength="30"  name="TrackerURL" value="{$datarow.TrackerURL|escape:'html'}" id="TrackerURL" >

                           </p>
                          
                          
                       
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="BrandID"  value="{$datarow.BrandID|escape:'html'}" >
                                    <input type="hidden" name="hiddenBrandLogo" id="hiddenBrandLogo"  value="{$datarow.BrandLogo|escape:'html'}" >
                                    
                                 
                                    {if $datarow.BrandID neq '' && $datarow.BrandID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
