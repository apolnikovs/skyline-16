{extends "Performance/MasterLayout.tpl"}

{block name=scripts append}

  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load('visualization', '1', { packages: ['geochart'] });
    {if $map == "europe"}{$map = "150"}{/if}

    function drawVisualization() {
      var data_country = google.visualization.arrayToDataTable([
            ['Country','Count'],
            {foreach $graph_country as $graphdata}
                    
                ["{if $graphdata.Name == 'UK'}GB{else}{$graphdata.Name}{/if}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
                
            {/foreach} 
            ]);
            
      var data_city = google.visualization.arrayToDataTable([
            ['City','Count'],
            {foreach $graph_county as $graphdata}
                {if $graphdata.Country == $map}
                    ["{$graphdata.Name}", {$graphdata.Count} ]{if not $graphdata@last},{/if}
                {/if}
            {/foreach} 
            ]);
      
      
         var options = {
            region: '{$map}',
            {if $map == 'world' || ($map == '150') || ($map == '002') || ($map == '019') || ($map == '142') || ($map == '009')}
                displayMode: 'region',
            {else}    
                displayMode: 'markers',
            {/if}
            colorAxis: { colors: ['orange','red'] },
            backgroundColor: '66CCff'
          };
    
      var geochart = new google.visualization.GeoChart(
          document.getElementById('visualization'));
          
      var previousRegion, previousResolution;
      google.visualization.events.addListener(geochart, 'regionClick', function(eventData) {
            previousRegion = options.region;
            previousResolution = options.resolution;

            options['region'] = eventData.region;

                if ($('#spid').val() == 0) {
                    window.location = '{$_subdomain}/Performance/Geographic/jobs={$jobs}/map='+eventData.region;
                } else {
                    window.location = '{$_subdomain}/Performance/Geographic/spid='+$('#spid').val()+'/jobs={$jobs}/map={$map}';
                }

           
      });
      google.visualization.events.addListener(geochart, 'error', function () {
            // back out one region
            options.region = previousRegion;
            options.resolution = previousResolution; 
            geochart.draw(data, options);
      }  ); 
      {if ($map == 'world') || ($map == '150') || ($map == '002') || ($map == '019') || ($map == '142') || ($map == '009')}
        geochart.draw(data_country, options);
      {else}
        geochart.draw(data_city, options);
      {/if}
    }
    
    $(document).on('change', "#spid", function() { 
        if ($('#spid').val() == 0) {
            window.location = '{$_subdomain}/Performance/Geographic/jobs={$jobs}/map={$map}';
        } else {
            window.location = '{$_subdomain}/Performance/Geographic/spid='+$('#spid').val()+'/jobs={$jobs}/map={$map}';
        }
    });
    
    $(document).on('change', "#jobs", function() { 
        window.location = '{$_subdomain}/Performance/Geographic/{if $spid != 0}spid={$spid}/{/if}jobs='+$('#jobs').val()+'/map={$map}';
    });
    
    $(document).on('change', "#map", function() { 
        window.location = '{$_subdomain}/Performance/Geographic/{if $spid != 0}spid={$spid}/{/if}jobs={$jobs}/map='+$('#map').val();
    });

    google.setOnLoadCallback(drawVisualization);
  </script>
{/block}

{block name=filters append}


        <li>
            Maps :
            <select id="map" name="map">
                <option value="world" {if $map == "world"}Selected{/if}>World</option>
                <option value="world"> </option>
                <option value="002" {if $map == "002"}Selected{/if}>Africa</option>
                <option value="019" {if $map == "019"}Selected{/if}>Americas</option>
                <option value="142" {if $map == "142"}Selected{/if}>Asia</option>
                <option value="150" {if $map == "150"}Selected{/if}>Europe</option>
                <option value="009" {if $map == "009"}Selected{/if}>Oceania</option>
                <option value="world"> </option>
                {foreach $countries as $country}
                 <option value="{$country.InternationalCode}" {if $country.InternationalCode == $map}Selected{/if}>{$country.Name|replace:'UK':'United Kingdom'}</option>
                {/foreach}

            </select>
        </li> 
        <li>
            Jobs :
            <select id="jobs" name="jobs">
                <option value="open" {if $jobs == "open"}Selected{/if}>Open</option>
                <option value="closed" {if $jobs == "closed"}Selected{/if}>Closed</option>
            </select>
        </li>    
        <li>
            Service Provider :
            <select id="spid" name="spid">
                <option value="0" {if $spid == 0}Selected{/if}>All</option>
                <option value="0"></option>
                {foreach $service_centre_repair_capacities as $scrp}
                    <option value="{$scrp.ServiceProviderID}" {if $scrp.ServiceProviderID == $spid}Selected{/if}>{$scrp.CompanyName|cat:' '|lower|capitalize|replace:'Tv ':'TV '}</option>
                {/foreach}
            </select>
        </li>

{/block}
    
{block name=PerformanceBody}
       
      <!-- begining of breadcrumbs and login -->




      <div id="visualization" style="position: relative;top:250px;"></div>
    
    	</div><!-- end of navigation -->
	</div>
    
    {/block}