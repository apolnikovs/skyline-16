{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}

<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
<style type="text/css" >
    .ui-combobox-input {
            width:299px;
        }
</style>    
<script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
 

    
<script type="text/javascript" >
 $(document).ready(function() {  
 
 
                    
 
                    
                    $(document).on('click', '.form-button', 
                    
                       
                    
                        function() {
                        
                          
                          
                            
                            //Assigning value from button selected.
                            if($(this).attr("id")!="goto_booing_btn")
                                {
                                   $("#jncJobTypeID").val($(this).attr("id").replace("goto_booing_btn_", ""));
                                }
                            
                            
                            
                           
                            $('#JobNetworkClientForm').validate({

                                        ignore: '',
                                         
                                        rules:  {
                                                    NetworkID:
                                                    {
                                                            required: true

                                                    },
                                                    ClientID:
                                                    {
                                                           required: true
                                                    },
                                                   /* DefaultBranchID:
                                                    {
                                                           required: true
                                                    },*/
                                                    JobTypeID:
                                                    {
                                                           required: true
                                                    }

                                        },
                                        messages: {
                                                    NetworkID:
                                                    {
                                                            required: "Please select Network."
                                                    },
                                                    ClientID:
                                                    {
                                                            required: "Please select Client."
                                                    },
                                                   /* DefaultBranchID:
                                                    {
                                                           required: "Please select Branch."
                                                    },*/
                                                    JobTypeID:
                                                    {
                                                            required: "Please select Job Type."
                                                    }
                                        },

                                        errorPlacement: function(error, element) {
                                              
                                                error.insertAfter( element );

                                        },
                                        errorClass: 'fieldError',
                                        onkeyup: false,
                                        onblur: false,
                                        errorElement: 'label'
                                        
                                        });




                        });
                
                   {if $showNetworkDropDown}
                 
                    $("#jncNetworkID").combobox({ change: function() { getClientsList() } });
                    
                   {/if}
               
                   {if $ClientID}
                       
                       checkServiceTypes();
                    
                   {else} 
                    
                     $("#jncClientID").combobox({ change: function() { 
             
                    //getBranchesList() 
                    
                            checkServiceTypes();
                                
                    
                        } });
                     
                   {/if}
                  
                  
                  {*
                  
                  {if !$BranchID && !$DefaultBranchID}
                      
                      $("#jncDefaultBranchID").combobox();
                      
                      {if !$ClientID}
                          $("#jncDefaultBranchIDIDElement").hide();
                      {/if}
                   
                  {/if}
                  
                  *}  


                  function checkServiceTypes()
                  {
                      
                      
                      {if $JobTypeID}
                       
                       
                        if($("#jncNetworkID").val()!='' &&  $("#jncClientID").val()!='')
                        {   
                                $.post("{$_subdomain}/Data/getServiceTypes/"+$("#jncJobTypeID").val()+"/"+$("#jncNetworkID").val()+"//"+$("#jncClientID").val()+"/",         
                                                 '',      
                                                 function(data){
                                                         var $jtServiceTypes  = eval("(" + data + ")");

                                                         if($jtServiceTypes && $jtServiceTypes.length>0)
                                                         {
                                                             $("#goto_booing_btn").show();

                                                             $("#serviceTypeErrorMsg").fadeOut('fast');

                                                            // alert("yes");
                                                         }
                                                         else
                                                         {
                                                              $("#goto_booing_btn").hide();
                                                              $("#serviceTypeErrorMsg").css('color','red').html("The Job Type you have selected is not associated with any Service Types.  Please select an alternative Job Type.").fadeIn('slow');

                                                           //   alert("no");
                                                         }



                                                 });
                          }
                          else
                          {
                                $("#goto_booing_btn").show();

                                $("#serviceTypeErrorMsg").fadeOut('fast');
                          }
                          
                       {else}  
                       
                            
                       
                            if($("#jncNetworkID").val()!='' &&  $("#jncClientID").val()!='')
                            {    
                                    
                                $("#serviceTypeErrorMsg").css('color','red').html("The Job Type you have selected is not associated with any Service Types.  Please select an alternative Job Type.").fadeIn('slow');  
                                  
                                {foreach $jobTypes as $jobType}

                                   $.post("{$_subdomain}/Data/getServiceTypes/{$jobType.JobTypeID|escape:'html'}/"+$("#jncNetworkID").val()+"//"+$("#jncClientID").val()+"/",         
                                                '',      
                                                function(data){
                                                        var $jtServiceTypes  = eval("(" + data + ")");

                                                        if($jtServiceTypes && $jtServiceTypes.length>0)
                                                        {
                                                            $("#goto_booing_btn_{$jobType.JobTypeID|escape:'html'}").show();
                                                                         
                                                            $("#serviceTypeErrorMsg").fadeOut('fast');

                                                          
                                                        }
                                                        else
                                                        {
                                                             $("#goto_booing_btn_{$jobType.JobTypeID|escape:'html'}").hide();
                                                           
                                                        }

                                                });

                                                
                               {/foreach}
                              
                                
                            }
                            else
                            {
                                 
                                  $("#serviceTypeErrorMsg").fadeOut('fast');
                            }   
                                
                       {/if}
                       
                      
                  }
                    

                      
                      
                   function getBranchesList()
                   {
                       
                            var $ClientID = $("#jncClientID").val();
                            
                            $DefaultBranchID1  = $("#jncClientID").children(":selected").attr("id").replace("DefaultBranchID_", "");
                            
                            $DefaultBranchID = parseInt($DefaultBranchID1.replace(/\D/g, ''), 10);

                            $DefaultBranchID = isNaN($DefaultBranchID) ? 0 : $DefaultBranchID;

                            
                            $branchDropDownList = '<option value="" selected="selected">Select from drop down</option>';     
                            if($ClientID && $ClientID!='')
                            {
                               

                                //Getting branches for selected client and network.   
                                $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+$("#jncNetworkID").val(),        

                                '',      
                                function(data){
                                        var $clientBranches = eval("(" + data + ")");

                                        if($clientBranches)
                                        {

                                            for(var $i=0;$i<$clientBranches.length;$i++)
                                            {

                                                $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                            }
                                        }

                                         $("#jncDefaultBranchID").html('');
                                         $("#jncDefaultBranchID").html($branchDropDownList);
                                         
                                         if($DefaultBranchID)
                                         {     
                                            $("#jncDefaultBranchID").val($DefaultBranchID);
                                            setValue("#jncDefaultBranchID");
                                            $("#jncDefaultBranchIDIDElement").hide();
                                         }
                                         else
                                         {
                                          
                                            if($clientBranches.length==1)
                                            {
                                                $("#jncDefaultBranchID").val($clientBranches[0]['BranchID']);    
                                            }
                                            else
                                            {
                                                $("#jncDefaultBranchID").val('');
                                            }
                                            setValue("#jncDefaultBranchID");
                                            $("#jncDefaultBranchIDIDElement").show();
                                         }
                                         
                                       

                                });
                            }
                            else
                            {
                                $("#jncDefaultBranchID").html('');
                                $("#jncDefaultBranchID").html($branchDropDownList);
                                
                                $("#jncDefaultBranchID").val('');
                                setValue("#jncDefaultBranchID");
                                $("#jncDefaultBranchIDIDElement").hide();
                            }
                           
                   }    
                      
                      
                      
                      
                      
                      
                      
                      
 
 
                   function getClientsList()
                   {
                       
                            var $NetworkID = $("#jncNetworkID").val();
                            
                            
                            
                            $clientDropDownList = '<option value="" selected="selected">Select from drop down</option>';     
                            if($NetworkID && $NetworkID!='')
                            {
                               

                                //Getting clients for selected network.   
                                $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                '',      
                                function(data){
                                        var $networkClients = eval("(" + data + ")");

                                        if($networkClients)
                                        {

                                            for(var $i=0;$i<$networkClients.length;$i++)
                                            {
                                                   $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'"   id="DefaultBranchID_'+$networkClients[$i]['DefaultBranchID']+'"     >'+$networkClients[$i]['ClientName']+'</option>';
                                            }
                                        }

                                         $("#jncClientID").html('');
                                         $("#jncClientID").html($clientDropDownList);
                                         
                                         if($networkClients.length==1)
                                         {    
                                            $("#jncClientID").val($networkClients[0]['ClientID']);
                                         }
                                         else
                                         {
                                            $("#jncClientID").val('');
                                         }
                                         
                                         setValue("#jncClientID");
                                        
                                       

                                });
                            }
                            else
                            {
                                $("#jncClientID").html('');
                                $("#jncClientID").html($clientDropDownList);
                                $("#jncClientID").val('');
                                setValue("#jncClientID");
                            }
                            
                            
                           
                       
                   }
 

            });
     

</script>

<form action="{$_subdomain}/Job/index" id="JobNetworkClientForm" name="JobNetworkClientForm" method="post" >
    <fieldset>
        <legend title="">New Job Booking</legend>
       
        
        {if $showNetworkDropDown}
        
            <p style="margin-top:20px;">
                <label for="jncNetworkID" >Network:<sup>*</sup></label>
                &nbsp;&nbsp;
		<select name="NetworkID" id="jncNetworkID" class="text" >
		    <option value="" selected="selected">Select from drop down</option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}" >{$network.CompanyName|escape:'html'}</option>
		    {/foreach}
		</select>
            </p>

        {else}
            
	    <input type="hidden" name="NetworkID" id="jncNetworkID"  value="{$NetworkID|escape:'html'}" >

        {/if}    
        
        
        
        
        {if $ClientID}
              <input type="hidden" name="ClientID" id="jncClientID"  value="{$ClientID|escape:'html'}" >
        {else}
           <p id="jncClientIDElement" >
               <label for="jncClientID" >Client:<sup>*</sup></label>
               &nbsp;&nbsp;  <select name="ClientID" id="jncClientID" class="text" >

                               <option value="" selected="selected">Select from drop down</option>

                              {foreach $clients as $client}

                                   <option value="{$client.ClientID}" id="DefaultBranchID_{$client.DefaultBranchID}" >{$client.ClientName|escape:'html'}</option>

                               {/foreach}
                              </select>
           </p>
        {/if}
        
        
        
        {*
        
        
        {if $BranchID}
            
              <input type="hidden" name="BranchID"   value="{$BranchID|escape:'html'}" >
              
        {elseif $DefaultBranchID}
            
            <input type="hidden" name="DefaultBranchID"  value="{$DefaultBranchID|escape:'html'}" >
            
        {else}
            
           <p id="jncDefaultBranchIDIDElement" >
               <label for="jncDefaultBranchID" >Branch:<sup>*</sup></label>
               &nbsp;&nbsp;  <select name="DefaultBranchID" id="jncDefaultBranchID" class="text" >

                               <option value="" selected="selected">Select from drop down</option>

                              {foreach $branches as $branch}

                                   <option value="{$branch.BranchID}" >{$branch.BranchName|escape:'html'}</option>

                               {/foreach}
                              </select>
           </p>
           
        {/if}
        
        
        *}
        
        
        <label class="fieldError" style="width:95%;margin-left:10px;display:none;" id="serviceTypeErrorMsg" ></label>
        
        {if $JobTypeID}
           
            
            <p>
            <label  >&nbsp;</label>
                &nbsp;&nbsp;
                <input type="hidden" name="JobTypeID" id="jncJobTypeID"  value="{$JobTypeID|escape:'html'}" >
                 
                <input type="submit" name="goto_booing_btn" id="goto_booing_btn" class="btnStandard"   value="Go" >


            </p>
            
        {else}
            
            {*
            <p>
                <label for="JobTypeID" >Job Type:<sup>*</sup></label>
                &nbsp;&nbsp;  <select name="JobTypeID" id="jncJobTypeID" class="text" >

                        <option value="" selected="selected">Select from drop down</option>

                       {foreach $jobTypes as $jobType}

                            <option value="{$jobType.JobTypeID}" >{$jobType.Name|escape:'html'}</option>

                        {/foreach}
                       </select>
            </p>
            *}
            
             <p style="text-align:center" >
            <input type="hidden" name="JobTypeID" id="jncJobTypeID"  value="" >
            
            
           
            
            {foreach $jobTypes as $jobType}
  
                
                <input type="submit" name="goto_booing_btn_{$jobType.JobTypeID|escape:'html'}" id="goto_booing_btn_{$jobType.JobTypeID|escape:'html'}"  class="btnStandard"   value="{$jobType.Name|escape:'html'}" >

            {/foreach}
            
             </p>
            
        {/if} 
        
         
        
       
        
    </fieldset>
</form>