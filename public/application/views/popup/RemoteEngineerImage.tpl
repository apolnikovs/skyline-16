{**
 * RemoteEngineerImage.tpl
 * 
 * Display the uploaded remote engineer image for a specific 
 *
 * @author     Andrew J. Williams <Andrew.Williams@awcomputech.com>
 * @copyright  2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.0
 *   
 * Changes
 * Date        Version Author                Reason
 * 18/05/2013  1.0     Andrew J. Williams    Initial Version
 ******************************************************************************}
   
<form action="#" id="RemoteEngineerImage" name="RemoteEngineerImage" method="post">
    <p align="center">
        <img src="{$ImageURL}" alt="{$Note}">
        <br>
        <i>{$Note}</i>
    </p>
</form>   