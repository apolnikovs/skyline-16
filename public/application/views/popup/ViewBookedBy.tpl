<fieldset>
    <legend>{$page['Labels']['network_legend']|escape:'html'}</legend>
    <div class="span-16 last">
        <p>
            <span class="span-2">{$page['Labels']['name']|escape:'html'}:</span>
            <span class="span-14 last">{$network['name']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['address']|escape:'html'}:</span>
            <span class="span-14 last">{$network['address']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['phone']|escape:'html'}:</span>
            <span class="span-14 last">{$network['phone']|escape:'html'|default: '&nbsp;'}</span>
        </p>
    </div>
</fieldset>

<fieldset>
    <legend>{$page['Labels']['client_legend']|escape:'html'}</legend>
    <div class="span-16 last">
        <p>
            <span class="span-2">{$page['Labels']['name']|escape:'html'}:</span>
            <span class="span-14 last">{$client['name']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['address']|escape:'html'}:</span>
            <span class="span-14 last">{$client['address']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['phone']|escape:'html'}:</span>
            <span class="span-14 last">{$client['phone']|escape:'html'|default: '&nbsp;'}</span>
        </p>
    </div>
</fieldset>

<fieldset>
    <legend>{$page['Labels']['branch_legend']|escape:'html'}</legend>
    <div class="span-16 last">
        <p>
            <span class="span-2">{$page['Labels']['brand']|escape:'html'}:</span>
            <span class="span-14 last">{$branch['brand']|escape:'html'|default: '&nbsp;'}</span>
        </p>       
        <p>
            <span class="span-2">{$page['Labels']['name']|escape:'html'}:</span>
            <span class="span-14 last">{$branch['name']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['address']|escape:'html'}:</span>
            <span class="span-14 last">{$branch['address']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['phone']|escape:'html'}:</span>
            <span class="span-14 last">{$branch['phone']|escape:'html'|default: '&nbsp;'}</span>
        </p>
    </div>
</fieldset>

<fieldset>
    <legend>{$page['Labels']['user_legend']|escape:'html'}</legend>
    <div class="span-16 last">
        <p>
            <span class="span-2">{$page['Labels']['name']|escape:'html'}:</span>
            <span class="span-14 last">{$user['name']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['username']|escape:'html'}:</span>
            {* 20/03/2013 this change requested by Joe. In his opinion this is a better solution than changing the username *}
            {if $user['username'] eq 'SW4NN'}
            <span class="span-14 last">Swann</span>
            {else}
            <span class="span-14 last">{$user['username']|escape:'html'|default: '&nbsp;'}</span>
            {/if}
        </p>
        <p>
            <span class="span-2">{$page['Labels']['usertype']|escape:'html'}:</span>
            <span class="span-14 last">{$user['usertype']|escape:'html'|default: '&nbsp;'}</span>
        </p>
        <p>
            <span class="span-2">{$page['Labels']['company']|escape:'html'}:</span>
            <span class="span-14 last">{$user['company']|escape:'html'|default: '&nbsp;'}</span>
        </p>        
    </div>
</fieldset>