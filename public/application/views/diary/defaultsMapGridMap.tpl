{extends "DemoLayout.tpl"}

{block name=config}
{$Title=""}
{$PageId = 88}

    


{/block}
{block name=scripts}

     
       


<script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>


<!--<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
<link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />-->
    
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/date.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
     
     },100);
     
    });
        function saveData(){
        
        $('#gridForm').submit();
        
        
        
        }
        var infowindow = new google.maps.InfoWindow();
        var defLat = 52.23793;
    var defLong = -0.90267 ;
    var mylatlong=defLat+','+defLong;
     $(document).ready(function() {
      $('.container').css('width',$(document).width());
     $('#header').css('width',$(document).width()-100);
    
    
     $('#map_canvas').css('width',$(document).width()-500);
     $('#domc').css('width',$(document).width()-120);
     $('#domc').css('height',$(document).height()-100);
     $('#map_canvas').css('height',$(document).height()-200);
     $('#gadressmap').css('width',$(document).width()-80-($(document).width()-400));
     $('#gadressmap').css('height',$(document).height()-230);
     
      var infowindow = new google.maps.InfoWindow();
        var defLat = 52.23793;
    var defLong = -0.90267 ;
    var mylatlong=defLat+','+defLong;
      setTimeout(function(){
     //resizing map&menu to fit screen
     
    
      var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address' : "15  CHANCERY PARK GROVE	OFFALY"}, function(results, status){
    var defLat = results[0].geometry.location.lat();
    var defLong =results[0].geometry.location.lng() ;
    var mylatlong=defLat+','+defLong;
   var map
       
	 
 	
var map =$('#map_canvas').gmap({ 'center':  mylatlong, 'zoom':8}).bind('init', function(event, map) { 
            var map=map;
 var gridcells={$def.CellSize}
        var rectangle;
        var ys={$def.Lat1};
        var xs={$def.Lng1};
        var ye={$def.Lat2};
        var xe={$def.Lng2};
        var dataArray=new Array();
        var xseg=gridcells*-1;//negative must be
        var yseg=gridcells; //positive
        var width=(((xe*-1)-(xs*-1))/gridcells)*-1;
        var height=(ye-ys)/gridcells*-1;
       
        var rectArr=[];
        var xy1= new google.maps.LatLng(ys,xs);
        var xy2= new google.maps.LatLng(ye,xe);
      // //console.log(width);
      // //console.log(height);
      
        var loadet=new Array();
         var values = new Array();
$.each($("input[name='daysSeleceted[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});
        {for $e=0 to $loadet|@count-1}
        loadet[{$e}]=new Array({$loadet[$e].Lat1},{$loadet[$e].Lng1},{$loadet[$e].Lat2},{$loadet[$e].Lng2});
                    
 $.each(values, function(index, value) {
 $('#saveData_'+value).val($('#saveData_'+value).val()+"{$loadet[$e].Lat1},{$loadet[$e].Lat2},{$loadet[$e].Lng1},{$loadet[$e].Lng2};");
 ////console.log($('#saveData_'+value).val())
});
        {/for}
      
     // //console.log(loadet);
         var latLngBounds = new google.maps.LatLngBounds(
        
         xy1,
          xy2
        );
        // rectangle = new google.maps.Rectangle({
         // map: map
       // });
        //rectangle.setBounds(latLngBounds);
        
        for (var i = 0; i < width; i++) 
        {
            xse=xs+i*xseg*-1;
            yse=ys-yseg;
           
            ////console.log(yse);
            for (var a = 0; a <= height; a++) {
            lat1=yse-a*yseg+yseg;
            lng1=xse;
            lat2=yse-a*yseg;
            lng2=xse+xseg*-1;
            var t2=new google.maps.LatLng(lat1,lng1);
            var t1=new google.maps.LatLng(lat2,lng2);
           // console.log(t1);
             
             //load preselected cells
             var color="#00000";
             var opacity=0.1
             for(t=0;t<loadet.length;t++){
             if(loadet[t][0]<=lat1.toFixed(2)&&loadet[t][1]>=lng1.toFixed(2)&&loadet[t][2]>=lat2.toFixed(2)&&loadet[t][1]<lng2.toFixed(2)){
             var color="#039F10";
             opacity=0.5
             }
             }
            var rectangle = new google.maps.Rectangle();
	var rectOptions = {
        strokeColor: "#000000",
        strokeOpacity: 0.8,
        strokeWeight: 0.5,
        fillColor: color,
        fillOpacity: opacity,
        map: map,
        bounds: new google.maps.LatLngBounds(t2,t1)
                }
                 rectangle.setOptions(rectOptions);
                //laod preseelcetd cells
                // var label = new Label({ map:map,map:map});
        //  label.bindTo('position', rectangle, 'position');
//label.set('text',colName(i)+a);
	 // rectArr.push(rectangle);
         
	  //bindWindow(rectangle, t2.lat().toFixed(2),t1.lat().toFixed(2),t2.lng().toFixed(2),t1.lng().toFixed(2),map)
	  bindWindow(rectangle, t2.lat().toFixed(2),t1.lat().toFixed(2),t2.lng().toFixed(2),t1.lng().toFixed(2),map)
          

          


          
            }
            layer = new google.maps.FusionTablesLayer(935280, {
                                         suppressInfoWindows: true
                                    });
            layer.setQuery("select geometry,name_1 from 935280");
layer.setMap(map);
           // xs=xse
            
        }

});	
});	
});}, 500);	

function findLocation(location, marker) {
	$('#gadressmap').html('<br>Langitude='+location.Ya+'<br>Langitude='+location.Za);
			
	$('#map_canvas').gmap('search', { 'location': location}, function(results, status) {
		if ( status === 'OK' ) {
			////console.log(results);
			
				$('#addLat').val(location.Ya);	
				$('#addLng').val(location.Za);	
			//$('#gadressmap').html(results[0].formatted_address+'<br>Langitude='+location.Ya+'<br>Langitude='+location.Za);
			//marker.setTitle(results[0].formatted_address);
			
			//openDialog(marker);
		}
	});
}




	
	  function bindWindow(rectangle,n1,n2,n3,n4,map){
          
          
          
        
        
	  google.maps.event.addListener(rectangle, 'click', function(event) {
          
          this.setOptions({ 'fillColor':"#039F10",'fillOpacity':0.5});
      
    
     // infowindow.setOptions({ 'maxWidth':30});
	  	//  infowindow.setContent('You selected cell '+n1 + ' '+n3+' '+n2 + ' '+n4)
                var values = new Array();
$.each($("input[name='daysSeleceted[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});
      $.each(values, function(index, value) {
 $('#saveData_'+value).val($('#saveData_'+value).val().replace(n1+","+n2+","+n3+","+n4+";",""));
 ////console.log($('#saveData_'+value).val())
});
 $.each(values, function(index, value) {
 $('#saveData_'+value).val($('#saveData_'+value).val()+n1+","+n2+","+n3+","+n4+";");
 //console.log($('#saveData_'+value).val())
});
                  
            
               
               }
        //  infowindow.setPosition(event.latLng)
		//  infowindow.open(map);
             
        );
        
        google.maps.event.addListener(rectangle, 'rightclick', function(event) {
          
            this.setOptions({ 'fillColor':"#000000",'fillOpacity':0.1});
        var values = new Array();
$.each($("input[name='daysSeleceted[]']:checked"), function() {
  values.push($(this).val());
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
  
});
        $.each(values, function(index, value) {
 $('#saveData_'+value).val($('#saveData_'+value).val().replace(n1+","+n2+","+n3+","+n4+";",""));
 ////console.log($('#saveData_'+value).val())
});
        
      //   $.post("{$_subdomain}/Diary/saveSPGeoCell",{ 'Lat1':n1,'Lng1':n3,'Lat2':n2,'Lng2':n4,'action':2 });
        });
        
         
        
	  }
        function colName(n) {
    var s = "";
    while(n >= 0) {
        s = String.fromCharCode(n % 26 + 97) + s;
        n = Math.floor(n / 26) - 1;
    }
    return s;

}
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
    </script>
{/block}
{block name=body}
<!DOCTYPE html />
<style type="text/css">
    .style1 { background-color:#ffffff;font-weight:bold;border:2px #006699 solid;}
    
   
    </style>
  <div id="domc" style="width:100%;height:100%" >
      {if !isset($prew)||$prew!=12}
    <div id="gadressmap" class="txt" style="position:relative;float:left;border:solid 2px black;left:1px;padding:15px;width:500px;height:400px" name="country" value="" />
    <h2>Map Area Allocation</h2>
    <hr>
    <p>
        <span>Engineer:{$engName}</span>
    </p>
    <p>
        <span>Date:{$date|date_format:"%d/%m/%Y"}</span>
    </p>
    <form method="post" id="gridForm" action="{$_subdomain}/Diary/saveSPGeoCell">
    
   
    <table >
        <td style="vertical-align:middle;background:none;" ></td>
        <td style="vertical-align:middle;background:none;" ><span style="font-size:10px">Apply Allocated Areas to selected dates</span></td>
        <input type="hidden" name="eid" value="{$eid}">
        <input type="hidden" name="date" value="{$date}">
        
     {for $r=0 to 6}
                                <tr>
                                    {if $r!=6}{$b="border-bottom:0;"}{else}{$b=""}{/if}
                                      <td class="firstCol" style="vertical-align:middle;background:none;border:1px solid #CCCCCC;{$b}" ><a href="#" class="DateSaveButton" id="PostcodeLabel_{$r}" style="padding:0px;margin:0px;" >{$dates[$r]|date_format:"%a %d %b"}</a>
                                        
                                          <br><img src="{$_subdomain}/css/Skins/{$_theme}/images/{if $active[$r]=="Active"}green_tick.png{else}red_cross.png{/if}" id="DayStatus_{$r}" align="center" style="padding-top:5px;"  width="20" height="20" >
                                       
                                         
                                      </td>
                                      <td style="vertical-align:middle;background:none;border:1px solid #CCCCCC;border-left:none;{$b}" > 
                                          <input type="checkbox" onclick="$('#saveData_{$dates[$r]}').val($('#saveData_{$date|date_format:"%Y-%m-%d"}').val());{if $date==$dates[$r]}$(this).attr('checked','checked');alert('Cannot unselect primary selected day!');{/if}" name="daysSeleceted[]" value="{$dates[$r]}" {if $date==$dates[$r]}checked=checked{/if}>
                                          <input type="hidden" name="saveData_{$dates[$r]}" id="saveData_{$dates[$r]}" style="width:100px">
                                      </td>
                                </tr> 
                                
                                {/for}
     </table>
      </form>
     <button class="btnStandard" type="button" onclick="saveData()">Save Map Arrea</button>
     <button class="gplus-red" type="button" style="float:right" onclick="window.location='{$_subdomain}/AppointmentDiary/skillsAreaMap/spID={$spid}/eID={$eid}/d={$date}'">Cancel</button>
</div>
     {else}
     <div id="gadressmap" class="txt" style="position:relative;float:left;border:solid 2px black;left:1px;padding:15px;width:500px;height:400px" name="country" value="" />
     <p>This is only preview mode.</p>
     <button class="btnStandard" type="button" onclick="window.location='{$_subdomain}/AppointmentDiary/diaryDefaults/spID={$spid}/sPage=9'">Back</button>    
     </div>
{/if}
 <div id="map_canvas" style="border: 2px solid black;float:right;height:300px;width:100%">
        </div>
   
    

    
       
    
    

{/block}