{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $LoginPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*}  

<script type="text/javascript">

$(document).ready(function(){

    $('input[type=text]').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    $('#answer').focus();

   
    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
    
});

function validateForm() {
    if ($('#answer').val()=='') {        
        $('#error').html("{$page['Errors']['security']}").show('slow');
        $('#answer').focus();
        return false;
    }
    return true;
}
</script>

{/block}

{block name=body}
<div class="main" id="lostPassword">
    
        <form id="lostPasswordForm" name="lostPasswordForm" method="post" action="{$_subdomain}/Login/lostPassword" class="prepend-5 span-14 last">
            
            <input type="hidden" name="branch" value="{$branch|escape:'html'}" />

            <fieldset>

                <legend title="IM5002">{$page['Text']['legend']|escape:'html'}</legend>
                {* <div class="serviceInstructionsBar">IM5002</div> *}
                
                <p class="information">
                    {$page['Text']['instructions']|escape:'html'}
                </p>
                       
                <p class="information" style="text-align: center; margin-bottom: 10px;">
                    {$page['Labels']['prompt']|escape:'html'}: {$security_question|escape:'html'}
                </p>                             
                               
                <p style="text-align: center;">
                    <label for="answer">{$page['Labels']['answer']|escape:'html'}</label>
                    <input type="text" name="answer" id="answer" value="" class="text auto-submit" tabIndex="1" />
                </p>
                
                <p style="text-align: center; margin: 0; ">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <a href="javascript:if (validateForm()) document.lostPasswordForm.submit();" tabIndex="2">{$page['Buttons']['submit']|escape:'html'}</a>
                    </span>
                </p>
		               
                {if $error eq ''}
                <p id="error" class="formError" style="display: none; text-align: center;" />
                {else}
                <p id="error" class="formError" style="text-align: center;" >{$error|escape:'html'}</p>
                {/if}
                <input name="username" type="hidden" value="{$username}" />
                <input name="branch" type="hidden" value="{$branch}" />
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="3">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
                              
            </fieldset>
  
        </form>
    
</div>
{/block}