{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $RAJobsPage}
    {$fullscreen=true}
{/block}


{block name=afterJqueryUI}

    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> 
    
    <style type="text/css" >
    
	.ui-combobox-input {
	    width:270px;
	}
    
    </style> 
    
{/block}


{block name=scripts}

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
    
<script type="text/javascript">
      
    function raJob($rowData) {
	if($rowData[0]) {
	    var params = document.URL.split("raJobs/")[1];
	    document.location.href = "{$_subdomain}/index/jobupdate/" + urlencode($rowData[0]) + "/?ref=raJobs&params=" + params;
	}
    }
      
    $(document).ready(function() {
	
	/*
	if (document.URL.search("atType") == "-1" && $("input[name='ATType[]']").length == 1) {
	    $("input[name='ATType[]']")[0].click();
	}
	*/
	
	if ($("input[name='ATType[]']").length == 1) {
	    $("input[name='ATType[]']")[0].click();
	}
	
	/*
	if ($("input[name='ATType[]']")[0].length && $("input[name='ATType[]']")[0].is(":checked")) {
	    console.log("checked");
	    $("input[name='ATType[]']")[0].click();
	}
	*/
	
	var checked = false;
	var count = 0;
	$("input:radio").each(function() {
	    count++;
	    if($(this).is(":checked")) {
		checked = true;
	    }
	});
	if(!checked) {
	    $("#chart_div, .raJobsResultsPanel").hide();
	    if(count == 1) {
		$("input:radio").click();
	    }
	}


	$(document).on("click", "#overrideStatus", function() {
	    $.ajax({
		type:   "POST",
		url:    "{$_subdomain}/JobUpdate/getAllRAStatuses",
		data:   { type: $("input[name='ATType[]']:checked").val() }
	    }).done(function(response) {
		var data = JSON.parse(response);
		var html = "";
		for(var i = 0; i < data.length; i++) {
		    html += "<option value='" + data[i]["RAStatusID"] + "'>" + data[i]["RAStatusName"] + "</option>";
		}
		$("#NewRAStatus").html(html);
	    });
	    return false;
	});

	$(document).on('click', "#insert_save_btn", function() { 

	    $("#insert_save_btn").hide();
	    $("#cancel_btn").hide();
	    $("#processDisplayText").show();

	    $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=raJobs/Type=ra",        
		    $("#StatusPreferencesForm").serialize(),      
		    function(data){
			document.location.href = "{$_subdomain}/Job/raJobs";
		    }
	    );
		   
	    return false;

	}); 
	
	$(document).on('click', "#cancel_btn", function() { 
	    $("#insert_save_btn").hide();
	    $("#cancel_btn").hide();
	    $("#processDisplayText").show();
            document.location.href = "{$_subdomain}/Job/raJobs";
        });  
	
	//Click handler for RA Type checkboxes.
	$(document).on('click', "#StatusPreferences", function() {
	    //It opens color box popup page.              
	    $.colorbox({   
		inline:	    true,
		href:	    "#DivStatusPreferences",
		title:	    '',
		opacity:    0.75,
		height:	    540,
		width:	    720,
		overlayClose: false,
		escKey:	    false,
		onLoad: function() {
		   // $('#cboxClose').remove();
		},
		onClosed: function() {
		   //location.href = "#EQ7";
		},
		onComplete: function() {
		    $.colorbox.resize();
		}
	    }); 
	});
        
        if("{$atType}" != '0') {
	    var atTypeParam = '/atType='+urlencode("{$atType}");
        } else {
            var atTypeParam = '';
        }
        
	if("{$sType}" != '0') {
	    var sTypeParam = '/sType='+urlencode("{$sType}");
	} else {
	    var sTypeParam = '';
	}
	
	$("#CurrentRAStatus").combobox({
	    change: function() {                  
		$(location).attr('href', '{$_subdomain}/Job/raJobs' + atTypeParam + "/sType="+$("#CurrentRAStatus").val() + 
				"/NewRAStatus="+$("#NewRAStatus").val());
	    }
	});
	
	$("#NewRAStatus").combobox({
	    change: function() {
		if($("#NewRAStatus option:selected").attr("statusCode") == 3) {
		    $("#authNo").parent().show();
		    $("#Notes").parent().hide();
		} else {
		    $("#authNo").parent().hide();
		    $("#Notes").parent().show();
		}
	    }
	});                                   
	
      
	//Click handler for finish button.
	$(document).on('click', "[name='ATType[]']", function() { 
	    if ($(this).is(':checked')) {
		$sltValue = $(this).val();
	    } else {
		$sltValue = 0;
	    } 
	    $(location).attr('href', '{$_subdomain}/Job/raJobs/atType=' + urlencode($sltValue));
	});
                
	//Click handler for TagAllJobs check box.
	$(document).on('click', "#TagAllJobs", function() { 
	    if ($(this).is(':checked')) {
		$(".checkBoxDataTable").attr("checked", "checked");
	    } 
	    $("#UnTagAllJobx").removeAttr("checked");
	});
                
	//Click handler for UnTagAllJobx check box.
	$(document).on('click', "#UnTagAllJobx", function() { 
	    if ($(this).is(':checked'))  {
		$(".checkBoxDataTable").removeAttr("checked"); 
	    } 
	    $("#TagAllJobs").removeAttr("checked");
	});
                 
	//Click handler for Cancel Selection check box.
	$(document).on('click', "#CancelSelection", function() { 
	    $.colorbox.close();
	});
                 
		 
	//Click handler for Confirm Continue check box.
	$(document).on('click', "#ConfirmContinue", function() { 
                
	    sltRAJobsList = '';
                                    
	    $('.checkBoxDataTable').each(function() {
	       sltRAJobsList += $(this).val()+',';
	    });

	    $("#sltRAJobsList").val(sltRAJobsList);  

	    $.post("{$_subdomain}/Job/updateRAJobs/",         
		    $("#raJobsStatusForm").serialize(),      
		    function(data) {
			var p = eval("(" + data + ")");
			if(p == "OK") {
			    //$("#centerInfoText").html("{$page['Text']['save_changes_done']|escape:'html'}").fadeIn('slow').delay("5000").fadeOut('slow');  
			    $(location).attr('href', '{$_subdomain}/Job/raJobs' + atTypeParam + "/sType=" + $("#CurrentRAStatus").val());         
			} else {
			    $("#centerInfoText").html("{$page['Errors']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  
			}
		    }
	    );
                
	    $.colorbox.close();
                
	});
                 
		 
	/*Add a click handler to the apply new ra status button form starts here*/  
	$(document).on('click', '#apply_new_ra_status_btn', function() {
                        
	    $('#raJobsStatusForm').validate({
		ignore: '',
                                        
                rules:  {
		    CurrentRAStatus:	{ required: true },
		    NewRAStatus:	{ required: true },
		    SameStatusFlag: {
			required:   function (element) {
					if($("#CurrentRAStatus").val() != '' && $("#CurrentRAStatus").val() == $("#NewRAStatus").val()) {
					    return true;
					} else {
					    return false;
					}
				    }
		    }, 
		    Notes:		{ required: false },                                                    
		    "RAJobsTagged[]":	{ required: true } 
		},
		    
		messages: {
		    CurrentRAStatus:	{ required: "{$page['Errors']['current_ra_status']|escape:'html'}" },
		    NewRAStatus:	{ required: "{$page['Errors']['new_ra_status']|escape:'html'}" },
		    SameStatusFlag:	{ required: "{$page['Errors']['same_ra_status']|escape:'html'}" },
		    Notes:		{ required: "{$page['Errors']['notes']|escape:'html'}" },  
		    "RAJobsTagged[]":	{ required: "{$page['Errors']['job_tag']|escape:'html'}" }
		},
		    
		errorPlacement: function(error, element) {
		    if(element.attr('name') == "RAJobsTagged[]") {
		       error.insertAfter($("#RAJobsTaggedError"));     
		    } else {
			error.insertAfter( element );
		    }            
		},

		errorClass:		'fieldError',
		onkeyup:		false,
		onblur:			false,
		errorElement:		'label',

		submitHandler:	function() {
				    $("#selectedNewRaStatus").html($("#NewRAStatusElement input:first").val());
				    $.colorbox( {   inline:	true,
						    href:	"#ApplyNewRAStatusConfirmation",
						    title:	'{$page['Text']['apply_new_ra_status_confirmation']|escape:'html'}',
						    opacity:	0.75,
						    height:	220,
						    width:	700,
						    overlayClose: false,
						    escKey:	false,
						    onLoad: function() {
							$('#cboxClose').remove();
						    }
				    }); 
				}
				
	    });
                        
	});


	if($("#CurrentRAStatus").val() != "") {

	    //RA job result data table starts here...  
	    $('#raJobsResults').PCCSDataTable({
	       displayButtons:	"P",
	       bServerSide:	false,
	       htmlTablePageId:    'raJobsResultsPanel',
	       htmlTableId:        'raJobsResults',
	       fetchDataUrl:       '{$_subdomain}/Job/raJobsResult'+atTypeParam+sTypeParam,
	       pickCallbackMethod: 'raJob',
	       dblclickCallbackMethod: 'raJob',
	       tooltipTitle:	"{$page['Text']['tooltip_title']|escape:'html'}",
	       searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	       colorboxForceClose:	false,
	       aoColumns: [ 
			
			
			{for $er=0 to $data_keys|@count-1}                    
                            {$vis='true'}  
                              
                                {if $er==0}{$vis='false'}{/if}
                                { 'bVisible': true ,'bSearchable': true },
                        {/for} 
                      { 'bSortable': false }
                            
                            
		]
	   });
           table=$('#raJobsResults').dataTable();
           $('#check_all').click( function() {
            $('input', table.fnGetNodes()).attr('checked',this.checked);
           
        } );
	   //RA job result data table ends here...  
       
	} else {
   
	    $("#raJobsResults").hide();
   
	}
       
    });
     
     
    //RA jobs chart starts here..
    google.load("visualization", "1", { packages:["corechart"] });
    google.setOnLoadCallback(drawChart);
    function drawChart() {
	var data = new google.visualization.DataTable();
	var gridLines = 0;
               
	var $statsResult = [
	    {foreach $statsResult as $sname}  
	       ["{$sname[0]}", {$sname[1]}, {$sname[2]}, "{$sname[3]}"]{if not $sname@last},{/if} 
	    {/foreach}
	];

	data.addColumn('string', "{$page['Text']['status_types']|escape:'html'}");
	$total_rows = 0;
	$data_rows = new Array();
	$data_rows[0] = new Array();
	$data_rows[0][0] = '';
	maxrows = 0;
	$colorsData = new Array();
                
	$cCnt = 1;
                
	for(var $i=0;$i<$statsResult.length;$i++) {
	    data.addColumn('number', $statsResult[$i][0], $statsResult[$i][2]);
	    data.addColumn({ type:'string',role:'tooltip' }); // tooltip col.

	    $total_rows += $statsResult[$i][1];                        

	    //$data_rows[0][$i+1] = $statsResult[$i][1];
	    $data_rows[0][$cCnt++] = $statsResult[$i][1];
	    $data_rows[0][$cCnt++] = $statsResult[$i][0];

	    if (gridLines <  $statsResult[$i][1]) {
		gridLines =  $statsResult[$i][1] + 1;
		maxrows = $statsResult[$i][1];
	    }

	    $colorsData[$i] = "#" + $statsResult[$i][3];
	}   
                
	data.addRows($data_rows);

	var options = {
	    'chartArea': { 
		'width': $(window).width()-350, 
		height: '80%',
		left : '70' 
	    },
	    'legend': { 
		position: 'right',
		textStyle: { 'fontSize' : '9' } 
	    },
	    'vAxis': { 
		title: "{$page['Text']['quantity']|escape:'html'}", 
		titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
		slantedText: false,
		minValue: 0,
		maxValue: maxrows,
		textColor: '#ffffff',
		gridlines: { count: gridLines }
	    },
	    'hAxis': { 
		title: "{$graph_title}",
		titleTextStyle: '{ fontName: "Arial, Helvetica, sans-serif" }',
		slantedText: false
	    },
	    colors: $colorsData         
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
	chart.draw(data, options);
                
	//a click handler which grabs some values then redirects the page
	google.visualization.events.addListener(chart, 'select', function() {
	
	    //grab a few details before redirecting
	    var selection = chart.getSelection();
	    //var col = selection[0].column;
	    //alert(chart.getColumnId(selection[0].column));
	    var col = data.getColumnId(selection[0].column);
	    //alert('The user selected ' + topping);

	    document.location.href = '{$_subdomain}/Job/raJobs' + '/atType=' + urlencode("{$atType}") + '/sType=' + col;

	});
                
    }
      
      
       //displaying table pref colorbox
function showTablePreferences(){
$.colorbox({ 
 
                        href:"{$_subdomain}/Job/tableDisplayPreferenceSetup/page=raJobs/table=job",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      //RA jobs chart ends here..
      
</script>


{/block}


{block name=body}
<div style="float:right">
    <a href="#" onclick="showTablePreferences();">Display Preferences</a>
</div>
<div class="breadcrumb" {if $fullscreen==true}style="width:100%"{/if}>
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>

{include file='include/menu.tpl'}
    
<div class="main" id="home"  {if $fullscreen==true}style="width:100%;min-width: 950px"{/if}>
    
    <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
    
    <div class="raJobsPanel" {if $fullscreen==true}style="min-width: 950px;margin: auto"{/if}>
        
	<form id="raJobsForm" name="raJobsForm" method="post" action="#" class="inline">
             
            <fieldset>
            <legend title="">{$page['Text']['rajob_legend']|escape:'html'}</legend>
		<p>
		    <div class="innerFieldSet">
			<fieldset>
			    <legend title="" class="innerLegend">
				{$page['Text']['authorisation_type_legend']|escape:'html'}
			    </legend>
			    {foreach $AuthorisationTypes as $at}
				{*
				<span class="leftAlign" >
				   <input type="checkbox" name="ATType[]"  {if $atType && $atType eq $at.Name} checked="checked" {/if}  value="{$at.Name|escape:'html'}" >     {$at.Value|escape:'html'}
				</span>
				*}
				<span class="leftAlign" >
				   <input type="radio" name="ATType[]" {if $atType && $atType == $at.Name}checked="checked"{/if} value="{$at.Name|escape:'html'}" /> {$at.Value|escape:'html'}
				</span>
			    {/foreach}
			</fieldset> 
		    </div>
		</p>
            
		<p>
		    <div id="chart_div" ></div>
		    <a href="#" id="StatusPreferences" style="float:right;font-size:12px;">
			{$page['Text']['status_preferences']|escape:'html'}
		    </a>
		</p>
          
	    </fieldset> 
        
	</form>
		    
    </div>   
    
                                    
    {if $totalRows != 0}                                 
       
	<div class="raJobsResultsPanel">
        
	    <form id="raJobsStatusForm" name="raJobsStatusForm" method="post" action="#" class="inline">
            
		<p id="CurrentRAStatusElement">
		    <label style="width:200px;">
			{$page['Text']['current_ra_status']|escape:'html'}: <span style="color:red;">*</span>
		    </label>  
		    <select name="CurrentRAStatus" id="CurrentRAStatus" class="text" >
		       <option value="" {if $sType == '0'}selected="selected"{/if}></option>
		       {foreach $statsResult as $rastl}
			    <option value="{$rastl.2}" {if $sType == $rastl.2}selected="selected"{/if}>{$rastl.0|escape:'html'}</option>
		       {/foreach}
		    </select>
		    <a href="#" id="overrideStatus" style="display:block; position:relative; float:right;">Override current status</a>
		</p>
		
		<p id="NewRAStatusElement">
		    <label style="width:200px;">
			{$page['Text']['new_ra_status']|escape:'html'}: <span style="color:red;">*</span>
		    </label> 
		    <select name="NewRAStatus" id="NewRAStatus" class="text">
			<option value="" {if $NewRAStatus == '0'}selected="selected"{/if}></option>
			{foreach $RAStatusTypesSubList as $rastl}
			    <option statusCode="{$rastl.RAStatusCode}" value="{$rastl.RAStatusID}" {if $NewRAStatus == $rastl.RAStatusID}selected="selected"{/if}>{$rastl.RAStatusName|escape:'html'}</option>
			{/foreach}
		    </select> 
		</p>
            
		<p>
		    <label style="width:200px;">
			{$page['Text']['notes']|escape:'html'}: <span style="color:red;">*</span>
		    </label> 
		    <textarea class="text" style="width:280px; max-height:200px; min-height:30px; height:30px; resize:vertical;" name="Notes" value="" id="Notes"></textarea>
		</p>

		<p style="display:none;">
		    <label style="width:200px;">
			{$page['Labels']['auth_no']|escape:'html'}: <span style="color:red;">*</span>
		    </label> 
		    <input type="text" class="text" style="width:270px;" name="authNo" value="" id="authNo" />
		</p>
	    
		<p>
		    <input type="hidden" name="SameStatusFlag" id="SameStatusFlag" value="" />
		    <input type="hidden" name="atType_hidden" id="atType_hidden" value="{$atType|escape:'html'}" />
		    &nbsp;
		</p>
		
		<p>
		    <label style="width:200px;">&nbsp;</label>
		    <input type="submit" name="apply_new_ra_status_btn" id="apply_new_ra_status_btn" class="btnStandard" value="{$page['Buttons']['apply_new_ra_status']|escape:'html'}" />
		   
		</p>
            
		<p>
		    &nbsp;<br>   
		    <input type="hidden" name="RAJobsTaggedError" id="RAJobsTaggedError" value="" />
		</p>
        
                <table id="raJobsResults" border="0" cellpadding="0" cellspacing="0" class="browse">
                    <thead>
                    
		     {foreach from=$data_keys key=kk item=vv}
                                  
                      <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="text-align:center;width:10px!important">
                                    <input style="margin-left:0px;" title="Tag all visible jobs" type='checkbox' value=0 id='check_all' />
                                </th>
                                </thead>
		    <tbody>
		    </tbody>
		</table>  
			
		<input type="hidden" name="sltRAJobsList" id="sltRAJobsList" />          
		
	    </form>
        
	</div>
			
	<div style="display:none;" >
	    <div id="ApplyNewRAStatusConfirmation"  >
		<p style="text-align:center">
		    {$page['Text']['status_change_confirm_text']|escape:'html'}: <strong>
			<span id="selectedNewRaStatus" ></span></strong><br/><br/>
			<a href="#" id="ConfirmContinue" class="form-button">
			    {$page['Text']['continue']|escape:'html'}
			</a>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#" id="CancelSelection" class="form-button">
			    {$page['Text']['cancel_selection']|escape:'html'}
			</a>
			<br>
		</p>
	    </div>
	</div>
        
    {/if}
  
    <div style="display:none;">
        
         <div id="DivStatusPreferences" class="SystemAdminFormPanel">
    
	    <form id="StatusPreferencesForm" name="StatusPreferencesForm" method="post" action="#" class="inline" autocomplete="off">

                <fieldset>
		    
                    <legend title="">{$page['Text']['status_preferences']|escape:'html'}</legend>

                    <p><label id="suggestText"></label></p>
                    
		    {foreach $RAStatusTypesList as $ras}
			<p>
			    <label>&nbsp;</label>
			    <input type="checkbox" style="width:30px;" class="text" name="StatusID[]" value="{$ras.RAStatusID|escape:'html'}"  {if $ras.Exists eq true} checked="checked"  {/if}>
			    &nbsp;&nbsp;{$ras.RAStatusName|escape:'html'}
			</p>
		    {/foreach} 

                    <p>
			<span class= "bottomButtons" >
			    <input type="submit" name="insert_save_btn" class="textSubmitButton" id="insert_save_btn" value="{$page['Buttons']['save']|escape:'html'}" />
			    &nbsp;
			    <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;" value="{$page['Buttons']['cancel']|escape:'html'}" />
			    <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16">
				&nbsp;&nbsp;
				{$page['Buttons']['process_record']|escape:'html'}
			    </span>
			</span>
		    </p>

		</fieldset>    

	    </form>        

	</div>

    </div>
                                    
</div>
				
{/block}
