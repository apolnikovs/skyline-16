SELECT		DISTINCT(ServiceBaseModel) AS ServiceBaseModel,

			@SoftwareUpdate := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'software_update'
						#PARAMS#
			) AS SoftwareUpdate,
			
			@Education := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'education'
						#PARAMS#
			) AS Education,
			
			@Settings := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'setup'
						#PARAMS#
			) AS Settings,
			
			@Damaged := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'damaged'
						#PARAMS#
			) AS Damaged,
			
			@FaultyAccessory := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'faulty_acc'
						#PARAMS#
			) AS FaultyAccessory,
			
			@NonUKModel := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'non_uk_mod'
						#PARAMS#
			) AS NonUKModel,
			
			@Unresolvable := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'unresolvable'
						#PARAMS#
			) AS Unresolvable,
			
			@BranchRepair := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'branch_repair'
						#PARAMS#
			) AS BranchRepair,
			
			@RepairRequired := (
				SELECT	COUNT(*)
				FROM	job AS j
				WHERE	j.ServiceBaseModel IS NOT NULL AND
						j.ServiceAction IS NOT NULL AND
						j.ServiceBaseModel = job.ServiceBaseModel AND
						j.ServiceAction = 'repair_required'
						#PARAMS#
			) AS RepairRequired,
			
			(@SoftwareUpdate + @Education + @Settings + @Damaged + @FaultyAccessory + @NonUKModel + @Unresolvable + @BranchRepair + 
			@RepairRequired) AS Total

FROM		job

WHERE		job.ServiceBaseModel IS NOT NULL AND job.ServiceAction IS NOT NULL #PARAMS#