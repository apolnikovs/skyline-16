
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.238');


# ---------------------------------------------------------------------- #
# Modify table contact_history                                           #
# ---------------------------------------------------------------------- #
ALTER TABLE `contact_history`
	CHANGE COLUMN `UserCode` `UserCode` VARCHAR(30) NULL DEFAULT NULL AFTER `ContactTime`;


# ---------------------------------------------------------------------- #
# Modify table status_history                                            #
# ---------------------------------------------------------------------- #
 ALTER TABLE `status_history`
	CHANGE COLUMN `UserCode` `UserCode` VARCHAR(30) NULL DEFAULT NULL AFTER `AdditionalInformation`;

 
 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.239');
