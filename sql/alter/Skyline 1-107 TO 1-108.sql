# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-26 12:06                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.107');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` DROP FOREIGN KEY `user_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `client` DROP FOREIGN KEY `branch_TO_client`;

ALTER TABLE `extended_warrantor` DROP FOREIGN KEY `user_TO_extended_warrantor`;

ALTER TABLE `network_client` DROP FOREIGN KEY `client_TO_network_client`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `client_TO_client_branch`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `client_TO_service_type_alias`;

ALTER TABLE `unit_client_type` DROP FOREIGN KEY `client_TO_unit_client_type`;

ALTER TABLE `user` DROP FOREIGN KEY `client_TO_user`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `brand` DROP FOREIGN KEY `client_TO_brand`;

ALTER TABLE `product` DROP FOREIGN KEY `client_TO_product`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `client_TO_unit_pricing_structure`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `client_TO_town_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `client_TO_postcode_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `client_TO_central_service_allocation`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `client_TO_bought_out_guarantee`;

ALTER TABLE `network` DROP FOREIGN KEY `client_TO_network`;

# ---------------------------------------------------------------------- #
# Modify table "client"                                                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` ADD COLUMN `EnableCatalogueSearch` ENUM('Yes','No') NOT NULL DEFAULT 'Yes';

ALTER TABLE `client` MODIFY `EnableCatalogueSearch` ENUM('Yes','No') NOT NULL DEFAULT 'Yes' AFTER `EnableProductDatabase`;

# ---------------------------------------------------------------------- #
# Modify table "extended_warrantor"                                      #
# ---------------------------------------------------------------------- #

CREATE INDEX `IDX_extended_warrantor_ModifiedUserID_FK` ON `extended_warrantor` (`ModifiedUserID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `client` ADD CONSTRAINT `user_TO_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `client` ADD CONSTRAINT `branch_TO_client` 
    FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `extended_warrantor` ADD CONSTRAINT `user_TO_extended_warrantor` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_client` ADD CONSTRAINT `client_TO_network_client` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `client_TO_client_branch` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `client_TO_service_type_alias` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `client_TO_unit_client_type` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `user` ADD CONSTRAINT `client_TO_user` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `brand` ADD CONSTRAINT `client_TO_brand` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `product` ADD CONSTRAINT `client_TO_product` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `client_TO_unit_pricing_structure` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `client_TO_town_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `client_TO_postcode_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `client_TO_central_service_allocation` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `client_TO_bought_out_guarantee` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `network` ADD CONSTRAINT `client_TO_network` 
    FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.108');
