# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.177');

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `job` ADD COLUMN `DateReturnedToCustomer` TIMESTAMP NULL DEFAULT NULL AFTER `RepairCompleteDate`;


# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD COLUMN `ThirdPartyServiceProvider` INT(11) NULL DEFAULT NULL AFTER `DefaultServiceProvider`; 

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.178');
