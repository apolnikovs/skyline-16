# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.149');

# ---------------------------------------------------------------------- #
# Add table "diary_holiday_diary"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_holiday_diary` (
	`DiaryHolidayID` INT(10) NOT NULL AUTO_INCREMENT,
	`ServiceProviderID` INT(10) NOT NULL DEFAULT '0',
	`ServiceProviderEngineerID` INT(10) NOT NULL DEFAULT '0',
	`StartTime` DATETIME NULL DEFAULT NULL,
	`EndTime` DATETIME NULL DEFAULT NULL,
	`Reason` VARCHAR(2000) NULL DEFAULT NULL,
	`Created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`DiaryHolidayID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Add table "diary_holiday_slots"                                        #
# ---------------------------------------------------------------------- #


CREATE TABLE `diary_holiday_slots` (
	`DiaryHolidaySlotsID` INT(10) NULL AUTO_INCREMENT,
	`DiaryHolidayDiaryID` INT(10) NULL,
	`HolidayDate` DATE NULL,
	`StartTimeSec` INT NULL,
	`EndTimeSec` INT NULL,
	`TotalTimeSec` INT NULL,
	`ServiceProviderEngineerID` INT NULL,
	PRIMARY KEY (`DiaryHolidaySlotsID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

ALTER TABLE `product` ADD COLUMN `Supplier` VARCHAR(30) NULL DEFAULT NULL AFTER `PaymentRoute`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.150');