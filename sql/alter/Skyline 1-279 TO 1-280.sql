# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.279');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 
CREATE TABLE IF NOT EXISTS part_status ( PartStatusID int(10) NOT NULL AUTO_INCREMENT, PartStatusName varchar(50) DEFAULT NULL, PartStatusDescription varchar(250) DEFAULT NULL, ServiceProviderID int(11) DEFAULT NULL, PartStatusDisplayOrder int(4) DEFAULT NULL, DisplayOrderSection enum('Yes','No') NOT NULL DEFAULT 'No', Status enum('Active','In-active') NOT NULL DEFAULT 'Active', Available enum('Y','N') DEFAULT NULL, InStock enum('Y','N') DEFAULT NULL, PRIMARY KEY (PartStatusID) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE part ADD COLUMN SPPartOrderID INT(11) NULL DEFAULT NULL AFTER SPPartRequisitionID;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.280');
