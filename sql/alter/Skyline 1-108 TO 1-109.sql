# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-01 10:03                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.108');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` DROP FOREIGN KEY `county_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `country_TO_service_provider`;

ALTER TABLE `service_provider` DROP FOREIGN KEY `user_TO_service_provider`;

ALTER TABLE `user` DROP FOREIGN KEY `network_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `service_provider_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `client_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `branch_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `brand_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `security_question_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `user_TO_user`;

ALTER TABLE `user` DROP FOREIGN KEY `customer_title_TO_user`;

ALTER TABLE `role` DROP FOREIGN KEY `user_TO_role`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `service_provider_TO_network_service_provider`;

ALTER TABLE `user_role` DROP FOREIGN KEY `user_TO_user_role`;

ALTER TABLE `assigned_permission` DROP FOREIGN KEY `role_TO_assigned_permission`;

ALTER TABLE `user_role` DROP FOREIGN KEY `role_TO_user_role`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `audit` DROP FOREIGN KEY `user_TO_audit`;

ALTER TABLE `appointment` DROP FOREIGN KEY `user_TO_appointment`;

ALTER TABLE `status_history` DROP FOREIGN KEY `user_TO_status_history`;

ALTER TABLE `status` DROP FOREIGN KEY `user_TO_status`;

ALTER TABLE `payment_type` DROP FOREIGN KEY `user_TO_payment_type`;

ALTER TABLE `country` DROP FOREIGN KEY `user_TO_country`;

ALTER TABLE `county` DROP FOREIGN KEY `user_TO_county`;

ALTER TABLE `job_type` DROP FOREIGN KEY `user_TO_job_type`;

ALTER TABLE `customer_title` DROP FOREIGN KEY `user_TO_customer_title`;

ALTER TABLE `permission` DROP FOREIGN KEY `user_TO_permission`;

ALTER TABLE `assigned_permission` DROP FOREIGN KEY `user_TO_assigned_permission`;

ALTER TABLE `branch` DROP FOREIGN KEY `user_TO_branch`;

ALTER TABLE `brand_branch` DROP FOREIGN KEY `user_TO_brand_branch`;

ALTER TABLE `brand` DROP FOREIGN KEY `user_TO_brand`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `user_TO_client_branch`;

ALTER TABLE `product` DROP FOREIGN KEY `user_TO_product`;

ALTER TABLE `completion_status` DROP FOREIGN KEY `user_TO_completion_status`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `user_TO_unit_pricing_structure`;

ALTER TABLE `unit_client_type` DROP FOREIGN KEY `user_TO_unit_client_type`;

ALTER TABLE `unit_type` DROP FOREIGN KEY `user_TO_unit_type`;

ALTER TABLE `client` DROP FOREIGN KEY `user_TO_client`;

ALTER TABLE `client_purchase_order` DROP FOREIGN KEY `user_TO_client_purchase_order`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `user_TO_town_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `user_TO_central_service_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `user_TO_postcode_allocation`;

ALTER TABLE `repair_skill` DROP FOREIGN KEY `user_TO_repair_skill`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `service_provider_TO_central_service_allocation`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `service_provider_TO_town_allocation`;

ALTER TABLE `model` DROP FOREIGN KEY `user_TO_model`;

ALTER TABLE `service_type` DROP FOREIGN KEY `user_TO_service_type`;

ALTER TABLE `service_type_alias` DROP FOREIGN KEY `user_TO_service_type_alias`;

ALTER TABLE `network_client` DROP FOREIGN KEY `user_TO_network_client`;

ALTER TABLE `manufacturer` DROP FOREIGN KEY `user_TO_manufacturer`;

ALTER TABLE `network_manufacturer` DROP FOREIGN KEY `user_TO_network_manufacturer`;

ALTER TABLE `network_service_provider` DROP FOREIGN KEY `user_TO_network_service_provider`;

ALTER TABLE `network` DROP FOREIGN KEY `user_TO_network`;

ALTER TABLE `client` DROP FOREIGN KEY `service_provider_TO_client`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `service_provider_TO_postcode_allocation`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `user_TO_bought_out_guarantee`;

ALTER TABLE `general_default` DROP FOREIGN KEY `user_TO_general_default`;

ALTER TABLE `email_job` DROP FOREIGN KEY `user_TO_email_job`;

ALTER TABLE `email` DROP FOREIGN KEY `user_TO_email`;

ALTER TABLE `ra_status` DROP FOREIGN KEY `user_TO_ra_status`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `user_TO_ra_history`;

ALTER TABLE `extended_warrantor` DROP FOREIGN KEY `user_TO_extended_warrantor`;

# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD COLUMN `DefaultTravelTime` INTEGER NOT NULL DEFAULT 120;

ALTER TABLE `service_provider` ADD COLUMN `DefaultTravelSpeed` INTEGER NOT NULL DEFAULT 120;

# ---------------------------------------------------------------------- #
# Modify table "user"                                                    #
# ---------------------------------------------------------------------- #

ALTER TABLE `user` ADD COLUMN `ManufacturerID` INTEGER;

ALTER TABLE `user` ADD COLUMN `ExtendedWarrantorID` INTEGER;

ALTER TABLE `user` MODIFY `ManufacturerID` INTEGER AFTER `BranchID`;

ALTER TABLE `user` MODIFY `ExtendedWarrantorID` INTEGER AFTER `ManufacturerID`;

# ---------------------------------------------------------------------- #
# Modify table "role"                                                    #
# ---------------------------------------------------------------------- #

ALTER TABLE `role` MODIFY `UserType` ENUM('Service Network','Client','Branch','Service Provider','Manufacturer', 'Extended Warrantor') NOT NULL;

# ---------------------------------------------------------------------- #
# Add table "viamente_end_day"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `viamente_end_day` (
    `ViamenteEndDayID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `RouteDate` DATE,
    `CreatedDate` TIMESTAMP,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` TIMESTAMP,
    CONSTRAINT `viamente_end_dayID` PRIMARY KEY (`ViamenteEndDayID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD CONSTRAINT `county_TO_service_provider` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `country_TO_service_provider` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `service_provider` ADD CONSTRAINT `user_TO_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user` ADD CONSTRAINT `network_TO_user` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `user` ADD CONSTRAINT `service_provider_TO_user` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user` ADD CONSTRAINT `client_TO_user` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `user` ADD CONSTRAINT `branch_TO_user` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `user` ADD CONSTRAINT `brand_TO_user` 
    FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `user` ADD CONSTRAINT `security_question_TO_user` 
    FOREIGN KEY (`SecurityQuestionID`) REFERENCES `security_question` (`SecurityQuestionID`);

ALTER TABLE `user` ADD CONSTRAINT `user_TO_user` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `user` ADD CONSTRAINT `customer_title_TO_user` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `role` ADD CONSTRAINT `user_TO_role` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `service_provider_TO_network_service_provider` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `user_role` ADD CONSTRAINT `user_TO_user_role` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `role_TO_assigned_permission` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `user_role` ADD CONSTRAINT `role_TO_user_role` 
    FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `audit` ADD CONSTRAINT `user_TO_audit` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `appointment` ADD CONSTRAINT `user_TO_appointment` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `status_history` ADD CONSTRAINT `user_TO_status_history` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `status` ADD CONSTRAINT `user_TO_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `payment_type` ADD CONSTRAINT `user_TO_payment_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `country` ADD CONSTRAINT `user_TO_country` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `county` ADD CONSTRAINT `user_TO_county` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `job_type` ADD CONSTRAINT `user_TO_job_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `customer_title` ADD CONSTRAINT `user_TO_customer_title` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `permission` ADD CONSTRAINT `user_TO_permission` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `assigned_permission` ADD CONSTRAINT `user_TO_assigned_permission` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `branch` ADD CONSTRAINT `user_TO_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `user_TO_brand_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `brand` ADD CONSTRAINT `user_TO_brand` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `user_TO_client_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `product` ADD CONSTRAINT `user_TO_product` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `completion_status` ADD CONSTRAINT `user_TO_completion_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `user_TO_unit_pricing_structure` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_client_type` ADD CONSTRAINT `user_TO_unit_client_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `unit_type` ADD CONSTRAINT `user_TO_unit_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `user_TO_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client_purchase_order` ADD CONSTRAINT `user_TO_client_purchase_order` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `user_TO_town_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `user_TO_central_service_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `user_TO_postcode_allocation` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `repair_skill` ADD CONSTRAINT `user_TO_repair_skill` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `service_provider_TO_central_service_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `service_provider_TO_town_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `model` ADD CONSTRAINT `user_TO_model` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_type` ADD CONSTRAINT `user_TO_service_type` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `service_type_alias` ADD CONSTRAINT `user_TO_service_type_alias` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_client` ADD CONSTRAINT `user_TO_network_client` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `manufacturer` ADD CONSTRAINT `user_TO_manufacturer` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `user_TO_network_manufacturer` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_service_provider` ADD CONSTRAINT `user_TO_network_service_provider` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network` ADD CONSTRAINT `user_TO_network` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client` ADD CONSTRAINT `service_provider_TO_client` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `service_provider_TO_postcode_allocation` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `user_TO_bought_out_guarantee` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `general_default` ADD CONSTRAINT `user_TO_general_default` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email_job` ADD CONSTRAINT `user_TO_email_job` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `email` ADD CONSTRAINT `user_TO_email` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `user_TO_ra_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `user_TO_ra_history` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `extended_warrantor` ADD CONSTRAINT `user_TO_extended_warrantor` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.109');
