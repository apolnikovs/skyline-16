-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.29-0ubuntu0.12.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-02-25 11:03:15
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table create.alternative_fields
CREATE TABLE IF NOT EXISTS `alternative_fields` (
  `alternativeFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `primaryFieldID` int(11) NOT NULL,
  `alternativeFieldName` varchar(250) NOT NULL,
  `status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `brandID` int(11) NOT NULL,
  PRIMARY KEY (`alternativeFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.alternative_fields: ~0 rows (approximately)
/*!40000 ALTER TABLE `alternative_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `alternative_fields` ENABLE KEYS */;


-- Dumping structure for table create.appointment
CREATE TABLE IF NOT EXISTS `appointment` (
  `AppointmentID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `AppointmentDate` date DEFAULT NULL,
  `AppointmentTime` varchar(8) DEFAULT NULL COMMENT 'May be AM, PM, ANY or hh:mm:ss',
  `AppointmentType` varchar(30) DEFAULT NULL,
  `EngineerCode` char(3) DEFAULT NULL,
  `OutCardLeft` tinyint(1) NOT NULL DEFAULT '0',
  `SBAppointID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `NonSkylineJobID` int(11) DEFAULT NULL,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `AppointmentStartTime` time DEFAULT NULL,
  `AppointmentEndTime` time DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  `CreatedUserID` int(11) DEFAULT NULL,
  `AppointmentOrphaned` enum('Yes','No') DEFAULT NULL,
  `AppointmentStatusID` int(11) DEFAULT NULL,
  `Notes` varchar(1000) DEFAULT NULL,
  `AppointmentChangeReason` varchar(1000) DEFAULT NULL,
  `WallMount` tinyint(1) DEFAULT NULL,
  `MenRequired` tinyint(2) DEFAULT NULL,
  `BookedBy` varchar(60) DEFAULT NULL,
  `ServiceProviderSkillsetID` int(11) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `ScreenSize` tinyint(3) DEFAULT NULL,
  `orphan` enum('Yes','No') DEFAULT 'No',
  `importance` tinyint(2) DEFAULT '0',
  `ViamenteStartTime` datetime DEFAULT NULL,
  `CreatedTimeStamp` datetime DEFAULT NULL,
  `ViamenteTravelTime` int(11) DEFAULT NULL,
  `SBusercode` varchar(6) DEFAULT NULL,
  `ViamenteUnreached` tinyint(4) DEFAULT '0',
  `OnlineDiarySkillSet` int(11) DEFAULT NULL,
  `ViamenteDepartTime` datetime DEFAULT NULL,
  `ViamenteServiceTime` datetime DEFAULT NULL,
  `ViamenteReturnTime` datetime DEFAULT NULL,
  `ForceEngineerToViamente` tinyint(1) DEFAULT '0',
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `AppointmentStartTime2` time DEFAULT NULL,
  `AppointmentEndTime2` time DEFAULT NULL,
  `CompletedBy` enum('Remote Engineer','Manual') DEFAULT NULL,
  `CompletionEngineerUserCode` char(3) DEFAULT NULL,
  `ArrivalDateTime` datetime DEFAULT NULL,
  `PartsAttached` tinyint(1) NOT NULL DEFAULT '0',
  `CompletionDateTime` datetime DEFAULT NULL,
  `Latitude` decimal(17,14) DEFAULT NULL,
  `Longitude` decimal(17,14) DEFAULT NULL,
  `ViamenteExclude` enum('Yes','No') DEFAULT 'No',
  `BreakStartSec` int(11) DEFAULT NULL,
  `BreakDurationSec` int(11) DEFAULT NULL,
  `LockPartsEngineer` enum('Yes','No') NOT NULL DEFAULT 'No',
  `Cancelled` enum('No','Yes') DEFAULT 'No',
  PRIMARY KEY (`AppointmentID`),
  KEY `IDX_appointment_1` (`AppointmentDate`),
  KEY `IDX_appointment_2` (`ServiceProviderID`),
  KEY `IDX_appointment_3` (`NonSkylineJobID`),
  KEY `IDX_appointment_4` (`DiaryAllocationID`),
  KEY `IDX_appointment_5` (`ServiceProviderEngineerID`),
  KEY `IDX_appointment_6` (`ServiceProviderID`),
  KEY `IDX_appointment_7` (`NonSkylineJobID`),
  KEY `IDX_appointment_8` (`DiaryAllocationID`),
  KEY `IDX_appointment_9` (`AppointmentDate`),
  KEY `IDX_appointment_10` (`ServiceProviderEngineerID`),
  KEY `IDX_appointment_JobID_FK` (`JobID`),
  KEY `IDX_appointment_UserID_FK` (`UserID`),
  CONSTRAINT `job_TO_appointment` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_appointment` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.appointment: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;


-- Dumping structure for table create.appointment_allocation_slot
CREATE TABLE IF NOT EXISTS `appointment_allocation_slot` (
  `AppointmentAllocationSlotID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  `TimeFrom` time DEFAULT NULL,
  `TimeTo` time DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `Type` enum('AM','PM','ANY') DEFAULT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`AppointmentAllocationSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.appointment_allocation_slot: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_allocation_slot` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_allocation_slot` ENABLE KEYS */;


-- Dumping structure for table create.appointment_source
CREATE TABLE IF NOT EXISTS `appointment_source` (
  `AppointmentSourceID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AppointmentSourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.appointment_source: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_source` ENABLE KEYS */;


-- Dumping structure for table create.appointment_status
CREATE TABLE IF NOT EXISTS `appointment_status` (
  `AppointmentStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`AppointmentStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.appointment_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_status` ENABLE KEYS */;


-- Dumping structure for table create.appointment_type
CREATE TABLE IF NOT EXISTS `appointment_type` (
  `AppointmentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(60) DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`AppointmentTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.appointment_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_type` ENABLE KEYS */;


-- Dumping structure for table create.assigned_permission
CREATE TABLE IF NOT EXISTS `assigned_permission` (
  `PermissionID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `AccessLevel` char(6) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PermissionID`,`RoleID`),
  UNIQUE KEY `TUC_assigned_permission_1` (`RoleID`,`PermissionID`),
  KEY `IDX_assigned_permission_PermissionID_FK` (`PermissionID`),
  KEY `IDX_assigned_permission_RoleID_FK` (`RoleID`),
  KEY `IDX_assigned_permission_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `permission_TO_assigned_permission` FOREIGN KEY (`PermissionID`) REFERENCES `permission` (`PermissionID`),
  CONSTRAINT `role_TO_assigned_permission` FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`),
  CONSTRAINT `user_TO_assigned_permission` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.assigned_permission: ~0 rows (approximately)
/*!40000 ALTER TABLE `assigned_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `assigned_permission` ENABLE KEYS */;


-- Dumping structure for table create.audit
CREATE TABLE IF NOT EXISTS `audit` (
  `AuditID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `AuditTrailActionID` int(11) NOT NULL,
  `Description` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditID`),
  KEY `IDX_audit_JobID_FK` (`JobID`),
  KEY `IDX_audit_AuditTrailActionID_FK` (`AuditTrailActionID`),
  KEY `IDX_audit_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `audit_trail_action_TO_audit` FOREIGN KEY (`AuditTrailActionID`) REFERENCES `audit_trail_action` (`AuditTrailActionID`),
  CONSTRAINT `job_TO_audit` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_audit` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.audit: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit` ENABLE KEYS */;


-- Dumping structure for table create.audit_new
CREATE TABLE IF NOT EXISTS `audit_new` (
  `AuditID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `TableName` varchar(100) NOT NULL,
  `Action` enum('update','delete') NOT NULL,
  `PrimaryID` varchar(50) NOT NULL,
  `FieldName` varchar(100) DEFAULT NULL,
  `Value` text,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AuditID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.audit_new: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_new` ENABLE KEYS */;


-- Dumping structure for table create.audit_trail_action
CREATE TABLE IF NOT EXISTS `audit_trail_action` (
  `AuditTrailActionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `ActionCode` int(11) NOT NULL,
  `Type` enum('Process','Action','Edit') NOT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`AuditTrailActionID`),
  KEY `IDX_audit_trail_action_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_audit_trail_action` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.audit_trail_action: ~0 rows (approximately)
/*!40000 ALTER TABLE `audit_trail_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trail_action` ENABLE KEYS */;


-- Dumping structure for table create.bought_out_guarantee
CREATE TABLE IF NOT EXISTS `bought_out_guarantee` (
  `BoughtOutGuaranteeID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BoughtOutGuaranteeID`),
  KEY `IDX_bought_out_guarantee_NetworkID_FK` (`NetworkID`),
  KEY `IDX_bought_out_guarantee_ClientID_FK` (`ClientID`),
  KEY `IDX_bought_out_guarantee_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_bought_out_guarantee_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_bought_out_guarantee_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_bought_out_guarantee_ModelID_FK` (`ModelID`),
  KEY `IDX_bought_out_guarantee_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_bought_out_guarantee` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `manufacturer_TO_bought_out_guarantee` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `model_TO_bought_out_guarantee` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_bought_out_guarantee` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_bought_out_guarantee` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `unit_type_TO_bought_out_guarantee` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_bought_out_guarantee` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.bought_out_guarantee: ~0 rows (approximately)
/*!40000 ALTER TABLE `bought_out_guarantee` DISABLE KEYS */;
/*!40000 ALTER TABLE `bought_out_guarantee` ENABLE KEYS */;


-- Dumping structure for table create.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `BranchID` int(11) NOT NULL AUTO_INCREMENT,
  `BranchName` varchar(40) DEFAULT NULL,
  `BranchNumber` varchar(40) DEFAULT NULL,
  `BranchType` enum('Store','Extra','Call Centre','Website') DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ServiceManager` varchar(250) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `UseAddressProgram` enum('Yes','No') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ServiceAppraisalRequired` enum('Yes','No') NOT NULL DEFAULT 'No',
  `DefaultServiceProvider` int(11) DEFAULT NULL,
  `ThirdPartyServiceProvider` int(11) DEFAULT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ContactFax` varchar(40) DEFAULT NULL,
  `AccountNo` varchar(40) DEFAULT NULL,
  `CurrentLocationOfProduct` enum('Customer','Branch','Service Provider','Supplier','other') DEFAULT NULL,
  PRIMARY KEY (`BranchID`),
  KEY `IDX_branch_CountyID_FK` (`CountyID`),
  KEY `IDX_branch_CountryID_FK` (`CountryID`),
  KEY `IDX_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `FK_branch_service_provider` (`DefaultServiceProvider`),
  KEY `branch_TO_service_provider1` (`ThirdPartyServiceProvider`),
  CONSTRAINT `branch_TO_service_provider1` FOREIGN KEY (`ThirdPartyServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `country_TO_branch` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_branch` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `FK_branch_service_provider` FOREIGN KEY (`DefaultServiceProvider`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


-- Dumping structure for table create.brand
CREATE TABLE IF NOT EXISTS `brand` (
  `BrandID` int(11) NOT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BrandName` varchar(40) DEFAULT NULL,
  `BrandLogo` varchar(255) DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `AutoSendEmails` enum('1','0') DEFAULT '1',
  `EmailType` enum('Generic','CRM') NOT NULL DEFAULT 'Generic',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CustomerTermsConditions` text,
  PRIMARY KEY (`BrandID`),
  KEY `IDX_brand_ClientID_FK` (`ClientID`),
  KEY `IDX_brand_NetworkID_FK` (`NetworkID`),
  KEY `IDX_brand_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_brand` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_brand` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_brand` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.brand: ~0 rows (approximately)
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;


-- Dumping structure for table create.brand_branch
CREATE TABLE IF NOT EXISTS `brand_branch` (
  `BrandBranchID` int(11) NOT NULL AUTO_INCREMENT,
  `BranchID` int(11) NOT NULL,
  `BrandID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BrandBranchID`),
  UNIQUE KEY `TUC_brand_branch_1` (`BrandID`,`BranchID`),
  KEY `IDX_brand_branch_BrandID_FK` (`BrandID`),
  KEY `IDX_brand_branch_BranchID_FK` (`BranchID`),
  KEY `IDX_brand_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `branch_TO_brand_branch` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `brand_TO_brand_branch` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`) ON UPDATE CASCADE,
  CONSTRAINT `user_TO_brand_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.brand_branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `brand_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand_branch` ENABLE KEYS */;


-- Dumping structure for table create.central_service_allocation
CREATE TABLE IF NOT EXISTS `central_service_allocation` (
  `CentralServiceAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CentralServiceAllocationID`),
  KEY `IDX_central_service_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_central_service_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_central_service_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_central_service_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_central_service_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_central_service_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_central_service_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_central_service_allocation_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_central_service_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_central_service_allocation_CountyID_FK` (`CountyID`),
  KEY `IDX_central_service_allocation_CountryID_FK` (`CountryID`),
  CONSTRAINT `client_TO_central_service_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_central_service_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_central_service_allocation` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `job_type_TO_central_service_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_central_service_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_central_service_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_central_service_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_central_service_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_central_service_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `unit_type_TO_central_service_allocation` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_central_service_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.central_service_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `central_service_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `central_service_allocation` ENABLE KEYS */;


-- Dumping structure for table create.claim_response
CREATE TABLE IF NOT EXISTS `claim_response` (
  `ClaimResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `ErrorCode` varchar(18) DEFAULT NULL,
  `ErrorDescription` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ClaimResponseID`),
  KEY `IDX_claim_response_JobID_FK` (`JobID`),
  CONSTRAINT `job_TO_claim_response` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.claim_response: ~0 rows (approximately)
/*!40000 ALTER TABLE `claim_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_response` ENABLE KEYS */;


-- Dumping structure for table create.client
CREATE TABLE IF NOT EXISTS `client` (
  `ClientID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `DefaultBranchID` int(11) DEFAULT NULL,
  `ClientName` varchar(40) DEFAULT NULL,
  `AccountNumber` text,
  `GroupName` varchar(40) DEFAULT NULL,
  `InvoiceNetwork` enum('Yes','No') DEFAULT NULL,
  `VATRateID` int(11) DEFAULT NULL,
  `VATNumber` varchar(40) DEFAULT NULL,
  `PaymentDiscount` decimal(10,2) DEFAULT '0.00',
  `PaymentTerms` int(11) DEFAULT NULL,
  `VendorNumber` varchar(40) DEFAULT NULL,
  `PurchaseOrderNumber` varchar(40) DEFAULT NULL,
  `SageAccountNumber` varchar(40) DEFAULT NULL,
  `SageNominalCode` int(11) DEFAULT NULL,
  `ForcePolicyNo` char(1) DEFAULT 'Y',
  `PromptETD` char(1) DEFAULT 'Y',
  `TradeSpecificUnitTypes` char(1) DEFAULT 'Y',
  `TradeSpecificManufacturers` char(1) DEFAULT 'Y',
  `TradeSpecificCallTypes` enum('Y','N') NOT NULL DEFAULT 'N',
  `TradeSpecificJobTypes` enum('Y','N') NOT NULL DEFAULT 'N',
  `EnableCompletionStatus` char(1) DEFAULT 'Y',
  `EnableProductDatabase` enum('Yes','No') NOT NULL DEFAULT 'No',
  `EnableCatalogueSearch` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `DefaultTurnaroundTime` int(2) DEFAULT NULL,
  `ClientShortName` varchar(9) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `InvoicedCompanyName` varchar(40) DEFAULT NULL,
  `InvoicedBuilding` varchar(40) DEFAULT NULL,
  `InvoicedStreet` varchar(40) DEFAULT NULL,
  `InvoicedArea` varchar(40) DEFAULT NULL,
  `InvoicedTownCity` varchar(40) DEFAULT NULL,
  `InvoicedCountyID` int(11) DEFAULT NULL,
  `InvoicedCountryID` int(11) DEFAULT NULL,
  `InvoicedPostalCode` varchar(40) DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `ForceReferralNumber` enum('Yes','No') DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientID`),
  KEY `IDX_client_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_client_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_client_DefaultBranchID_FK` (`DefaultBranchID`),
  CONSTRAINT `branch_TO_client` FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `service_provider_TO_client` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_client` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.client: ~0 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;


-- Dumping structure for table create.client_branch
CREATE TABLE IF NOT EXISTS `client_branch` (
  `ClientBranchID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `BranchID` int(11) NOT NULL,
  `NetworkID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientBranchID`),
  UNIQUE KEY `TUC_client_branch_1` (`NetworkID`,`BranchID`,`ClientID`),
  KEY `IDX_client_branch_ClientID_FK` (`ClientID`),
  KEY `IDX_client_branch_BranchID_FK` (`BranchID`),
  KEY `IDX_client_branch_NetworkID_FK` (`NetworkID`),
  KEY `IDX_client_branch_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `branch_TO_client_branch` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `client_TO_client_branch` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_client_branch` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_client_branch` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.client_branch: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_branch` ENABLE KEYS */;


-- Dumping structure for table create.client_purchase_order
CREATE TABLE IF NOT EXISTS `client_purchase_order` (
  `ClientPurchaseOrder` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) DEFAULT NULL,
  `PurchaseOrderNumber` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ClientPurchaseOrder`),
  KEY `IDX_client_purchase_order_BrandID_FK` (`BrandID`),
  KEY `IDX_client_purchase_order_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_client_purchase_order` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_client_purchase_order` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.client_purchase_order: ~0 rows (approximately)
/*!40000 ALTER TABLE `client_purchase_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_purchase_order` ENABLE KEYS */;


-- Dumping structure for table create.completion_status
CREATE TABLE IF NOT EXISTS `completion_status` (
  `CompletionStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `CompletionStatus` varchar(20) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Sort` int(11) DEFAULT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CompletionStatusID`),
  KEY `IDX_completion_status_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_completion_status_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_completion_status` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_completion_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.completion_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `completion_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `completion_status` ENABLE KEYS */;


-- Dumping structure for table create.contact_history
CREATE TABLE IF NOT EXISTS `contact_history` (
  `ContactHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `ContactHistoryActionID` int(11) NOT NULL,
  `BrandID` int(11) DEFAULT '1000',
  `ContactDate` date DEFAULT NULL,
  `ContactTime` time DEFAULT NULL,
  `UserCode` varchar(15) DEFAULT NULL,
  `Subject` varchar(50) DEFAULT NULL,
  `Note` varchar(254) DEFAULT NULL,
  `IsDownloadedToSC` tinyint(1) NOT NULL DEFAULT '0',
  `SystemAllocated` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`ContactHistoryID`),
  KEY `IDX_contact_history_JobID_FK` (`JobID`),
  KEY `IDX_contact_history_ContactHistoryActionID_FK` (`ContactHistoryActionID`),
  CONSTRAINT `contact_history_action_TO_contact_history` FOREIGN KEY (`ContactHistoryActionID`) REFERENCES `contact_history_action` (`ContactHistoryActionID`),
  CONSTRAINT `job_TO_contact_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.contact_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_history` ENABLE KEYS */;


-- Dumping structure for table create.contact_history_action
CREATE TABLE IF NOT EXISTS `contact_history_action` (
  `ContactHistoryActionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `ActionCode` int(11) NOT NULL,
  `Type` enum('User Defined','System Defined') NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`ContactHistoryActionID`),
  KEY `IDX_contact_history_action_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_contact_history_action` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Contact History Actions details.';

-- Dumping data for table create.contact_history_action: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_history_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_history_action` ENABLE KEYS */;


-- Dumping structure for table create.contact_us_messages
CREATE TABLE IF NOT EXISTS `contact_us_messages` (
  `ContactUsMessageID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `ContactUsSubjectID` int(11) DEFAULT NULL,
  `Message` text,
  `CreatedDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.contact_us_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_us_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us_messages` ENABLE KEYS */;


-- Dumping structure for table create.contact_us_subject
CREATE TABLE IF NOT EXISTS `contact_us_subject` (
  `ContactUsSubjectID` int(11) DEFAULT NULL,
  `Subject` varchar(200) DEFAULT NULL,
  `PriorityOrder` int(11) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.contact_us_subject: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact_us_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us_subject` ENABLE KEYS */;


-- Dumping structure for table create.country
CREATE TABLE IF NOT EXISTS `country` (
  `CountryID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `CountryCode` int(11) DEFAULT NULL,
  `InternationalCode` varchar(3) NOT NULL,
  `Currency` varchar(20) DEFAULT NULL,
  `CurrencySymbol` varchar(1) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountryID`),
  KEY `IDX_country_BrandID_FK` (`BrandID`),
  KEY `IDX_country_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_country` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_country` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains country details.';

-- Dumping data for table create.country: ~0 rows (approximately)
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;


-- Dumping structure for table create.country_vat_rate
CREATE TABLE IF NOT EXISTS `country_vat_rate` (
  `CountryID` int(11) NOT NULL,
  `VatRateID` int(11) NOT NULL,
  PRIMARY KEY (`CountryID`,`VatRateID`),
  KEY `IDX_country_vat_rate_VatRateID_FK` (`VatRateID`),
  KEY `IDX_country_vat_rate_CountryID_FK` (`CountryID`),
  CONSTRAINT `country_TO_country_vat_rate` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `vat_rate_TO_country_vat_rate` FOREIGN KEY (`VatRateID`) REFERENCES `vat_rate` (`VatRateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.country_vat_rate: ~0 rows (approximately)
/*!40000 ALTER TABLE `country_vat_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_vat_rate` ENABLE KEYS */;


-- Dumping structure for table create.county
CREATE TABLE IF NOT EXISTS `county` (
  `CountyID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `CountyCode` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountyID`),
  KEY `IDX_county_BrandID_FK` (`BrandID`),
  KEY `IDX_county_CountryID_FK` (`CountryID`),
  KEY `IDX_county_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_county` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `country_TO_county` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `user_TO_county` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains county details of each country.';

-- Dumping data for table create.county: ~0 rows (approximately)
/*!40000 ALTER TABLE `county` DISABLE KEYS */;
/*!40000 ALTER TABLE `county` ENABLE KEYS */;


-- Dumping structure for table create.courier
CREATE TABLE IF NOT EXISTS `courier` (
  `CourierID` int(11) NOT NULL AUTO_INCREMENT,
  `CourierName` varchar(100) NOT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `Online` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `AccountNo` varchar(40) NOT NULL,
  `IPAddress` varchar(32) DEFAULT NULL,
  `UserName` varchar(40) DEFAULT NULL,
  `Password` varchar(40) DEFAULT NULL,
  `ReportFailureDays` tinyint(2) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CourierID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.courier: ~0 rows (approximately)
/*!40000 ALTER TABLE `courier` DISABLE KEYS */;
/*!40000 ALTER TABLE `courier` ENABLE KEYS */;


-- Dumping structure for table create.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CustomerTitleID` int(11) DEFAULT NULL,
  `ContactFirstName` varchar(40) DEFAULT NULL,
  `ContactLastName` varchar(40) DEFAULT NULL,
  `ContactHomePhone` varchar(40) DEFAULT NULL,
  `ContactWorkPhoneExt` varchar(10) DEFAULT NULL,
  `ContactWorkPhone` varchar(40) DEFAULT NULL,
  `ContactFax` varchar(40) DEFAULT NULL,
  `ContactMobile` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `DataProtection` enum('Yes','No') DEFAULT NULL,
  `DataProtectionEmail` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionLetter` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionSMS` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionMobile` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionHome` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `DataProtectionOffice` enum('Marketing','SVCCampaign','Both') DEFAULT NULL,
  `SamsungCustomerRef` varchar(10) DEFAULT NULL,
  `CustomerType` varchar(10) DEFAULT NULL,
  `CompanyName` varchar(100) DEFAULT NULL,
  `Password` varchar(256) DEFAULT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `LastLoggedIn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SecurityQuestionID` int(11) DEFAULT NULL,
  `SecurityQuestionAnswer` varchar(256) DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `EmailAuthKey` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `IDX_customer_1` (`PostalCode`),
  KEY `IDX_customer_2` (`ContactLastName`),
  KEY `IDX_customer_3` (`ContactFirstName`),
  KEY `IDX_customer_CountyID_FK` (`CountyID`),
  KEY `IDX_customer_CountryID_FK` (`CountryID`),
  KEY `IDX_customer_CustomerTitleID_FK` (`CustomerTitleID`),
  CONSTRAINT `country_TO_customer` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_customer` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_title_TO_customer` FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


-- Dumping structure for table create.customer_title
CREATE TABLE IF NOT EXISTS `customer_title` (
  `CustomerTitleID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TitleCode` int(11) DEFAULT NULL,
  `Type` enum('Male','Female','Unknown') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CustomerTitleID`),
  KEY `IDX_customer_title_BrandID_FK` (`BrandID`),
  KEY `IDX_customer_title_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_Title` (`Title`),
  CONSTRAINT `brand_TO_customer_title` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_customer_title` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Customer Titles details.';

-- Dumping data for table create.customer_title: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_title` ENABLE KEYS */;


-- Dumping structure for table create.diary_allocation
CREATE TABLE IF NOT EXISTS `diary_allocation` (
  `DiaryAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `AllocatedDate` date DEFAULT NULL,
  `AppointmentAllocationSlotID` int(11) DEFAULT NULL,
  `TimeFrom` time DEFAULT NULL,
  `NumberOfAllocations` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  `SlotsLeft` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`DiaryAllocationID`),
  KEY `IDX_diary_allocation_1` (`ServiceProviderID`),
  KEY `IDX_diary_allocation_2` (`AllocatedDate`),
  KEY `IDX_diary_allocation_3` (`AppointmentAllocationSlotID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_allocation` ENABLE KEYS */;


-- Dumping structure for table create.diary_holiday_diary
CREATE TABLE IF NOT EXISTS `diary_holiday_diary` (
  `DiaryHolidayID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `ServiceProviderEngineerID` int(10) NOT NULL DEFAULT '0',
  `StartTime` datetime DEFAULT NULL,
  `EndTime` datetime DEFAULT NULL,
  `Reason` varchar(2000) DEFAULT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DiaryHolidayID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_holiday_diary: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_holiday_diary` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_holiday_diary` ENABLE KEYS */;


-- Dumping structure for table create.diary_holiday_slots
CREATE TABLE IF NOT EXISTS `diary_holiday_slots` (
  `DiaryHolidaySlotsID` int(10) NOT NULL AUTO_INCREMENT,
  `DiaryHolidayDiaryID` int(10) DEFAULT NULL,
  `HolidayDate` date DEFAULT NULL,
  `StartTimeSec` int(11) DEFAULT NULL,
  `EndTimeSec` int(11) DEFAULT NULL,
  `TotalTimeSec` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DiaryHolidaySlotsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_holiday_slots: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_holiday_slots` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_holiday_slots` ENABLE KEYS */;


-- Dumping structure for table create.diary_postcode_allocation
CREATE TABLE IF NOT EXISTS `diary_postcode_allocation` (
  `DiaryPostcodeAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`DiaryPostcodeAllocationID`),
  KEY `IDX_diary_postcode_allocation_1` (`DiaryAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_postcode_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_postcode_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_postcode_allocation` ENABLE KEYS */;


-- Dumping structure for table create.diary_sp_map
CREATE TABLE IF NOT EXISTS `diary_sp_map` (
  `diarySPMapID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `MapDate` date DEFAULT NULL,
  `MapUUID` varchar(64) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`diarySPMapID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_sp_map: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_sp_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_sp_map` ENABLE KEYS */;


-- Dumping structure for table create.diary_town_allocation
CREATE TABLE IF NOT EXISTS `diary_town_allocation` (
  `DiaryTownAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `DiaryAllocationID` int(11) DEFAULT NULL,
  `Town` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`DiaryTownAllocationID`),
  KEY `IDX_diary_town_allocation_1` (`DiaryAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.diary_town_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `diary_town_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `diary_town_allocation` ENABLE KEYS */;


-- Dumping structure for table create.doc_api
CREATE TABLE IF NOT EXISTS `doc_api` (
  `DocApiID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Version` varchar(10) NOT NULL,
  `Author` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Baseuri` varchar(50) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DocApiID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.doc_api: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_api` ENABLE KEYS */;


-- Dumping structure for table create.doc_categories
CREATE TABLE IF NOT EXISTS `doc_categories` (
  `DocCategoriesID` int(11) NOT NULL AUTO_INCREMENT,
  `DocApiID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Path` varchar(50) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DocCategoriesID`),
  KEY `IDX_doc_categories_DocApiID_FK` (`DocApiID`),
  CONSTRAINT `doc_api_TO_doc_categories` FOREIGN KEY (`DocApiID`) REFERENCES `doc_api` (`DocApiID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.doc_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_categories` ENABLE KEYS */;


-- Dumping structure for table create.doc_method_response
CREATE TABLE IF NOT EXISTS `doc_method_response` (
  `DocMethodResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `DocProceduresID` int(11) NOT NULL,
  `AnswerCode` int(11) NOT NULL,
  `Response` text NOT NULL,
  `Description` text,
  `CDATA` text,
  PRIMARY KEY (`DocMethodResponseID`),
  KEY `IDX_doc_method_response_DocProceduresID_FK` (`DocProceduresID`),
  CONSTRAINT `doc_procedures_TO_doc_method_response` FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.doc_method_response: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_method_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_method_response` ENABLE KEYS */;


-- Dumping structure for table create.doc_procedures
CREATE TABLE IF NOT EXISTS `doc_procedures` (
  `DocProceduresID` int(11) NOT NULL AUTO_INCREMENT,
  `DocCategoriesID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  `Path` varchar(50) DEFAULT NULL,
  `methods` varchar(100) NOT NULL,
  `formats` varchar(100) NOT NULL,
  `auth_types` varchar(100) DEFAULT NULL,
  `DocApiID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DocProceduresID`),
  KEY `IDX_doc_procedures_DocCategoriesID_FK` (`DocCategoriesID`),
  CONSTRAINT `doc_categories_TO_doc_procedures` FOREIGN KEY (`DocCategoriesID`) REFERENCES `doc_categories` (`DocCategoriesID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.doc_procedures: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_procedures` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_procedures` ENABLE KEYS */;


-- Dumping structure for table create.doc_procedure_parameters
CREATE TABLE IF NOT EXISTS `doc_procedure_parameters` (
  `DocProcedureParametersID` int(11) NOT NULL AUTO_INCREMENT,
  `DocProceduresID` int(11) NOT NULL,
  `CodeLanguage` varchar(50) NOT NULL,
  `CDATA` text NOT NULL,
  `Default` varchar(50) NOT NULL,
  `Option` varchar(50) NOT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Size` int(11) DEFAULT NULL,
  `Example` text NOT NULL,
  `response_ex` text NOT NULL,
  `Required` tinyint(1) NOT NULL,
  PRIMARY KEY (`DocProcedureParametersID`),
  KEY `IDX_doc_procedure_parameters_DocProceduresID_FK` (`DocProceduresID`),
  CONSTRAINT `doc_procedures_TO_doc_procedure_parameters` FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.doc_procedure_parameters: ~0 rows (approximately)
/*!40000 ALTER TABLE `doc_procedure_parameters` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_procedure_parameters` ENABLE KEYS */;


-- Dumping structure for table create.email
CREATE TABLE IF NOT EXISTS `email` (
  `EmailID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailCode` varchar(64) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `Message` text,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EmailID`),
  KEY `IDX_email_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_email` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.email: ~0 rows (approximately)
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;


-- Dumping structure for table create.email_job
CREATE TABLE IF NOT EXISTS `email_job` (
  `EmailJobID` int(11) NOT NULL AUTO_INCREMENT,
  `MailBody` text,
  `MailAccessCode` varchar(32) DEFAULT NULL,
  `JobID` int(11) DEFAULT NULL,
  `EmailID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EmailJobID`),
  KEY `IDX_email_job_JobID_FK` (`JobID`),
  KEY `IDX_email_job_EmailID_FK` (`EmailID`),
  KEY `IDX_email_job_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `email_TO_email_job` FOREIGN KEY (`EmailID`) REFERENCES `email` (`EmailID`),
  CONSTRAINT `job_TO_email_job` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `user_TO_email_job` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.email_job: ~0 rows (approximately)
/*!40000 ALTER TABLE `email_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_job` ENABLE KEYS */;


-- Dumping structure for table create.extended_warrantor
CREATE TABLE IF NOT EXISTS `extended_warrantor` (
  `ExtendedWarrantorID` int(11) NOT NULL AUTO_INCREMENT,
  `ExtendedWarrantorName` varchar(64) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ExtendedWarrantorID`),
  KEY `IDX_extended_warrantor_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_extended_warrantor` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.extended_warrantor: ~0 rows (approximately)
/*!40000 ALTER TABLE `extended_warrantor` DISABLE KEYS */;
/*!40000 ALTER TABLE `extended_warrantor` ENABLE KEYS */;


-- Dumping structure for table create.general_default
CREATE TABLE IF NOT EXISTS `general_default` (
  `GeneralDefaultID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `IDNo` int(11) NOT NULL,
  `Category` varchar(20) NOT NULL,
  `DefaultName` varchar(40) NOT NULL,
  `Default` varchar(20) NOT NULL,
  `Description` text NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`GeneralDefaultID`),
  KEY `IDX_general_default_BrandID_FK` (`BrandID`),
  KEY `IDX_general_default_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_general_default` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_general_default` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.general_default: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_default` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_default` ENABLE KEYS */;


-- Dumping structure for table create.help_text
CREATE TABLE IF NOT EXISTS `help_text` (
  `HelpTextID` int(11) NOT NULL AUTO_INCREMENT,
  `HelpTextCode` varchar(64) NOT NULL,
  `HelpTextTitle` varchar(255) DEFAULT NULL,
  `HelpText` text,
  `CreatedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`HelpTextID`),
  UNIQUE KEY `HelpTextCode` (`HelpTextCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains help text for each element.';

-- Dumping data for table create.help_text: ~0 rows (approximately)
/*!40000 ALTER TABLE `help_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `help_text` ENABLE KEYS */;


-- Dumping structure for table create.job
CREATE TABLE IF NOT EXISTS `job` (
  `JobID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BrandID` int(11) DEFAULT NULL,
  `BranchID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) NOT NULL,
  `StatusID` int(11) DEFAULT NULL,
  `OpenJobStatus` enum('in_store','with_supplier','awaiting_collection','customer_notified','closed') DEFAULT NULL,
  `NetworkRefNo` varchar(20) DEFAULT NULL,
  `AgentRefNo` varchar(20) DEFAULT NULL,
  `ServiceCentreJobNo` int(11) DEFAULT NULL,
  `JobSite` enum('field call','workshop') DEFAULT NULL,
  `ServiceBaseManufacturer` varchar(30) DEFAULT NULL,
  `ServiceBaseModel` varchar(24) DEFAULT NULL,
  `ServiceBaseUnitType` varchar(30) DEFAULT NULL,
  `SerialNo` varchar(24) DEFAULT NULL,
  `VestelModelCode` varchar(30) DEFAULT NULL,
  `VestelManufactureDate` char(4) DEFAULT NULL,
  `DateOfPurchase` date DEFAULT NULL,
  `GuaranteeCode` char(4) DEFAULT NULL,
  `CustomerType` char(1) DEFAULT NULL,
  `ConditionCode` char(4) DEFAULT NULL,
  `SymptomCode` char(4) DEFAULT NULL,
  `DefectType` char(4) DEFAULT NULL,
  `ReportedFault` varchar(2000) DEFAULT NULL,
  `RepairType` enum('customer','stock') DEFAULT NULL,
  `Notes` text,
  `FaultOccurredDate` date DEFAULT NULL,
  `FaultOccurredTime` time DEFAULT NULL,
  `RepairCompleteDate` date DEFAULT NULL,
  `DateReturnedToCustomer` timestamp NULL DEFAULT NULL,
  `DownloadedToSC` tinyint(1) DEFAULT NULL,
  `ItemLocation` varchar(40) DEFAULT NULL,
  `RepairDescription` text,
  `CompletionStatus` varchar(40) DEFAULT NULL,
  `OriginalRetailer` varchar(75) DEFAULT NULL,
  `RetailerLocation` varchar(100) DEFAULT NULL,
  `DateOfManufacture` date DEFAULT NULL,
  `PolicyNo` varchar(20) DEFAULT NULL,
  `ETDDate` date DEFAULT NULL,
  `ETDTime` time DEFAULT NULL,
  `Accessories` varchar(200) DEFAULT NULL,
  `UnitCondition` varchar(150) DEFAULT NULL,
  `Insurer` varchar(30) DEFAULT NULL,
  `RepairAuthStatus` enum('required','not_required','confirmed','rejected') DEFAULT NULL,
  `AuthorisationNo` varchar(20) DEFAULT NULL,
  `DateBooked` date DEFAULT NULL,
  `TimeBooked` time DEFAULT NULL,
  `DateUnitReceived` date DEFAULT NULL,
  `ClosedDate` date DEFAULT NULL,
  `Status` varchar(40) DEFAULT NULL,
  `CancelReason` text,
  `EngineerCode` varchar(20) DEFAULT NULL,
  `EngineerName` varchar(50) DEFAULT NULL,
  `ClaimNumber` varchar(20) DEFAULT NULL,
  `ClaimTransmitStatus` int(11) DEFAULT NULL,
  `eInvoiceStatus` int(11) DEFAULT NULL,
  `ChargeableLabourCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableLabourVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeablePartsCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableDeliveryCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableDeliveryVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableVATCost` decimal(10,2) DEFAULT '0.00',
  `ChargeableSubTotal` decimal(10,2) DEFAULT '0.00',
  `ChargeableTotalCost` decimal(10,2) DEFAULT '0.00',
  `AgentChargeableInvoiceNo` int(11) DEFAULT '0',
  `AgentChargeableInvoiceDate` date DEFAULT NULL,
  `ProductLocation` enum('Customer','Branch','Service Provider','Supplier','other') DEFAULT NULL,
  `ModelID` int(11) DEFAULT NULL,
  `BookedBy` int(11) NOT NULL,
  `ModifiedUserID` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ContractTracking` varchar(4) DEFAULT NULL,
  `SamsungWarrantyParts` varchar(1) DEFAULT NULL,
  `SamsungWarrantyLabour` varchar(1) DEFAULT NULL,
  `RMANumber` int(11) DEFAULT NULL,
  `AgentStatus` varchar(30) DEFAULT NULL,
  `StockCode` varchar(64) DEFAULT NULL,
  `EngineerAssignedDate` date DEFAULT NULL,
  `EngineerAssignedTime` date DEFAULT NULL,
  `WarrantyNotes` varchar(50) DEFAULT NULL,
  `NewFirmwareVersion` varchar(20) DEFAULT NULL,
  `ClaimBillNumber` varchar(15) DEFAULT NULL,
  `EstimateStatus` enum('Awaiting Confiirmation','Accepted','Rejected') DEFAULT NULL,
  `ReceiptNo` varchar(40) DEFAULT NULL,
  `ColAddCompanyName` varchar(100) DEFAULT NULL,
  `ColAddBuildingNameNumber` varchar(40) DEFAULT NULL,
  `ColAddStreet` varchar(40) DEFAULT NULL,
  `ColAddLocalArea` varchar(40) DEFAULT NULL,
  `ColAddTownCity` varchar(60) DEFAULT NULL,
  `ColAddCountyID` int(11) DEFAULT NULL,
  `ColAddCountryID` int(11) DEFAULT NULL,
  `ColAddPostcode` varchar(10) DEFAULT NULL,
  `ColAddEmail` varchar(40) DEFAULT NULL,
  `ColAddPhone` varchar(40) DEFAULT NULL,
  `ColAddPhoneExt` varchar(10) DEFAULT NULL,
  `ExtendedWarrantyNo` varchar(40) DEFAULT NULL,
  `JobSourceID` int(11) DEFAULT NULL,
  `JobRating` int(2) DEFAULT NULL,
  `batchID` int(11) DEFAULT NULL,
  `RAType` enum('repair','return','estimate','invoice','on hold') DEFAULT NULL,
  `RAStatusID` int(11) DEFAULT NULL,
  `CollectionDate` date DEFAULT NULL,
  `CourierID` int(11) DEFAULT NULL,
  `SamsungSubServiceType` varchar(2) DEFAULT NULL,
  `SamsungRefNo` varchar(15) DEFAULT NULL,
  `ConsumerType` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`JobID`),
  KEY `IDX_job_1` (`GuaranteeCode`),
  KEY `IDX_job_2` (`DateBooked`),
  KEY `IDX_job_3` (`ClosedDate`),
  KEY `IDX_job_4` (`JobSourceID`),
  KEY `IDX_job_5` (`NetworkRefNo`),
  KEY `IDX_job_6` (`ServiceCentreJobNo`),
  KEY `IDX_job_7` (`OpenJobStatus`),
  KEY `IDX_job_NetworkID_FK` (`NetworkID`),
  KEY `IDX_job_BranchID_FK` (`BranchID`),
  KEY `IDX_job_ClientID_FK` (`ClientID`),
  KEY `IDX_job_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_job_CustomerID_FK` (`CustomerID`),
  KEY `IDX_job_ProductID_FK` (`ProductID`),
  KEY `IDX_job_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_job_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_job_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_job_StatusID_FK` (`StatusID`),
  KEY `IDX_job_ModelID_FK` (`ModelID`),
  KEY `IDX_job_BookedBy_FK` (`BookedBy`),
  KEY `IDX_job_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_job_ColAddCountyID_FK` (`ColAddCountyID`),
  KEY `IDX_job_ColAddCountryID_FK` (`ColAddCountryID`),
  KEY `IDX_job_RAStatusID_FK` (`RAStatusID`),
  KEY `IDX_job_SerialNo` (`SerialNo`),
  KEY `BrandID` (`BrandID`),
  KEY `IDX_ServiceBaseUnitType` (`ServiceBaseUnitType`),
  CONSTRAINT `branch_TO_job` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `client_TO_job` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_job` FOREIGN KEY (`ColAddCountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_job` FOREIGN KEY (`ColAddCountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_TO_job` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `job_type_TO_job` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_job` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `model_TO_job` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_job` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `product_TO_job` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  CONSTRAINT `ra_status_TO_job` FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`),
  CONSTRAINT `service_provider_TO_job` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_job` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `status_TO_job` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`),
  CONSTRAINT `user_TO_job` FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`),
  CONSTRAINT `user_TO_job_ModifiedUser` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.job: ~0 rows (approximately)
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
/*!40000 ALTER TABLE `job` ENABLE KEYS */;


-- Dumping structure for table create.job_import_batch_data
CREATE TABLE IF NOT EXISTS `job_import_batch_data` (
  `batchID` int(11) NOT NULL AUTO_INCREMENT,
  `batchNumber` int(11) NOT NULL,
  `clientID` int(11) NOT NULL,
  `batchRowCount` int(11) NOT NULL,
  `batchDescription` varchar(250) NOT NULL,
  `batchState` enum('created','importing','finished') NOT NULL,
  `batchHash` varchar(40) NOT NULL,
  `batchTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`batchID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.job_import_batch_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_import_batch_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_import_batch_data` ENABLE KEYS */;


-- Dumping structure for table create.job_source
CREATE TABLE IF NOT EXISTS `job_source` (
  `JobSourceID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`JobSourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.job_source: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_source` ENABLE KEYS */;


-- Dumping structure for table create.job_type
CREATE TABLE IF NOT EXISTS `job_type` (
  `JobTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Type` varchar(30) NOT NULL,
  `TypeCode` int(11) NOT NULL,
  `Priority` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`JobTypeID`),
  KEY `IDX_job_type_1` (`Type`),
  KEY `IDX_job_type_BrandID_FK` (`BrandID`),
  KEY `IDX_job_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_job_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_job_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Job Type details.';

-- Dumping data for table create.job_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_type` ENABLE KEYS */;


-- Dumping structure for table create.manufacturer
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `ManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `ManufacturerName` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `AuthorisationRequired` enum('Yes','No') DEFAULT 'No',
  `AuthReqChangedDate` timestamp NULL DEFAULT NULL,
  `AuthorisationManager` varchar(100) DEFAULT NULL,
  `AuthorisationManagerEmail` varchar(100) DEFAULT NULL,
  `ManufacturerLogo` varchar(40) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ManufacturerID`),
  UNIQUE KEY `IDX_manufacturer_1` (`ManufacturerName`),
  KEY `IDX_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_manufacturer` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;


-- Dumping structure for table create.manufacturer_service_provider
CREATE TABLE IF NOT EXISTS `manufacturer_service_provider` (
  `ManufacturerID` int(10) NOT NULL,
  `ServiceProviderID` int(10) NOT NULL,
  `Authorised` enum('Yes','No') DEFAULT 'Yes',
  `DataChecked` enum('Yes','No') DEFAULT 'No',
  PRIMARY KEY (`ManufacturerID`,`ServiceProviderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.manufacturer_service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `manufacturer_service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer_service_provider` ENABLE KEYS */;


-- Dumping structure for table create.model
CREATE TABLE IF NOT EXISTS `model` (
  `ModelID` int(11) NOT NULL AUTO_INCREMENT,
  `ManufacturerID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ModelNumber` varchar(30) DEFAULT NULL,
  `ModelName` varchar(24) DEFAULT NULL,
  `ModelDescription` text,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ModelID`),
  UNIQUE KEY `TUC_model_1` (`ModelNumber`,`ManufacturerID`),
  KEY `IDX_model_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_model_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_model_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_model` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `unit_type_TO_model` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_model` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.model: ~0 rows (approximately)
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
/*!40000 ALTER TABLE `model` ENABLE KEYS */;


-- Dumping structure for table create.network
CREATE TABLE IF NOT EXISTS `network` (
  `NetworkID` int(11) NOT NULL AUTO_INCREMENT,
  `DefaultClientID` int(11) DEFAULT NULL,
  `CreditPrice` decimal(10,2) DEFAULT '0.00',
  `CreditLevel` int(11) DEFAULT '0',
  `ReorderLevel` int(11) DEFAULT '0',
  `CreditLevelMailSentOn` date DEFAULT NULL,
  `InvoiceCompanyName` varchar(30) DEFAULT NULL,
  `VATRateID` int(11) DEFAULT NULL,
  `VATNumber` varchar(15) DEFAULT NULL,
  `SageReferralNominalCode` int(11) DEFAULT NULL,
  `SageLabourNominalCode` int(11) DEFAULT NULL,
  `SagePartsNominalCode` int(11) DEFAULT NULL,
  `SageCarriageNominalCode` int(11) DEFAULT NULL,
  `CompanyName` varchar(50) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `Terms` text,
  `CustomerAssurancePolicy` text,
  `TermsConditions` text,
  `Skin` varchar(20) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ServiceManagerForename` varchar(50) DEFAULT NULL,
  `ServiceManagerSurname` varchar(50) DEFAULT NULL,
  `ServiceManagerEmail` varchar(100) DEFAULT NULL,
  `AdminSupervisorForename` varchar(50) DEFAULT NULL,
  `AdminSupervisorSurname` varchar(50) DEFAULT NULL,
  `AdminSupervisorEmail` varchar(100) DEFAULT NULL,
  `NetworkManagerForename` varchar(50) DEFAULT NULL,
  `NetworkManagerSurname` varchar(50) DEFAULT NULL,
  `NetworkManagerEmail` varchar(100) DEFAULT NULL,
  `SendASCReport` enum('Yes','No') DEFAULT NULL,
  `SendNetworkReport` enum('Yes','No') DEFAULT NULL,
  PRIMARY KEY (`NetworkID`),
  KEY `IDX_network_1` (`CompanyName`),
  KEY `IDX_network_CountryID_FK` (`CountryID`),
  KEY `IDX_network_CountyID_FK` (`CountyID`),
  KEY `IDX_network_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_network_DefaultClientID_FK` (`DefaultClientID`),
  CONSTRAINT `client_TO_network` FOREIGN KEY (`DefaultClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_network` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_network` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `user_TO_network` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.network: ~0 rows (approximately)
/*!40000 ALTER TABLE `network` DISABLE KEYS */;
/*!40000 ALTER TABLE `network` ENABLE KEYS */;


-- Dumping structure for function create.NETWORKDAYS
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `NETWORKDAYS`(first_date DATETIME, second_date DATETIME, day_start_time TIME, day_close_time TIME) RETURNS int(11)
    DETERMINISTIC
BEGIN

  DECLARE start_date DATETIME;
  DECLARE end_date DATETIME;
  DECLARE start_time TIME;
  DECLARE end_time TIME;
  DECLARE daily_start_time TIME;
  DECLARE daily_close_time TIME;
  DECLARE firstday TIME;
  DECLARE lastday TIME;
  DECLARE daydiff INT;
  DECLARE partdays float;
  DECLARE dayminutes INT;
  DECLARE dayhours float;

  IF (first_date is null or second_date is null) THEN
    RETURN 0;
  END IF;

  -- SET dayseconds = 24*60*60;

  IF (first_date < second_date) THEN
    SET start_date = first_date;
    SET end_date = second_date;
  ELSE
    SET start_date = second_date;
    SET end_date = first_date;
  END IF;

  -- Check if start_date and end_date are the same day

    IF (DATE(start_date) = DATE(end_date)) THEN
    RETURN TIME_TO_SEC(TIMEDIFF(TIME(end_date),TIME(start_date))) / 3600;
  END IF;

  -- Calculate the length of a working day in seconds

  IF (day_start_time is null) THEN
    SET daily_start_time = '09:00:00';
  ELSE
    SET daily_start_time = day_start_time;
  END IF;

  IF (day_close_time is null) THEN
    SET daily_close_time = '17:00:00';
  ELSE
    SET daily_close_time = day_close_time;
  END IF;

  SET dayminutes = TIME_TO_SEC(TIMEDIFF(daily_close_time, daily_start_time)) / 60 ;
  SET dayhours = dayminutes / 60;

  -- Calculate Difference in days

  SET daydiff = DATEDIFF(end_date, start_date);

  -- Calculate part days at beginning and end of event
 
  SET start_time = TIME(start_date);
  IF ( start_time > daily_close_time) THEN
    SET start_time = daily_close_time;
  END IF;
  IF ( start_time < daily_start_time) THEN
    SET start_time = daily_start_time;
  END IF;

  SET end_time = TIME(end_date);
  IF ( end_time < daily_start_time) THEN
    SET end_time = daily_start_time;
  END IF;
  IF ( end_time > daily_close_time) THEN
    SET end_time = daily_close_time;
  END IF;

  SET firstday = TIMEDIFF(daily_close_time, start_time);
  SET lastday = TIMEDIFF(end_time, daily_start_time);
  SET partdays = ((skyline_alphaHOUR(firstday) * 60) + MINUTE(firstday) + (HOUR(lastday) * 60) + MINUTE(lastday)) / dayminutes;
 
  RETURN (((daydiff - 1) - (FLOOR(daydiff / 7) * 2)) * 24) + (partdays * dayhours);

END//
DELIMITER ;


-- Dumping structure for table create.network_client
CREATE TABLE IF NOT EXISTS `network_client` (
  `NetworkClientID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientID` int(11) NOT NULL,
  `NetworkID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkClientID`),
  KEY `IDX_network_client_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_client_ClientID_FK` (`ClientID`),
  KEY `IDX_network_client_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_network_client` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_network_client` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_network_client` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.network_client: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_client` ENABLE KEYS */;


-- Dumping structure for table create.network_manufacturer
CREATE TABLE IF NOT EXISTS `network_manufacturer` (
  `NetworkManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `CostCodeAccountNumber` varchar(40) DEFAULT NULL,
  `AccountNumber` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkManufacturerID`),
  UNIQUE KEY `TUC_network_manufacturer_1` (`NetworkID`,`ManufacturerID`),
  KEY `IDX_network_manufacturer_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_network_manufacturer_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_manufacturer_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `manufacturer_TO_network_manufacturer` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_network_manufacturer` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `user_TO_network_manufacturer` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.network_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_manufacturer` ENABLE KEYS */;


-- Dumping structure for table create.network_service_provider
CREATE TABLE IF NOT EXISTS `network_service_provider` (
  `NetworkServiceProviderID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `AccountNo` varchar(20) DEFAULT NULL,
  `SageSalesAccountNo` varchar(40) DEFAULT NULL,
  `SagePurchaseAccountNo` varchar(40) DEFAULT NULL,
  `TrackingUsername` varchar(40) DEFAULT NULL,
  `TrackingPassword` varchar(40) DEFAULT NULL,
  `WarrantyUsername` varchar(40) DEFAULT NULL,
  `WarrantyPassword` varchar(40) DEFAULT NULL,
  `WarrantyAccountNumber` varchar(40) DEFAULT NULL,
  `Area` varchar(40) DEFAULT NULL,
  `WalkinJobsUpload` tinyint(1) DEFAULT NULL,
  `CompanyCode` varchar(255) DEFAULT NULL,
  `SamsungDownloadEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetworkServiceProviderID`),
  UNIQUE KEY `TUC_network_service_provider_1` (`NetworkID`,`ServiceProviderID`),
  KEY `IDX_network_service_provider_NetworkID_FK` (`NetworkID`),
  KEY `IDX_network_service_provider_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_network_service_provider_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `network_TO_network_service_provider` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `service_provider_TO_network_service_provider` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_network_service_provider` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.network_service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `network_service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `network_service_provider` ENABLE KEYS */;


-- Dumping structure for table create.non_skyline_job
CREATE TABLE IF NOT EXISTS `non_skyline_job` (
  `NonSkylineJobID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `AppointmentID` int(11) DEFAULT NULL,
  `ServiceProviderJobNo` int(11) DEFAULT NULL,
  `NetworkRefNo` varchar(22) DEFAULT NULL,
  `CustHomeTelNo` varchar(50) DEFAULT NULL,
  `CustWorkTelNo` varchar(50) DEFAULT NULL,
  `CustMobileNo` varchar(50) DEFAULT NULL,
  `CollContactNumber` varchar(50) DEFAULT NULL,
  `CustomerSurname` varchar(40) DEFAULT NULL,
  `CustomerTitle` varchar(10) DEFAULT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  `JobType` varchar(50) DEFAULT NULL,
  `JobSourceID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceType` varchar(50) DEFAULT NULL,
  `CustomerAddress1` varchar(50) DEFAULT NULL,
  `CustomerAddress2` varchar(50) DEFAULT NULL,
  `CustomerAddress3` varchar(50) DEFAULT NULL,
  `CustomerAddress4` varchar(50) DEFAULT NULL,
  `ProductType` varchar(50) DEFAULT NULL,
  `Manufacturer` varchar(50) DEFAULT NULL,
  `ModelNumber` varchar(50) DEFAULT NULL,
  `Client` varchar(50) DEFAULT NULL,
  `ReportedFault` varchar(2000) DEFAULT NULL,
  `CustomerContactNumber` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`NonSkylineJobID`),
  KEY `IDX_non_skyline_job_1` (`ServiceProviderID`),
  KEY `IDX_non_skyline_job_2` (`AppointmentID`),
  KEY `IDX_non_skyline_job_3` (`ServiceProviderJobNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.non_skyline_job: ~0 rows (approximately)
/*!40000 ALTER TABLE `non_skyline_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `non_skyline_job` ENABLE KEYS */;


-- Dumping structure for table create.part
CREATE TABLE IF NOT EXISTS `part` (
  `PartID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `PartNo` varchar(25) DEFAULT NULL,
  `PartDescription` varchar(30) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Supplier` varchar(100) DEFAULT NULL,
  `SectionCode` char(4) DEFAULT NULL,
  `DefectCode` char(4) DEFAULT NULL,
  `RepairCode` char(4) DEFAULT NULL,
  `IsMajorFault` tinyint(1) DEFAULT NULL,
  `CircuitReference` varchar(10) DEFAULT NULL,
  `PartSerialNo` varchar(50) DEFAULT NULL,
  `OrderNo` int(11) DEFAULT NULL,
  `OrderStatus` varchar(30) DEFAULT NULL,
  `OrderDate` date DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL,
  `InvoiceNo` varchar(30) DEFAULT NULL,
  `UnitCost` decimal(10,2) DEFAULT '0.00',
  `SaleCost` decimal(10,2) DEFAULT '0.00',
  `VATRate` decimal(4,2) DEFAULT NULL,
  `SBPartID` int(11) DEFAULT NULL,
  `PartStatus` varchar(30) DEFAULT NULL,
  `OldPartSerialNo` varchar(20) DEFAULT NULL,
  `IsOtherCost` char(1) DEFAULT NULL,
  `IsAdjustment` char(1) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `SupplierOrderNo` varchar(10) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PartID`),
  KEY `IDX_part_JobID_FK` (`JobID`),
  CONSTRAINT `job_TO_part` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.part: ~0 rows (approximately)
/*!40000 ALTER TABLE `part` DISABLE KEYS */;
/*!40000 ALTER TABLE `part` ENABLE KEYS */;


-- Dumping structure for table create.payment_type
CREATE TABLE IF NOT EXISTS `payment_type` (
  `PaymentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `TypeCode` int(11) DEFAULT NULL,
  `SystemCode` varchar(255) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PaymentTypeID`),
  KEY `IDX_payment_type_BrandID_FK` (`BrandID`),
  KEY `IDX_payment_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_payment_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_payment_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Payment Type details.';

-- Dumping data for table create.payment_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_type` ENABLE KEYS */;


-- Dumping structure for table create.permission
CREATE TABLE IF NOT EXISTS `permission` (
  `PermissionID` int(11) NOT NULL,
  `Name` varchar(40) DEFAULT NULL,
  `Description` text,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `URLSegment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PermissionID`),
  KEY `IDX_permission_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_permission` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` (`PermissionID`, `Name`, `Description`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `URLSegment`) VALUES
	(7000, 'tes', 'tes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-02-06 09:51:30', NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;


-- Dumping structure for table create.postcode
CREATE TABLE IF NOT EXISTS `postcode` (
  `Postcode` varchar(4) NOT NULL,
  `CountyCode` int(11) DEFAULT NULL,
  PRIMARY KEY (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.postcode: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode` ENABLE KEYS */;


-- Dumping structure for table create.postcode_allocation
CREATE TABLE IF NOT EXISTS `postcode_allocation` (
  `PostcodeAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `OutOfArea` enum('No','Yes') NOT NULL DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostcodeAllocationID`),
  KEY `IDX_postcode_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_postcode_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_postcode_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_postcode_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_postcode_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_postcode_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_postcode_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_postcode_allocation_CountryID_FK` (`CountryID`),
  KEY `IDX_postcode_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  CONSTRAINT `client_TO_postcode_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_postcode_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `job_type_TO_postcode_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_postcode_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_postcode_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_postcode_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_postcode_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_postcode_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `user_TO_postcode_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.postcode_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_allocation` ENABLE KEYS */;


-- Dumping structure for table create.postcode_allocation_data
CREATE TABLE IF NOT EXISTS `postcode_allocation_data` (
  `PostcodeAllocationID` int(11) NOT NULL,
  `PostcodeArea` varchar(15) NOT NULL,
  KEY `IDX_postcode_allocation_data_1` (`PostcodeArea`),
  KEY `IDX_postcode_allocation_data_PostcodeAllocationID_FK` (`PostcodeAllocationID`),
  CONSTRAINT `postcode_allocation_TO_postcode_allocation_data` FOREIGN KEY (`PostcodeAllocationID`) REFERENCES `postcode_allocation` (`PostcodeAllocationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.postcode_allocation_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_allocation_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_allocation_data` ENABLE KEYS */;


-- Dumping structure for table create.postcode_lat_long
CREATE TABLE IF NOT EXISTS `postcode_lat_long` (
  `PostCode` varchar(10) NOT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  PRIMARY KEY (`PostCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.postcode_lat_long: ~0 rows (approximately)
/*!40000 ALTER TABLE `postcode_lat_long` DISABLE KEYS */;
/*!40000 ALTER TABLE `postcode_lat_long` ENABLE KEYS */;


-- Dumping structure for table create.preferential_clients_manufacturers
CREATE TABLE IF NOT EXISTS `preferential_clients_manufacturers` (
  `UserID` int(11) NOT NULL,
  `PreferentialID` int(11) NOT NULL,
  `PageName` varchar(40) NOT NULL,
  `PreferentialType` enum('Manufacturer','Brand') NOT NULL,
  `Priority` int(11) DEFAULT NULL,
  UNIQUE KEY `UserID_2` (`UserID`,`PreferentialID`,`PageName`,`PreferentialType`),
  KEY `UserID` (`UserID`,`PageName`,`PreferentialType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.preferential_clients_manufacturers: ~0 rows (approximately)
/*!40000 ALTER TABLE `preferential_clients_manufacturers` DISABLE KEYS */;
/*!40000 ALTER TABLE `preferential_clients_manufacturers` ENABLE KEYS */;


-- Dumping structure for table create.primary_fields
CREATE TABLE IF NOT EXISTS `primary_fields` (
  `primaryFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `primaryFieldName` varchar(250) NOT NULL,
  PRIMARY KEY (`primaryFieldID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.primary_fields: ~0 rows (approximately)
/*!40000 ALTER TABLE `primary_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `primary_fields` ENABLE KEYS */;


-- Dumping structure for table create.product
CREATE TABLE IF NOT EXISTS `product` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `ModelID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `NetworkID` int(11) DEFAULT NULL,
  `ProductNo` varchar(10) DEFAULT NULL,
  `ActualSellingPrice` decimal(10,2) DEFAULT '0.00',
  `AuthorityLimit` decimal(10,2) DEFAULT '0.00',
  `PaymentRoute` enum('Claim Back','Invoice') DEFAULT NULL,
  `Supplier` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`),
  KEY `IDX_product_ModelID_FK` (`ModelID`),
  KEY `IDX_product_ClientID_FK` (`ClientID`),
  KEY `IDX_product_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_product_NetworkID_FK` (`NetworkID`),
  KEY `IDX_product_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_product` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `model_TO_product` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
  CONSTRAINT `network_TO_product` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `unit_type_TO_product` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_product` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.product: ~0 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


-- Dumping structure for table create.ra_history
CREATE TABLE IF NOT EXISTS `ra_history` (
  `RAHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `RAStatusID` int(11) NOT NULL,
  `Notes` varchar(1000) DEFAULT NULL,
  `CreatedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) NOT NULL,
  `RAType` enum('repair','return','estimate','invoice','on hold') DEFAULT NULL,
  PRIMARY KEY (`RAHistoryID`),
  KEY `IDX_ra_history_JobID_FK` (`JobID`),
  KEY `IDX_ra_history_RAStatusID_FK` (`RAStatusID`),
  KEY `IDX_ra_history_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `job_TO_ra_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `ra_status_TO_ra_history_2` FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`),
  CONSTRAINT `user_TO_ra_history` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.ra_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_history` ENABLE KEYS */;


-- Dumping structure for table create.ra_status
CREATE TABLE IF NOT EXISTS `ra_status` (
  `RAStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `ListOrder` int(11) NOT NULL,
  `BrandID` int(11) NOT NULL,
  `Status` enum('Active','In-active') DEFAULT NULL,
  `CreatedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `RAStatusName` varchar(40) DEFAULT NULL,
  `RAStatusCode` int(11) NOT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`RAStatusID`),
  KEY `IDX_ra_status_BrandID_FK` (`BrandID`),
  KEY `IDX_ra_status_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `brand_TO_ra_status` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `user_TO_ra_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.ra_status: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_status` ENABLE KEYS */;


-- Dumping structure for table create.ra_status_subset
CREATE TABLE IF NOT EXISTS `ra_status_subset` (
  `ParentRAStatusID` int(11) NOT NULL,
  `ChildRAStatusID` int(11) NOT NULL,
  UNIQUE KEY `IDX_ra_status_subset_1` (`ParentRAStatusID`,`ChildRAStatusID`),
  CONSTRAINT `ra_status_TO_ra_status_subset` FOREIGN KEY (`ParentRAStatusID`) REFERENCES `ra_status` (`RAStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.ra_status_subset: ~0 rows (approximately)
/*!40000 ALTER TABLE `ra_status_subset` DISABLE KEYS */;
/*!40000 ALTER TABLE `ra_status_subset` ENABLE KEYS */;


-- Dumping structure for table create.repair_skill
CREATE TABLE IF NOT EXISTS `repair_skill` (
  `RepairSkillID` int(11) NOT NULL AUTO_INCREMENT,
  `RepairSkillName` varchar(40) DEFAULT NULL,
  `RepairSkillCode` enum('ConsumerElectronics','DomesticAppliance') DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RepairSkillID`),
  KEY `IDX_repair_skill_1` (`RepairSkillName`),
  KEY `IDX_repair_skill_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_repair_skill` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.repair_skill: ~0 rows (approximately)
/*!40000 ALTER TABLE `repair_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `repair_skill` ENABLE KEYS */;


-- Dumping structure for table create.role
CREATE TABLE IF NOT EXISTS `role` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `Parent` int(11) DEFAULT '0',
  `Name` varchar(40) NOT NULL,
  `UserType` enum('Service Network','Client','Branch','Service Provider','Manufacturer','Extended Warrantor') NOT NULL,
  `Comment` text,
  `InactivityTimeout` int(11) NOT NULL DEFAULT '24' COMMENT 'Inactivity Timeout for logged in users in minutes',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`RoleID`),
  KEY `IDX_role_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_role` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Dumping structure for table create.security_question
CREATE TABLE IF NOT EXISTS `security_question` (
  `SecurityQuestionID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Question` varchar(255) NOT NULL,
  `QuestionCode` int(11) NOT NULL,
  `SystemCode` varchar(255) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`SecurityQuestionID`),
  KEY `IDX_security_question_BrandID_FK` (`BrandID`),
  CONSTRAINT `brand_TO_security_question` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Security Question details.';

-- Dumping data for table create.security_question: ~0 rows (approximately)
/*!40000 ALTER TABLE `security_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `security_question` ENABLE KEYS */;


-- Dumping structure for table create.service_provider
CREATE TABLE IF NOT EXISTS `service_provider` (
  `ServiceProviderID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(50) DEFAULT NULL,
  `BuildingNameNumber` varchar(40) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Street` varchar(40) DEFAULT NULL,
  `LocalArea` varchar(40) DEFAULT NULL,
  `TownCity` varchar(60) DEFAULT NULL,
  `CountyID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `WeekdaysOpenTime` time DEFAULT NULL,
  `WeekdaysCloseTime` time DEFAULT NULL,
  `SaturdayOpenTime` time DEFAULT NULL,
  `SaturdayCloseTime` time DEFAULT NULL,
  `ServiceProvided` text,
  `SynopsisOfBusiness` text,
  `ContactPhoneExt` varchar(10) DEFAULT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `AdminSupervisor` varchar(40) DEFAULT NULL,
  `ServiceManager` varchar(40) DEFAULT NULL,
  `Accounts` varchar(40) DEFAULT NULL,
  `IPAddress` varchar(40) DEFAULT NULL,
  `Port` int(11) DEFAULT NULL,
  `InvoiceToCompanyName` varchar(40) DEFAULT NULL,
  `UseAddressProgram` enum('Yes','No') DEFAULT NULL,
  `ServiceBaseVersion` varchar(255) DEFAULT NULL,
  `ServiceBaseLicence` varchar(255) DEFAULT NULL,
  `VATRateID` varchar(40) DEFAULT NULL,
  `VATNumber` varchar(40) DEFAULT NULL,
  `Platform` enum('ServiceBase','API','Skyline','RMA Tracker') DEFAULT NULL,
  `Skin` varchar(20) DEFAULT NULL,
  `ReplyEmail` varchar(64) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ViamenteKey` varchar(50) DEFAULT NULL,
  `DiaryStatus` enum('Active','In-Active') NOT NULL DEFAULT 'In-Active',
  `OnlineDiary` enum('Active','In-Active') DEFAULT 'In-Active',
  `DefaultTravelTime` int(11) NOT NULL DEFAULT '120',
  `DefaultTravelSpeed` int(11) NOT NULL DEFAULT '120',
  `AutoChangeTimeSlotLabel` enum('Yes','No') NOT NULL DEFAULT 'No',
  `CourierStatus` enum('Active','In-active') NOT NULL DEFAULT 'In-active',
  `RunViamenteToday` enum('Yes','No') DEFAULT 'No',
  `DiaryAdministratorEmail` varchar(255) DEFAULT NULL,
  `SetupUniqueTimeSlotPostcodes` enum('No','Yes') NOT NULL DEFAULT 'No',
  `UnlockingPassword` varchar(64) DEFAULT NULL,
  `PasswordProtectNextDayBookings` enum('Yes','No') NOT NULL DEFAULT 'No',
  `PasswordProtectNextDayBookingsTime` time DEFAULT NULL,
  `AutoSelectDay` enum('','Today','NextDay') DEFAULT '',
  `AutoDisplayTable` enum('','Summary','Appointment') DEFAULT '',
  `AutoSpecifyEngineerByPostcode` enum('Yes','No') DEFAULT 'No',
  `ServiceManagerForename` varchar(50) DEFAULT NULL,
  `ServiceManagerSurname` varchar(50) DEFAULT NULL,
  `ServiceManagerEmail` varchar(100) DEFAULT NULL,
  `AdminSupervisorForename` varchar(50) DEFAULT NULL,
  `AdminSupervisorSurname` varchar(50) DEFAULT NULL,
  `AdminSupervisorEmail` varchar(100) DEFAULT NULL,
  `SendASCReport` enum('Yes','No') DEFAULT NULL,
  `EngineerDefaultStartTime` time DEFAULT NULL,
  `EngineerDefaultEndTime` time DEFAULT NULL,
  `NumberOfVehicles` int(11) DEFAULT NULL,
  `NumberOfWaypoints` int(11) DEFAULT NULL,
  `DiaryAllocationType` enum('Postcode','GridMapping') NOT NULL DEFAULT 'Postcode',
  `SendRouteMapsEmail` time DEFAULT NULL,
  `Multimaps` int(2) DEFAULT '6',
  `EmailServiceInstruction` enum('Active','In-Active') DEFAULT 'In-Active',
  PRIMARY KEY (`ServiceProviderID`),
  KEY `IDX_service_provider_1` (`CompanyName`),
  KEY `IDX_service_provider_2` (`DiaryStatus`),
  KEY `IDX_service_provider_CountyID_FK` (`CountyID`),
  KEY `IDX_service_provider_CountryID_FK` (`CountryID`),
  KEY `IDX_service_provider_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `country_TO_service_provider` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_service_provider` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `user_TO_service_provider` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_contacts
CREATE TABLE IF NOT EXISTS `service_provider_contacts` (
  `ServiceProviderID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `ContactPhone` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ServiceProviderID`,`ClientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_contacts` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_engineer
CREATE TABLE IF NOT EXISTS `service_provider_engineer` (
  `ServiceProviderEngineerID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `EngineerFirstName` varchar(40) DEFAULT NULL,
  `EngineerLastName` varchar(40) DEFAULT NULL,
  `ServiceBaseUserCode` varchar(4) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `MobileNumber` varchar(20) DEFAULT NULL,
  `RouteColour` varchar(20) DEFAULT NULL,
  `StartShift` time DEFAULT NULL,
  `EndShift` time DEFAULT NULL,
  `LunchBreakDuration` tinyint(3) DEFAULT NULL,
  `LunchPeriodFrom` time DEFAULT NULL,
  `LunchPeriodTo` time DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `StartPostcode` varchar(8) DEFAULT NULL,
  `EndPostcode` varchar(8) DEFAULT NULL,
  `StartLocation` enum('Home','Office','Other') DEFAULT NULL,
  `EndLocation` enum('Home','Office','Other') DEFAULT NULL,
  `StartHomePostcode` varchar(100) DEFAULT NULL COMMENT 'Used as main start pc',
  `StartOfficePostcode` varchar(10) DEFAULT NULL,
  `StartOtherPostcode` varchar(10) DEFAULT NULL,
  `EndHomePostcode` varchar(100) DEFAULT NULL COMMENT 'Used as main end pc',
  `EndOfficePostcode` varchar(10) DEFAULT NULL,
  `EndOtherPostcode` varchar(10) DEFAULT NULL,
  `MondayActive` enum('yes','no') DEFAULT 'no',
  `MondayStartShift` time DEFAULT NULL,
  `MondayEndShift` time DEFAULT NULL,
  `MondayStartPostcode` varchar(8) DEFAULT NULL,
  `MondayEndPostcode` varchar(8) DEFAULT NULL,
  `TuesdayActive` enum('yes','no') DEFAULT 'no',
  `TuesdayStartShift` time DEFAULT NULL,
  `TuesdayEndShift` time DEFAULT NULL,
  `TuesdayStartPostcode` varchar(8) DEFAULT NULL,
  `TuesdayEndPostcode` varchar(8) DEFAULT NULL,
  `WednesdayActive` enum('yes','no') DEFAULT 'no',
  `WednesdayStartShift` time DEFAULT NULL,
  `WednesdayEndShift` time DEFAULT NULL,
  `WednesdayStartPostcode` varchar(8) DEFAULT NULL,
  `WednesdayEndPostcode` varchar(8) DEFAULT NULL,
  `ThursdayActive` enum('yes','no') DEFAULT 'no',
  `ThursdayStartShift` time DEFAULT NULL,
  `ThursdayEndShift` time DEFAULT NULL,
  `ThursdayStartPostcode` varchar(8) DEFAULT NULL,
  `ThursdayEndPostcode` varchar(8) DEFAULT NULL,
  `FridayActive` enum('yes','no') DEFAULT 'no',
  `FridayStartShift` time DEFAULT NULL,
  `FridayEndShift` time DEFAULT NULL,
  `FridayStartPostcode` varchar(8) DEFAULT NULL,
  `FridayEndPostcode` varchar(8) DEFAULT NULL,
  `SaturdayActive` enum('yes','no') DEFAULT 'no',
  `SaturdayStartShift` time DEFAULT NULL,
  `SaturdayEndShift` time DEFAULT NULL,
  `SaturdayStartPostcode` varchar(8) DEFAULT NULL,
  `SaturdayEndPostcode` varchar(8) DEFAULT NULL,
  `SundayActive` enum('yes','no') DEFAULT 'no',
  `SundayStartShift` time DEFAULT NULL,
  `SundayEndShift` time DEFAULT NULL,
  `SundayStartPostcode` varchar(8) DEFAULT NULL,
  `SundayEndPostcode` varchar(8) DEFAULT NULL,
  `SentToViamente` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `Deleted` enum('No','Yes') NOT NULL DEFAULT 'No',
  `SpeedFactor` int(11) NOT NULL DEFAULT '100',
  `EngineerImportance` int(11) NOT NULL DEFAULT '50',
  `ViamenteExclude` enum('Yes','No') NOT NULL DEFAULT 'No',
  `SetupType` enum('Unique','Replicate') DEFAULT 'Unique',
  `ReplicateType` enum('FourWeeksOnly','WeeklyCycle','MonthlyCycle') DEFAULT 'FourWeeksOnly',
  `ReplicateStatusFlag` enum('No','Yes') DEFAULT 'No',
  `ReplicatePostcodeAllocation` enum('Yes','No') DEFAULT 'Yes',
  PRIMARY KEY (`ServiceProviderEngineerID`),
  KEY `IDX_service_provider_engineer_1` (`ServiceProviderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_engineer: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_engineer_details
CREATE TABLE IF NOT EXISTS `service_provider_engineer_details` (
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerID` int(11) NOT NULL,
  `WorkDate` date NOT NULL,
  `StartShift` time NOT NULL,
  `EndShift` time NOT NULL,
  `StartPostcode` varchar(100) NOT NULL,
  `EndPostcode` varchar(100) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`ServiceProviderEngineerDetailsID`),
  KEY `ServiceProviderEngineerID` (`ServiceProviderEngineerID`,`WorkDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_engineer_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_details` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_engineer_skillset
CREATE TABLE IF NOT EXISTS `service_provider_engineer_skillset` (
  `ServiceProviderEngineerSkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderSkillsetID` int(11) DEFAULT NULL,
  `ServiceProviderEngineerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ServiceProviderEngineerSkillsetID`),
  KEY `IDX_service_provider_engineer_skillset_1` (`ServiceProviderSkillsetID`),
  KEY `IDX_service_provider_engineer_skillset_2` (`ServiceProviderEngineerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_engineer_skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_skillset` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_skillset` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_engineer_skillset_day
CREATE TABLE IF NOT EXISTS `service_provider_engineer_skillset_day` (
  `ServiceProviderEngineerSkillsetDayID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderEngineerDetailsID` int(11) NOT NULL,
  `ServiceProviderSkillsetID` int(11) NOT NULL,
  PRIMARY KEY (`ServiceProviderEngineerSkillsetDayID`),
  UNIQUE KEY `IDX_service_provider_engineer_skillset_day_1` (`ServiceProviderEngineerDetailsID`,`ServiceProviderSkillsetID`),
  KEY `ServiceProviderEngineerDetailsID` (`ServiceProviderEngineerDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_engineer_skillset_day: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_skillset_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_skillset_day` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_engineer_workload
CREATE TABLE IF NOT EXISTS `service_provider_engineer_workload` (
  `ServiceProviderEngineerWorkloadID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) NOT NULL,
  `ServiceProviderEngineerID` int(11) NOT NULL,
  `WorkingDay` date NOT NULL,
  `TotalWorkTimeSec` int(11) NOT NULL,
  `TotalIdleTimeSec` int(11) NOT NULL,
  `TotalServiceTimeSec` int(11) NOT NULL,
  `TotalDriveTimeSec` int(11) NOT NULL,
  `TotalSteps` int(11) NOT NULL,
  PRIMARY KEY (`ServiceProviderEngineerWorkloadID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_engineer_workload: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_engineer_workload` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_engineer_workload` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_geotags
CREATE TABLE IF NOT EXISTS `service_provider_geotags` (
  `ServiceProviderGeoTagsID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) NOT NULL,
  `Postcode` varchar(8) DEFAULT NULL,
  `Latitude` float(6,4) NOT NULL,
  `Longitude` float(7,4) NOT NULL,
  PRIMARY KEY (`ServiceProviderGeoTagsID`),
  KEY `ServiceProviderID` (`ServiceProviderID`),
  KEY `Postcode` (`Postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_geotags: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_geotags` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_geotags` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_skillset
CREATE TABLE IF NOT EXISTS `service_provider_skillset` (
  `ServiceProviderSkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `SkillsetID` int(11) DEFAULT NULL,
  `ServiceDuration` smallint(4) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `EngineersRequired` tinyint(2) DEFAULT '1',
  `Status` enum('Active','In-active') DEFAULT 'Active',
  PRIMARY KEY (`ServiceProviderSkillsetID`),
  KEY `IDX_service_provider_skillset_1` (`ServiceProviderID`),
  KEY `IDX_service_provider_skillset_2` (`SkillsetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_skillset` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_skillset` ENABLE KEYS */;


-- Dumping structure for table create.service_provider_trade_account
CREATE TABLE IF NOT EXISTS `service_provider_trade_account` (
  `ServiceProviderTradeAccountID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `TradeAccount` varchar(40) DEFAULT NULL,
  `Postcode` varchar(10) DEFAULT NULL,
  `Monday` enum('Yes','No') DEFAULT 'No',
  `Tuesday` enum('Yes','No') DEFAULT 'No',
  `Wednesday` enum('Yes','No') DEFAULT 'No',
  `Thursday` enum('Yes','No') DEFAULT 'No',
  `Friday` enum('Yes','No') DEFAULT 'No',
  `Saturday` enum('Yes','No') DEFAULT 'No',
  `Sunday` enum('Yes','No') DEFAULT 'No',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceProviderTradeAccountID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_provider_trade_account: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_provider_trade_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_provider_trade_account` ENABLE KEYS */;


-- Dumping structure for table create.service_type
CREATE TABLE IF NOT EXISTS `service_type` (
  `ServiceTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeName` varchar(40) DEFAULT NULL,
  `ServiceTypeCode` int(11) DEFAULT NULL,
  `Code` varchar(64) DEFAULT NULL,
  `Priority` int(11) DEFAULT NULL,
  `Chargeable` enum('Y','N') NOT NULL DEFAULT 'N',
  `Warranty` enum('Y','N') NOT NULL DEFAULT 'N',
  `DateOfPurchaseForced` enum('N','B','C') NOT NULL DEFAULT 'N',
  `OverrideTradeSettingsPolicy` enum('Y','N') NOT NULL DEFAULT 'N',
  `ForcePolicyNoAtBooking` enum('Y','N') NOT NULL DEFAULT 'N',
  `OverrideTradeSettingsModel` enum('Y','N') NOT NULL DEFAULT 'N',
  `ForceModelNumber` enum('Y','N') NOT NULL DEFAULT 'N',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceTypeID`),
  KEY `IDX_service_type_1` (`ServiceTypeName`),
  KEY `IDX_service_type_BrandID_FK` (`BrandID`),
  KEY `IDX_service_type_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_service_type_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_Warranty` (`Warranty`),
  CONSTRAINT `brand_TO_service_type` FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `job_type_TO_service_type` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `user_TO_service_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_type` ENABLE KEYS */;


-- Dumping structure for table create.service_type_alias
CREATE TABLE IF NOT EXISTS `service_type_alias` (
  `ServiceTypeAliasID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) NOT NULL,
  `ServiceTypeID` int(11) NOT NULL,
  `ServiceTypeMask` int(11) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ServiceTypeAliasID`),
  UNIQUE KEY `TUC_service_type_alias_1` (`ClientID`,`NetworkID`,`ServiceTypeID`),
  KEY `IDX_service_type_alias_ClientID_FK` (`ClientID`),
  KEY `IDX_service_type_alias_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_service_type_alias_NetworkID_FK` (`NetworkID`),
  KEY `IDX_service_type_alias_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_service_type_alias` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `network_TO_service_type_alias` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `service_type_TO_service_type_alias` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `user_TO_service_type_alias` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.service_type_alias: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_type_alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_type_alias` ENABLE KEYS */;


-- Dumping structure for table create.shipping
CREATE TABLE IF NOT EXISTS `shipping` (
  `ShippingID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `CourierID` int(11) DEFAULT NULL,
  `ConsignmentNo` varchar(50) DEFAULT NULL,
  `ConsignmentDate` timestamp NULL DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ShippingID`),
  KEY `job_TO_shipping` (`JobID`),
  KEY `courier_TO_shipping` (`CourierID`),
  CONSTRAINT `courier_TO_shipping` FOREIGN KEY (`CourierID`) REFERENCES `courier` (`CourierID`),
  CONSTRAINT `job_TO_shipping` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.shipping: ~0 rows (approximately)
/*!40000 ALTER TABLE `shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping` ENABLE KEYS */;


-- Dumping structure for table create.skillset
CREATE TABLE IF NOT EXISTS `skillset` (
  `SkillsetID` int(11) NOT NULL AUTO_INCREMENT,
  `SkillsetName` varchar(40) DEFAULT NULL,
  `ServiceDuration` smallint(4) DEFAULT NULL,
  `EngineersRequired` tinyint(2) DEFAULT '1',
  `RepairSkillID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  `AppointmentTypeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SkillsetID`),
  KEY `IDX_skillset_1` (`AppointmentTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.skillset: ~0 rows (approximately)
/*!40000 ALTER TABLE `skillset` DISABLE KEYS */;
/*!40000 ALTER TABLE `skillset` ENABLE KEYS */;


-- Dumping structure for table create.sp_geo_cells
CREATE TABLE IF NOT EXISTS `sp_geo_cells` (
  `SpGeoCellsID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) NOT NULL DEFAULT '0',
  `DiaryAllocationID` int(10) NOT NULL DEFAULT '0',
  `Lat1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lat2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Lng2` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`SpGeoCellsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores service provider geocell information';

-- Dumping data for table create.sp_geo_cells: ~0 rows (approximately)
/*!40000 ALTER TABLE `sp_geo_cells` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_geo_cells` ENABLE KEYS */;


-- Dumping structure for table create.sp_grid_defaults
CREATE TABLE IF NOT EXISTS `sp_grid_defaults` (
  `SpGridDefaultsID` int(10) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(10) DEFAULT NULL,
  `Lat1` decimal(10,2) DEFAULT NULL,
  `Lng1` decimal(10,2) DEFAULT NULL,
  `Lat2` decimal(10,2) DEFAULT NULL,
  `Lng2` decimal(10,2) DEFAULT NULL,
  `CellSize` decimal(10,2) NOT NULL DEFAULT '1.00',
  `UpdatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UserUpdated` varchar(50) NOT NULL,
  PRIMARY KEY (`SpGridDefaultsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.sp_grid_defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `sp_grid_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `sp_grid_defaults` ENABLE KEYS */;


-- Dumping structure for table create.status
CREATE TABLE IF NOT EXISTS `status` (
  `StatusID` int(11) NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(40) DEFAULT NULL,
  `TimelineStatus` enum('booked','viewed','in_progress','parts_ordered','parts_received','site_visit','revisit','closed') DEFAULT NULL,
  `Colour` varchar(10) DEFAULT NULL,
  `Category` enum('Cancelled','Client','Complete','Field Call','In Progress','Invoiced','Parts','System','Waiting For Info') DEFAULT NULL,
  `Branch` tinyint(1) DEFAULT NULL,
  `ServiceProvider` tinyint(1) DEFAULT NULL,
  `FieldCall` tinyint(1) DEFAULT NULL,
  `Support` tinyint(1) DEFAULT NULL,
  `PartsOrders` tinyint(1) DEFAULT NULL,
  `RelatedTable` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`StatusID`),
  KEY `IDX_status_1` (`StatusName`),
  KEY `IDX_status_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `user_TO_status` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.status: ~0 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;


-- Dumping structure for table create.status_history
CREATE TABLE IF NOT EXISTS `status_history` (
  `StatusHistoryID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) DEFAULT NULL,
  `StatusID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `AdditionalInformation` varchar(40) DEFAULT NULL,
  `UserCode` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`StatusHistoryID`),
  KEY `IDX_status_history_StatusID_FK` (`StatusID`),
  KEY `IDX_status_history_JobID_FK` (`JobID`),
  KEY `IDX_status_history_UserID_FK` (`UserID`),
  CONSTRAINT `job_TO_status_history` FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`),
  CONSTRAINT `status_TO_status_history` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`),
  CONSTRAINT `user_TO_status_history` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.status_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_history` ENABLE KEYS */;


-- Dumping structure for table create.status_permission
CREATE TABLE IF NOT EXISTS `status_permission` (
  `StatusPermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `PermissionType` enum('user','role','network','client','brand','branch') NOT NULL,
  `EntityID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  PRIMARY KEY (`StatusPermissionID`),
  KEY `status_permission_TO_status` (`StatusID`),
  CONSTRAINT `status_permission_TO_status` FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.status_permission: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_permission` ENABLE KEYS */;


-- Dumping structure for table create.status_preference
CREATE TABLE IF NOT EXISTS `status_preference` (
  `UserID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `Page` varchar(40) NOT NULL,
  `Type` varchar(2) NOT NULL,
  UNIQUE KEY `IDX_status_preference_1` (`Page`,`UserID`,`Type`,`StatusID`),
  KEY `user_TO_status_preference` (`UserID`),
  CONSTRAINT `user_TO_status_preference` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.status_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `status_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_preference` ENABLE KEYS */;


-- Dumping structure for table create.table_state
CREATE TABLE IF NOT EXISTS `table_state` (
  `UserID` int(10) NOT NULL,
  `TableID` varchar(250) NOT NULL,
  `StateData` text NOT NULL,
  PRIMARY KEY (`UserID`),
  KEY `TableID` (`TableID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.table_state: ~0 rows (approximately)
/*!40000 ALTER TABLE `table_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_state` ENABLE KEYS */;


-- Dumping structure for table create.testimonials
CREATE TABLE IF NOT EXISTS `testimonials` (
  `CustomerID` int(11) NOT NULL,
  `ServiceProviderID` int(11) NOT NULL,
  `TestimonialText` text NOT NULL,
  `TestimonialUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.testimonials: ~0 rows (approximately)
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;


-- Dumping structure for table create.town_allocation
CREATE TABLE IF NOT EXISTS `town_allocation` (
  `TownAllocationID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) NOT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `RepairSkillID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `JobTypeID` int(11) DEFAULT NULL,
  `ServiceTypeID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CountyID` int(11) NOT NULL,
  `Town` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TownAllocationID`),
  KEY `IDX_town_allocation_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_town_allocation_NetworkID_FK` (`NetworkID`),
  KEY `IDX_town_allocation_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_town_allocation_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_town_allocation_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_town_allocation_ClientID_FK` (`ClientID`),
  KEY `IDX_town_allocation_JobTypeID_FK` (`JobTypeID`),
  KEY `IDX_town_allocation_ServiceTypeID_FK` (`ServiceTypeID`),
  KEY `IDX_town_allocation_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_town_allocation_CountryID_FK` (`CountryID`),
  KEY `IDX_town_allocation_CustomerID_FK` (`CustomerID`),
  KEY `IDX_town_allocation_CountyID_FK` (`CountyID`),
  CONSTRAINT `client_TO_town_allocation` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `country_TO_town_allocation` FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`),
  CONSTRAINT `county_TO_town_allocation` FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`),
  CONSTRAINT `customer_TO_town_allocation` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`),
  CONSTRAINT `job_type_TO_town_allocation` FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`),
  CONSTRAINT `manufacturer_TO_town_allocation` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_town_allocation` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `repair_skill_TO_town_allocation` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `service_provider_TO_town_allocation` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `service_type_TO_town_allocation` FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`),
  CONSTRAINT `unit_type_TO_town_allocation` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_town_allocation` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.town_allocation: ~0 rows (approximately)
/*!40000 ALTER TABLE `town_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `town_allocation` ENABLE KEYS */;


-- Dumping structure for table create.unit_client_type
CREATE TABLE IF NOT EXISTS `unit_client_type` (
  `UnitClientTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `UnitTypeID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `UnitTurnaroundTime` int(2) DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitClientTypeID`),
  KEY `IDX_unit_client_type_ClientID_FK` (`ClientID`),
  KEY `IDX_unit_client_type_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_unit_client_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_unit_client_type` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `unit_type_TO_unit_client_type` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_unit_client_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.unit_client_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_client_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_client_type` ENABLE KEYS */;


-- Dumping structure for table create.unit_pricing_structure
CREATE TABLE IF NOT EXISTS `unit_pricing_structure` (
  `UnitPricingStructureID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `UnitTypeID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `CompletionStatusID` int(11) DEFAULT NULL,
  `JobSite` enum('field call','workshop') NOT NULL,
  `ServiceRate` decimal(10,2) DEFAULT '0.00',
  `ReferralFee` decimal(10,2) DEFAULT '0.00',
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitPricingStructureID`),
  KEY `IDX_unit_pricing_structure_NetworkID_FK` (`NetworkID`),
  KEY `IDX_unit_pricing_structure_ClientID_FK` (`ClientID`),
  KEY `IDX_unit_pricing_structure_UnitTypeID_FK` (`UnitTypeID`),
  KEY `IDX_unit_pricing_structure_ManufacturerID_FK` (`ManufacturerID`),
  KEY `IDX_unit_pricing_structure_CompletionStatusID_FK` (`CompletionStatusID`),
  KEY `IDX_unit_pricing_structure_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `client_TO_unit_pricing_structure` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `completion_status_TO_unit_pricing_structure` FOREIGN KEY (`CompletionStatusID`) REFERENCES `completion_status` (`CompletionStatusID`),
  CONSTRAINT `manufacturer_TO_unit_pricing_structure` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `network_TO_unit_pricing_structure` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `unit_type_TO_unit_pricing_structure` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `user_TO_unit_pricing_structure` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.unit_pricing_structure: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_pricing_structure` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_pricing_structure` ENABLE KEYS */;


-- Dumping structure for table create.unit_type
CREATE TABLE IF NOT EXISTS `unit_type` (
  `UnitTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `RepairSkillID` int(11) DEFAULT NULL,
  `UnitTypeName` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitTypeID`),
  UNIQUE KEY `IDX_unit_type_1` (`RepairSkillID`,`UnitTypeName`),
  KEY `IDX_unit_type_RepairSkillID_FK` (`RepairSkillID`),
  KEY `IDX_unit_type_ModifiedUserID_FK` (`ModifiedUserID`),
  CONSTRAINT `repair_skill_TO_unit_type` FOREIGN KEY (`RepairSkillID`) REFERENCES `repair_skill` (`RepairSkillID`),
  CONSTRAINT `user_TO_unit_type` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.unit_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_type` ENABLE KEYS */;


-- Dumping structure for table create.unit_type_manufacturer
CREATE TABLE IF NOT EXISTS `unit_type_manufacturer` (
  `UnitTypeManufacturerID` int(11) NOT NULL AUTO_INCREMENT,
  `UnitTypeID` int(11) NOT NULL,
  `ManufacturerID` int(11) NOT NULL,
  `ProductGroup` varchar(10) DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT NULL,
  `EndDate` timestamp NULL DEFAULT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UnitTypeManufacturerID`),
  KEY `UnitTypeID` (`UnitTypeID`),
  KEY `ManufacturerID` (`ManufacturerID`),
  KEY `FK__user` (`ModifiedUserID`),
  CONSTRAINT `FK__manufacturer` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
  CONSTRAINT `FK__unit_type` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
  CONSTRAINT `FK__user` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.unit_type_manufacturer: ~0 rows (approximately)
/*!40000 ALTER TABLE `unit_type_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit_type_manufacturer` ENABLE KEYS */;


-- Dumping structure for procedure create.UpgradeSchemaVersion
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpgradeSchemaVersion`(p_version varchar(20))
BEGIN
    DECLARE bad_version CONDITION FOR SQLSTATE '99001';
    DECLARE current_version varchar(20);
    SELECT `VersionNo` from `version` order by `Updated` desc limit 0,1 into current_version;
    IF p_version <> current_version THEN
         SIGNAL bad_version
            SET MESSAGE_TEXT='Cannot Upgrade Schema.';
    END IF;
END//
DELIMITER ;


-- Dumping structure for table create.user
CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `NetworkID` int(11) DEFAULT NULL,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `BranchID` int(11) DEFAULT NULL,
  `ManufacturerID` int(11) DEFAULT NULL,
  `ExtendedWarrantorID` int(11) DEFAULT NULL,
  `SecurityQuestionID` int(11) NOT NULL,
  `DefaultBrandID` int(11) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Password` varchar(32) DEFAULT NULL,
  `Answer` varchar(40) DEFAULT NULL,
  `Position` varchar(40) DEFAULT NULL,
  `SuperAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `LastLoggedIn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CustomerTitleID` int(11) DEFAULT NULL,
  `ContactFirstName` varchar(40) DEFAULT NULL,
  `ContactLastName` varchar(40) DEFAULT NULL,
  `ContactHomePhone` varchar(40) DEFAULT NULL,
  `ContactWorkPhoneExt` varchar(10) DEFAULT NULL,
  `ContactWorkPhone` varchar(40) DEFAULT NULL,
  `ContactFax` varchar(40) DEFAULT NULL,
  `ContactMobile` varchar(40) DEFAULT NULL,
  `ContactEmail` varchar(40) DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `BranchJobSearchScope` enum('Branch Only','Brand','Client') DEFAULT 'Client',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GaDial0Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial0Red` int(11) NOT NULL DEFAULT '80',
  `GaDial1Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial1Red` int(11) NOT NULL DEFAULT '80',
  `GaDial2Yellow` int(11) NOT NULL DEFAULT '60',
  `GaDial2Red` int(11) NOT NULL DEFAULT '80',
  `GaDial3Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial3Red` int(11) NOT NULL DEFAULT '800',
  `GaDial3Max` int(11) NOT NULL DEFAULT '1000',
  `GaDial4Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial4Red` int(11) NOT NULL DEFAULT '800',
  `GaDial4Max` int(11) NOT NULL DEFAULT '1000',
  `GaDial5Yellow` int(11) NOT NULL DEFAULT '600',
  `GaDial5Red` int(11) NOT NULL DEFAULT '800',
  `GaDial5Max` int(11) NOT NULL DEFAULT '1000',
  `GaDialBoundaryLower` int(11) NOT NULL DEFAULT '7',
  `GaDialBoundaryUpper` int(11) NOT NULL DEFAULT '7',
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `IDX_user_Username` (`Username`),
  KEY `IDX_user_NetworkID_FK` (`NetworkID`),
  KEY `IDX_user_ServiceProviderID_FK` (`ServiceProviderID`),
  KEY `IDX_user_ClientID_FK` (`ClientID`),
  KEY `IDX_user_BranchID_FK` (`BranchID`),
  KEY `IDX_user_DefaultBrandID_FK` (`DefaultBrandID`),
  KEY `IDX_user_SecurityQuestionID_FK` (`SecurityQuestionID`),
  KEY `IDX_user_ModifiedUserID_FK` (`ModifiedUserID`),
  KEY `IDX_user_CustomerTitleID_FK` (`CustomerTitleID`),
  CONSTRAINT `branch_TO_user` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`),
  CONSTRAINT `brand_TO_user` FOREIGN KEY (`DefaultBrandID`) REFERENCES `brand` (`BrandID`),
  CONSTRAINT `client_TO_user` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`),
  CONSTRAINT `customer_title_TO_user` FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`),
  CONSTRAINT `network_TO_user` FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`),
  CONSTRAINT `security_question_TO_user` FOREIGN KEY (`SecurityQuestionID`) REFERENCES `security_question` (`SecurityQuestionID`),
  CONSTRAINT `service_provider_TO_user` FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`),
  CONSTRAINT `user_TO_user` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Dumping structure for table create.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `UserID` int(11) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`RoleID`),
  KEY `IDX_user_role_UserID_FK` (`UserID`),
  KEY `IDX_user_role_RoleID_FK` (`RoleID`),
  CONSTRAINT `role_TO_user_role` FOREIGN KEY (`RoleID`) REFERENCES `role` (`RoleID`),
  CONSTRAINT `user_TO_user_role` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.user_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;


-- Dumping structure for table create.vat_rate
CREATE TABLE IF NOT EXISTS `vat_rate` (
  `VatRateID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) NOT NULL,
  `VatCode` int(11) NOT NULL,
  `VatRate` double(6,2) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  PRIMARY KEY (`VatRateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table contains Vat Rate details.';

-- Dumping data for table create.vat_rate: ~0 rows (approximately)
/*!40000 ALTER TABLE `vat_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `vat_rate` ENABLE KEYS */;


-- Dumping structure for table create.version
CREATE TABLE IF NOT EXISTS `version` (
  `VersionNo` varchar(10) NOT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`VersionNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.version: ~5 rows (approximately)
/*!40000 ALTER TABLE `version` DISABLE KEYS */;
INSERT INTO `version` (`VersionNo`, `Updated`) VALUES
	('1.190', '2013-02-25 11:02:11');
/*!40000 ALTER TABLE `version` ENABLE KEYS */;


-- Dumping structure for table create.viamente_end_day
CREATE TABLE IF NOT EXISTS `viamente_end_day` (
  `ViamenteEndDayID` int(11) NOT NULL AUTO_INCREMENT,
  `ServiceProviderID` int(11) DEFAULT NULL,
  `RouteDate` date DEFAULT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ViamenteEndDayID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.viamente_end_day: ~0 rows (approximately)
/*!40000 ALTER TABLE `viamente_end_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `viamente_end_day` ENABLE KEYS */;


-- Dumping structure for table create.welcome_messages
CREATE TABLE IF NOT EXISTS `welcome_messages` (
  `WelcomeMessageID` int(11) NOT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `WelcomeMessageDefaultsID` int(11) DEFAULT NULL,
  `JobID` int(11) DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Status` enum('Active','In-active') DEFAULT 'Active',
  PRIMARY KEY (`WelcomeMessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.welcome_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `welcome_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `welcome_messages` ENABLE KEYS */;


-- Dumping structure for table create.welcome_message_defaults
CREATE TABLE IF NOT EXISTS `welcome_message_defaults` (
  `WelcomeMessageDefaultsID` int(11) NOT NULL,
  `Title` text,
  `HyperlinkText` varchar(100) DEFAULT NULL,
  `Message` text,
  `Type` enum('Message','Alert') DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT NULL,
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`WelcomeMessageDefaultsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table create.welcome_message_defaults: ~0 rows (approximately)
/*!40000 ALTER TABLE `welcome_message_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `welcome_message_defaults` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
