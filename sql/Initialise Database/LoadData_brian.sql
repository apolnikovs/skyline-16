CREATE TABLE IF NOT EXISTS import_noise_postcodes (
  Network varchar(40),
  RepairSkill varchar(20),
  MfgName varchar(40),
  JobType varchar(40),
  ServiceType varchar(40),
  OutOfArea varchar(3),
  Postcode varchar(10),
  CompanyName varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/NoisePostcodes.csv' 
INTO TABLE import_noise_postcodes
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_noise_postcodes2 (
  ID INTEGER NOT NULL AUTO_INCREMENT,
  Network varchar(40),
  RepairSkill varchar(20),
  MfgName varchar(40),
  JobType varchar(40),
  ServiceType varchar(40),
  CompanyName varchar(40),
  PRIMARY KEY (ID)
);

insert into import_noise_postcodes2 (Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName)
select distinct Network,RepairSkill, MfgName, JobType, ServiceType, CompanyName from import_noise_postcodes;

CREATE TABLE IF NOT EXISTS import_sony_postcodes (
  Network varchar(40),
  RepairSkill varchar(20),
  MfgName varchar(40),
  JobType varchar(40),
  ServiceType varchar(40),
  OutOfArea varchar(3),
  Postcode varchar(10),
  CompanyName varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/SonyPostcodes.csv' 
INTO TABLE import_sony_postcodes
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_sony_postcodes2 (
  ID INTEGER NOT NULL AUTO_INCREMENT,
  Network varchar(40),
  RepairSkill varchar(20),
  MfgName varchar(40),
  JobType varchar(40),
  ServiceType varchar(40),
  CompanyName varchar(40),
  PRIMARY KEY (ID)
);

insert into import_sony_postcodes2 (Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName)
select distinct Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName from import_sony_postcodes;

CREATE INDEX idx_noise_postcodes2_1 ON import_noise_postcodes2 (Network,RepairSkill,MfgName,JobType,ServiceType,CompanyName);
CREATE INDEX idx_sony_postcodes2_1 ON import_sony_postcodes2 (Network,RepairSkill,MfgName,JobType,ServiceType,CompanyName);

CREATE TABLE IF NOT EXISTS import_branch_users (
Username varchar(20),
PassWord varchar(32),
FullName varchar(40),
Role varchar(40),
Branch varchar(40),
Email varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/BranchUsers.csv' 
INTO TABLE import_branch_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_client_brand_branch (
network varchar(50),
Client varchar(50),
AssignedASC varchar(40),
BrandID int,
Brand varchar(40),
RetailerName varchar(40),
Street varchar(40),
Area varchar(40),
Town varchar(60),
County varchar(40),
Postcode varchar(10),
PhoneNo varchar(40),
FaxNo varchar(40),
Email varchar(40),
Contact varchar(40),
AccountNo varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ClientBrandBranch.csv' 
INTO TABLE import_client_brand_branch
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_client_users (
Username varchar(20),
PassWord varchar(32),
FullName varchar(40),
Role varchar(40),
Retailer varchar(40),
Email varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ClientUsers.csv' 
INTO TABLE import_client_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_manufacturers (
  MfgName varchar(30),
  CostCodeAccountNo varchar(40),
  AccountNumber varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/Manufacturers.csv' 
INTO TABLE import_manufacturers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_providers (
  companyname varchar(50),
  street varchar (40),
  localarea varchar (40),
  towncity varchar (40),
  county varchar (255),
  postalcode varchar (10),
  contactphone varchar (40),
  contactfax varchar (40),
  contactemail varchar (40),
  contactname varchar (50),
  vatnumber varchar (40),
  sageaccountnumber varchar (40),
  sagepiaccount varchar (40),
  accountemailaddress varchar (40),
  invoicetocompanyname varchar (40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ServiceProviders.csv' 
INTO TABLE import_service_providers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_types (
  servicetypename varchar(40),
  code varchar(64),
  jobtype varchar(30)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ServiceTypes.csv' 
INTO TABLE import_service_types
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS import_network_service_providers
(
	wid VARCHAR (50),
	companyname varchar(50),
	accountnumber VARCHAR (40),
	companycode VARCHAR (255),
	trackingusername VARCHAR (40),
	trackingpassword VARCHAR (40),
	region VARCHAR (2),
	no_link INT,
	warrantyusername VARCHAR (40),
	warrantypassword VARCHAR (40), 
	warrantyaccountnumber VARCHAR (40),
	ascarea VARCHAR (20),
	walkinjobsupload TINYINT (1)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/NetworkServiceProviders.csv' 
INTO TABLE import_network_service_providers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_unit_types (
  unittype varchar(30),
  jobtype varchar(30),
  repairskill varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/UnitTypes.csv' 
INTO TABLE import_unit_types
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_completion_statuses (
  statusname varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/CompletionStatus.csv' 
INTO TABLE import_completion_statuses
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_contact_actions (
	actioncode integer,
	actionname varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ContactActions.csv' 
INTO TABLE import_contact_actions
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_irish_counties (
  County varchar(40),
  InternationalCode varchar(3),
  CountryName varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/IrishCounties.csv' 
INTO TABLE import_irish_counties
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_network_users (
Username varchar(20),
PassWord varchar(32),
FullName varchar(40),
Role varchar(40),
Network varchar(40),
Email varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/NetworkUsers.csv' 
INTO TABLE import_network_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_centre_users (
Username varchar(20),
PassWord varchar(32),
FullName varchar(40),
Role varchar(40),
ServiceCentre varchar(40),
Email varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ServiceCentreUsers.csv' 
INTO TABLE import_service_centre_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_status (
  statusname varchar(40)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/Status.csv' 
INTO TABLE import_status
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_uk_postcode_05 (
  postcode varchar(5) NOT NULL,
  eastings int(11) NOT NULL DEFAULT '0',
  northings int(11) NOT NULL DEFAULT '0',
  latitude decimal(12,5) NOT NULL DEFAULT '0.00000',
  longitude decimal(12,5) NOT NULL DEFAULT '0.00000',
  town varchar(255) DEFAULT NULL,
  region varchar(255) DEFAULT NULL,
  country varchar(10) NOT NULL,
  country_string varchar(20) NOT NULL
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/uk_postcode_05.csv' 
INTO TABLE import_uk_postcode_05
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_bog (
  Network varchar(50),
  Client varchar(50),
  Manufacturer varchar(30),
  RepairSkill varchar(40),
  UnitType varchar(30),
  Model varchar(30)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/BoughtOutGuarantees.csv' 
INTO TABLE import_bog
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_central_service (
  Network varchar(50),
  Country varchar(10),
  RepairSkill varchar(40),
  Client varchar(50),
  MfgName varchar(40),
  JobType varchar(30),
  ServiceType varchar(30),
  UnitType varchar(30),
  UKCountyID varchar(30),
  CompanyName varchar(50)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/CentralService.csv' 
INTO TABLE import_central_service
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_ireland_towns (
  Network varchar(50),
  TownName varchar(40),
  CountyName varchar(40),
  ServiceCentre varchar(50) 
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/IrelandTownAllocations.csv' 
INTO TABLE import_ireland_towns
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_products (
	CATNO varchar(10),
	ProductType varchar(30),
	Manufacturer varchar(30),
	ASP decimal(10,2),
	Model varchar(30),
	AuthorityLimit decimal(10,2),
	ClaimbackInvoiceNoise char(2)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/ArgosCatalogue.csv' 
INTO TABLE import_products
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS import_unit_pricing_repair (
	Network varchar(40),
	Client varchar(40),
	ClientAccount varchar(10),
	UnitType varchar(40),
	JobSite varchar(40),
	CompletionStatus varchar(20),
	ServiceRate decimal(10,2),
	ReferralFee decimal(10,2)
);

LOAD DATA INFILE 'C:/wamp/www/skyline/sql/Initialise Database/RMA Import Data/UnitPricingRepair.csv' 
INTO TABLE import_unit_pricing_repair
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;